function showDetailOrder(_iOrderId)
{
    $('#content_bat').css('display','none') ;
    $('.content_order').css('visibility','visible') ;
    
    $('.content_order').slideUp(function(){
        $('.loading').css('display','block') ;
    }) ;
    $('#tableau_' + _iOrderId).slideDown(function(){
        $('.loading').css('display','none') ;
    }
    ) ;
}
function enregistrementFichier(_iOrderId, _iProductId, _zCodeFichier)
{
    $.ajax({
		url: 'index.php?route=gestionfichier/gestionfichier/enregistrementfichier',
		type: 'post',
		data: 'order_id='+_iOrderId+'&product_id='+_iProductId+'&code_fichier='+_zCodeFichier,
		dataType: 'json',
		success: function(json) {
			if (json['error']) {
				console.log('error') ;
			}

			if (json['success']) {
				console.log('success') ;
			}
			
			$("#lnkverou" + _iProductId + "_" + _iOrderId).show() ; 
			$("#lnksuppr" + _iProductId + "_" + _iOrderId).show() ; 
		},
        async : false ,
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
	});
}
function recuperationFichier(_zCodeFichier, _zElementId, _zTypeElement)
{
    $.ajax({
		url: 'index.php?route=gestionfichier/gestionfichier/recuperationFichier',
		type: 'post',
		data: 'code='+_zCodeFichier,
		dataType: 'text',
		success: function(data) {
			$('#' + _zTypeElement + _zElementId).html(data) ;
		},
        async : false ,
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
	});
}
function actionFichier(_iOrderId, _iProductId, _zAction)
{
    if($("#spn" + _iProductId + "_" + _iOrderId).html() == '')
    {
        alert('Pas de fichier pour cette ligne') ;
        return false ;
    }
    $.ajax({
		url: 'index.php?route=gestionfichier/gestionfichier/actionFichier',
		type: 'post',
		data: 'order_id='+_iOrderId+'&product_id='+_iProductId+'&action='+_zAction,
		dataType: 'text',
		success: function(data) {
            if(_zAction == 'envoyer')
            {
                $(".elmenvoi" + _iProductId + "_" + _iOrderId).remove() ; 
                $("#lnkverou" + _iProductId + "_" + _iOrderId).remove() ; 
                $("#lnksuppr" + _iProductId + "_" + _iOrderId).hide() ; 
                $("#spnverou" + _iProductId + "_" + _iOrderId).show() ; 
            }
			if(_zAction == 'supprimer'){
				$("#lnkverou" + _iProductId + "_" + _iOrderId).hide() ; 
                $("#lnksuppr" + _iProductId + "_" + _iOrderId).hide() ;
				$("#spn" + _iProductId + "_" + _iOrderId).html('') ;
			}
		},
        async : false ,
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
	});
}