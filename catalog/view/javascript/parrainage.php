
<?php
//inserer differents type de verification de transaction 
    class ModelCheckoutParrainage extends Model{

        public function parrainExist($filleul, $parrain){
           $query = $this->db->query("SELECT id_transaction FROM " . DB_PREFIX . "module_transaction WHERE id_filleul = '" . $filleul . "' AND id_pere = '" . $parrain . "'");
            $resultat = $query->rows;
            $exist = sizeof($resultat);
             if(!$exist==0){
                return true;    
            }else{
                return false;
            }
        }

        public function addPointParrainage($filleulID, $pereNIV, $point){
            $this->db->query("UPDATE " . DB_PREFIX . "module_parrain SET pointGagner = pointGagner + '" . $point . "' WHERE id_parrain = '" . $pereNIV . "'");
        }

        public function existTransaction($pere, $fils){
            $query = $this->db->query("SELECT id_transaction FROM " . DB_PREFIX . "module_transaction WHERE id_filleul = '" . $fils . "' AND id_pere ='" . $pere . "'");
            $resultat =  $query->row;
            if(isset($resultat)){
                return True;
            }else{
                return False;
            }
            
        }

        public function addTransaction($filleulID, $pereID, $pointAjouter, $date){
            $this->db->query("INSERT INTO " . DB_PREFIX . "module_transaction SET id_filleul = '" . $filleulID . "', id_pere = '" . $pereID . "', Aajouter_point = '" . $pointAjouter."', dateTransaction = '" . $date . "'");      
        }
		
		public function insertCompteCredit($customer_id,$montant){
			$date_expiration = new DateTime(date("Y-m-d"));		
			$date_expiration->add(new DateInterval('P365D'));
            $this->db->query("INSERT INTO card_customer_parrainage SET `customer_id` = '" . (int)$customer_id . "', `ajout` = '" . true . "', `montants` = '" . $montant . "', `date_action` = '" . date("Y-m-d") . "',`is_valid`='" . false . "',`date_expiration` = '".$date_expiration->format('Y-m-d')."'");      
        }
		
		public function getParrain($customer_id){
			$query = $this->db->query("SELECT parrain_id FROM " . DB_PREFIX . "parrainage WHERE customer_id = '" . $customer_id ."'");
			return $query->row;
		}
	
		public function getParrains($customer_id){
			$parrains = array();
            //Niveau 1
			$parrainNiv1 = $this->getParrain($customer_id);
			if(!empty($parrainNiv1)){
				$parrains[] = array('parrain_id'=>$parrainNiv1['parrain_id'],'pourc'=>'6');
				//Niveau 2
				$parrainNiv2 = $this->getParrain($parrainNiv1['parrain_id']);
				if(!empty($parrainNiv2)){
					$parrains[] = array('parrain_id'=>$parrainNiv2['parrain_id'],'pourc'=>'3');
					//Niveau 3
					$parrainNiv3 = $this->getParrain($parrainNiv2['parrain_id']);
					if(!empty($parrainNiv3)){
						$parrains[] = array('parrain_id'=>$parrainNiv3['parrain_id'],'pourc'=>'1');
					}
				}
			}
			return $parrains;
        }
}