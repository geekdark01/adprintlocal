function showDetailOrder(_iOrderId)
{
    $('.content_order').slideUp() ;
    $('#tableau_' + _iOrderId).slideDown() ;
}
function actionBat(_iOrderId, _iProductId, _iStatus)
{
    $.ajax({
		url: 'index.php?route=bat/bat/actionBat',
		type: 'post',
		data: 'order_id='+_iOrderId+'&product_id='+_iProductId+'&status='+_iStatus,
		dataType: 'text',
		success: function(data) {
                $(".lnkverou" + _iProductId + "_" + _iOrderId + "_0").remove() ;
                $(".lnkverou" + _iProductId + "_" + _iOrderId + "_" + _iStatus).show() ;
		},
        async : false ,
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
	});
}