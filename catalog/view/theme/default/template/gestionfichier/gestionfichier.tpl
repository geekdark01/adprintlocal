<?php echo $header; ?>
<script src="catalog/view/javascript/gestionfichier.js" type="text/javascript"></script>
<div class="container">
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
        <ul class="breadcrumbs">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
         </ul>
         
         <div class="block2 select-order" style="margin-bottom: 30px;">
            <h2 class="pink bold"><?php echo $text_gestion_fichier; ?></h2>
            <div class="padder">
                <p><b><?php echo $text_choix_commande ; ?>:</b></p>
            </div>
            <div class="form-order-changer">
                    <select id="input-sort" class="form-control" onchange="showDetailOrder(this.value);" style="display: inline-block;">
                        <option value="0" disabled selected>Choix d'une commande</option>
                        <?php
                            echo $order_id ;
                            $iOrderIdFirst  = 0 ;
                            $iCompteur      = 0 ;
                        ?>
                        <?php foreach($toOrderAccount as $oOrderAccount): ?>
                            <?php
                                if($iCompteur == 0){
                                    $iOrderIdFirst = ($order_id == 0) ? $oOrderAccount['order_id'] : $order_id ;
                                }
                            ?>
                            <option value="<?php echo $oOrderAccount['order_id'] ; ?>">Order #<?php echo $oOrderAccount['order_id'] ; ?></option>
                            <?php $iCompteur ++  ?>
                        <?php endforeach; ?>
                    </select>
                    <span class="loading"><img src="<?php echo HTTP_IMAGE; ?>loading.gif"></span>
            </div>
        </div>
        
        <div id="content_bat">
            <div class="content">
                <h2 class="dashboard-title-title">Mes Bons à Tirer (BAT)</h2>
                <hr class="separation-title-content">
                <div class="col-md-12">
                    <div class="col-xs-12 col-md-8">
                        <img src="<?php echo HTTP_IMAGE; ?>imprimerie-en-ligne-bat-step.png" class="img-responsive" style="float:left;" alt=""> 
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <p style="float:right;text-align:center; padding-right:25px;font-size: 1.13em;">
                        Téléchargez l'ensemble des BAT de votre commande.<br>
                        Contrôlez l'intégralité de vos fichiers téléchargés.<br>
                        <span style="color:#5CBB00">Validez</span> ou <span style="color:#E51D1D">Refusez</span>.
                        </p>
                    </div>
                </div>
            </div>
        </div>
       
        <div class="table-responsive">
            <?php foreach($toOrderAccount as $oOrderAccount): ?>
                <table class="table table-bordered content_order" id="tableau_<?php echo $oOrderAccount['order_id'] ; ?>">
                    <thead>
                        <tr>
                            <td>ID</td>
                            <td><?php echo $text_titre_produit ; ?></td>
                            <td><?php echo $text_titre_statut ; ?></td>
                            <td><?php echo $text_titre_action ; ?></td>
                            <td><?php echo $text_titre_action ; ?></td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($order_product[$oOrderAccount['order_id']] as $toContenu): ?>
                            <tr>
                                <td>
                                    <?php echo $toContenu['product_id'] ;  ?>
                                </td>
                                <td>
                                    <?php echo $toContenu['name'] ;  ?>
                                </td>
                                <td>
                                    <?php if(!$toContenu['verou_fichier']) : ?>
                                        <button class="elmenvoi<?php echo $toContenu['product_id'] . '_' . $toContenu['order_id'] ; ?> btn btn-warning" type="button" id="button-upload<?php echo $toContenu['product_id'] . '_' . $toContenu['order_id'] ; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default btn-block"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
                                        <input class="elmenvoi<?php echo $toContenu['product_id'] . '_' . $toContenu['order_id'] ; ?>" type="hidden" name="option[<?php echo $toContenu['product_id'] . '_' . $toContenu['order_id'] ; ?>]" value="" id="input-option<?php echo $toContenu['product_id'] . '_' . $toContenu['order_id'] ;; ?>" />
                                    <?php endif; ?>&nbsp;&nbsp;
                                    <span id="spn<?php echo $toContenu['product_id'] . '_' . $toContenu['order_id'] ; ?>"></span>
                                    <?php if($toContenu['code_fichier']) : ?>
                                        <script type="text/javascript">
                                            $(document).ready(function(){
                                                recuperationFichier('<?php echo $toContenu['code_fichier'] ; ?>', '<?php echo $toContenu['product_id'] . '_' . $toContenu['order_id'] ; ?>', 'spn') ;
                                            }) ;
                                        </script>
                                    <?php endif; ?>
                                </td>
                                <td>
                                    <a id="lnkverou<?php echo $toContenu['product_id'] . '_' . $toContenu['order_id'] ; ?>" <?php if($toContenu['verou_fichier']): ?>style="display:none;"<?php endif; ?> href="javascript:void(0);" id="lnk<?php echo $toContenu['product_id'] . '_' . $toContenu['order_id'] ; ?>" class="btn btn-link" onclick="actionFichier(<?php echo $toContenu['order_id'] ; ?>, <?php echo $toContenu['product_id'] ; ?>, 'envoyer') ;"><?php echo $text_envoyer ; ?></a>
                                    <span <?php if(!$toContenu['verou_fichier']): ?>style="display:none;"<?php endif; ?> id="spnverou<?php echo $toContenu['product_id'] . '_' . $toContenu['order_id'] ; ?>"><?php echo $text_envoyé ; ?></span>
                                </td>
                                <td>

                                    <!-- -->

                                </td>
                            </tr>
                        <?php endforeach ; ?>
                    </tbody>
                </table>
            <?php endforeach ; ?>
        </div>
        
        <?php if($iOrderIdFirst != 0): ?>
            <script type="text/javascript">
                $(document).ready(function(){
                    showDetailOrder(<?php echo $iOrderIdFirst ; ?>) ;
                }) ;
            </script>
        <?php endif ; ?>
        <script type="text/javascript">
            $('button[id^=\'button-upload\']').on('click', function() {
                var node = this;

                $('#form-upload').remove();

                $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

                $('#form-upload input[name=\'file\']').trigger('click');

                if (typeof timer != 'undefined') {
                    clearInterval(timer);
                }

                timer = setInterval(function() {
                    if ($('#form-upload input[name=\'file\']').val() != '') {
                        clearInterval(timer);

                        $.ajax({
                            url: 'index.php?route=tool/upload',
                            type: 'post',
                            dataType: 'json',
                            data: new FormData($('#form-upload')[0]),
                            cache: false,
                            contentType: false,
                            processData: false,
                            beforeSend: function() {
                                $(node).button('loading');
                            },
                            complete: function() {
                                $(node).button('reset');
                            },
                            success: function(json) {
                                $('.text-danger').remove();

                                if (json['error']) {
                                    $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
                                }

                                if (json['success']) {
                                    alert(json['success']);

                                    $(node).parent().find('input').val(json['code']);
                                    var zInputHiddenId = $(node).parent().find('input').attr('id') ;
                                    zInputHiddenId = zInputHiddenId.replace('input-option', '') ;
                                    console.log(zInputHiddenId) ;
                                    var tzInputHiddenId = new Array() ;
                                    tzInputHiddenId = zInputHiddenId.split('_') ;
                                    enregistrementFichier(tzInputHiddenId[1], tzInputHiddenId[0], json['code']) ;
                                    recuperationFichier(json['code'], zInputHiddenId, 'spn') ;
                                }
                            },
                            error: function(xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });
                    }
                }, 500);
            });
        </script>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?> 