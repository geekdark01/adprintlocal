<?php echo $header; ?>

<style>

.ul_titres{
  
  width: 100% ;
  
}

.li_titres{
  
  float: left;
  width: 150px;
  text-align:center;
      font-weight: bold;
    text-transform: uppercase;
  
    border-left: 1px solid #dfdfdf;
    border-top: 1px solid #dfdfdf;
    border-bottom: 1px solid #dfdfdf;
    height: 100px;
    padding-top: 10px;
  
  background-color: #fafafa;
  
  
  
}

.li_titres a{
  color: #6c6c6c;
    text-decoration: none;
}

.li_titres a:hover{
    text-decoration: underline;
}


.bonjour{
      text-align: center;
}

.titre_compte{
    color: #EC008C;

}

.texte_1{
    height: 40px;
  padding-top: 5px;
}

.icones{
    font-size: 30px;
      width: 100%;
}

.texte_2{
  height: 150px;
  margin-top: 50px;
    border: 2px solid #dfdfdf;
}

.texte_3{
  height: 150px;
  margin-top: 50px;
    border: 2px solid #dfdfdf;
  margin-left:20px;
  padding: 0px;
}


.mes_infos
{
    height: 30px;
    padding: 5px;
  font-family: Arial !important;
    font-size: 1.2em;
    text-transform: uppercase;
    color: #6c6c6c;
    font-weight: 500;
}

.icones_bas{
  font-size: 20px;
  width:50px;
}

.images{
  height:100%;
}



.menu_actif{
  background-color: #fff;
    border-bottom: none;
}

.div_gris{
  background: #F6F6F6;
}

.titre_gris{
  text-align: center;
    font-size: 13px;
    font-weight: 700;
  height: 35px;
  padding-top: 3px;
}

.zero{
      color: #f40088;
    font-size: 45px;
    font-weight: bold;
    text-align: center;
}

.zero_2{
      color: #f40088;
    font-weight: bold;
    text-align: center;
}


.content_gris{
   text-align: center;
       height: 70px;
}

.voir_details{
  height: 30px;
    text-align: center;
}

.div_230{
   height: 230px;
}   

.acheter{
      height: 30px;
    padding: 5px;
    text-align: center;
    background: #a3a3a3;
    margin-top: 10px;
    font-size: 13px;
    color: #FFF;
}

.contactez{
  margin-top: 50px;text-align: center;
}

</style>

<div class="container" style="min-height:900px">

<div class=" col-sm-12">
<h2 class="titre_compte" >Mon compte </h2>
</div>

<div class="col-sm-12 texte_1">
  <div class="bonjour col-sm-10"  >Bonjour <a href="" ><?php echo $firstname ; ?> <?php echo $lastname; ?> </a> </div> 
  <div class="col-sm-2" > <a href="index.php?route=account/logout" >Se déconnecter </a> </div>
</div>


<ul class="ul_titres" >

  <li class="li_titres menu_actif" >
  <a href="" >
  <span class="icones glyphicon glyphicon-blackboard"></span> Tableau de bord</a> </li>
  <li class="li_titres"> <a href="" ><span class="icones glyphicon glyphicon glyphicon-user"></span> Informations du compte</a></li>
  <li class="li_titres"><a href="" ><span class="icones glyphicon glyphicon-download-alt"></span> Mes commandes et factures</a></li>
  <li class="li_titres"> <a href="" ><span class="icones  glyphicon glyphicon-pencil"></span> Mes devis</a></li>
  <li class="li_titres"> <a href="" ><span class="icones glyphicon glyphicon-book"></span> Gestion de fichiers bon à tirer</a></li>
  <li class="li_titres"> <a href="" ><span class="icones glyphicon glyphicon-thumbs-up"></span> Mes avantages</a></li>
  <li class="li_titres" style="border-right: 1px solid #dfdfdf;" > <a href="" ><span class="icones glyphicon glyphicon-education"></span> Service satisfaction client</a></li>

</ul>



<div class="col-sm-12" >
  
  <div class="col-sm-12 texte_2" >
    <div class="col-sm-12 mes_infos" >INFORMATIONS DU COMPTE</div>
    <div class="col-sm-12" ><span class="glyphicon  glyphicon-user icones_bas" ></span> <?php echo $firstname ; ?> <?php echo $lastname; ?> </div>
    <div class="col-sm-12" ><span class="glyphicon  glyphicon-envelope icones_bas" ></span> <?php  echo $email ; ?> </div>
    <div class="col-sm-12" ><span class="glyphicon  glyphicon-lock icones_bas" ></span> ***** </div>  
    
  </div>


</div>



<div class="col-sm-12" >
  
  <div class="col-sm-5 texte_2 div_230" style=" padding:0px ">
    <div class="col-sm-12 mes_infos" >VOTRE DERNIÈRE COMMANDE</div>
    <table class="table table-bordered table-hover">
          <thead>
            <tr>
              <td class="text-right">ID</td>
              <td class="text-left">CLient</td>
              <td class="text-right">Produit</td>
              <td class="text-left">Status</td>
              <td class="text-right">Total</td>
              <td class="text-left">Date</td>
              <td></td>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($orders as $order) { ?>
            <tr>
              <td class="text-right">#<?php echo $order['order_id']; ?></td>
              <td class="text-left"><?php echo $order['name']; ?></td>
              <td class="text-right"><?php echo $order['products']; ?></td>
              <td class="text-left"><?php echo $order['status']; ?></td>
              <td class="text-right"><?php echo $order['total']; ?></td>
              <td class="text-left"><?php echo $order['date_added']; ?></td>
              <td class="text-right"><a href="<?php echo $order['view']; ?>" data-toggle="tooltip" title="<?php echo $button_view; ?>" class="btn btn-info"><i class="fa fa-eye"></i></a></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
    
  </div>

  <div class="col-sm-5 texte_3 div_230" > 
    <div class="col-sm-12 mes_infos" >MON COMPTE EASYBANK & MES PARRAINAGES</div>
    <div class="col-sm-12">
      <div class="col-sm-6">
        <div class="col-sm-12 div_gris">
      
        <div class="titre_gris"> Mon compte EasyBank </div>
        <div class="content_gris"> <span class="zero" > 0 </span> </br> <span class="zero_2" >EasyCrediZ</span> </div>
        <div class="voir_details"> <a href="">Voir détails </a> </div>
      
        </div>
      
        <div class="col-sm-12 acheter">
          Acheter des EasyCrediZ
        </div>
      
      </div>
      
      
      <div class="col-sm-6">
        <div class="col-sm-12 div_gris">
      
        <div class="titre_gris"> Mon réseau de filleuls </div>
        <div class="content_gris"> <span class="zero" > 0 </span> </br> <span class="zero_2" >filleuls</span> </div>
        <div class="voir_details"> <a href="">En obtenir + </a> </div>
      
        </div>
      
        <div class="col-sm-12 acheter">
          Gagner des EasyCrediZ
        </div>
      
      </div>
      
    
    </div>
  </div>

</div>


<!--
<div class="col-sm-12 contactez" >
  <img src="http://adprint-dev.com/image/contacter-easyflyer-conseiller.jpg" title="Adprint" alt="Adprint" class="img-responsive images"> 
</div>
-->

<div class="col-sm-10" style=" background-color: #f3f3f3;text-align: center;margin-top: 20px;    height: 110px;"> 
  <div class="col-sm-4" >
    <div style="padding: 5px;font-size: 18px;" > <span class="glyphicon  glyphicon-envelope icones_bas" ></span>  Newsletter</div>
    <div style="    height: 25px;"> Recevez toutes nos promotions et ventes flash </div>
    <div>  <a href="" class="btn sinscrire"  style="background: #ed008c;padding: 6px 30px 5px 30px;color: #fff;" >S'inscrire</a> </div>
  </div>
  
  <div class="col-sm-4" >
    <div style="padding: 5px;font-size: 18px;" > <span class="glyphicon  glyphicon-lock icones_bas" ></span>  Paiement sécurisé</div>
    <div style="">  
    <img src="http://adprint-dev.com/image/imprimerie-en-ligne-paiement-securise-footer-2.jpg" title="Adprint" alt="Adprint" class="img-responsive images">
    </div>
  </div>
  
  <div class="col-sm-4" >
    <div style="padding: 5px;font-size: 18px;" > <span class="glyphicon  glyphicon-lock icones_bas" ></span>  Suivez-nous sur </div>
    <div style="">
      <a href="https://www.facebook.com/adprintmadagascar">
    <img style="margin-left: 50px;" src="http://adprint-dev.com/image/imprimerie-en-ligne-icon-social-footer-3.jpg" title="Adprint" alt="Adprint" class="img-responsive images">
      </a>
    </div>
  </div>
  
  
</div>


</div>

<?php echo $footer; ?> 