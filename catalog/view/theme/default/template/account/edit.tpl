<?php echo $header; ?>

<style>

.ul_titres{
  
  width: 100% ;
  
}

.li_titres{
  
  float: left;
  width: 14%;
  text-align:center;
  font-weight: bold;
  text-transform: uppercase;
  
  border-left: 1px solid #dfdfdf;
  border-top: 1px solid #dfdfdf;
  border-bottom: 1px solid #dfdfdf;
  height: 100px;
  padding-top: 10px;
  
  background-color: #fafafa;
  
}

.li_titres a{
  color: #6c6c6c;
    text-decoration: none;
}

.li_titres a:hover{
    text-decoration: underline;
}


.bonjour{
      text-align: center;
}

.titre_compte{
    color: #EC008C;

}

.texte_1{
    height: 40px;
  padding-top: 5px;
}

.icones{
    font-size: 30px;
      width: 100%;
}

.texte_2{
  height: 150px;
  margin-top: 10px;
    border: 2px solid #dfdfdf;
}

.texte_3{
  height: 150px;
  margin-top: 50px;
    border: 2px solid #dfdfdf;
  margin-left:20px;
  padding: 0px;
}


.mes_infos
{
    height: 30px	;
    padding: 10px	;
	font-family: Arial !important;
    font-size: 1.2em;
    /* text-transform: uppercase; */
    color: #6c6c6c	;
    font-weight: 600;
	color: #EC008C	;
}

.icones_bas{
  font-size: 20px;
  width:50px;
}

.images{
  height:100%;
}



.menu_actif{
  background-color: #fff;
    border-bottom: none;
}

.div_gris{
  background: #F6F6F6;
}

.titre_gris{
  text-align: center;
    font-size: 13px;
    font-weight: 700;
  height: 35px;
  padding-top: 3px;
}

.zero{
      color: #f40088;
    font-size: 45px;
    font-weight: bold;
    text-align: center;
}

.zero_2{
      color: #f40088;
    font-weight: bold;
    text-align: center;
}


.content_gris{
   text-align: center;
       height: 70px;
}

.voir_details{
  height: 30px;
    text-align: center;
}

.div_230{
   height: 230px;
}   

.acheter{
      height: 30px;
    padding: 5px;
    text-align: center;
    background: #a3a3a3;
    margin-top: 10px;
    font-size: 13px;
    color: #FFF;
}

.contactez{
  margin-top: 50px;text-align: center;
}

.control-label {
text-align: left !important; 
}

</style>
  
  
  
<script type="text/javascript">
$(document).ready(function() {
    // Generate a simple captcha
  
    function randomNumber(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    };
  
    $('#captchaOperation').html([randomNumber(1, 100), '+', randomNumber(1, 200), '='].join(' '));

  
    $('#defaultForm').bootstrapValidator({
//        live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            input-firstname: {
                group: '.col-lg-4',
                validators: {
                    notEmpty: {
                        message: 'Le prénom ne doit pas être vide'
                    }
                }
            },
            input-lastname: {
                group: '.col-lg-4',
                validators: {
                    notEmpty: {
                        message: 'Le nom ne doit pas être vide'
                    }
                }
            },
            input-email: {
                validators: {
                    emailAddress: {
                        message: 'Doit être un mail valide'
                    }
                }
            },
            password_apres: {
                validators: {
                    notEmpty: {
                        message: 'Le mot de passe ne doit pas être vide'
                    }
                }
            },
            password_apres_confirm: {
                validators: {
                    notEmpty: {
                        message: 'Le mot de passe ne doit pas être vide'
                    },
                    identical: {
                        field: 'password_apres',
                        message: 'doit être identique au mot de passe'
                    }
                }
            },
            input-telephone: {
                validators: {
                    emailAddress: {
                        message: 'le numéro ne doit pas être vide'
                    }
                }
            }      
        }
    });
    
    // Validate the form manually
    $('#validateBtn').click(function() {
        $('#defaultForm').bootstrapValidator('validate');
    });


});
</script>
  
  

<div class="container" style="min-height:900px">

<div class=" col-sm-12">
<h2 class="titre_compte" >Mon compte </h2>
</div>

<div class="col-sm-12 texte_1">
  <div class="bonjour col-sm-10"  >Bonjour <a href="index.php?route=account/account" > <?php echo $firstname ; ?> <?php echo $lastname; ?> </a> </div> 
  <div class="col-sm-2" > <a href="index.php?route=account/logout" >Se déconnecter </a> </div>
</div>


<ul class="ul_titres" >

  <li class="li_titres" >
  <a href="index.php?route=account/account" >
  <span class="icones glyphicon glyphicon-blackboard"></span> Tableau de bord</a>  </li>
  <li class="li_titres menu_actif"> <a href="index.php?route=account/edit" > <span class="icones glyphicon glyphicon glyphicon-user"></span> Informations du compte</a></li>
  <li class="li_titres"><a href="index.php?route=account/order" ><span class="icones glyphicon glyphicon-download-alt"></span> Mes commandes et factures</a></li>
  <li class="li_titres"> <a href="index.php?route=devis/devis/listedevis" > <span class="icones   glyphicon glyphicon-pencil"></span> Mes devis</a></li>
  <li class="li_titres"> <a href="index.php?route=account/account/gestionfichier" > <span class="icones glyphicon glyphicon-book"></span> Gestion de fichiers bon à tirer</a></li>
  <li class="li_titres"> <a href="index.php?route=account/avantage" ><span class="icones glyphicon glyphicon-thumbs-up"></span> Mes avantages</a></li>
  <li class="li_titres" style="border-right: 1px solid #dfdfdf;" > <a href="index.php?route=account/satisfaction" ><span class="icones glyphicon glyphicon-education"></span> Service satisfaction client</a></li>

</ul>



<div class="col-sm-12" >

   <div class=" mes_infos" >Éditer les informations du compte</div>
  
	<div class="col-sm-12 texte_2" style=" height: auto;" >

	<legend> INFORMATIONS DU COMPTE </legend>
   
    
    <form id="defaultForm" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
        
    <div class="col-sm-6" >

        <fieldset>
         
          <div class="form-group required">
            <label class="col-sm-5 control-label" for="input-firstname">Prénom<?php //echo $entry_firstname; ?> </label>
            <div class="col-sm-7">
              <input type="text" name="firstname" value="<?php echo $firstname; ?>" placeholder="<?php echo $entry_firstname; ?>" id="input-firstname" class="form-control" />
              <?php if ($error_firstname) { ?>
              <div class="text-danger"><?php echo $error_firstname; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-5 control-label" for="input-lastname"> Nom <?php // echo $entry_lastname; ?></label>
            <div class="col-sm-7">
              <input type="text" name="lastname" value="<?php echo $lastname; ?>" placeholder="<?php echo $entry_lastname; ?>" id="input-lastname" class="form-control" />
              <?php if ($error_lastname) { ?>
              <div class="text-danger"><?php echo $error_lastname; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-5 control-label" for="input-email"> Adresse mail <?php // echo $entry_email; ?></label>
            <div class="col-sm-7">
              <input type="email" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" />
              <?php if ($error_email) { ?>
              <div class="text-danger"><?php echo $error_email; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-5 control-label" for="input-telephone">Numéro de mobile <?php // echo $entry_telephone; ?></label>
            <div class="col-sm-7">
              <input type="tel" name="telephone" value="<?php echo $telephone; ?>" placeholder="<?php echo $entry_telephone; ?>" id="input-telephone" class="form-control" />
              <?php if ($error_telephone) { ?>
              <div class="text-danger"><?php echo $error_telephone; ?></div>
              <?php } ?>
            </div>
          </div>
      
          


          <div class="form-group required">
          
            <div class="col-sm-10">
              <input onchange="
               $('#bloc_password').toggle();
               if( parseInt( $('#change_password').val() ) == 0 ) 
                   $('#change_password').val(1); 
               else
                   $('#change_password').val(0);

              " id="cocher_mot_de_passe" style="    float: left;width: 30px !important;height: 20px;" type="checkbox" name="" value="<?php echo $telephone; ?>"  class="form-control" />
             
              <div > Mot de passe </div>
             
            </div>
          </div>


          <input type="hidden" name="change_password" id="change_password" value="0" >

      <div id="bloc_password" style="display:none;">

      

          <div class="form-group required">
            <label class="col-sm-5 control-label" for="input-password-apres"> Nouveau mot de passe <?php // echo $entry_email; ?></label>
            <div class="col-sm-7">
              <input type="password" name="password_apres"  id="password_apres" class="form-control" />
            </div>
            <?php if ($error_NIF) { ?>
              <div class="text-danger"><?php echo $error_NIF; ?></div>
              <?php } ?>
          </div>

          <div class="form-group required">
            <label class="col-sm-5 control-label" for="input-password-apres-confirm"> Confirmer le nouveau mot de passe</label>
            <div class="col-sm-7">
              <input type="password" name="password_apres_confirm"  id="password_apres_confirm" class="form-control" />
              
            </div>
          </div>

      </div>    


        </fieldset>

        <div class="buttons clearfix">
         
          <div class="pull-right">
            <input id="validateBtn" style="background-color: #a3a3a3;background-image: none;border-color: #a3a3a3;" type="submit" value="Sauvergarder"  class="btn btn-primary" />
          </div>

        </div>

       </div>

		<div class="col-sm-6" >

        <div class="form-group required">
            <label class="col-sm-4 control-label" >Vous êtes ?<span class="etoile"> </span> </label>
            <div class="col-sm-8">
				 
          <strong>Particulier</strong> 
          <input type="radio" value="2" name="customer_group_id" onclick="$('#bloc_societe').hide();$('#bloc_NIF').hide();$('#bloc_STAT').hide();$('#bloc_secteur').hide(); TVA.checked= false;" />
					&nbsp;&nbsp;&nbsp;&nbsp; 				 
				 
					<strong>Société</strong> 
          <input type="radio" value="1"  name="customer_group_id" onclick="$('#bloc_societe').show();$('#bloc_secteur').show();$('#bloc_NIF').show();$('#bloc_STAT').show(); TVA.checked= true;" />
				
            </div>
        </div>
		
     <?php /*   <div class="form-group">
			<label class="control-label col-sm-4" for="product_affinity[]">Affinités produits : </label><br>
			<div class="col-sm-8">
			<?php foreach($affinities as $affinity){
				if(in_array($affinity['affinity_id'],$product_affinity)){
				?><input name="product_affinity[]" checked value="<?php echo $affinity['affinity_id'];?>"  type="checkbox"> <?php echo $affinity['libelle'];?><br>
			<?php } else{
				?>
					<input name="product_affinity[]" value="<?php echo $affinity['affinity_id'];?>"  type="checkbox"> <?php echo $affinity['libelle'];?><br>
				<?php
				} 
			}?>
			</div>
		</div>
    */ ?>

    <div class="form-group required" id="bloc_societe" >
            <label class="col-sm-4 control-label" for="input-company"><strong>Nom de Société </strong>  </label>
            <div class="col-sm-8">
              <input type="text" name="company" value="<?php echo $company; ?>"  id="input-company" class="form-control" />
            </div>
    </div>

    <div class="form-group required" id="bloc_NIF">
            <label class="col-sm-4 control-label" for="input-NIF"><strong>NIF </strong> </label>
            <div class="col-sm-8">
              <input type="text" name="NIF" value="<?php echo $NIF; ?>"  id="input-NIF" class="form-control" />
              <?php if ($error_NIF) { ?>
              <div class="text-danger"><?php echo $error_NIF; ?></div>
              <?php } ?>
            </div>
    </div>
       <div class="form-group required" id="bloc_STAT">
            <label class="col-sm-4 control-label" for="input-STAT"><strong>STAT </strong> </label>
            <div class="col-sm-8">
              <input type="text" name="STAT" value="<?php echo $STAT; ?>"  id="input-STAT" class="form-control" />
              <?php if ($error_STAT) { ?>
              <div class="text-danger"><?php echo $error_STAT; ?></div>
              <?php } ?>
            </div>
    </div>
      
		<div class="form-group required" id="bloc_secteur"  >
            <label class="col-sm-4 control-label" for="input-secteur"> <strong>Secteur d'activité </strong></label>
            <div class="col-sm-8">
             <select name="secteur" class="form-control" id="input-secteur">
                <option value="">-- Sélectionnez un secteur d'activité --</option>
                <option value="Administration" >Administration</option>
                <option value="Agriculture, animaux">Agriculture, animaux</option>
                <option value="Alimentation">Alimentation</option>
                <option value="Art, spectacle">Art, spectacle</option>
                <option value="Automobile, transports">Automobile, transports</option>
                <option value="Autre">Autre</option>
                <option value="Beauté, spa">Beauté, spa</option>
                <option value="Commerce, boutique">Commerce, boutique</option>
                <option value="Construction, réparation">Construction, réparation</option>
                <option value="Droit">Droit</option>
                <option value="Education, formation">Education, formation</option>
                <option value="Finance, assurance">Finance, assurance</option>
                <option value="Immobilier">Immobilier</option>
                <option value="Industrie">Industrie</option>
                <option value="Religieux, spirituel">Religieux, spirituel</option>
                <option value="Restauration, tourisme">Restauration, tourisme</option>
                <option value="Revendeur, professionnel arts graphiques">Revendeur, professionnel arts graphiques</option>
                <option value="Santé">Santé</option>
                <option value="Services">Services</option>
                <option value="Sport">Sport</option>
              </select>
              <?php if ($error_secteur) { ?>
              <div class="text-danger"><?php echo $error_secteur; ?></div>
              <?php } ?>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-4 control-label" for="TVA"><strong>Assujettié à TVA</strong></br> <small style="font-size:smaller;font-weight:normal;">(Veuillez cocher pour avoir les produits avec TTC - très conseillé pour les sociétés)</small> <?php //echo $entry_TVA; ?></label>
            <div class="col-sm-8">
              <input type="checkbox" name="TVA" value='1'/>
            </div>
          </div>

       </div>

          <?php /*<div class="form-group" style="display:none;">
                      <label class="col-sm-2 control-label" for="input-fax"><?php echo $entry_fax; ?></label>
                      <div class="col-sm-10">
                        <input type="text" name="fax" value="<?php echo $fax; ?>" placeholder="<?php echo $entry_fax; ?>" id="input-fax" class="form-control" />
                      </div>
                    </div> */ ?>
          

      </form>
     
  <div class="clearfix"></div>

  </div>

<div class="clearfix"></div>
</div>



<style>

.bloc2{
	
	margin: 10px;
    border: 2px solid #dfdfdf;
}



</style>



 <div class="col-sm-12 mes_infos" >Carnet d'adresses</div>


 <div class="col-sm-12 bloc2" >
 <legend> ADRESSES PAR DEFAUT </legend>
 
   <?php foreach ($addresses as $result) { ?>
          <div class="col-sm-6">
		  <div class="col-sm-12">  Adresse de facturation par defaut </div>
            <div class="col-sm-12"><?php echo $result['address']; ?></div>
			 <div class="col-sm-12">
		   <a href="<?php echo $result['update']; ?>" style="text-decoration: underline;"> Modifier l'adresse de facturation </a>
		   </div>
          </div>
		  
		  
		<div class="col-sm-6">
		  <div class="col-sm-12">  Adresse de livraison par defaut </div>
            <div class="col-sm-12"><?php echo $result['address']; ?></div>
			 <div class="col-sm-12">
		   <a href="<?php echo $result['update']; ?>" style="text-decoration: underline;"> Modifier  </a>
		   </div>
         </div>
		  
          <?php } ?>
		
		
	<div class="col-sm-6" style="padding: 10px;text-transform:uppercase;">
		saisies d'adresses supplémentaires
		
	<div class="col-sm-12" >
			<a href="<?php echo $add; ?>" class="btn btn-primary pull-right" > Ajouter une nouvelle adresse </a>
	</div>  	 
		
	</div>  

<div class="clearfix"></div>

</div>

<div class="col-sm-10" style=" background-color: #f3f3f3;text-align: center;margin-top: 20px;    height: 110px;"> 
  <div class="col-sm-4" >
    <div style="padding: 5px;font-size: 18px;" > <span class="glyphicon  glyphicon-envelope icones_bas" ></span>  Newsletter</div>
    <div style="    height: 25px;"> Recevez toutes nos promotions et ventes flash </div>
    <div>  <a href="" class="btn sinscrire"  style="background: #ed008c;padding: 6px 30px 5px 30px;color: #fff;" >S'inscrire</a> </div>
  </div>
  
  <div class="col-sm-4" >
    <div style="padding: 5px;font-size: 18px;" > <span class="glyphicon  glyphicon-lock icones_bas" ></span>  Paiement sécurisé</div>
    <div style="">  
    <img src="/image/imprimerie-en-ligne-paiement-securise-footer-2.jpg" title="Adprint" alt="Adprint" class="img-responsive images">
    </div>
  </div>
  
  <div class="col-sm-4" >
    <div style="padding: 5px;font-size: 18px;" > <span class="glyphicon  glyphicon-lock icones_bas" ></span>  Suivez-nous sur </div>
    <div style="">
      <a href="https://www.facebook.com/adprintmadagascar">
    <img style="margin-left: 50px;" src="/image/imprimerie-en-ligne-icon-social-footer-3.jpg" title="Adprint" alt="Adprint" class="img-responsive images">
      </a>
    </div>
  </div>
  
  
</div>


</div>
<script>
	if($('input[name=customer_group_id]:checked').val()!=1){
		$('#bloc_societe').hide();$('#bloc_secteur').hide();$('#bloc_NIF').hide();$('#bloc_STAT').hide();
	}
</script>
<?php echo $footer; ?> 