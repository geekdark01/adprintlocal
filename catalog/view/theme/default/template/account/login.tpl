﻿<?php echo $header; ?>
<div class="container content_container">
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="col-md-offset-1 <?php echo $class; ?>"><?php echo $content_top; ?>
      <div class="row">
	  
	  
<style>

.me_connecter{
	
	background-color: #bb0c1b ;
    background-repeat: repeat-x;
    background-image: none;
    border-color: #bb0c1b ;
    width: 100%;

}

.inscription{
	border: none !important;
    background-color: #fff !important;
}	
	
      
</style>		

		
        <div class="col-sm-6">
          <div class="well inscription">
            <h2><span style="color: #bb0c1b!important;" >Déjà </span> Clients ?<?php // echo $text_returning_customer; ?></h2>
            <p><strong>Si vous avez déjà un compte, veuillez vous identifier.<?php //echo $text_i_am_returning_customer; ?></strong></p>
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
				<div class="form-group required">
					<label class="control-label" for="input-email"><strong>Adresse mail </strong><?php //echo $entry_email; ?></label>
					<input type="text" name="email" value="<?php echo $email; ?>" id="input-email" class="form-control" />
				</div>
				<div class="form-group required">
					<label class="control-label" for="input-password"><strong>Mot de passe </strong><?php //echo $entry_password; ?></label>
					<input type="password" name="password" value="<?php echo $password; ?>" id="input-password" class="form-control" />
					<a href="<?php echo $forgotten; ?>"><span style="color: #bb0c1b!important;" >Mot de passe oublié ?</span><?php //echo $text_forgotten; ?></a>	
					<!--Add text *Champ obligatoire-->

					<p class="pull-right registerquired">* Champs obligatoires</p>
					
				</div>
				<input type="submit" value="Me connecter<?php //echo $button_login; ?>" class="me_connecter btn btn-primary" />
				<?php if ($redirect) { ?>
				<input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
				<?php } ?>
				 <div class=" row form-group   connexion-form-content">

          
      </div>
            </form>
			
<!-- inscription facebook -->
<script>
	
  // Load the SDK asynchronously
	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
  
	window.fbAsyncInit = function() {			  
		FB.init({
			appId      : '884134801734426' ,
			//cookie     : true,  // enable cookies to allow the server to access 
							// the session
			//xfbml      : true,  // parse social plugins on this page
			version    : 'v2.9' // use graph api version 2.8
		});
	};
				  
	function toLogInFB()
	{
		FB.login(function(response) {                            
			if(response.authResponse)
			{
				getUserInfo();
			}else{
				console.log("L'utilisateur a annulé la connexion ou n'a pas entièrement autorisé.");
			}
		},{scope: 'public_profile,email'});
	}
					
	function Logout()
	{
		FB.logout(function(){document.location.reload();});
	}
					
	
	function getUserInfo()
	{
		
		$('#loading').show();
		FB.api('/me?fields=email,last_name,first_name,name', function(response) {
			//alert(response.name);
			var str = "<b>Name</b> : "+response.name+"<br>";
			str +="<b>First name : </b>"+response.first_name+"<br>";
			str +="<b>Last name : </b>"+response.last_name+"<br>";
			str +="<b>Email :</b> "+response.email+"<br>";
		
			$.ajax({
				url : 'http://adprint-dev.com/index.php?route=account/register/login',
				type : 'POST',
				data : 'email=' + response.email,
				dataType : 'html',
				success : function( code_html , statut){ // code_html contient le HTML renvoyé
				   
				   var res = parseInt( code_html );
				   
				   if( res == 0 )
				   {
					  $('#erreur_facebook').show();
				   }
				   else
				   {
					  window.location="http://adprint-dev.com/index.php?route=account/account";  
				   }
				},
				complete : function()
				{
					$('#loading').hide();
				}
			});
		});
	}
					
	function toLogInFB_register()
	{
		FB.login(function(response) {
			if(response.authResponse)
			{
				getUserInfo_register();
			}else{
				console.log("L'utilisateur a annulé la connexion ou n'a pas entièrement autorisé.");
		   }
		},{scope: 'public_profile,email'});
	}
		
	function getUserInfo_register()
    {		
		$('#loading_register').show();
		
		FB.api('/me?fields=email,last_name,first_name,name', function(response) {
		
		var str = "<b>Name</b> : "+response.name+"<br>";
		str +="<b>First name : </b>"+response.first_name+"<br>";
		str +="<b>Last name : </b>"+response.last_name+"<br>";
		str +="<b>Email :</b> "+response.email+"<br>";
		$.ajax({
			url : 'http://adprint-dev.com/index.php?route=account/register/inscription_fb',
			type : 'POST',
			data : 'email=' + response.email + '&first_name=' + response.first_name + '&last_name=' + response.last_name,
			dataType : 'html',
			success : function( code_html , statut){ // code_html contient le HTML renvoyé
				var res = parseInt( code_html );
			   
				if( res == 0 )
				{
					$('#mail_register').html(response.email);
					$('#erreur_facebook_register').show();
				}
				else
				{
					window.location="http://adprint-dev.com/index.php?route=account/success";  
				}
			},
			complete : function()
			{
				$('#loading_register').hide();
			}
		});			
	});
}
</script>
		
		
		</br></br>
		
		<div style="text-align:center;" >
		<button onclick="toLogInFB();" style="height: 50px;width: 300px;background-size: 300px 60px !important;background: url(http://adprint-dev.com/image/catalog/btn_facebook_connect.png) no-repeat center top;"> </button>
			
			
		<div class="alert alert-danger" style="display:none" id="erreur_facebook" >
			<strong>Erreur lors de la connexion , </strong> vérifiez si vous êtes bien inscrit sur le site.
		</div>
		
		<div id="loading" style="text-align: center;display:none" >
			<img src="http://adprint-dev.com/image/catalog/loading.gif" style="width: 50px;">
		</div>
			
			<div class="clear"></div>
		</div>
	  
			
			
          </div>
        </div>
		
		  <div class="col-sm-6">
          <div class="well">
            <h2><span style="color: #bb0c1b!important;" >Nouveaux </span>Clients ?<?php //echo $text_new_customer; ?></h2>
            <p><strong>Créez votre compte sur Adprint, votre imprimerie en ligne et accédez à de nombreux avantages<?php //echo $text_register; ?></strong></p>
            <p><?php //echo $text_register_account; ?></p>
            <a href="<?php echo $register; ?>" class="me_connecter btn btn-primary">Créer un compte <?php //echo $button_continue; ?></a>
			
					<div style="text-align:center;    margin-top: 40px;" >
						<button onclick="toLogInFB_register();" style="height: 40px;width: 290px;background: url('http://adprint-dev.com/image/catalog/facebook_up.png') no-repeat center top;"> </button>
						
						<div class="clear"></div>
					</div>
					
					
					<div class="alert alert-danger" style="display:none" id="erreur_facebook_register" >
						<strong>Erreur !</strong> adresse mail '<span id="mail_register" ></span>' déjà utilisée.
					</div>
					
					<div id="loading_register" style="text-align: center;display:none" >
						<img src="http://adprint-dev.com/image/catalog/loading.gif" style="width: 150px;">
					</div>
			
			</div>
			
			
					
	  
			
        </div>
		
		
      </div>
      <?php echo $content_bottom; ?></div>
	  
    <?php //echo $column_right; ?>
	
	</div>

	</div>
<?php echo $footer; ?>