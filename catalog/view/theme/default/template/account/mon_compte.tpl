<?php echo $header; ?>

<style>

.ul_titres{
  
  width: 100% ;
  
}

.li_titres{
  
  float: left;
  width: 14%;
  text-align:center;
      font-weight: bold;
    text-transform: uppercase;
  
    border-left: 1px solid #dfdfdf;
    border-top: 1px solid #dfdfdf;
    border-bottom: 1px solid #dfdfdf;
    height: 100px;
    padding-top: 10px;
  
  background-color: #fafafa;
  
  
  
}

.li_titres a{
  color: #6c6c6c;
    text-decoration: none;
}

.li_titres a:hover{
    text-decoration: underline;
}


.bonjour{
      text-align: center;
}

.titre_compte{
    color: #EC008C;

}

.texte_1{
    height: 40px;
  padding-top: 5px;
}

.icones{
    font-size: 30px;
      width: 100%;
}

.texte_2{
  height: 150px;
  margin-top: 50px;
    border: 2px solid #dfdfdf;
}

.texte_3{
  height: 150px;
  margin-top: 50px;
  border: 2px solid #dfdfdf;
  padding: 0px;
}


.mes_infos
{
    height: 30px;
    padding: 5px;
  font-family: Arial !important;
    font-size: 1.2em;
    text-transform: uppercase;
    color: #6c6c6c;
    font-weight: 500;
}

.icones_bas{
  font-size: 20px;
  width:50px;
}

.images{
  height:100%;
}



.menu_actif{
  background-color: #fff;
    border-bottom: none;
}

.div_gris{
  background: #F6F6F6;
}

.titre_gris{
	text-align: center;
    font-size: 13px;
    font-weight: 700;
	height: 50px;
	padding-top: 3px;
}

.zero{
    color: #f40088;
    font-size: 35px;
    font-weight: bold;
    /*text-align: center;*/
    height: 40px;
}

.zero_2{
      color: #f40088;
    font-weight: bold;
    text-align: center;
}


.content_gris{
   text-align: center;
       height: 70px;
}

.voir_details{
    height: 30px;
    text-align: center;
}

.div_230{
   height: 280px;
}   

.acheter{
      height: 30px;
    padding: 5px;
    text-align: center;
    background: #a3a3a3;
    margin-top: 10px;
    font-size: 13px;
    color: #FFF;
}

.contactez{
  margin-top: 50px;text-align: center;
}

</style>
	
<div class="container" style="min-height:900px">
	
	<div class=" col-sm-12">
		<h2 class="titre_compte" >Mon compte </h2>
	</div>
	
	<div class="col-sm-12 texte_1">
	  <div class="bonjour col-sm-10"  > Bonjour <a href="index.php?route=account/account" > <?php echo $firstname ; ?> <?php echo $lastname; ?> </div> 
	  <div class="col-sm-2" > <a href="index.php?route=account/logout" >Se déconnecter </a> </div>
	</div>


<ul class="ul_titres" >

  <li class="li_titres menu_actif" >
  <a href="index.php?route=account/account" >
  <span class="icones glyphicon glyphicon-blackboard"></span> Tableau de bord</a>  </li>
  <li class="li_titres"> <a href="index.php?route=account/edit" > <span class="icones glyphicon glyphicon glyphicon-user"></span> Informations du compte</a></li>
  <li class="li_titres"> <a href="index.php?route=account/order" > <span class="icones glyphicon glyphicon-download-alt"></span> Mes commandes et factures</a></li>
  <li class="li_titres"> <a href="index.php?route=devis/devis/listedevis" > <span class="icones   glyphicon glyphicon-pencil"></span> Mes devis</a></li>
  <li class="li_titres"> <a href="index.php?route=account/account/gestionfichier" > <span class="icones glyphicon glyphicon-book"></span> Gestion de fichiers bon à tirer</a></li>
  <li class="li_titres"> <a href="index.php?route=account/avantage" ><span class="icones glyphicon glyphicon-thumbs-up"></span> Mes avantages</a></li>
  <li class="li_titres" style="border-right: 1px solid #dfdfdf;" > <a href="index.php?route=account/satisfaction" ><span class="icones glyphicon glyphicon-education"></span> Service satisfaction client</a></li>

</ul>



<div class="col-sm-12" >
  
  <div class="col-sm-5 texte_2" >
    <div class="col-sm-12 mes_infos" >MES INFORMATIONS PERSONNELLES</div>
    <div class="col-sm-12" ><span class="glyphicon  glyphicon-user icones_bas" ></span> <?php echo $firstname ; ?> <?php echo $lastname; ?> </div>
    <div class="col-sm-12" ><span class="glyphicon  glyphicon-envelope icones_bas" ></span> <?php  echo $email ; ?> </div>
    <div class="col-sm-12" ><span class="glyphicon  glyphicon-lock icones_bas" ></span> ***** </div>  
    
  </div>
  
<!--
  <div class="col-sm-6 texte_3" > 
   
   <img src="http://adprint-dev.com/image/encart-information-compte-2.jpg" title="Adprint" alt="Adprint" class="img-responsive images"> 
  
  </div>
-->
  
  
</div>




<style>

.numero_commande{    
  color: #f40088;
    font-weight: bold;
}

</style>


<div class="col-sm-12" >
  
	<div class="col-sm-5 texte_2 div_230" style=" padding:0px ">
		<div class="col-sm-12 mes_infos" >VOTRE DERNIÈRE COMMANDE</div>
			<?php  if( count( $orders ) > 0 ) { 
			$order = $orders[0]; ?>  
			<div class="col-sm-12" style="padding: 15px;">
				<div class="col-sm-9" >
					N° de commande : <span class="numero_commande"> <?php echo $order['order_id']; ?> </span>
				</div>
				<div class="col-sm-3" >
					<?php echo $order['date_added']; ?>
				</div>
			</div>
        
        <div class="row" style="padding: 15px;">
        <div class="col-sm-9" >
          <?php if($order['delai_valid']) { ?>
            Demande délai de payment :
          <?php } else { ?>
            Votre délai de payment est : 
          <?php } ?>
        </div>
        <div class="col-sm-3" >
          <?php echo $order['delai']; ?>
        </div>
      </div>
      
			<div class="col-sm-12">
  
				<?php  foreach( $produits as $produit) {  ?>
				<div class="col-sm-12">  
					<div class="col-sm-2" style="height:50px" > 
						<a href="/index.php?route=product/product&product_id=<?php echo $produit['product_id'] ; ?>" > 
						<?php if(  strlen( $produit['image'] ) ) { ?>
							<img style="width:50px;height:50px;" src="/image/<?php echo $produit['image'] ; ?>" >
						<?php } ?>
						</a> 
					</div>
  
					<div class="col-sm-8" style="text-align:left;">
						<a href="/index.php?route=product/product&product_id=<?php echo $produit['product_id'] ; ?>" > 
					  <?php echo $produit['name'] ; ?> </a>
					</div>
				</div>
			<?php  } ?>   

				<div class="col-sm-12 col-sm-offset-2" >
					Statut :  <span class="numero_commande"> <?php   echo $order['status'] ; ?> </span>
				</div>
  
				<a href="/index.php?route=account/order">
				<div class="col-sm-4 acheter" style="margin-left: 30%;width: 200px;">
					Voir toutes mes commandes
				</div>
				</a>
			</div>
	<?php 	} else{ ?>
				<div class="col-sm-12" style="padding: 5px;">
					Vous n'avez pas encore passé de commande
				</div>
	<?php 	}?>  
    
	</div>

	<div class="col-sm-6 col-md-offset-1 texte_3 div_230" > 
		<div class="col-sm-12 mes_infos" >MON COMPTE ADMONEY & MES PARRAINAGES</div>
		
		<div class="col-sm-12">
			<div class="col-sm-6">
				<div class="col-sm-12 div_gris">
				 
					<div class="col-sm-12 titre_gris"> Mon compte Admoney </div>
				
					<div class="col-sm-12 content_gris"> 
						<div class="col-sm-12 zero" > <?php echo number_format($addprintcredit,2);?></div> 
						<div class="col-sm-12 zero_2" >Admoney</div> 
					</div>
				
					<div class="col-sm-12 voir_details"> <a href="<?php echo $detailsAdmoney; ?>">Voir détails </a> </div>
			    
				</div>
				<a href="<?php echo $achatAdmoney; ?>">
					<div class="col-sm-12 acheter">
						Acheter des Admoney
					</div>
				</a>
			</div>
      
      
			<div class="col-sm-6">
				<div class="col-sm-12 div_gris">
			  
				<div class="titre_gris"> Mon réseau de filleuls </div>
				<div class="content_gris"> <span class="zero" > 0 </span> </br> <span class="zero_2" >filleuls</span> </div>
				<div class="voir_details"> <a href="<?php echo $ajoutFilleuils ;?>">En obtenir + </a> </div>
			  
				</div>
			  
				<div class="col-sm-12 acheter">
				  Gagner des Admoney
				</div>
			  
			</div>
		</div>
  </div>

</div>


<!--
<div class="col-sm-12 contactez" >
  <img src="http://adprint-dev.com/image/contacter-easyflyer-conseiller.jpg" title="Adprint" alt="Adprint" class="img-responsive images"> 
</div>

<div class="col-sm-12" style=" background-color: #f3f3f3;text-align: center;margin-top: 20px;    height: 110px;"> 

  <div class="col-sm-4" >
    <div style="padding: 5px;font-size: 18px;" > <span class="glyphicon  glyphicon-envelope icones_bas" ></span>  Newsletter</div>
    <div style="    height: 25px;"> Recevez toutes nos promotions et ventes flash </div>
    <div>  <a href="" class="btn sinscrire"  style="background: #ed008c;padding: 6px 30px 5px 30px;color: #fff;" >S'inscrire</a> </div>
  </div>
  
  <div class="col-sm-4" >
    <div style="padding: 5px;font-size: 18px;" > <span class="glyphicon  glyphicon-lock icones_bas" ></span>  Paiement sécurisé</div>
    <div style="">  
    <img src="/image/imprimerie-en-ligne-paiement-securise-footer-2.jpg" title="Adprint" alt="Adprint" class="img-responsive ">
    </div>
  </div>

  <div class="col-sm-4" >
    <div style="padding: 5px;font-size: 18px;" > <span class="glyphicon  glyphicon-lock icones_bas" ></span>  Suivez-nous sur </div>
    <div style="">
      <a href="https://www.facebook.com/adprintmadagascar">
    <img style="margin-left: 50px;" src="/image/imprimerie-en-ligne-icon-social-footer-3.jpg" title="Adprint" alt="Adprint" class="img-responsive ">
      </a>
    </div>
  </div>
 
     -->
</div>


</div>

<?php echo $footer; ?> 