<?php echo $header; ?>

<style>

.ul_titres{
  
  width: 100% ;
  
}

.li_titres{
  
  float: left;
  width: 14%;
  text-align:center;
  font-weight: bold;
  text-transform: uppercase;
  
  border-left: 1px solid #dfdfdf;
  border-top: 1px solid #dfdfdf;
  border-bottom: 1px solid #dfdfdf;
  height: 100px;
  padding-top: 10px;
  
  background-color: #fafafa;
  
}

.li_titres a{
  color: #6c6c6c;
    text-decoration: none;
}

.li_titres a:hover{
    text-decoration: underline;
}


.bonjour{
      text-align: center;
}

.titre_compte{
    color: #EC008C;

}

.texte_1{
    height: 40px;
  padding-top: 5px;
}

.icones{
    font-size: 30px;
      width: 100%;
}

.texte_2{
  height: 150px;
  margin-top: 10px;
    border: 2px solid #dfdfdf;
}

.texte_3{
  height: 150px;
  margin-top: 50px;
    border: 2px solid #dfdfdf;
  margin-left:20px;
  padding: 0px;
}


.mes_infos
{
    height: 30px	;
    padding: 10px	;
	font-family: Arial !important;
    font-size: 1.2em;
    /* text-transform: uppercase; */
    color: #6c6c6c	;
    font-weight: 600;
	color: #EC008C	;
}

.icones_bas{
  font-size: 20px;
  width:50px;
}

.images{
  height:100%;
}



.menu_actif{
  background-color: #fff;
    border-bottom: none;
}

.div_gris{
  background: #F6F6F6;
}

.titre_gris{
  text-align: center;
    font-size: 13px;
    font-weight: 700;
  height: 35px;
  padding-top: 3px;
}

.zero{
      color: #f40088;
    font-size: 45px;
    font-weight: bold;
    text-align: center;
}

.zero_2{
      color: #f40088;
    font-weight: bold;
    text-align: center;
}


.content_gris{
   text-align: center;
       height: 70px;
}

.voir_details{
  height: 30px;
    text-align: center;
}

.div_230{
   height: 230px;
}   

.acheter{
      height: 30px;
    padding: 5px;
    text-align: center;
    background: #a3a3a3;
    margin-top: 10px;
    font-size: 13px;
    color: #FFF;
}

.contactez{
  margin-top: 50px;text-align: center;
}

.control-label {
text-align: left !important; 
}

</style>
  
  
 

<div class="container" style="min-height:900px">

<div class=" col-sm-12">
<h2 class="titre_compte" >Mes avantages</h2>
</div>
<ul class="ul_titres" >

  <li class="li_titres" >
  <a href="index.php?route=account/account" >
  <span class="icones glyphicon glyphicon-blackboard"></span> Tableau de bord</a>  </li>
  <li class="li_titres "> <a href="index.php?route=account/edit" > <span class="icones glyphicon glyphicon glyphicon-user"></span> Informations du compte</a></li>
  <li class="li_titres"><a href="index.php?route=account/order" ><span class="icones glyphicon glyphicon-download-alt"></span> Mes commandes et factures</a></li>
  <li class="li_titres"> <a href="index.php?route=devis/devis/listedevis" > <span class="icones   glyphicon glyphicon-pencil"></span> Mes devis</a></li>
  <li class="li_titres"> <a href="index.php?route=account/account/gestionfichier" > <span class="icones glyphicon glyphicon-book"></span> Gestion de fichiers bon à tirer</a></li>
  <li class="li_titres menu_actif"> <a href="index.php?route=account/avantage" ><span class="icones glyphicon glyphicon-thumbs-up"></span> Mes avantages</a></li>
  <li class="li_titres" style="border-right: 1px solid #dfdfdf;" > <a href="index.php?route=account/satisfaction" ><span class="icones glyphicon glyphicon-education"></span> Service satisfaction client</a></li>

</ul>

<div class="col-sm-12" >
	<div class=" mes_infos" >La boite à outils du parrainage</div>
  
	<div class="col-sm-12 texte_2" style=" height: auto;" >

		<div class="mes_infos" style="text-align:center"> <b style="color:black">Mon Code Parrain : </b><?php echo $code; ?>  </div>
		<hr>
		<p>  FAQ : Fonctionnement parrainage ? Comment utiliser mes credits ? <br>
		* 1 credit = Ar 1 HT d’achat sur l’ensemble des produits et services   </p>
		<hr>
		
		 <?php if ($success!='') { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  
		<form id="defaultForm" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal"> 
			<div class="col-sm-6" >
			<h2>Parrainage par email</h2>
			<fieldset>
				<div class="form-group">
					<label class="col-sm-3 control-label" for="input-firstname">Filleul 1</label>
					<div class="col-sm-7">
					  <input type="email" name="email-1" id="input-firstname" class="form-control" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label" for="input-lastname"> Filleul 2 </label>
					<div class="col-sm-7">
					  <input type="email" name="email-2" id="input-lastname" class="form-control" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label" for="input-email"> Filleul 3 </label>
					<div class="col-sm-7">
					  <input type="email" name="email-3" id="input-email" class="form-control" />
					</div>
				</div>  
				<div class="form-group">
					<label class="col-sm-3 control-label" for="input-email"> Filleul 4 </label>
					<div class="col-sm-7">
					  <input type="email" name="email-4" id="input-email" class="form-control" />
					</div>
				</div> 
				<div class="form-group">
					<label class="col-sm-3 control-label" for="input-email"> Filleul 5 </label>
					<div class="col-sm-7">
					  <input type="email" name="email-5" id="input-email" class="form-control" />
					</div>
				</div> 
				<input type="hidden" name="nb_mail" value='5'/>
			</fieldset>
			</div>    
			
			<div class="col-sm-6" >
				<h2>Votre message</h2>
					<fieldset>
					<div class="form-group">
<textarea class="form-control" rows="12"name="msg">
	Bonjour,
	
	Je vous invite à découvrir AdPrint, mon service d'imprimerie en ligne à bas prix.
	Vous bénéficierez d'un service client gratuit et disponible, d'un contrôle de vos fichiers avant impression, de la livraison gratuite et d'un vaste choix parmi plus de 20 000 supports de communication en 1 clic.
	En vous inscrivant avec mon mail parrain, <?php echo $code; ?>, vous profiterez de 10 addprintcredit qui vous seront crédités après votre première commande (1 addprintcredit = Ar 1 HT).

	Cordialement,

<?php echo $client; ?></textarea>
				</div>
				</fieldset>
				<div class="buttons clearfix">
					<div class="pull-right">
						<input id="validateBtn" style="background-color: #a3a3a3;background-image: none;border-color: #a3a3a3;" type="submit" value="Envoyer mon invitation à mes contacts" class="btn btn-primary" />
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

<div class="col-sm-10" style=" background-color: #f3f3f3;text-align: center;margin-top: 20px;    height: 110px;"> 
  <div class="col-sm-4" >
    <div style="padding: 5px;font-size: 18px;" > <span class="glyphicon  glyphicon-envelope icones_bas" ></span>  Newsletter</div>
    <div style="    height: 25px;"> Recevez toutes nos promotions et ventes flash </div>
    <div>  <a href="" class="btn sinscrire"  style="background: #ed008c;padding: 6px 30px 5px 30px;color: #fff;" > S'inscrire </a> </div>
  </div>
  
  <div class="col-sm-4" >
    <div style="padding: 5px;font-size: 18px;" > <span class="glyphicon  glyphicon-lock icones_bas" ></span> Paiement sécurisé</div>
    <div style="">  
    <img src="/image/imprimerie-en-ligne-paiement-securise-footer-2.jpg" title="Adprint" alt="Adprint" class="img-responsive images">
    </div>
  </div>
  
  <div class="col-sm-4" >
    <div style="padding: 5px;font-size: 18px;" > <span class="glyphicon  glyphicon-lock icones_bas" ></span>  Suivez-nous sur </div>
    <div style="">
      <a href="https://www.facebook.com/adprintmadagascar">
    <img style="margin-left: 50px;" src="/image/imprimerie-en-ligne-icon-social-footer-3.jpg" title="Adprint" alt="Adprint" class="img-responsive images">
      </a>
    </div>
  </div>
  
  
</div>


</div>

<?php echo $footer; ?> 