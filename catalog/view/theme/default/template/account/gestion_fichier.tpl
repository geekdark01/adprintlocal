<?php echo $header; ?>
<script src="catalog/view/javascript/bat.js" type="text/javascript"></script>
<style>

.ul_titres{
  
  width: 100% ;
  
}

.li_titres{
  
  float: left;
  width: 14%;
  text-align:center;
      font-weight: bold;
    text-transform: uppercase;
  
    border-left: 1px solid #dfdfdf;
    border-top: 1px solid #dfdfdf;
    border-bottom: 1px solid #dfdfdf;
    height: 100px;
    padding-top: 10px;
  
  background-color: #fafafa;
  
  
  
}

.li_titres a{
  color: #6c6c6c;
    text-decoration: none;
}

.li_titres a:hover{
    text-decoration: underline;
}


.bonjour{
      text-align: center;
}

.titre_compte{
    color: #EC008C;

}

.texte_1{
    height: 40px;
  padding-top: 5px;
}

.icones{
    font-size: 30px;
      width: 100%;
}

.texte_2{
  height: 150px;
  margin-top: 50px;
    border: 2px solid #dfdfdf;
}

.texte_3{
  height: 150px;
  margin-top: 50px;
  border: 2px solid #dfdfdf;
  padding: 0px;
}


.mes_infos
{
    height: 30px;
    padding: 5px;
  font-family: Arial !important;
    font-size: 1.2em;
    text-transform: uppercase;
    color: #6c6c6c;
    font-weight: 500;
}

.icones_bas{
  font-size: 20px;
  width:50px;
}

.images{
  height:100%;
}



.menu_actif{
  background-color: #fff;
    border-bottom: none;
}

.div_gris{
  background: #F6F6F6;
}

.titre_gris{
	text-align: center;
    font-size: 13px;
    font-weight: 700;
	height: 50px;
	padding-top: 3px;
}

.zero{
    color: #f40088;
    font-size: 45px;
    font-weight: bold;
    text-align: center;
    height: 40px;
}

.zero_2{
      color: #f40088;
    font-weight: bold;
    text-align: center;
}


.content_gris{
   text-align: center;
       height: 70px;
}

.voir_details{
    height: 30px;
    text-align: center;
}

.div_230{
   height: 250px;
}   

.acheter{
      height: 30px;
    padding: 5px;
    text-align: center;
    background: #a3a3a3;
    margin-top: 10px;
    font-size: 13px;
    color: #FFF;
}

.contactez{
  margin-top: 50px;text-align: center;
}

</style>
	
<div class="container" style="min-height:900px">
	
	<div class=" col-sm-12">
		<h2 class="titre_compte" >Gestion des fichiers </h2>
	</div>
	
	<div class="col-sm-12 texte_1">
	  <div class="bonjour col-sm-10"  > Bonjour <a href="index.php?route=account/account" > <?php echo $firstname ; ?> <?php echo $lastname; ?> </div> 
	  <div class="col-sm-2" > <a href="index.php?route=account/logout" >Se déconnecter </a> </div>
	</div>


<ul class="ul_titres" >
  
  <li class="li_titres" >
  <a href="index.php?route=account/account" >
  <span class="icones glyphicon glyphicon-blackboard"></span> Tableau de bord</a>  </li>
  <li class="li_titres"> <a href="index.php?route=account/edit" > <span class="icones glyphicon glyphicon glyphicon-user"></span> Informations du compte</a></li>
  <li class="li_titres"> <a href="index.php?route=account/order" > <span class="icones glyphicon glyphicon-download-alt"></span> Mes commandes et factures</a></li>
  <li class="li_titres"> <a href="index.php?route=devis/devis/listedevis" > <span class="icones   glyphicon glyphicon-pencil"></span> Mes devis</a></li>
  <li class="li_titres menu_actif"> <a href="index.php?route=account/account/gestionfichier" > <span class="icones glyphicon glyphicon-book"></span> Gestion de fichiers bon à tirer</a></li>
  <li class="li_titres"> <a href="index.php?route=account/avantage" ><span class="icones glyphicon glyphicon-thumbs-up"></span> Mes avantages</a></li>
  <li class="li_titres" style="border-right: 1px solid #dfdfdf;" > <a href="index.php?route=account/satisfaction" ><span class="icones glyphicon glyphicon-education"></span> Service satisfaction client</a></li>
  
</ul>


<script src="catalog/view/javascript/gestionfichier.js" type="text/javascript"></script>
<div class="container">
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
        <div class="block2 select-order" style="margin-bottom: 30px;">
            <h2 class="pink bold"><?php echo $text_gestion_fichier; ?></h2>
            <div class="padder">
                <p><b><?php echo $text_choix_commande ; ?>:</b></p>
            </div>
            <div class="form-order-changer">
                    <select id="input-sort" class="form-control" onchange="showDetailOrder(this.value);" style="display: inline-block;">
                        <option value="0" disabled selected>Choix d'une commande</option>
                        <?php
                            echo $order_id ;
                        ?>
                        <?php foreach($toOrderAccount as $oOrderAccount): ?>
                            <option value="<?php echo $oOrderAccount['order_id'] ; ?>">Order #<?php echo $oOrderAccount['order_id'] ; ?></option>
                            <?php $iCompteur ++  ?>
                        <?php endforeach; ?>
                    </select>
                    <span class="loading"><img src="<?php echo HTTP_IMAGE; ?>loading.gif"></span>
            </div>
        </div>
        
        <div id="content_bat">
            <div class="content">
                <h2 class="dashboard-title-title">Mes Bons à Tirer (BAT)</h2>
                <hr class="separation-title-content">
                <div class="col-md-12">
					<div class="row">
                    <div class="col-xs-12 col-md-8">
                        <img src="<?php echo HTTP_IMAGE; ?>imprimerie-en-ligne-bat-step.png" class="img-responsive" style="float:left;" alt=""> 
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <p style="float:right;text-align:center; padding-right:25px;font-size: 1.13em;">
                        Téléchargez l'ensemble des BAT de votre commande.<br>
                        Contrôlez l'intégralité de vos fichiers téléchargés.<br>
                        <span style="color:#5CBB00">Validez</span> ou <span style="color:#E51D1D">Refusez</span>.
                        </p>
                    </div>
                    </div>
					<div class="row">
				<div class="table-responsive">
					<table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>N° de Cmd</th>
                            <th><?php echo $text_titre_produit ; ?></th>
                            <th>BAT à vérifier</th>
							 <th>Date BAT</th>
                            <th>Validation</th>
                        </tr>
                    </thead>
                    <tbody>
						<?php
						foreach($toOrderAccount as $oOrderAccount):
						foreach($order_product[$oOrderAccount['order_id']] as $toContenu): 
						if(count($toContenu['bat']) <= 0){
							continue;
						}
						?>
                                <tr>
                                    <td>
                                        <?php echo $oOrderAccount['order_id'] ;  ?>
                                    </td>
                                    <td>
                                        <?php echo $toContenu['name'] ;  ?>
                                    </td>
                                   
										<?php foreach($toContenu['bat'] as $toBat): ?>
										 <td>
											<a href="<?php echo $toBat['url_fichier'] ; ?>"><?php echo $toBat['fichier'] ; ?></a>
											</td>
											 <td>
											&nbsp;<span><?php echo $toBat['date_added'] ; ?></span><br/>
											 </td>
										<?php endforeach ; ?>
                                    <td>
                                        <?php if($toContenu['bat_status'] == 0): ?>
                                            <span class="lnkverou<?php echo $toContenu['product_id'] . '_' . $toContenu['order_id'] ; ?>_1" style="display:none;"><?php echo $text_valide ; ?></span>
                                            <span class="lnkverou<?php echo $toContenu['product_id'] . '_' . $toContenu['order_id'] ; ?>_2" style="display:none;"><?php echo $text_refuse ; ?></span>
                                            <a class="lnkverou<?php echo $toContenu['product_id'] . '_' . $toContenu['order_id'] ; ?>_0 btn btn-link"  href="javascript:void(0);" onclick="actionBat(<?php echo $toContenu['order_id'] ; ?>, <?php echo $toContenu['product_id'] ; ?>, 1) ;"><?php echo $text_valider ; ?></a>
                                            &nbsp;
                                            <a class="lnkverou<?php echo $toContenu['product_id'] . '_' . $toContenu['order_id'] ; ?>_0 btn btn-link"  href="javascript:void(0);" onclick="actionBat(<?php echo $toContenu['order_id'] ; ?>, <?php echo $toContenu['product_id'] ; ?>, 2) ;"><?php echo $text_refuser ; ?></a>
                                        <?php elseif($toContenu['bat_status'] == 1): ?>
                                            <span class="lnkverou<?php echo $toContenu['product_id'] . '_' . $toContenu['order_id'] ; ?>_1"><?php echo $text_valide ; ?></span>
                                        <?php elseif($toContenu['bat_status'] == 2): ?>
                                            <span class="lnkverou<?php echo $toContenu['product_id'] . '_' . $toContenu['order_id'] ; ?>_2"><?php echo $text_refuse ; ?></span>
                                        <?php else:?>
                                            &nbsp;-&nbsp;
                                        <?php endif ; ?>
                                    </td>
                                </tr>
                            <?php endforeach ; ?>
                            <?php endforeach ; ?>
					</tbody>
					</table>
				</div>
					</div>
                </div>

            </div>
        </div>
       
        <div class="table-responsive">
            <?php foreach($toOrderAccount as $oOrderAccount): ?>
                <table class="table table-bordered content_order" id="tableau_<?php echo $oOrderAccount['order_id'] ; ?>">
                    <thead>
                        <tr>
                            <th>N° de Cmd</th>
                            <th><?php echo $text_titre_produit ; ?></th>
                            <th>Fichier</th>
                            <th><?php echo $text_titre_action ; ?></th>
                              <!--San-001-Validation-->
                            <th>Status Fichier</th>
                              <!--San-001-Validation-->
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($order_product[$oOrderAccount['order_id']] as $toContenu): 
						?>
                            <tr>
                                <td>
                                    <?php echo $oOrderAccount['order_id'] ; ?>
                                </td>
                                <td>
                                    <?php echo $toContenu['name'] ;  ?>
                                </td>
                                <td>
                                    <?php if(!$toContenu['verou_fichier']) { ?>
                                        <button class="elmenvoi<?php echo $toContenu['product_id'] . '_' . $toContenu['order_id'] ; ?> btn btn-warning" type="button" id="button-upload<?php echo $toContenu['product_id'] . '_' . $toContenu['order_id'] ; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default btn-block"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
                                        <input class="elmenvoi<?php echo $toContenu['product_id'] . '_' . $toContenu['order_id'] ; ?>" type="hidden" name="option[<?php echo $toContenu['product_id'] . '_' . $toContenu['order_id'] ; ?>]" value="" id="input-option<?php echo $toContenu['product_id'] . '_' . $toContenu['order_id'] ;; ?>" />
                                    <?php } ?>&nbsp;&nbsp;
                                    <span id="spn<?php echo $toContenu['product_id'] . '_' . $toContenu['order_id'] ; ?>"></span>
                                    <?php if($toContenu['code_fichier'] && $toContenu['fichier_status'] != 3) { ?>
                                        <script type="text/javascript">
                                            $(document).ready(function(){
                                                recuperationFichier('<?php echo $toContenu['code_fichier'] ; ?>', '<?php echo $toContenu['product_id'] . '_' . $toContenu['order_id'] ; ?>', 'spn') ;
                                            }) ;
                                        </script>
									<?php }else if($toContenu['code_fichier'] && $toContenu['fichier_status'] == 3) { ?>
                                        <button class="elmenvoi<?php echo $toContenu['product_id'] . '_' . $toContenu['order_id'] ; ?> btn btn-warning" type="button" id="button-upload<?php echo $toContenu['product_id'] . '_' . $toContenu['order_id'] ; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default btn-block"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
                                        <input class="elmenvoi<?php echo $toContenu['product_id'] . '_' . $toContenu['order_id'] ; ?>" type="hidden" name="option[<?php echo $toContenu['product_id'] . '_' . $toContenu['order_id'] ; ?>]" value="" id="input-option<?php echo $toContenu['product_id'] . '_' . $toContenu['order_id'] ;; ?>" />
                                    <?php } ?>
                                </td>
                                <td>
                                    <?php if($toContenu['fichier_status'] != 3) { ?>
                                    <a id="lnksuppr<?php echo $toContenu['product_id'] . '_' . $toContenu['order_id'] ; ?>" <?php if($toContenu['verou_fichier'] || !$toContenu['code_fichier'] ): ?>style="display:none;"<?php endif;?> href="javascript:void(0);" id="lnk<?php echo $toContenu['product_id'] . '_' . $toContenu['order_id'] ; ?>" class="btn btn-link" onclick="actionFichier(<?php echo $toContenu['order_id'] ; ?>, <?php echo $toContenu['product_id'] ; ?>, 'supprimer') ;">Supprimer</a>
									
									<a id="lnkverou<?php echo $toContenu['product_id'] . '_' . $toContenu['order_id'] ; ?>" <?php if($toContenu['verou_fichier']|| !$toContenu['code_fichier']): ?>style="display:none;"<?php endif; ?> href="javascript:void(0);" id="lnk<?php echo $toContenu['product_id'] . '_' . $toContenu['order_id'] ; ?>" class="btn btn-link" onclick="actionFichier(<?php echo $toContenu['order_id'] ; ?>, <?php echo $toContenu['product_id'] ; ?>, 'envoyer') ;"><?php echo $text_envoyer ; ?></a>
                                    <span <?php if(!$toContenu['verou_fichier']): ?>style="display:none;"<?php endif; ?> id="spnverou<?php echo $toContenu['product_id'] . '_' . $toContenu['order_id'] ; ?>"><?php echo $text_envoyé ; ?></span>
                                    <?php } ?> 
                                </td>
                                <!--San-001-Validation-->
                                <td>
                                     <?php if($toContenu['fichier_status']==1){ ?>
                                         <?php echo 'Validé' ;  ?>
                                    <?php }elseif($toContenu['fichier_status']==2){ ?>
                                        <?php echo 'Réfusé' ;  ?>
                                    <?php }elseif($toContenu['fichier_status']==0){ ?>
                                        <?php echo 'En attente' ;?>
                                    <?php }elseif($toContenu['fichier_status']==3){ ?>
                                        <?php echo 'Demande de renvoie' ;  ?>
                                    <?php } ?>
                                  <!--San-001-Validation-->
                                </td>
                            </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
            <?php endforeach ; ?>
        </div>
        
        <?php if(isset($iOrderIdFirst)): ?>
            <script type="text/javascript">
                $(document).ready(function(){
                    showDetailOrder(<?php echo $iOrderIdFirst ; ?>) ;
                }) ;
            </script>
        <?php endif ; ?>
        <script type="text/javascript">
            $('button[id^=\'button-upload\']').on('click', function() {
                var node = this;

                $('#form-upload').remove();

                $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

                $('#form-upload input[name=\'file\']').trigger('click');

                if (typeof timer != 'undefined') {
                    clearInterval(timer);
                }

                timer = setInterval(function() {
                    if ($('#form-upload input[name=\'file\']').val() != '') {
                        clearInterval(timer);

                        $.ajax({
                            url: 'index.php?route=tool/upload',
                            type: 'post',
                            dataType: 'json',
                            data: new FormData($('#form-upload')[0]),
                            cache: false,
                            contentType: false,
                            processData: false,
                            beforeSend: function() {
                                $(node).button('loading');
                            },
                            complete: function() {
                                $(node).button('reset');
                            },
                            success: function(json) {
                                $('.text-danger').remove();

                                if (json['error']) {
                                    $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
                                }

                                if (json['success']) {
                                    //alert(json['success']); 

                                    $(node).parent().find('input').val(json['code']);
                                    var zInputHiddenId = $(node).parent().find('input').attr('id') ;
                                    zInputHiddenId = zInputHiddenId.replace('input-option', '') ;
                                    console.log(zInputHiddenId) ;
                                    var tzInputHiddenId = new Array() ;
                                    tzInputHiddenId = zInputHiddenId.split('_') ;
                                    enregistrementFichier(tzInputHiddenId[1], tzInputHiddenId[0], json['code']) ;
                                    recuperationFichier(json['code'], zInputHiddenId, 'spn') ;
                                }
                            },
                            error: function(xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });
                    }
                }, 500);
            });
        </script>

</div>






</div>

<?php echo $footer; ?> 