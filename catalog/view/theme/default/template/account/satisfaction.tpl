<?php echo $header; ?>

<style>

.ul_titres{
  
  width: 100% ;
  
}

.li_titres{
  
  float: left;
  width: 14%;
  text-align:center;
  font-weight: bold;
  text-transform: uppercase;
  
  border-left: 1px solid #dfdfdf;
  border-top: 1px solid #dfdfdf;
  border-bottom: 1px solid #dfdfdf;
  height: 100px;
  padding-top: 10px;
  
  background-color: #fafafa;
  
}

.li_titres a{
  color: #6c6c6c;
    text-decoration: none;
}

.li_titres a:hover{
    text-decoration: underline;
}


.bonjour{
      text-align: center;
}

.titre_compte{
    color: #EC008C;

}

.texte_1{
    height: 40px;
  padding-top: 5px;
}

.icones{
    font-size: 30px;
      width: 100%;
}

.texte_2{
  height: 150px;
  margin-top: 10px;
    border: 2px solid #dfdfdf;
}

.texte_3{
  height: 150px;
  margin-top: 50px;
    border: 2px solid #dfdfdf;
  margin-left:20px;
  padding: 0px;
}


.mes_infos
{
    height: 30px  ;
    padding: 10px  ;
  font-family: Arial !important;
    font-size: 1.2em;
    /* text-transform: uppercase; */
    color: #6c6c6c  ;
    font-weight: 600;
  color: #EC008C  ;
}

.icones_bas{
  font-size: 20px;
  width:50px;
}

.images{
  height:100%;
}



.menu_actif{
  background-color: #fff;
    border-bottom: none;
}

.div_gris{
  background: #F6F6F6;
}

.titre_gris{
  text-align: center;
    font-size: 13px;
    font-weight: 700;
  height: 35px;
  padding-top: 3px;
}

.zero{
      color: #f40088;
    font-size: 45px;
    font-weight: bold;
    text-align: center;
}

.zero_2{
      color: #f40088;
    font-weight: bold;
    text-align: center;
}


.content_gris{
   text-align: center;
       height: 70px;
}

.voir_details{
  height: 30px;
    text-align: center;
}

.div_230{
   height: 230px;
}   

.acheter{
      height: 30px;
    padding: 5px;
    text-align: center;
    background: #a3a3a3;
    margin-top: 10px;
    font-size: 13px;
    color: #FFF;
}

.contactez{
  margin-top: 50px;text-align: center;
}

.control-label {
text-align: left !important;
}

</style>
  
  
  
<script type="text/javascript">

</script>
  
  

<div class="container" style="min-height:900px">

<div class=" col-sm-12">
<h2 class="titre_compte" >Mon compte </h2>
</div>

<div class="col-sm-12 texte_1">
  <div class="bonjour col-sm-10"  >Bonjour <a href="index.php?route=account/account" > <?php echo $firstname ; ?> <?php echo $lastname; ?> </a> </div>
  <div class="col-sm-2" > <a href="index.php?route=account/logout" >Se déconnecter </a> </div>
</div>


<ul class="ul_titres" >

  <li class="li_titres" >
  <a href="index.php?route=account/account" >
  <span class="icones glyphicon glyphicon-blackboard"></span> Tableau de bord</a>  </li>
  <li class="li_titres"> <a href="index.php?route=account/edit" > <span class="icones glyphicon glyphicon glyphicon-user"></span> Informations du compte</a></li>
  <li class="li_titres"><a href="index.php?route=account/order" ><span class="icones glyphicon glyphicon-download-alt"></span> Mes commandes et factures</a></li>
  <li class="li_titres"> <a href="index.php?route=devis/devis/listedevis" > <span class="icones   glyphicon glyphicon-pencil"></span> Mes devis</a></li>
  <li class="li_titres"> <a href="index.php?route=account/account/gestionfichier" > <span class="icones glyphicon glyphicon-book"></span> Gestion de fichiers bon à tirer</a></li>
  <li class="li_titres"> <a href="index.php?route=account/avantage" ><span class="icones glyphicon glyphicon-thumbs-up"></span> Mes avantages</a></li>
  <li class="li_titres menu_actif" style="border-right: 1px solid #dfdfdf;" > <a href="index.php?route=account/satisfaction" ><span class="icones glyphicon glyphicon-education"></span> Service satisfaction client</a></li>

</ul>



<div class="col-sm-12" >

  <div class=" mes_infos" >Merci de formuler votre question</div>
  
  <div class="col-sm-12 texte_2" style=" height: auto;padding-bottom:10px;" >
    
    <form id="forma" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
	
		<div class="col-sm-12">
		  <h3> <?php echo $text_information; ?> </h3>
	    </div>
        
		<div class="col-sm-6" >

			<fieldset>
			 
			  <div class="form-group required">
				<label class="col-sm-12 control-label" for="input-firstname"><?php echo $input_firstname; ?></label>
				<div class="col-sm-12">
				  <input type="text" name="nom" value="<?php echo $firstname . " " . $lastname ; ?>" id="nomid" class="form-control" placeholder="<?php echo $input_firstname; ?>" required />
				</div>
			  </div>
			  
			  <div class="form-group required">
				<label class="col-sm-12 control-label" for="input-confirm"><?php echo $tel_confirm; ?></label>
				<div class="col-sm-12">
				  <input type="text" name="tel" value="" placeholder="<?php echo $tel_confirm ; ?>"  class="form-control" required/>
				  
				</div>
			  </div>
			
			</fieldset>

		   </div>


		   <div class="col-sm-6" >
				  <div class="form-group required">
					<label class="col-sm-12 control-label" for="input-confirm"><?php echo $adresse; ?></label>
					<div class="col-sm-12">
					  <input type="email" name="mail" value="<?php echo $email ; ?>" placeholder="<?php echo $adresse ; ?>"  class="form-control" required />
					  
					</div>
					</div>
					
					<?php if($customer_group_id==1){ ?>
					<div class="form-group required">
						<label class="col-sm-12 control-label" for="input-confirm"><?php echo $societe; ?></label>
						<div class="col-sm-12">
						  <input type="text" name="societe" value="" placeholder="<?php echo $societe ; ?>" class="form-control" required />					  
						</div>
					</div>
					<?php } else{
						?>
					<div class="form-group">
						<label class="col-sm-12 control-label" for="input-confirm"><?php echo $societe; ?></label>
						<div class="col-sm-12">
						  <input type="text" name="societe" value="" placeholder="<?php echo $societe ; ?>" class="form-control" />
						</div>
					</div>
						<?php
					}?>

		   </div>
		   
		   <div class="col-sm-12">
			  <h3> VOTRE REQUÊTE AUPRES DU SERVICE RETOUR & SATISTACTION CLIENT </h3>
		   </div>
		   
		   
		   <div class="col-sm-12" >
			<fieldset>
			 
			  <div class="form-group required">
				<label class="col-sm-12 control-label" for="input-confirm"><?php echo $sujetdmd; ?></label>
				<div class="col-sm-12">
				  <input type="text" name="sujetdmd" value="<?php echo ""; ?>" placeholder="<?php echo $sujetdmd ; ?>"  class="form-control" />			 
				</div>
			  </div>
			
			</fieldset>
		   </div>
		   
		   <div class="col-sm-12" >
			<fieldset>
			  <div class="form-group required">
				  <label class="col-sm-12 control-label" for="input-confirm"><?php echo $selectioncmd; ?></label>
				  <div class="col-sm-12">
					  <select class="form-control" id="detail" name="choixcommande">
						<option value=''>-- Faites un choix --</option>
						<?php  foreach($result as $oResul) { ?>
						<option value='<?php echo $oResul['date_added']." n°".$oResul['order_id']?>'>
						Cmd n°<?php echo $oResul['order_id'] . " - " . $oResul['date_added']; ?>
						</option>
						<?php } ?>
					  </select>
				  </div>
			  </div>
			</fieldset>
		   </div>
		   
		   <div class="col-sm-6" >
			<fieldset>
			  <div class="form-group required">
				  <label class="col-sm-12 control-label" for="input-confirm"><?php echo $motifclient; ?></label>
				  <div class="col-sm-12">
					  <select class="form-control" id="detail" name="motifclientsaisie">
						<option value="">-- Faites un choix --</option>
						<option value="Retard de livraison">Retard de livraison</option>
						<option value="Produit abimé">Produit abimé</option>
						<option value="finition">Finition(pelliculage, plastification, oeillets, ...) </option>
						<option value="Probleme de coupe">Problème de coupe</option>
						<option value="Erreur de format">Erreur de format</option>
						<option value="Trace, tâche, ...">Trace, tâche, ...</option>
						<option value="Couleur">Couleur</option>
						<option value="Quantité">Quantité</option>
						<option value="Mauvais produit reçu">Mauvais produit reçu</option>
					  </select>
				  </div>
				  
			  </div>
			</fieldset>
		   </div>
		   
		   <div class="col-sm-6" >
			<fieldset>
			  <div class="form-group required">
				  <label class="col-sm-12 control-label" for="input-confirm"><?php echo $souhait; ?></label>
				  <div class="col-sm-12">
					  <select class="form-control" id="detail" name="chx">
						<option value="">-- Faites un choix --</option>
						<option value="Retirage">Retirage</option>
						<option value="Geste commercial">Geste commercial</option>
						<option value="Remboursement">Remboursement</option>
					  </select>
				  </div>
				  
			  </div>
			</fieldset>
		   </div>
		   
		   <div class="col-sm-12" >
			<fieldset>
			  <div class="form-group required">
				  <label class="col-sm-12 control-label" for="input-firstname"><?php echo $commentaire; ?> </label>
				  <div class="col-sm-12">
					  <textarea class="form-control" name="comment" rows="5"></textarea>
				  </div>
				  
			  </div>
			</fieldset>
		   </div>
		   
		   <div class="col-sm-12" >
			<fieldset>
			  <div class="form-group required">
				  <label class="col-sm-12 control-label" for="input-firstname">Séléctionner à partir de votre disque dur des photos représentatrices de votre requête au SAV : </label>
				  <div class="col-sm-12 upload">
						<div id="uploader">
						
						</div>
				  </div>
				  <div class="col-sm-12 upload">
					<p id="infoUpload" style="padding-top:20px;font-style:italic;clear:both;">Les images doivent être d'une <strong>taille suffisante</strong> et au <strong>format JPEG</strong> (fichiers .jpg, .jpeg, .JPG ou .JPEG, .pdf).</p>
				  </div>
				  
			  </div>
			</fieldset>
		   </div>
		   
			<div class="clearfix"></div>
	
		     <div class="col-sm-7">
				  <button type="submit" title="Valider" class="btn btn-flat btn-darker-gray btn-text-min bold dashboard-centred-btn" style="float:right;">Envoyer votre demande</button>
			  </div>
			  <div class="col-sm-5">
				  <p class="required required-label highlighted-red" style="float:right;">* Champs obligatoires</p>
			  </div>
		   
		   

     </form>
     
	<div class="clearfix"></div>

	  
	
       
    
  </div>

<div class="clearfix"></div>
</div>
<script>
	//satisfaction
	// alert("pup2");
	$("#uploader").plupload({
		// General settings
		runtimes : 'html5,flash,silverlight,html4',
		url : 'uploads/upload.php',
		// User can upload no more then 20 files in one go (sets multiple_queues to false)
		max_file_count: 20,
		
		chunk_size: '1mb',
		// Resize images on clientside if we can
		resize : {
			width : 200, 
			height : 200, 
			quality : 90,
			crop: true // crop to exact dimensions
		},
		
		filters : {
			// Maximum file size
			max_file_size : '1000mb',
			// Specify what files to browse for
			mime_types: [
				{title : "Image files", extensions : "jpg,gif,png"},
				{title : "Zip files", extensions : "zip"}
			]
		},
		// Rename files by clicking on their titles
		rename: true,
		
		// Sort files
		sortable: true,
		// Enable ability to drag'n'drop files onto the widget (currently only HTML5 supports that)
		dragdrop: true,
		// Views to activate
		views: {
			list: true,
			thumbs: true, // Show thumbs
			active: 'thumbs'
		},
		// Flash settings
		flash_swf_url : '../../js/Moxie.swf',
		// Silverlight settings
		silverlight_xap_url : '../../js/Moxie.xap'
	});
	// Handle the case when form was submitted before uploading has finished
	$('#forma').submit(function(e) {
		// Files in queue upload them first
		if ($('#uploader').plupload('getFiles').length > 0) {
			// When all files are uploaded submit form
			$('#uploader').on('complete', function() {
				$('#forma')[0].submit();
			});
			$('#uploader').plupload('start');
		} else {
			alert("You must have at least one file in the queue.");
		}
		return false; // Keep the form from submitting
	});
	
	$(".plupload_view_switch").addClass('hide');
	$("#uploader_browse").addClass('btn btn-primary');
	// $("#uploader_start").addClass('hide');
	$("#uploader_start").click(function(){
		$('#uploader').plupload('start');
	});
	$(".plupload_wrapper").css('border', '1px solid #DFDFDF');
	$(".plupload_file_status").css('padding', '15px');
	$(".plupload_file_size").css('padding', '15px');
</script>


<style>

.bloc2{
  
  margin: 10px;
    border: 2px solid #dfdfdf;
}

.highlighted-red {
	color: #FF0000;
}





</style>
</div>

<?php echo $footer; ?> 