<footer>
  <div class="container">
    <div class="row">
      <?php if ($informations) { ?>
      <div class="col-sm-3">
        <h5>A propos d’Adprint :</h5>
        <ul class="list-unstyled">
          <?php foreach ($informations as $information) { ?>
          <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
          <?php } ?>
      <li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
        </ul>
      </div>
      <?php } ?>
      <div class="col-sm-3">
        <h5>Contactez-nous</h5>
        <ul class="list-unstyled">
          <li><a href="<?php echo $contact; ?>">Formulaire de contact</a></li>
          <li><a href="?route=information/information&information_id=9">Plan de repérage </a></li>
        </ul>
      </div>
      <div class="col-sm-3">
        <h5>Nos services</h5>
        <ul class="list-unstyled">
          <li><a href="?route=information/information&information_id=10">Parrainage</a></li>
          <li><a href="<?php echo $addprintcard; ?>">Pack Admoney</a></li>
          <li><a href="#">Info BAT</a></li>
          <li><a href="#">Infos signature adprint.</a></li>
      <li><a href="#">Ventes Flash.</a></li>
      <li><a href="#">Promotions.</a></li>
        </ul>
      </div>
      <div class="col-sm-3">
        <h5>Espace client</h5>
        <ul class="list-unstyled">
          <li><a href="<?php echo $account; ?>">Mon compte</a></li>
          <li><a href="<?php echo $order; ?>">Historique de mes commandes</a></li>
        </ul>
      </div>
    </div>
    <hr>
    <p><?php echo $powered; ?></p>
  </div>
</footer>

<!--
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
Please donate via PayPal to donate@opencart.com
//-->

<!-- Theme created by Welford Media for OpenCart 2.0 www.welfordmedia.co.uk -->
</body></html>