<!-- wedget menu -->
<div class="content-wedget">
	<ul class="wedgetMenu">
    <li>
      <a href="javascript:void(0);">
        <i class="fa fa-plus fa-3x"></i>
        <p>Services</p>
      </a>
      <ul class="wedgetService">
        <li>
          <a href="">
          <P>Besoin d'aide</P>
          </a>
        </li>
        <li>
          <a href="">
            <P>Espace 100% Revendeurs</P>
            <span>Pour les professionnels des Arts Graphiques</span>
          </a>
        </li>
        <li>
          <a href="">
            <P>Franchise / Corporate</P>
            <span>Un espace de commande et de gestion sur-mesure</span>
          </a>
        </li>
        <li>
          <a href="">
            <P>Parrainez votre réseau</P>
            <span>Et faites des économies</span>
          </a>
        </li>
        <li>
          <a href="<?php echo $infographie;?>">
            <P>Service Infographie</P>
            <span>Des forfaits créations pour vos maquettes</span>
          </a>
        </li>
      </ul>
    </li>

    <li>
		<a href="javascript:void(0);">
			<i class="fa fa-user fa-3x"></i>
			<p>Mon Compte</p>
		</a>
		<ul class="wedgetUser">
			<?php if(!$logged){ ?>
			<li>
				<a id="ident" class="btn-identifier" href="<?php echo $login; ?>"></span> Identifiez-vous</a>
				<a id="fb" class="btn-facebook" href="<?php echo $login; ?>">Se connecter avec facebook</a>
           Nouveau client ? <a href="<?php echo $register; ?>">Rejoignez-nous</a>
		    </li>
		  <?php } ?>
        
			<li>
			  <a href="<?php echo $mon_compte; ?>"><span>Mon compte</span></a>
			  <a href="<?php echo $mes_commandes; ?>"><span>Mes commandes</span></a>
			  <a href="<?php echo $mes_devis; ?>"><span>Mes Devis</span></a>
			  <a href="<?php echo $transfert_fichier; ?>"><span>Transfert de fichier</span></a>
			  <a href="<?php echo $parrainage; ?>"><span>Parrainage</span></a>
			  <a href="<?php echo $bat; ?>"><span>Mes Bons à Tirer (BAT)</span></a>
			</li>
			<li>
			  <a href=""><span>Besoin d'aide</span></a>
			</li>
			<?php if($logged){ ?>
			<li>
				<a href="<?php echo $logout; ?>"><span>Se déconnecter</span></a>
		    </li>
		  <?php } ?>
		</ul>
    </li> 

    <li id="panier" ng-model="panier">
		<a href="#">
			<i class="fa fa-shopping-cart fa-3x"></i>
			<span id="panierTotal"><?php echo $text_items; ?></span>
			<p>Mon Panier</p>
		</a>
		<ul class="wedgetPanier">
			<li>
			<div class="dropdown-content ">
				<?php if ($products || $vouchers) { ?>
				<li>
				<table class="table">
					<?php foreach ($products as $product) { ?>
					<tr>
						<td class="">
							  <?php if ($product['thumb']) { ?>
							  <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail" /></a>
							  <?php } ?>
						</td>
						<td class="">
                          <a href="<?php echo $product['href']; ?>">
                              <?php echo $product['name']; ?>
                          </a>
                          <!-- <?php if ($product['option']) { ?> -->
                          <!-- <?php foreach ($product['option'] as $option) { ?> -->
                          <!-- <br /> - <small><?php echo $option['name']; ?> <?php echo $option['value']; ?></small> -->
                          <!-- <?php } ?> -->
                          <!-- <?php } ?> -->
                          <!-- <?php if ($product['recurring']) { ?> -->
                          <!-- <br /> - <small><?php echo $text_recurring; ?> <?php echo $product['recurring']; ?></small> -->
                          <!-- <?php } ?> -->
                        <p>quantité : <?php echo $product['quantity']; ?></p>
						</td>
						<td class=""><button style="background-color:transparent;" type="button" onclick="cart.remove('<?php echo $product['cart_id']; ?>');" title="<?php echo $button_remove; ?>" class="btn-xs"><i style="color:#DD281E;" class="fa fa-trash"></i></button>
						</td>
					</tr>
					<tr>
						<td class=" text-right" colspan="3">
							<span style="color:#DD281E;"><?php echo $product['total']; ?></span>
						</td>
					</tr>
					<?php } ?>
					<?php foreach ($vouchers as $voucher) { ?>
					<tr>
						<td class="text-center"></td>
						<td class="">
							<?php echo $voucher['description']; ?>
						</td>
						<td class="text-right">x&nbsp;1</td>
						<td class="text-right">
							<?php echo $voucher['amount']; ?>
						</td>
						<td class="text-danger"><button type="button" onclick="voucher.remove('<?php echo $voucher['key']; ?>');" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></td>
					</tr>
                  <?php } ?>
				</table>
				</li>
				<li>
					<div>

						<p class="text-center" style="color:#DD281E;">
							<?php 
						foreach ($totals as $total) {
							if($total['title']=="Sous-total"){
						?>
							Total : <?php echo $total['text']; ?>
						<?php
							}
						}
						?>							
						</p>
						<p class="text-center">
							<a class="btn btn-primary" href="<?php echo $cart; ?>">
							Commander
							</a>
						</p>
					</div>
				</li>
				<?php } else { ?>
				<li>
					<p class="">
						<?php echo $text_empty; ?>
					</p>
				</li>
			<?php } ?>
			</div>
			</li>
		</ul>
	</li>
	</ul>
</div>
<!-- wedget menu -->

