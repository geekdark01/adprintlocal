﻿<?php echo $header; ?>

<style>

.div_image{
	float: left;
    width: 100%;
    text-align: center;
	margin: 10px;
}

.text-center{
	padding-left: 10px;
    margin-bottom: 12px;
    color: #18b4ea;
    font-size: 16px;
    font-weight: bold;
    clear: both;
	text-align: center!important;
}
   
.text-justify{
	width: 80%;
    margin: 0 auto;
}   
   
</style>

<div class="container content_container">
  <!-- SI EXISTENCE D'AFFICHAGE DOSSIER -->
  <?php if($affichageDossier > 0 && !isset($style) ) { ?>  
                <div class="page-title-category col-sm-12">
                    <br/>
                    <div class="breadcrumbs">
						
						
					
						<ul>
                             <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                             <li><a href="<?php echo $breadcrumb['href']; ?>" title="" itemprop="url"><span itemprop="title"><?php echo $breadcrumb['text']; ?></span></a>
                                <span>&gt; </span>
                             </li>
                             <?php } ?>
                         </ul>
                      </div>
                     <h1><?php foreach ($breadcrumbs as $breadcrumb) { echo $breadcrumb['text']."&nbsp;" ; } ?></h1> 
                </div>
                
                <div class="container">
				
				
				<div class="div_image">
				
				
				<a href="<?php if( isset( $lien_image_menu ) ) echo $lien_image_menu ;?>">
					<img src="/image/<?php if( isset( $image_menu ) ) echo $image_menu ;?>" >
				</a>
				
				</div>
				
				<?php  
				//var_dump( $categories );
				?>
				 <?php  foreach ($categories as $oCategoryDossier) { ?>
                    <div class="product-layout col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="product-thumb transition liste_produit">
							
							
							
                            <div class="image">
							<a href="<?php echo $oCategoryDossier['href']; ?>">	
							<img src="<?php echo $oCategoryDossier['thumb'] ; ?>" alt="<?php echo $oCategoryDossier['name']; ?>" title="<?php echo $oCategoryDossier['name']; ?>" class="img-responsive" />
							</a>
							</div>
							<a href="<?php echo $oCategoryDossier['href']; ?>">	
                            <h3 class="cat_name"><?php echo $oCategoryDossier['name']; ?></h3>
							</a>
                            <?php  foreach ($oCategoryDossier['filles'] as $oCategoryFils) { ?>
                                <a href="http://adprint-dev.com/index.php?route=product/category&path=<?php echo $category_id."_".$oCategoryFils['category_id']; ?>" title="" /><p>> <?php echo $oCategoryFils['name']; ?></p></a>
                            <?php }  ?>
                        </div>
                    </div>
                    <?php }  ?>
                 <?php /* foreach ($category_info_dossier as $oCategoryDossier) { ?>
                    <div class="product-layout col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="product-thumb transition">
				
						<?php
						//var_dump( $oCategoryDossier );
						?>
				
						
                            <div class="image">
							
							<img src="http://adprint-dev.com/image/<?php echo $oCategoryDossier['fils'][0]['image'] ; ?>" alt="<?php echo $oCategoryDossier['nom_dossier']; ?>" title="<?php echo $oCategoryDossier['nom_dossier']; ?>" class="img-responsive" />
							
							</div>
                            <h3 class="cat_name"><?php echo $oCategoryDossier['nom_dossier']; ?></h3>
                            <?php foreach ($oCategoryDossier['fils'] as $oCategoryFils) { ?>
                                <a href="<?php echo $oCategoryFils['href']; ?>" title="" /><p>> <?php echo $oCategoryFils['name']; ?></p></a>
                            <?php } ?>
                        </div>
                    </div>
                    <?php } */ ?>
                </div>
				
				
				
				<div class="text text-justify">

				<?php if( isset( $description ) ) echo $description ;?>
						
				</div>
										
				
                
  <?php }else{ ?>
  <!-- AFFICHAGE IMAGE -->
  <?php if($affichage_tableau == 0) { ?>
                <div class="page-title-category col-sm-12">
                    <br/>
                    <div class="breadcrumbs">
					
					<!--
					<img src="http://adprint-dev.com/image/bandeau-categorie-carte-fidelite-bt-nouveau.jpg" >
					-->
                         <ul>
                             <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                             <li><a href="<?php echo $breadcrumb['href']; ?>" title="" itemprop="url"><span itemprop="title"><?php echo $breadcrumb['text']; ?></span></a>
                                <span>&gt; </span>
                             </li>
                             <?php } ?>
                         </ul>
                      </div>
                     <h1><?php foreach ($breadcrumbs as $breadcrumb) { echo $breadcrumb['text']."&nbsp;" ; } ?></h1> 
                </div>
                
                <div class="container-int">
                    <?php 
                    if(isset($categories_bas) && $categories_bas != null){
                        foreach ($categories_bas as $category) { ?>
                        <div class="product-layout col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <div class="product-thumb transition liste_produit">
                                    <div class="image"><a href="<?php echo $category['href']; ?>"><img src="<?php echo $category['thumb']; ?>" alt="<?php echo $category['name']; ?>" title="<?php echo $category['name']; ?>" class="img-responsive" /></a></div>
                                    <h3 class="cat_name"><?php echo $category['name']; ?></h3>
                                    <?php  
                                        $prix_minimum = ($category['prix_minimum'] != '$0.00' ) ? "<p class='cat_price'><span>D&egrave;s </span>".$category['prix_minimum']." HT</p>" : "<p class='cat_price'><span>&nbsp;</span>&nbsp;</p>" ;
                                        echo $prix_minimum ;
                                    ?> 
                                </div>
                        </div>
                    <?php } 
                    }else
                    { ?>
                        <!-- AFFICHAGE DU PRODUIT -->
                        <?php if ($products) { ?>
                         <div class="category-products">
                                <h3 class="step-choose">Votre produit</h3>
                                <div class="tva-switcher" style="float:right;margin-right:5px;">
                                    <script type="text/javascript">
                                            $(document).ready(function(){
                                                $('.tvaswitch').change(function(){
                                                    var value = $(this).val();
                                                    if(value== 1){
                                                        $('.price-including-tax').hide();
                                                        $('.price-excluding-tax').fadeIn();

                                                    }else if(value == 2){
                                                        $('.price-excluding-tax').hide();
                                                        $('.price-including-tax').fadeIn();
                                                    }
                                                });
                                                $("input[name=tvaswitch][value=1]").attr('checked', 'checked');
                                            });
                                    </script>

                                    <label>Prix Ht</label>
                                    <input name="tvaswitch" class="tvaswitch" value="1" type="radio">
                                    <label>Prix TTC</label>
                                    <input name="tvaswitch" class="tvaswitch" value="2" type="radio">
                                </div>
                                <div class="products-containe">
                                        <ol class="products-list" id="products-list">
                                                <?php foreach ($products as $product) { ?>
                                                    <li class="item">
                                                        <div class="product-shop">
                                                            <div class="product-name">
                                                                <a href="<?php echo $product['href']; ?>" title="<?php echo $product['name']; ?>">
                                                                    <?php echo $product['name']; ?>
                                                                </a>
                                                                <?php if ($product['price']) { ?>
                                                                <div class="price-box">
                                                                    <?php if ($product['tax']) { ?>
                                                                         
                                                                           <span class="price-excluding-tax" style="display: block;">
                                                                                <span class="price" ><?php echo $product['tax']; ?></span><span class="label">HT</span>
                                                                           </span>
                                                                    <?php } ?>
                                                                     <?php if (!$product['special']) { ?>
                                                                        <span class="price-including-tax" style="display: none;">
                                                                            <span class="label" style="display:inline-block;"></span>
                                                                            <span class="price" > <?php echo $product['price']; ?></span><span class="label">TTC</span>
                                                                        </span>
                                                                      <?php } else { ?>
                                                                        <span class="price-including-tax" style="display: none;">
                                                                            <span class="label" style="display:inline-block;"></span>
                                                                            <span class="price" > <?php echo $product['special']; ?></span><span class="label">TTC</span>
                                                                        </span>
                                                                      <?php } ?>
                                                                <?php } ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                <?php }?> 
                                          </ol>
                                          <div class="list-produit-end"></div>
                                    </div>
                            </div>
                            <?php } 
                           else { ?>
                           <p><?php echo $text_empty; ?></p>
                           <?php }
                    }
                    ?>
                    <!-- AFFICHAGE DU PRODUIT -->
                </div>
          <?php } ?>
          <!-- AFFICHAGE EN TABLEAU DEUXIEME RANG CATEGORIE -->
          <?php if($affichage_tableau == 1) { ?>
              <div class="row"><?php echo $column_left; ?>
                <?php if ($column_left && $column_right) { ?>
                <?php $class = 'col-sm-6'; ?>
                <?php } elseif ($column_left || $column_right) { ?>
                <?php $class = 'col-sm-9'; ?>
                <?php } else { ?>
                <?php $class = 'col-sm-12'; ?>
                <?php } ?>
                <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
                    <div class="page-title-category col-sm-12">
                        <div class="breadcrumbs">
                             <ul>
                                 <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                                 <li><a href="<?php echo $breadcrumb['href']; ?>" title="" itemprop="url"><span itemprop="title"><?php echo $breadcrumb['text']; ?></span></a>
                                    <span>&gt; </span>
                                 </li>
                                 <?php } ?>
                             </ul>
                          </div>
                         <h1><?php foreach ($breadcrumbs as $breadcrumb) { echo $breadcrumb['text']."&nbsp;" ; } ?></h1> 
                    </div>
                  <!--dev101-->
                  <div class="cms_block" style="margin-bottom:10px;">
                    <div class="experience-big col-sm-12">
                        <div class="img_left col-sm-6 col-xs-12">
                            <!--<img src="<?php //echo HTTP_IMAGE; ?>brochure-livre-pas-cher.png" class="image-encart" alt="" height="93" width="93">-->
                            
                            <?php if ($thumb) { ?>
                                <img src="<?php echo $thumb; ?>"  title="<?php echo $heading_title; ?>" class="image-exp-big img-responsive"  alt="<?php echo $heading_title; ?>" height="100%" width="90%">
                            <?php }
                            else{ ?>
                                <img src="<?php echo $default_img_category; ?>"  title="<?php echo $heading_title; ?>" class="image-exp-big img-responsive"  alt="<?php echo $heading_title; ?>" height="100%" width="90%">
                            <?php } ?>
                        </div>
                        <div class="right col-sm-3 col-xs-12">
                            <ul>
                                <li><h3><?php echo $heading_title; ?></h3></li>
                                <?php if ($description) { ?>
                                    <?php echo $description; // <li>?>
                                <?php } 
                                else{ ?>
                                    <?php echo "Aucune description"; // <li>?>
                              <?php } ?>
                            <li><img src="<?php echo HTTP_IMAGE; ?>vos-options-ci-dessous.png" style="float:right;margin-right:40px;" alt="Option produit brochures A6"></li>
                            </ul>
                        </div>
                    </div>
                    
                    <!--tableau-->
                    <?php if ( isset($toCategoriesNiveau1) && $toCategoriesNiveau1 != array() ) { ?>
                    <div class="category-inpage-navigation">
                        <div class="cat-content">
                            <div class="cat-listing">
                                <div class="nav-header">
                                    <div class="nav-header-left">
                                        <div class="nav-header-right">
                                            <span class="products-header-delimeter">&nbsp;&nbsp;&nbsp;</span>
                                            Notre gamme Brochure Livre                            </div>
                                    </div>
                                </div>
                                <div class="nav-main-content">
                                    <div class="nav-main-content-left-corner">
                                        <div class="nav-main-content-right-corner">
                                            <ul>
                                                <?php foreach( $toCategoriesNiveau1 as $oCategoriesNiveau1 ) { 
                                                    if($oCategoriesNiveau1['category_id'] == $child_id) {
                                                        echo '<li class="current" style="height:37px;"><a href="'.$oCategoriesNiveau1['href'].'" class="current">'.$oCategoriesNiveau1['name'].' </a></li>' ;
                                                        echo '<li class="separator current" style="height:37px;"></li>' ;
                                                    }
                                                    else{
                                                        echo '<li class="" style="height:37px;"><a href="'.$oCategoriesNiveau1['href'].'">'.$oCategoriesNiveau1['name'].' </a></li>' ;
                                                        echo '<li class="separator" style="height:37px;"></li>' ;
                                                    }
                                                    
                                                } ?>
                                            </ul>
                                            <div style="clear: both;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <?php if ( isset($toCategoriesNiveau2) && $toCategoriesNiveau2 != array() ) { ?>
                    <div class="category-inpage-navigation">
                        <div class="cat-content">
                            <div class="cat-listing">
                                <div class="nav-header">
                                    <div class="nav-header-left">
                                        <div class="nav-header-right">
                                            <span class="products-header-delimeter">&nbsp;&nbsp;&nbsp;</span>
                                            Notre gamme Brochure Livre                            </div>
                                    </div>
                                </div>
                                <div class="nav-main-content">
                                    <div class="nav-main-content-left-corner">
                                        <div class="nav-main-content-right-corner">
                                            <ul>
                                                <?php foreach( $toCategoriesNiveau2 as $oCategoriesNiveau2 ) { 
                                                    if($oCategoriesNiveau2['category_id'] == $child_id_1) {
                                                        echo '<li class="current" style="height:37px;"><a href="'.$oCategoriesNiveau2['href'].'" class="current">'.$oCategoriesNiveau2['name'].' </a></li>' ;
                                                        echo '<li class="separator current" style="height:37px;"></li>' ;
                                                    }
                                                    else{
                                                        echo '<li class="" style="height:37px;"><a href="'.$oCategoriesNiveau2['href'].'">'.$oCategoriesNiveau2['name'].' </a></li>' ;
                                                        echo '<li class="separator" style="height:37px;"></li>' ;
                                                    }
                                                    
                                                } ?>
                                            </ul>
                                            <div style="clear: both;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <?php if ( isset($toCategoriesNiveau3) && $toCategoriesNiveau3 != array() || $child_id_2 != 0 ) { 
                        $parts = explode('_', (string)$toCategoriesNiveau3[0]['href']);
                        $display = " " ;
                        if (isset($parts[3])) {
                            $display = ( $parts[3] == 0 ) ? "style='display:none'" : " " ;
                        }
                    ?>
                    <div class="category-inpage-navigation" <?php echo $display ;?>>
                        <div class="cat-content">
                            <div class="cat-listing">
                                <div class="nav-header">
                                    <div class="nav-header-left">
                                        <div class="nav-header-right">
                                            <span class="products-header-delimeter">&nbsp;&nbsp;&nbsp;</span>
                                            Notre gamme Brochure Livre                            </div>
                                    </div>
                                </div>
                                <div class="nav-main-content">
                                    <div class="nav-main-content-left-corner">
                                        <div class="nav-main-content-right-corner">
                                            <ul>
                                                <?php foreach( $toCategoriesNiveau3 as $oCategoriesNiveau3 ) { 
                                                    if($oCategoriesNiveau3['category_id'] == $child_id_2) {
                                                        echo '<li class="current" style="height:37px;"><a href="'.$oCategoriesNiveau3['href'].'" class="current">'.$oCategoriesNiveau3['name'].' </a></li>' ;
                                                        echo '<li class="separator current" style="height:37px;"></li>' ;
                                                    }
                                                    else{
                                                        echo '<li class="" style="height:37px;"><a href="'.$oCategoriesNiveau3['href'].'">'.$oCategoriesNiveau3['name'].' </a></li>' ;
                                                        echo '<li class="separator" style="height:37px;"></li>' ;
                                                    }
                                                    
                                                } ?>
                                            </ul>
                                            <div style="clear: both;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <?php if ( isset($toCategoriesNiveau4) && $toCategoriesNiveau4 != array() ) { ?>
                    <div class="category-inpage-navigation">
                        <div class="cat-content">
                            <div class="cat-listing">
                                <div class="nav-header">
                                    <div class="nav-header-left">
                                        <div class="nav-header-right">
                                            <span class="products-header-delimeter">&nbsp;&nbsp;&nbsp;</span>
                                            Notre gamme Brochure Livre                            </div>
                                    </div>
                                </div>
                                <div class="nav-main-content">
                                    <div class="nav-main-content-left-corner">
                                        <div class="nav-main-content-right-corner">
                                            <ul>
                                                <?php foreach( $toCategoriesNiveau4 as $oCategoriesNiveau4 ) { 
                                                    if($oCategoriesNiveau4['category_id'] == $child_id_3) {
                                                        echo '<li class="current" style="height:37px;"><a href="'.$oCategoriesNiveau4['href'].'" class="current">'.$oCategoriesNiveau4['name'].' </a></li>' ;
                                                        echo '<li class="separator current" style="height:37px;"></li>' ;
                                                    }
                                                    else{
                                                        echo '<li class="" style="height:37px;"><a href="'.$oCategoriesNiveau4['href'].'">'.$oCategoriesNiveau4['name'].' </a></li>' ;
                                                        echo '<li class="separator" style="height:37px;"></li>' ;
                                                    }
                                                    
                                                } ?>
                                            </ul>
                                            <div style="clear: both;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    
                    <?php 
                    if ( isset($toCategoriesNiveau5) && $toCategoriesNiveau5 != array() ) { ?>
                    <div class="category-inpage-navigation">
                        <div class="cat-content">
                            <div class="cat-listing">
                                <div class="nav-header">
                                    <div class="nav-header-left">
                                        <div class="nav-header-right">
                                            <span class="products-header-delimeter">&nbsp;&nbsp;&nbsp;</span>
                                            Notre gamme Brochure Livre                           </div>
                                    </div>
                                </div>
                                <div class="nav-main-content">
                                    <div class="nav-main-content-left-corner">
                                        <div class="nav-main-content-right-corner">
                                            <ul>
                                                <?php foreach( $toCategoriesNiveau5 as $oCategoriesNiveau5 ) { 
                                                    if($oCategoriesNiveau5['category_id'] == $child_id_4) {
                                                        echo '<li class="current" style="height:37px;"><a href="'.$oCategoriesNiveau5['href'].'" class="current">'.$oCategoriesNiveau5['name'].' </a></li>' ;
                                                        echo '<li class="separator current" style="height:37px;"></li>' ;
                                                    }
                                                    else{
                                                        echo '<li class="" style="height:37px;"><a href="'.$oCategoriesNiveau5['href'].'">'.$oCategoriesNiveau5['name'].' </a></li>' ;
                                                        echo '<li class="separator" style="height:37px;"></li>' ;
                                                    }
                                                    
                                                } ?>
                                            </ul>
                                            <div style="clear: both;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    
                    <?php if ( isset($toCategoriesNiveau6) && $toCategoriesNiveau6 != array() ) { ?>
                    <div class="category-inpage-navigation">
                        <div class="cat-content">
                            <div class="cat-listing">
                                <div class="nav-header">
                                    <div class="nav-header-left">
                                        <div class="nav-header-right">
                                            <span class="products-header-delimeter">&nbsp;&nbsp;&nbsp;</span>
                                            Notre gamme Brochure Livre                            </div>
                                    </div>
                                </div>
                                <div class="nav-main-content">
                                    <div class="nav-main-content-left-corner">
                                        <div class="nav-main-content-right-corner">
                                            <ul>
                                                <?php foreach( $toCategoriesNiveau6 as $oCategoriesNiveau6 ) { 
                                                    if($oCategoriesNiveau6['category_id'] == $child_id_5) {
                                                        echo '<li class="current" style="height:37px;"><a href="'.$oCategoriesNiveau6['href'].'" class="current">'.$oCategoriesNiveau6['name'].' </a></li>' ;
                                                        echo '<li class="separator current" style="height:37px;"></li>' ;
                                                    }
                                                    else{
                                                        echo '<li class="" style="height:37px;"><a href="'.$oCategoriesNiveau6['href'].'">'.$oCategoriesNiveau6['name'].' </a></li>' ;
                                                        echo '<li class="separator" style="height:37px;"></li>' ;
                                                    }
                                                    
                                                } ?>
                                            </ul>
                                            <div style="clear: both;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    
                    <?php if ( isset($toCategoriesNiveau7) && $toCategoriesNiveau7 != array() ) { ?>
                    <div class="category-inpage-navigation">
                        <div class="cat-content">
                            <div class="cat-listing">
                                <div class="nav-header">
                                    <div class="nav-header-left">
                                        <div class="nav-header-right">
                                            <span class="products-header-delimeter">&nbsp;&nbsp;&nbsp;</span>
                                            Notre gamme Brochure Livre                            </div>
                                    </div>
                                </div>
                                <div class="nav-main-content">
                                    <div class="nav-main-content-left-corner">
                                        <div class="nav-main-content-right-corner">
                                            <ul>
                                                <?php foreach( $toCategoriesNiveau7 as $oCategoriesNiveau7 ) { 
                                                    if($oCategoriesNiveau7['category_id'] == $child_id_6) {
                                                        echo '<li class="current" style="height:37px;"><a href="'.$oCategoriesNiveau7['href'].'" class="current">'.$oCategoriesNiveau7['name'].' </a></li>' ;
                                                        echo '<li class="separator current" style="height:37px;"></li>' ;
                                                    }
                                                    else{
                                                        echo '<li class="" style="height:37px;"><a href="'.$oCategoriesNiveau7['href'].'">'.$oCategoriesNiveau7['name'].' </a></li>' ;
                                                        echo '<li class="separator" style="height:37px;"></li>' ;
                                                    }
                                                    
                                                } ?>
                                            </ul>
                                            <div style="clear: both;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    
                    <?php if ( isset($toCategoriesNiveau8) && $toCategoriesNiveau8 != array() ) { ?>
                    <div class="category-inpage-navigation last">
                        <div class="cat-content">
                            <div class="cat-listing">
                                <div class="nav-header">
                                    <div class="nav-header-left">
                                        <div class="nav-header-right">
                                            <span class="products-header-delimeter">&nbsp;&nbsp;&nbsp;</span>
                                            Notre gamme Brochure Livre                            </div>
                                    </div>
                                </div>
                                <div class="nav-main-content">
                                    <div class="nav-main-content-left-corner">
                                        <div class="nav-main-content-right-corner">
                                            <ul>
                                                <?php foreach( $toCategoriesNiveau8 as $oCategoriesNiveau8 ) { 
                                                    if($oCategoriesNiveau8['category_id'] == $child_id_7) {
                                                        echo '<li class="current" style="height:37px;"><a href="'.$oCategoriesNiveau8['href'].'" class="current">'.$oCategoriesNiveau8['name'].' </a></li>' ;
                                                        echo '<li class="separator current" style="height:37px;"></li>' ;
                                                    }
                                                    else{
                                                        echo '<li class="" style="height:37px;"><a href="'.$oCategoriesNiveau8['href'].'">'.$oCategoriesNiveau8['name'].' </a></li>' ;
                                                        echo '<li class="separator" style="height:37px;"></li>' ;
                                                    }
                                                    
                                                } ?>
                                            </ul>
                                            <div style="clear: both;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    
                    <!--tableau-->
                    
                    <div class="clear:both;"></div>
                  </div>
                  <!--dev101-->
                         <?php if ($products) { ?>
                         <div class="category-products">
                                <h3 class="step-choose">Votre produit</h3>
                                <div class="tva-switcher" style="float:right;margin-right:5px;">
                                    <script type="text/javascript">
                                            $(document).ready(function(){
                                                $('.tvaswitch').change(function(){
                                                    var value = $(this).val();
                                                    if(value== 1){
                                                        $('.price-including-tax').hide();
                                                        $('.price-excluding-tax').fadeIn();

                                                    }else if(value == 2){
                                                        $('.price-excluding-tax').hide();
                                                        $('.price-including-tax').fadeIn();
                                                    }
                                                });
                                                $("input[name=tvaswitch][value=1]").attr('checked', 'checked');
                                            });
                                    </script>

                                    <label>Prix Ht</label>
                                    <input name="tvaswitch" class="tvaswitch" value="1" type="radio">
                                    <label>Prix TTC</label>
                                    <input name="tvaswitch" class="tvaswitch" value="2" type="radio">
                                </div>
                                <div class="products-containe">
                                        <ol class="products-list" id="products-list">
                                                <?php foreach ($products as $product) { ?>
                                                    <li class="item">
                                                        <div class="product-shop">
                                                            <div class="product-name">
                                                                <a href="<?php echo $product['href']; ?>" title="<?php echo $product['name']; ?>">
                                                                    <?php echo $product['name']; ?>
                                                                </a>
                                                                <?php if ($product['price']) { ?>
                                                                <div class="price-box">
                                                                    <?php if ($product['tax']) { ?>
                                                                         
                                                                           <span class="price-excluding-tax" style="display: block;">
                                                                                <span class="price" ><?php echo $product['tax']; ?></span><span class="label">HT</span>
                                                                           </span>
                                                                    <?php } ?>
                                                                     <?php if (!$product['special']) { ?>
                                                                        <span class="price-including-tax" style="display: none;">
                                                                            <span class="label" style="display:inline-block;"></span>
                                                                            <span class="price" > <?php echo $product['price']; ?></span><span class="label">TTC</span>
                                                                        </span>
                                                                      <?php } else { ?>
                                                                        <span class="price-including-tax" style="display: none;">
                                                                            <span class="label" style="display:inline-block;"></span>
                                                                            <span class="price" > <?php echo $product['special']; ?></span><span class="label">TTC</span>
                                                                        </span>
                                                                      <?php } ?>
                                                                <?php } ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                <?php }?> 
                                          </ol>
                                          <div class="list-produit-end"></div>
                                          <div class="list-category-description">
                                               <h2>Impression personnalis&eacute;e de brochures A6 20 pages </h2>
                                                <p><b>Le livre 100% personnalisable</b> imprim&eacute; sur papier 135g est le support de communication indispensable &agrave; la r&eacute;alisation de catalogue produit, carnet de voyage, livret sponsor, bulletin municipal... R&acute;alisez votre support de communication en haute d&acute;finition sur papier 135g couch&acute; brillant ou demi-mat (au choix), pour promouvoir votre activit&acute;, votre entreprise ou vos solutions.</p>
                                                <p>Impression offset quadri sur brochure format : A6 ferm&eacute; (10,5 x 15 cm) // A5 ouvert (15 x 21 cm) sens de lecture en portrait (&agrave; la Française). Livraison gratuite partout en France hors Corse en J+6 ouvr&acute;s. Possibilit&acute; de livrer en J+4.</p>
                                          </div>
                                    </div>
                            </div>
                            <?php } 
                           else { ?>
                           <p><?php echo $text_empty; ?></p>
                           <?php }?> 
                  
                </div>
                <?php echo $column_right; ?>
                </div>
            <?php } ?>
          <!-- AFFICHAGE EN TABLEAU DEUXIEME RANG CATEGORIE -->
   <?php }?>
  <!-- SI EXISTENCE D'AFFICHAGE DOSSIER -->
  <?php echo $content_bottom; ?>
</div>

<?php echo $footer; ?>
