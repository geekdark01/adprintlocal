<?php echo $header; ?>

<style>

.div_image{
	float: left;
    width: 100%;
    text-align: center;
	margin: 10px;
}

.text-center{
	padding-left: 10px;
    margin-bottom: 12px;
    color: #18b4ea;
    font-size: 16px;
    font-weight: bold;
    clear: both;
	text-align: center!important;
}
   
.text-justify{
	width: 80%;
    margin: 0 auto;
}   
   
</style>

<div class="container content_container">
  <!-- SI EXISTENCE D'AFFICHAGE DOSSIER -->
  <?php if($affichageDossier > 0) { ?>
                <div class="page-title-category col-sm-12">
                    <br/>
                    <div class="breadcrumbs">
						
						
					
						<ul>
                             <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                             <li><a href="<?php echo $breadcrumb['href']; ?>" title="" itemprop="url"><span itemprop="title"><?php echo $breadcrumb['text']; ?></span></a>
                                <span>&gt; </span>
                             </li>
                             <?php } ?>
                         </ul>
                      </div>
                     <h1><?php foreach ($breadcrumbs as $breadcrumb) { echo $breadcrumb['text']."&nbsp;" ; } ?></h1> 
                </div>
                
                <div class="container">
				
				
				<div class="div_image">
					<img src="http://adprint-dev.com/image/accueil_menu/marquage_veh_1.jpg" >
				</div>
				
                 <?php foreach ($category_info_dossier as $oCategoryDossier) { ?>
                    <div class="product-layout col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="product-thumb transition">
						
						
                            <div class="image"><img src="http://adprint-dev.com/image/accueil_menu/carte_x.jpg" alt="<?php echo $oCategoryDossier['nom_dossier']; ?>" title="<?php echo $oCategoryDossier['nom_dossier']; ?>" class="img-responsive" /></div>
                            <h3 class="cat_name"><?php echo $oCategoryDossier['nom_dossier']; ?></h3>
                            <?php foreach ($oCategoryDossier['fils'] as $oCategoryFils) { ?>
                                <a href="<?php echo $oCategoryFils['href']; ?>" title="" /><p>> <?php echo $oCategoryFils['name']; ?></p></a>
                            <?php } ?>
                        </div>
                    </div>
                    <?php } ?>
                </div>
				
				
				
				<div class="div_image">
					<img src="http://adprint-dev.com/image/accueil_menu/flyers_1.png" >
					
				</div>
									
					<div class="text text-justify">

					<h2 class="text-center">Flyers professionnels personnalis�s  : le moins cher </h2><br>

					<p>Besoin de flyer pour communiquer ? Votre <b>imprimerie en ligne</b> vous propose une large gamme de <b>flyers personnalis�s</b> pour tous vos besoins en communication. Optimisez la visibilit� de votre entreprise ou de vos �v�nements gr�ce � l�<b>impression de flyer pas cher</b>.</p>

					<h3>Impression flyer professionnel : le support de communication indispensable pour votre entreprise</h3>

					<p>L�<b>impression de flyer professionnel</b> est le <b>support de communication</b> indispensable pour votre entreprise. Faites imprimer des <b>flyers personnalis�s</b> pour communiquer sur votre entreprise, sur vos produits ou sur vos services propos�s au sein de votre entreprise.</p>
					<p>Utilisez vos <b>flyers personnalis�s</b> � l�image de votre entreprise lors d�un <b>salon</b>, une <b>foire</b> ou une <b>exposition</b>. Vous pourrez distribuer vos <b>flyers</b> sur votre <a title="communication stand" href="/plv-expo-stand.html">stand</a> lors d�un <b>salon</b> ou les laisser en livre service sur un <a title="comptoir d'accueil personnalis�" href="/plv-expo-stand/stands-comptoirs/comptoir-accueil.html">comptoir d�accueil personnalis�</a> ou sur un <a title="porte document flyer" href="/plv-expo-stand/presentoirs.html">porte document</a>.</p>

					<p>Le <b>flyer</b> est un <b>support de communication de masse</b>, il sera essentiel lors de vos <b>op�rations de communication</b> comme le <b>street marketing</b>, la <b>distribution de flyers</b>, l�<b>animation commerciale en magasin</b>, la distribution d�<a title="objets publicitaires personnalis�s" href="/objet-publicitaire.html">objets publicitaires personnalis�s</a>... Le <b>flyer</b> permettra de communiquer aupr�s d�un large public et permettra d�informer vos prospects sur votre entreprise.</p> 

					<h3>Impression flyer �v�nementiel : le support de communication indispensable pour vos �v�nements</h3>

					<p>L�<b>impression de flyer �v�nementiel</b> est indispensable pour communiquer sur vos <b>�v�nements</b> (<b>flyer pour �v�nement professionnel</b>, <b>flyer pour �v�nement culturel</b>, <b>flyer pour �v�nement caritatif</b>�).</p>
					<p>Le <b>flyer �v�nementiel</b> permettra de faire conna�tre votre �v�nement et d�attirer un grand nombre de personnes � participer.</p>
					<p><b>Imprimez le flyer</b> � l�image de votre �v�nement gr�ce � notre <b>imprimerie en ligne</b>. Le <b>flyer</b> est un <b>support de communication dynamique</b> qui permet d�<b>optimiser la visibilit� de votre �v�nement</b>.</p>

					<h3>Impression flyer standard � bas prix</h3>

					<p>Profitez de l�<b>impression de flyer</b> standard � <b>bas prix</b> chez Easyflyer, l�<b>imprimerie en ligne</b>. Le <b>flyer pas cher</b> a un <b>tr�s bon rapport quantit� / qualit� / prix</b>.</p> 
					<p>Selon vos besoins, de <b>nombreux formats</b> sont disponibles sur notre site d�<b>imprimerie en ligne</b>. Le standard du march� est le <b>flyer A5</b>, impression recto ou recto verso. Le <b>flyer A5</b> est souvent distribu� lors d�<b>op�rations commerciales</b>. Retrouvez aussi d�autres formats de <b>flyers</b> : <a title="impression flyer A4" href="/flyers/standard/21-x-29-7-cm-a4.html">flyer A4</a>, flyer A6 en format carte postale.
					Selon l�utilisation du flyer, vous avez le <b>choix sur le grammage du papier</b> : flyer 80g � flyer 400g et choisissez un <b>flyer avec pelliculage</b> ou un <b>flyer sans pelliculage</b>.</p> 


					<h3>Impression flyer original</h3>

					<p>Pour vous d�marquer de vos concurrents et marquer l�esprit de vos clients, Easyflyer, l�<b>imprimerie en ligne</b> vous propose le <b>flyer original</b>. Une large gamme de <b>flyers originaux</b> sont � votre disposition pour laisser libre cours � votre imagination.</p> 
					<p>Le <b>flyer vernis s�lectif</b> a un <b>rendu tr�s qualitatif</b>, il permettra de faire ressortir des zones d�impression et des d�tails qui feront de votre flyer, un <a title="flyer haut de gamme" href="/flyers/haut-de-gamme.html">flyer haut de gamme</a>. Le <b>flyer vernis s�lectif</b> est tr�s utilis� pour donner une <b>image qualitative</b>. Vous pouvez utiliser le <b>flyer vernis s�lectif</b> comme <a title="impression carton d'invitation personnalis�" href="/flyers/standard/10-x-21-cm-carte-de-correspondance/300g.html">carton d�invitation</a> pour une <b>soir�e d�entreprise</b>, <b>soir�e priv�e</b>, <b>soir�e VIP</b>.</p>
					<p>Pour un rendu prestige, plusieurs mod�les de <b>flyers haut de gamme</b> sont disponibles sur notre site d�<b>imprimerie en ligne</b> comme le <b>flyer � vernis paillette</b>, le <b>flyer avec dorure</b> ou le <b>flyer textur�</b>.</p> 

					<p>Vous n�avez pas envie d�un <a title="impression flyer carr�" href="/flyers/standard/15-x-15-cm.html">flyer carr�</a> ou d'un <b>flyer rectangle</b> ? Choisissez le <b>flyer � la d�coupe personnalis�e</b> pour un rendu original. Retrouvez des <b>flyers ronds</b>, des <b>flyers en forme de coeur</b> pour communiquer � la <b>Saint Valentin</b> et le <b>flyer � accrocher aux portes</b>.</p>

					<p>Vous �tes attentif � l�environnement et vous voulez donner une <b>image �co-responsable</b> � votre entreprise ? Choisissez le <b>flyer �cologique en papier recycl�</b>.</p> 

					<h3>Pourquoi commander l�impression de flyer chez Easyflyer, l'imprimerie en ligne fran�aise?</h3>

					<p>Commandez les <b>flyers les moins chers</b> de France chez Easyflyer, l�<b>imprimerie en ligne fran�aise</b>. B�n�ficiez de la <b>livraison gratuite de flyers professionnels</b> ainsi que de la <b>livraison rapide de flyer</b>. Profitez aussi du 100% <b>satisfait ou rembours� sans condition</b>.</p>


					</div>				
				
                
  <?php }else{ ?>
  <!-- AFFICHAGE IMAGE -->
  <?php if($affichage_tableau == 0) { ?>
                <div class="page-title-category col-sm-12">
                    <br/>
                    <div class="breadcrumbs">
					
					<!--
					<img src="http://adprint-dev.com/image/bandeau-categorie-carte-fidelite-bt-nouveau.jpg" >
					-->
                         <ul>
                             <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                             <li><a href="<?php echo $breadcrumb['href']; ?>" title="" itemprop="url"><span itemprop="title"><?php echo $breadcrumb['text']; ?></span></a>
                                <span>&gt; </span>
                             </li>
                             <?php } ?>
                         </ul>
                      </div>
                     <h1><?php foreach ($breadcrumbs as $breadcrumb) { echo $breadcrumb['text']."&nbsp;" ; } ?></h1> 
                </div>
                
				
				
				<div class="div_image">
					<img src="http://adprint-dev.com/image/accueil_menu/carte_de_visite_accueil.jpg" >
				</div>
				
                <div class="container-int">
                    <?php 
                    if(isset($categories) && $categories != null){
                        foreach ($categories as $category) { ?>
                        <div class="product-layout col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <div class="product-thumb transition">
                                    <div class="image"><a href="<?php echo $category['href']; ?>"><img src="<?php echo $category['thumb']; ?>" alt="<?php echo $category['name']; ?>" title="<?php echo $category['name']; ?>" class="img-responsive" /></a></div>
                                    <h3 class="cat_name"><?php echo $category['name']; ?></h3>
                                    <?php  
                                        $prix_minimum = ($category['prix_minimum'] != '$0.00' ) ? "<p class='cat_price'><span>D&egrave;s </span>".$category['prix_minimum']." HT</p>" : "<p class='cat_price'><span>&nbsp;</span>&nbsp;</p>" ;
                                        echo $prix_minimum ;
                                    ?> 
                                </div>
                        </div>
                    <?php } 
					
					?>
					
					
					<div class="text text-justify">

					<h2 class="text-center">Marquage v�hicule personnalis�s  : le moins cher </h2><br>

					<p>Besoin de flyer pour communiquer ? Votre <b>imprimerie en ligne</b> vous propose une large gamme de <b>flyers personnalis�s</b> pour tous vos besoins en communication. Optimisez la visibilit� de votre entreprise ou de vos �v�nements gr�ce � l�<b>impression de flyer pas cher</b>.</p>

					<h3>Impression flyer professionnel : le support de communication indispensable pour votre entreprise</h3>

					<p>L�<b>impression de flyer professionnel</b> est le <b>support de communication</b> indispensable pour votre entreprise. Faites imprimer des <b>flyers personnalis�s</b> pour communiquer sur votre entreprise, sur vos produits ou sur vos services propos�s au sein de votre entreprise.</p>
					<p>Utilisez vos <b>flyers personnalis�s</b> � l�image de votre entreprise lors d�un <b>salon</b>, une <b>foire</b> ou une <b>exposition</b>. Vous pourrez distribuer vos <b>flyers</b> sur votre <a title="communication stand" href="/plv-expo-stand.html">stand</a> lors d�un <b>salon</b> ou les laisser en livre service sur un <a title="comptoir d'accueil personnalis�" href="/plv-expo-stand/stands-comptoirs/comptoir-accueil.html">comptoir d�accueil personnalis�</a> ou sur un <a title="porte document flyer" href="/plv-expo-stand/presentoirs.html">porte document</a>.</p>

					<p>Le <b>flyer</b> est un <b>support de communication de masse</b>, il sera essentiel lors de vos <b>op�rations de communication</b> comme le <b>street marketing</b>, la <b>distribution de flyers</b>, l�<b>animation commerciale en magasin</b>, la distribution d�<a title="objets publicitaires personnalis�s" href="/objet-publicitaire.html">objets publicitaires personnalis�s</a>... Le <b>flyer</b> permettra de communiquer aupr�s d�un large public et permettra d�informer vos prospects sur votre entreprise.</p> 

					<h3>Impression flyer �v�nementiel : le support de communication indispensable pour vos �v�nements</h3>

					<p>L�<b>impression de flyer �v�nementiel</b> est indispensable pour communiquer sur vos <b>�v�nements</b> (<b>flyer pour �v�nement professionnel</b>, <b>flyer pour �v�nement culturel</b>, <b>flyer pour �v�nement caritatif</b>�).</p>
					<p>Le <b>flyer �v�nementiel</b> permettra de faire conna�tre votre �v�nement et d�attirer un grand nombre de personnes � participer.</p>
					<p><b>Imprimez le flyer</b> � l�image de votre �v�nement gr�ce � notre <b>imprimerie en ligne</b>. Le <b>flyer</b> est un <b>support de communication dynamique</b> qui permet d�<b>optimiser la visibilit� de votre �v�nement</b>.</p>

					<h3>Impression flyer standard � bas prix</h3>

					<p>Profitez de l�<b>impression de flyer</b> standard � <b>bas prix</b> chez Easyflyer, l�<b>imprimerie en ligne</b>. Le <b>flyer pas cher</b> a un <b>tr�s bon rapport quantit� / qualit� / prix</b>.</p> 
					<p>Selon vos besoins, de <b>nombreux formats</b> sont disponibles sur notre site d�<b>imprimerie en ligne</b>. Le standard du march� est le <b>flyer A5</b>, impression recto ou recto verso. Le <b>flyer A5</b> est souvent distribu� lors d�<b>op�rations commerciales</b>. Retrouvez aussi d�autres formats de <b>flyers</b> : <a title="impression flyer A4" href="/flyers/standard/21-x-29-7-cm-a4.html">flyer A4</a>, flyer A6 en format carte postale.
					Selon l�utilisation du flyer, vous avez le <b>choix sur le grammage du papier</b> : flyer 80g � flyer 400g et choisissez un <b>flyer avec pelliculage</b> ou un <b>flyer sans pelliculage</b>.</p> 


					<h3>Impression flyer original</h3>

					<p>Pour vous d�marquer de vos concurrents et marquer l�esprit de vos clients, Easyflyer, l�<b>imprimerie en ligne</b> vous propose le <b>flyer original</b>. Une large gamme de <b>flyers originaux</b> sont � votre disposition pour laisser libre cours � votre imagination.</p> 
					<p>Le <b>flyer vernis s�lectif</b> a un <b>rendu tr�s qualitatif</b>, il permettra de faire ressortir des zones d�impression et des d�tails qui feront de votre flyer, un <a title="flyer haut de gamme" href="/flyers/haut-de-gamme.html">flyer haut de gamme</a>. Le <b>flyer vernis s�lectif</b> est tr�s utilis� pour donner une <b>image qualitative</b>. Vous pouvez utiliser le <b>flyer vernis s�lectif</b> comme <a title="impression carton d'invitation personnalis�" href="/flyers/standard/10-x-21-cm-carte-de-correspondance/300g.html">carton d�invitation</a> pour une <b>soir�e d�entreprise</b>, <b>soir�e priv�e</b>, <b>soir�e VIP</b>.</p>
					<p>Pour un rendu prestige, plusieurs mod�les de <b>flyers haut de gamme</b> sont disponibles sur notre site d�<b>imprimerie en ligne</b> comme le <b>flyer � vernis paillette</b>, le <b>flyer avec dorure</b> ou le <b>flyer textur�</b>.</p> 

					<p>Vous n�avez pas envie d�un <a title="impression flyer carr�" href="/flyers/standard/15-x-15-cm.html">flyer carr�</a> ou d'un <b>flyer rectangle</b> ? Choisissez le <b>flyer � la d�coupe personnalis�e</b> pour un rendu original. Retrouvez des <b>flyers ronds</b>, des <b>flyers en forme de coeur</b> pour communiquer � la <b>Saint Valentin</b> et le <b>flyer � accrocher aux portes</b>.</p>

					<p>Vous �tes attentif � l�environnement et vous voulez donner une <b>image �co-responsable</b> � votre entreprise ? Choisissez le <b>flyer �cologique en papier recycl�</b>.</p> 

					<h3>Pourquoi commander l�impression de flyer chez Easyflyer, l'imprimerie en ligne fran�aise?</h3>

					<p>Commandez les <b>flyers les moins chers</b> de France chez Easyflyer, l�<b>imprimerie en ligne fran�aise</b>. B�n�ficiez de la <b>livraison gratuite de flyers professionnels</b> ainsi que de la <b>livraison rapide de flyer</b>. Profitez aussi du 100% <b>satisfait ou rembours� sans condition</b>.</p>


					</div>	
					
					
					
					
					<?php
					
                    }else
                    { ?>
                        <!-- AFFICHAGE DU PRODUIT -->
                        <?php if ($products) { ?>
                         <div class="category-products">
                                <h3 class="step-choose">Votre produit</h3>
                                <div class="tva-switcher" style="float:right;margin-right:5px;">
                                    <script type="text/javascript">
                                            $(document).ready(function(){
                                                $('.tvaswitch').change(function(){
                                                    var value = $(this).val();
                                                    if(value== 1){
                                                        $('.price-including-tax').hide();
                                                        $('.price-excluding-tax').fadeIn();

                                                    }else if(value == 2){
                                                        $('.price-excluding-tax').hide();
                                                        $('.price-including-tax').fadeIn();
                                                    }
                                                });
                                                $("input[name=tvaswitch][value=1]").attr('checked', 'checked');
                                            });
                                    </script>

                                    <label>Prix Ht</label>
                                    <input name="tvaswitch" class="tvaswitch" value="1" type="radio">
                                    <label>Prix TTC</label>
                                    <input name="tvaswitch" class="tvaswitch" value="2" type="radio">
                                </div>
                                <div class="products-containe">
                                        <ol class="products-list" id="products-list">
                                                <?php foreach ($products as $product) { ?>
                                                    <li class="item">
                                                        <div class="product-shop">
                                                            <div class="product-name">
                                                                <a href="<?php echo $product['href']; ?>" title="<?php echo $product['name']; ?>">
                                                                    <?php echo $product['name']; ?>
                                                                </a>
                                                                <?php if ($product['price']) { ?>
                                                                <div class="price-box">
                                                                    <?php if ($product['tax']) { ?>
                                                                         
                                                                           <span class="price-excluding-tax" style="display: block;">
                                                                                <span class="price" ><?php echo $product['tax']; ?></span><span class="label">HT</span>
                                                                           </span>
                                                                    <?php } ?>
                                                                     <?php if (!$product['special']) { ?>
                                                                        <span class="price-including-tax" style="display: none;">
                                                                            <span class="label" style="display:inline-block;"></span>
                                                                            <span class="price" > <?php echo $product['price']; ?></span><span class="label">TTC</span>
                                                                        </span>
                                                                      <?php } else { ?>
                                                                        <span class="price-including-tax" style="display: none;">
                                                                            <span class="label" style="display:inline-block;"></span>
                                                                            <span class="price" > <?php echo $product['special']; ?></span><span class="label">TTC</span>
                                                                        </span>
                                                                      <?php } ?>
                                                                <?php } ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                <?php }?> 
                                          </ol>
                                          <div class="list-produit-end"></div>
                                    </div>
                            </div>
                            <?php } 
                           else { ?>
                           <p><?php echo $text_empty; ?></p>
                           <?php }
                    }
                    ?>
                    <!-- AFFICHAGE DU PRODUIT -->
                </div>
          <?php } ?>
          <!-- AFFICHAGE EN TABLEAU DEUXIEME RANG CATEGORIE -->
          <?php if($affichage_tableau == 1) { ?>
              <div class="row"><?php echo $column_left; ?>
                <?php if ($column_left && $column_right) { ?>
                <?php $class = 'col-sm-6'; ?>
                <?php } elseif ($column_left || $column_right) { ?>
                <?php $class = 'col-sm-9'; ?>
                <?php } else { ?>
                <?php $class = 'col-sm-12'; ?>
                <?php } ?>
                <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
                    <div class="page-title-category col-sm-12">
                        <div class="breadcrumbs">
                             <ul>
                                 <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                                 <li><a href="<?php echo $breadcrumb['href']; ?>" title="" itemprop="url"><span itemprop="title"><?php echo $breadcrumb['text']; ?></span></a>
                                    <span>&gt; </span>
                                 </li>
                                 <?php } ?>
                             </ul>
                          </div>
                         <h1><?php foreach ($breadcrumbs as $breadcrumb) { echo $breadcrumb['text']."&nbsp;" ; } ?></h1> 
                    </div>
                  <!--dev101-->
                  <div class="cms_block" style="margin-bottom:10px;">
                    <div class="experience-big col-sm-12">
                        <div class="img_left col-sm-6 col-xs-12">
                            <!--<img src="<?php //echo HTTP_IMAGE; ?>brochure-livre-pas-cher.png" class="image-encart" alt="" height="93" width="93">-->
                            
                            <?php if ($thumb) { ?>
                                <img src="<?php echo $thumb; ?>"  title="<?php echo $heading_title; ?>" class="image-exp-big img-responsive"  alt="<?php echo $heading_title; ?>" height="100%" width="90%">
                            <?php }
                            else{ ?>
                                <img src="<?php echo $default_img_category; ?>"  title="<?php echo $heading_title; ?>" class="image-exp-big img-responsive"  alt="<?php echo $heading_title; ?>" height="100%" width="90%">
                            <?php } ?>
                        </div>
                        <div class="right col-sm-3 col-xs-12">
                            <ul>
                                <li><h3><?php echo $heading_title; ?></h3></li>
                                <?php if ($description) { ?>
                                    <?php echo $description; // <li>?>
                                <?php } 
                                else{ ?>
                                    <?php echo "Aucune description"; // <li>?>
                              <?php } ?>
                            <li><img src="<?php echo HTTP_IMAGE; ?>vos-options-ci-dessous.png" style="float:right;margin-right:40px;" alt="Option produit brochures A6"></li>
                            </ul>
                        </div>
                    </div>
                    
                    <!--tableau-->
                    <?php if ( isset($toCategoriesNiveau1) && $toCategoriesNiveau1 != array() ) { ?>
                    <div class="category-inpage-navigation">
                        <div class="cat-content">
                            <div class="cat-listing">
                                <div class="nav-header">
                                    <div class="nav-header-left">
                                        <div class="nav-header-right">
                                            <span class="products-header-delimeter">&nbsp;&nbsp;&nbsp;</span>
                                            Notre gamme Brochure Livre                            </div>
                                    </div>
                                </div>
                                <div class="nav-main-content">
                                    <div class="nav-main-content-left-corner">
                                        <div class="nav-main-content-right-corner">
                                            <ul>
                                                <?php foreach( $toCategoriesNiveau1 as $oCategoriesNiveau1 ) { 
                                                    if($oCategoriesNiveau1['category_id'] == $child_id) {
                                                        echo '<li class="current" style="height:37px;"><a href="'.$oCategoriesNiveau1['href'].'" class="current">'.$oCategoriesNiveau1['name'].' </a></li>' ;
                                                        echo '<li class="separator current" style="height:37px;"></li>' ;
                                                    }
                                                    else{
                                                        echo '<li class="" style="height:37px;"><a href="'.$oCategoriesNiveau1['href'].'">'.$oCategoriesNiveau1['name'].' </a></li>' ;
                                                        echo '<li class="separator" style="height:37px;"></li>' ;
                                                    }
                                                    
                                                } ?>
                                            </ul>
                                            <div style="clear: both;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <?php if ( isset($toCategoriesNiveau2) && $toCategoriesNiveau2 != array() ) { ?>
                    <div class="category-inpage-navigation">
                        <div class="cat-content">
                            <div class="cat-listing">
                                <div class="nav-header">
                                    <div class="nav-header-left">
                                        <div class="nav-header-right">
                                            <span class="products-header-delimeter">&nbsp;&nbsp;&nbsp;</span>
                                            Notre gamme Brochure Livre                            </div>
                                    </div>
                                </div>
                                <div class="nav-main-content">
                                    <div class="nav-main-content-left-corner">
                                        <div class="nav-main-content-right-corner">
                                            <ul>
                                                <?php foreach( $toCategoriesNiveau2 as $oCategoriesNiveau2 ) { 
                                                    if($oCategoriesNiveau2['category_id'] == $child_id_1) {
                                                        echo '<li class="current" style="height:37px;"><a href="'.$oCategoriesNiveau2['href'].'" class="current">'.$oCategoriesNiveau2['name'].' </a></li>' ;
                                                        echo '<li class="separator current" style="height:37px;"></li>' ;
                                                    }
                                                    else{
                                                        echo '<li class="" style="height:37px;"><a href="'.$oCategoriesNiveau2['href'].'">'.$oCategoriesNiveau2['name'].' </a></li>' ;
                                                        echo '<li class="separator" style="height:37px;"></li>' ;
                                                    }
                                                    
                                                } ?>
                                            </ul>
                                            <div style="clear: both;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <?php if ( isset($toCategoriesNiveau3) && $toCategoriesNiveau3 != array() || $child_id_2 != 0 ) { 
                        $parts = explode('_', (string)$toCategoriesNiveau3[0]['href']);
                        $display = " " ;
                        if (isset($parts[3])) {
                            $display = ( $parts[3] == 0 ) ? "style='display:none'" : " " ;
                        }
                    ?>
                    <div class="category-inpage-navigation" <?php echo $display ;?>>
                        <div class="cat-content">
                            <div class="cat-listing">
                                <div class="nav-header">
                                    <div class="nav-header-left">
                                        <div class="nav-header-right">
                                            <span class="products-header-delimeter">&nbsp;&nbsp;&nbsp;</span>
                                            Notre gamme Brochure Livre                            </div>
                                    </div>
                                </div>
                                <div class="nav-main-content">
                                    <div class="nav-main-content-left-corner">
                                        <div class="nav-main-content-right-corner">
                                            <ul>
                                                <?php foreach( $toCategoriesNiveau3 as $oCategoriesNiveau3 ) { 
                                                    if($oCategoriesNiveau3['category_id'] == $child_id_2) {
                                                        echo '<li class="current" style="height:37px;"><a href="'.$oCategoriesNiveau3['href'].'" class="current">'.$oCategoriesNiveau3['name'].' </a></li>' ;
                                                        echo '<li class="separator current" style="height:37px;"></li>' ;
                                                    }
                                                    else{
                                                        echo '<li class="" style="height:37px;"><a href="'.$oCategoriesNiveau3['href'].'">'.$oCategoriesNiveau3['name'].' </a></li>' ;
                                                        echo '<li class="separator" style="height:37px;"></li>' ;
                                                    }
                                                    
                                                } ?>
                                            </ul>
                                            <div style="clear: both;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <?php if ( isset($toCategoriesNiveau4) && $toCategoriesNiveau4 != array() ) { ?>
                    <div class="category-inpage-navigation">
                        <div class="cat-content">
                            <div class="cat-listing">
                                <div class="nav-header">
                                    <div class="nav-header-left">
                                        <div class="nav-header-right">
                                            <span class="products-header-delimeter">&nbsp;&nbsp;&nbsp;</span>
                                            Notre gamme Brochure Livre                            </div>
                                    </div>
                                </div>
                                <div class="nav-main-content">
                                    <div class="nav-main-content-left-corner">
                                        <div class="nav-main-content-right-corner">
                                            <ul>
                                                <?php foreach( $toCategoriesNiveau4 as $oCategoriesNiveau4 ) { 
                                                    if($oCategoriesNiveau4['category_id'] == $child_id_3) {
                                                        echo '<li class="current" style="height:37px;"><a href="'.$oCategoriesNiveau4['href'].'" class="current">'.$oCategoriesNiveau4['name'].' </a></li>' ;
                                                        echo '<li class="separator current" style="height:37px;"></li>' ;
                                                    }
                                                    else{
                                                        echo '<li class="" style="height:37px;"><a href="'.$oCategoriesNiveau4['href'].'">'.$oCategoriesNiveau4['name'].' </a></li>' ;
                                                        echo '<li class="separator" style="height:37px;"></li>' ;
                                                    }
                                                    
                                                } ?>
                                            </ul>
                                            <div style="clear: both;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    
                    <?php 
                    if ( isset($toCategoriesNiveau5) && $toCategoriesNiveau5 != array() ) { ?>
                    <div class="category-inpage-navigation">
                        <div class="cat-content">
                            <div class="cat-listing">
                                <div class="nav-header">
                                    <div class="nav-header-left">
                                        <div class="nav-header-right">
                                            <span class="products-header-delimeter">&nbsp;&nbsp;&nbsp;</span>
                                            Notre gamme Brochure Livre                           </div>
                                    </div>
                                </div>
                                <div class="nav-main-content">
                                    <div class="nav-main-content-left-corner">
                                        <div class="nav-main-content-right-corner">
                                            <ul>
                                                <?php foreach( $toCategoriesNiveau5 as $oCategoriesNiveau5 ) { 
                                                    if($oCategoriesNiveau5['category_id'] == $child_id_4) {
                                                        echo '<li class="current" style="height:37px;"><a href="'.$oCategoriesNiveau5['href'].'" class="current">'.$oCategoriesNiveau5['name'].' </a></li>' ;
                                                        echo '<li class="separator current" style="height:37px;"></li>' ;
                                                    }
                                                    else{
                                                        echo '<li class="" style="height:37px;"><a href="'.$oCategoriesNiveau5['href'].'">'.$oCategoriesNiveau5['name'].' </a></li>' ;
                                                        echo '<li class="separator" style="height:37px;"></li>' ;
                                                    }
                                                    
                                                } ?>
                                            </ul>
                                            <div style="clear: both;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    
                    <?php if ( isset($toCategoriesNiveau6) && $toCategoriesNiveau6 != array() ) { ?>
                    <div class="category-inpage-navigation">
                        <div class="cat-content">
                            <div class="cat-listing">
                                <div class="nav-header">
                                    <div class="nav-header-left">
                                        <div class="nav-header-right">
                                            <span class="products-header-delimeter">&nbsp;&nbsp;&nbsp;</span>
                                            Notre gamme Brochure Livre                            </div>
                                    </div>
                                </div>
                                <div class="nav-main-content">
                                    <div class="nav-main-content-left-corner">
                                        <div class="nav-main-content-right-corner">
                                            <ul>
                                                <?php foreach( $toCategoriesNiveau6 as $oCategoriesNiveau6 ) { 
                                                    if($oCategoriesNiveau6['category_id'] == $child_id_5) {
                                                        echo '<li class="current" style="height:37px;"><a href="'.$oCategoriesNiveau6['href'].'" class="current">'.$oCategoriesNiveau6['name'].' </a></li>' ;
                                                        echo '<li class="separator current" style="height:37px;"></li>' ;
                                                    }
                                                    else{
                                                        echo '<li class="" style="height:37px;"><a href="'.$oCategoriesNiveau6['href'].'">'.$oCategoriesNiveau6['name'].' </a></li>' ;
                                                        echo '<li class="separator" style="height:37px;"></li>' ;
                                                    }
                                                    
                                                } ?>
                                            </ul>
                                            <div style="clear: both;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    
                    <?php if ( isset($toCategoriesNiveau7) && $toCategoriesNiveau7 != array() ) { ?>
                    <div class="category-inpage-navigation">
                        <div class="cat-content">
                            <div class="cat-listing">
                                <div class="nav-header">
                                    <div class="nav-header-left">
                                        <div class="nav-header-right">
                                            <span class="products-header-delimeter">&nbsp;&nbsp;&nbsp;</span>
                                            Notre gamme Brochure Livre                            </div>
                                    </div>
                                </div>
                                <div class="nav-main-content">
                                    <div class="nav-main-content-left-corner">
                                        <div class="nav-main-content-right-corner">
                                            <ul>
                                                <?php foreach( $toCategoriesNiveau7 as $oCategoriesNiveau7 ) { 
                                                    if($oCategoriesNiveau7['category_id'] == $child_id_6) {
                                                        echo '<li class="current" style="height:37px;"><a href="'.$oCategoriesNiveau7['href'].'" class="current">'.$oCategoriesNiveau7['name'].' </a></li>' ;
                                                        echo '<li class="separator current" style="height:37px;"></li>' ;
                                                    }
                                                    else{
                                                        echo '<li class="" style="height:37px;"><a href="'.$oCategoriesNiveau7['href'].'">'.$oCategoriesNiveau7['name'].' </a></li>' ;
                                                        echo '<li class="separator" style="height:37px;"></li>' ;
                                                    }
                                                    
                                                } ?>
                                            </ul>
                                            <div style="clear: both;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    
                    <?php if ( isset($toCategoriesNiveau8) && $toCategoriesNiveau8 != array() ) { ?>
                    <div class="category-inpage-navigation last">
                        <div class="cat-content">
                            <div class="cat-listing">
                                <div class="nav-header">
                                    <div class="nav-header-left">
                                        <div class="nav-header-right">
                                            <span class="products-header-delimeter">&nbsp;&nbsp;&nbsp;</span>
                                            Notre gamme Brochure Livre                            </div>
                                    </div>
                                </div>
                                <div class="nav-main-content">
                                    <div class="nav-main-content-left-corner">
                                        <div class="nav-main-content-right-corner">
                                            <ul>
                                                <?php foreach( $toCategoriesNiveau8 as $oCategoriesNiveau8 ) { 
                                                    if($oCategoriesNiveau8['category_id'] == $child_id_7) {
                                                        echo '<li class="current" style="height:37px;"><a href="'.$oCategoriesNiveau8['href'].'" class="current">'.$oCategoriesNiveau8['name'].' </a></li>' ;
                                                        echo '<li class="separator current" style="height:37px;"></li>' ;
                                                    }
                                                    else{
                                                        echo '<li class="" style="height:37px;"><a href="'.$oCategoriesNiveau8['href'].'">'.$oCategoriesNiveau8['name'].' </a></li>' ;
                                                        echo '<li class="separator" style="height:37px;"></li>' ;
                                                    }
                                                    
                                                } ?>
                                            </ul>
                                            <div style="clear: both;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    
                    <!--tableau-->
                    
                    <div class="clear:both;"></div>
                  </div>
                  <!--dev101-->
                         <?php if ($products) { ?>
                         <div class="category-products">
                                <h3 class="step-choose">Votre produit</h3>
                                <div class="tva-switcher" style="float:right;margin-right:5px;">
                                    <script type="text/javascript">
                                            $(document).ready(function(){
                                                $('.tvaswitch').change(function(){
                                                    var value = $(this).val();
                                                    if(value== 1){
                                                        $('.price-including-tax').hide();
                                                        $('.price-excluding-tax').fadeIn();

                                                    }else if(value == 2){
                                                        $('.price-excluding-tax').hide();
                                                        $('.price-including-tax').fadeIn();
                                                    }
                                                });
                                                $("input[name=tvaswitch][value=1]").attr('checked', 'checked');
                                            });
                                    </script>

                                    <label>Prix Ht</label>
                                    <input name="tvaswitch" class="tvaswitch" value="1" type="radio">
                                    <label>Prix TTC</label>
                                    <input name="tvaswitch" class="tvaswitch" value="2" type="radio">
                                </div>
                                <div class="products-containe">
                                        <ol class="products-list" id="products-list">
                                                <?php foreach ($products as $product) { ?>
                                                    <li class="item">
                                                        <div class="product-shop">
                                                            <div class="product-name">
                                                                <a href="<?php echo $product['href']; ?>" title="<?php echo $product['name']; ?>">
                                                                    <?php echo $product['name']; ?>
                                                                </a>
                                                                <?php if ($product['price']) { ?>
                                                                <div class="price-box">
                                                                    <?php if ($product['tax']) { ?>
                                                                         
                                                                           <span class="price-excluding-tax" style="display: block;">
                                                                                <span class="price" ><?php echo $product['tax']; ?></span><span class="label">HT</span>
                                                                           </span>
                                                                    <?php } ?>
                                                                     <?php if (!$product['special']) { ?>
                                                                        <span class="price-including-tax" style="display: none;">
                                                                            <span class="label" style="display:inline-block;"></span>
                                                                            <span class="price" > <?php echo $product['price']; ?></span><span class="label">TTC</span>
                                                                        </span>
                                                                      <?php } else { ?>
                                                                        <span class="price-including-tax" style="display: none;">
                                                                            <span class="label" style="display:inline-block;"></span>
                                                                            <span class="price" > <?php echo $product['special']; ?></span><span class="label">TTC</span>
                                                                        </span>
                                                                      <?php } ?>
                                                                <?php } ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                <?php }?> 
                                          </ol>
                                          <div class="list-produit-end"></div>
                                          <div class="list-category-description">
                                               <h2>Impression personnalis&eacute;e de brochures A6 20 pages </h2>
                                                <p><b>Le livre 100% personnalisable</b> imprim&eacute; sur papier 135g est le support de communication indispensable &agrave; la r&eacute;alisation de catalogue produit, carnet de voyage, livret sponsor, bulletin municipal... R&acute;alisez votre support de communication en haute d&acute;finition sur papier 135g couch&acute; brillant ou demi-mat (au choix), pour promouvoir votre activit&acute;, votre entreprise ou vos solutions.</p>
                                                <p>Impression offset quadri sur brochure format : A6 ferm&eacute; (10,5 x 15 cm) // A5 ouvert (15 x 21 cm) sens de lecture en portrait (&agrave; la Fran?se). Livraison gratuite partout en France hors Corse en J+6 ouvr&acute;s. Possibilit&acute; de livrer en J+4.</p>
                                          </div>
                                    </div>
                            </div>
                            <?php } 
                           else { ?>
                           <p><?php echo $text_empty; ?></p>
                           <?php }?> 
                  
                </div>
                <?php echo $column_right; ?>
                </div>
            <?php } ?>
          <!-- AFFICHAGE EN TABLEAU DEUXIEME RANG CATEGORIE -->
   <?php }?>
  <!-- SI EXISTENCE D'AFFICHAGE DOSSIER -->
  <?php echo $content_bottom; ?>
</div>

<?php echo $footer; ?>
