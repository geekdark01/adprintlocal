﻿<?php echo $header; ?>

<style>



.banner{
	width: 90%;
    margin: 0 auto;
	background-image: url('http://adprint-dev.com/image/catalog/banner_image.jpg');
    background-size: 100% auto;
    background-repeat: no-repeat;
    background-position: center bottom;
	height: 350px;
	
}


.banner-content{
	width: 300px;
	margin: 30px;
    padding-top: 10px;
}


.header-divider{
    width: 100%;
    text-align: center;
}


.header-divider-text{
	line-height: 1.3em;
    font-weight: bold;
    text-transform: uppercase;
    color: #00111a;
}

.header-divider .header-divider-text:before, .header-divider .header-divider-text:after {
    
}

.pseudo-list{
	    margin-top: 30px;
		text-align: center;
}

.pseudo-list-item{
	width: 30%;
	float:left;
	padding: 5px;
}

.pseudo-list-separator
{
float:left;
width: 25px;
}

.pseudo-list-style
{
	width: 28px;
    font-weight: bold;
    font-size: 50px;
    color: #38454f;
    margin-right: 10px;
}


.pseudo-list-content{
	font-size: 13px;
}

.contenu_steps{
width: 75%;
margin:0 auto;
padding-left: 10px;
}

.action-wrapper {
       text-align: center;
    height: 200px;
    padding: 50px;
}

.text-boutton{
       background: #00a5ff;
    color: white;
    padding: 20px;
    font-weight: bold;
    font-size: 16px;
    padding: 18px 20px 16px 20px;
    border: 1px solid #c8cbcc;
    border-radius: 3px;
    cursor: pointer;
}

</style>


<div class="container">
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">
	
	
	<div class="banner">
		<div class="banner-content">
			<h1 class="basic">Service de modifications graphiques</h1>
			<h5 class="basic">
			  Nos graphistes sont à votre service pour effectuer vos modifications graphiques, qu'elles soient mineures ou importantes.
			</h5>
			<div class="credit-benefit">
			  <h2 class="basic">
				Seulement
				<span class="undefined-price">5,00 €</span> <span class="short-tax-message">(HT)</span>
			  </h2>
			  <h5 class="basic">
				Ce montant sera déduit de votre prochaine commande.
			  </h5>
			</div>
			<!--
			<div>
			  <a href="#services-explanations" class="textbutton textbutton-skin-primary textbutton-super textbutton-skin-emphasis">
				En savoir plus
			  </a>
			</div>
			-->
		  </div>
	</div>
	
	
		<div class="howitworks-explanations">
			  <h2 class="header-divider">
				<div class="header-divider-text">
				  Voilà comment ça marche
				</div>
			  </h2>
			  
			 
		</div>
		
		<div class="pseudo-list steps">
			<div class="contenu_steps">
				<div class="pseudo-list-item step">
				  <span class="pseudo-list-style">1</span>
				  <h6 class="basic pseudo-list-content">
					Expliquez à nos graphistes quels changements vous voulez apporter sur votre graphisme.
				  </h6>
				</div>
				<div class="pseudo-list-separator"><img src="http://adprint-dev.com/image/catalog/separator.png"></div>
				<div class="pseudo-list-item step">
				  <span class="pseudo-list-style">2</span>
				  <h6 class="basic pseudo-list-content">
					Ils vous présenteront votre nouveau graphisme dans un délai de 24 heures.
				  </h6>
				</div>
				<div class="pseudo-list-separator"><img src="http://adprint-dev.com/image/catalog/separator.png"></div>
				<div class="pseudo-list-item step">
				  <span class="pseudo-list-style">3</span>
				  <h6 class="basic pseudo-list-content">
					Utilisez votre nouveau graphisme sur le produit imprimé de votre choix.
				  </h6>
				</div>
			  </div>
	
	
				<div style="clear:both"></div>
	
	
	
	</div>
	
	
	<div class="action-wrapper">
    <a href="http://adprint-dev.com/index.php?route=servicemodification/index/commencer" class="text-boutton">
      Je commence
    </a>
  </div>
    
	<?php echo $column_right; ?>
	</div>
</div>
</div>

<?php echo $footer; ?>