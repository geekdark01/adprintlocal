﻿<?php echo $header; ?>

 <link href="catalog/view/theme/default/template/servicemodification/fine-uploader/fine-uploader-gallery.min.css" rel="stylesheet">
 <script src="catalog/view/theme/default/template/servicemodification/fine-uploader/fine-uploader.min.js"></script>
	
    <script type="text/template" id="qq-template">
        <div class="qq-uploader-selector qq-uploader qq-gallery" qq-drop-area-text="Déposer des fichiers ici" style="    background-color: #ddd;">
            <div class="qq-total-progress-bar-container-selector qq-total-progress-bar-container">
                <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-total-progress-bar-selector qq-progress-bar qq-total-progress-bar"></div>
            </div>
            <div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
                <span class="qq-upload-drop-area-text-selector"></span>
            </div>
            <div class="qq-upload-button-selector qq-upload-button" style="width:150px">
                <div>Télécharger un fichier</div>
            </div>
            <span class="qq-drop-processing-selector qq-drop-processing">
                <span>Processing dropped files...</span>
                <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
            </span>
            <ul class="qq-upload-list-selector qq-upload-list" role="region" aria-live="polite" aria-relevant="additions removals">
                <li>
                    <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
                    <div class="qq-progress-bar-container-selector qq-progress-bar-container">
                        <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-progress-bar-selector qq-progress-bar"></div>
                    </div>
                    <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
                    <div class="qq-thumbnail-wrapper">
                        <img class="qq-thumbnail-selector" qq-max-size="120" qq-server-scale>
                    </div>
                    <button type="button" class="qq-upload-cancel-selector qq-upload-cancel">X</button>
                    <button type="button" class="qq-upload-retry-selector qq-upload-retry">
                        <span class="qq-btn qq-retry-icon" aria-label="Retry"></span>
                        Retry
                    </button>

                    <div class="qq-file-info">
                        <div class="qq-file-name">
                            <span class="qq-upload-file-selector qq-upload-file"></span>
                            <span class="qq-edit-filename-icon-selector qq-btn qq-edit-filename-icon" aria-label="Edit filename"></span>
                        </div>
                        <input class="qq-edit-filename-selector qq-edit-filename" tabindex="0" type="text">
                        <span class="qq-upload-size-selector qq-upload-size"></span>
                        <button type="button" class="qq-btn qq-upload-delete-selector qq-upload-delete">
                            <span class="qq-btn qq-delete-icon" aria-label="Delete"></span>
                        </button>
                        <button type="button" class="qq-btn qq-upload-pause-selector qq-upload-pause">
                            <span class="qq-btn qq-pause-icon" aria-label="Pause"></span>
                        </button>
                        <button type="button" class="qq-btn qq-upload-continue-selector qq-upload-continue">
                            <span class="qq-btn qq-continue-icon" aria-label="Continue"></span>
                        </button>
                    </div>
                </li>
            </ul>

            <dialog class="qq-alert-dialog-selector">
                <div class="qq-dialog-message-selector"></div>
                <div class="qq-dialog-buttons">
                    <button type="button" class="qq-cancel-button-selector">Close</button>
                </div>
            </dialog>

            <dialog class="qq-confirm-dialog-selector">
                <div class="qq-dialog-message-selector"></div>
                <div class="qq-dialog-buttons">
                    <button type="button" class="qq-cancel-button-selector">No</button>
                    <button type="button" class="qq-ok-button-selector">Yes</button>
                </div>
            </dialog>

            <dialog class="qq-prompt-dialog-selector">
                <div class="qq-dialog-message-selector"></div>
                <input type="text">
                <div class="qq-dialog-buttons">
                    <button type="button" class="qq-cancel-button-selector">Cancel</button>
                    <button type="button" class="qq-ok-button-selector">Ok</button>
                </div>
            </dialog>
        </div>
    </script>
<style>


#brief-title{
    margin-top: 50px;
}
.design-requirement{
    width: 80%;
    margin: 0 auto;
    margin-top: 50px;
}

.requirement-optionInput{
    display: inline-block;
    vertical-align: top;
   
}

.requirement-option-desc{
	/*display: inline-block;*/
    vertical-align: top;
}

.OptionTitle{
	margin: 0;
    font-weight: bold;
    font-size: 16px;
    color: inherit;
}

.OptionDescription {
font-size: 14px;
}

.stylized-checkbox-input {
    width: 20px;
    height: 20px;
	margin:0px;
}

.requirement-option{
    height: 65px;
}

.div_check_box{
    height: 100%;
    width: 30px;
    float: left;
}

.OptionDescription{
margin-top: 0px;
}

#comment-heading{
	font-size: 15px;
}



.action-wrapper {
       text-align: center;
    height: 200px;
    padding: 50px;
}

.text-boutton{
       background: #00a5ff;
    color: white;
    padding: 20px;
    font-weight: bold;
    font-size: 16px;
    padding: 18px 20px 16px 20px;
    border: 1px solid #c8cbcc;
    border-radius: 3px;
    cursor: pointer;
}

.alert{
	display:none;
}



</style>


<div class="container">
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">
		
		
	<form action="index.php?route=servicemodification/index/sauvermodification" method="post" id="formulaire" >	
		
		
	<h1 id="brief-title" class="basic graphite">Expliquez à nos graphistes quels sont vos besoins </h1>
	
	
	<div class="design-requirement combined-brief-section">
    <h2 id="requirement-section-title" class="basic ds-section-title">1. Sélectionnez toutes les modifications souhaitées.</h2> 
        
		<div id="danger_options" class="alert alert-danger">
		  <strong>Obligatoire !</strong> cocher au moins une des options .
		</div>
            <div class="requirement-option col-5 ">
                <div class="div_check_box">
				<span class="requirement-optionInput stylized-checkbox"><input class="stylized-checkbox-input " id="option1" name="option1" type="checkbox" value="2"><label for="livetext1"></label></span> 
				</div>
                <div class="requirement-option-desc">
					<h5 class="OptionTitle basic">Modification du texte, logo ou arrière-plan</h5>
                    <h6 class="OptionDescription basic">Nous pouvons changer un nom, une couleur, un arrière-plan, ainsi que les polices et la taille des caractères.</h6>
                </div>
            </div>
            <div class="requirement-option col-5 ">
				<div class="div_check_box">
                <span class="requirement-optionInput stylized-checkbox"><input class="stylized-checkbox-input " id="option2" name="option2" type="checkbox" value="3"><label for="livetext2"></label></span>
				</div>
                <div class="requirement-option-desc">
                    <h5 class="OptionTitle basic">Modification d'un modèle</h5>
                    <h6 class="OptionDescription basic">Vous souhaitez modifier un modèle&nbsp;? Il suffit de nous envoyer une capture d'écran (ou autre support similaire) et de nous expliquer ce que vous souhaitez changer.</h6>
                </div>
            </div>
            <div class="requirement-option col-5 ">
				<div class="div_check_box">
                <span class="requirement-optionInput stylized-checkbox"><input class="stylized-checkbox-input " id="option3" name="option3" type="checkbox" value="4"><label for="livetext3"></label></span>
				</div>
                <div class="requirement-option-desc">
                    <h5 class="OptionTitle basic">Correction d'image</h5>
                    <h6 class="OptionDescription basic">Nous pouvons augmenter la résolution de logos, ainsi que retoucher ou ajouter des effets sur des photos.</h6>
                </div>
            </div>
            <div class="requirement-option col-5 not-fixed-height">
				<div class="div_check_box">
                <span class="requirement-optionInput stylized-checkbox"><input class="stylized-checkbox-input " id="option4" name="option4" type="checkbox" value="5"><label for="livetext4"></label></span>
				</div>
                <div class="requirement-option-desc">
                    <h5 class="OptionTitle basic">Autres modifications</h5>
                    <h6 class="OptionDescription basic"></h6>
                </div>
            </div>
        <div style="clear: both;"></div>
       
	   
		
    <div class="comment">
        <h5 id="comment-heading" class="basic">
                <span>Indiquez-nous exactement les modifications dont vous avez besoin. Notez que nous pouvons modifier plusieurs produits ou versions si nécessaire.</span>
                <span id="comment-optional" class="ds-required basic" style="display: inline-block;">*</span>
        </h5>
        <textarea style="height:150px" id="comments" name="comments" placeholder="Exemple&nbsp;: Inverser la couleur du logo et celle de l'arrière-plan (arrière-plan en vert et logo en bleu)." maxlength="5000" class="stylized-textarea comment-box" rows="4" cols="70"></textarea>
        <!-- Check for number of allowed characters -->
		</br>
		<div id="danger_texte" class="alert alert-danger">
		  <strong>Obligatoire !</strong> indiquer les modifications à faire .
		</div>
    </div>
</div>




<div class="design-requirement combined-brief-section">
    <h2 id="requirement-section-title" class="basic ds-section-title">2. Téléchargez les fichiers originaux de vos graphismes.</h2> 
        
		<p>Envoyez-nous des images de la meilleure qualité pour nous permettre d'effectuer vos modifications aussi précisément que possible.
		<div id="danger_fichier" class="alert alert-danger">
		  <strong>Obligatoire !</strong> Télécharger au moins un fichier .
		</div>
		<div id="uploader"></div>
    <script>
        // Some options to pass to the uploader are discussed on the next page
        var uploader = new qq.FineUploader({
			debug: true,
            element: document.getElementById("uploader"),
			request: {
				endpoint: '<?php echo $base ; ?>image/servicemodification/endpoint.php'
			},
			deleteFile: {
				enabled: true,
				endpoint: '<?php echo $base ; ?>image/servicemodification/endpoint.php'
			},
			callbacks: {
				onComplete: function(id, name, responseJSON, autre ) {
					//$("#liste_images").append("<input type='hidden' value");		
					//console.log( "/"+responseJSON.uuid+"/"+responseJSON.uploadName );
					$("#liste_images").append(" <input name='images[]' id='"+responseJSON.uuid+"' type='hidden'   value='"+responseJSON.uuid+"/"+responseJSON.uploadName+"'  >");
				}
			}
        })
		
		
		
	function controller()	
	{	
	
		$(".alert").hide();
	
		// controller les checkbox 
		if( !$("#option1").is(':checked') && !$("#option2").is(':checked') && !$("#option3").is(':checked') && !$("#option4").is(':checked')  )
		{
			$("#danger_options").show();
			return true;
		}
		
		
		// controller la zone de texte
		if( $("#comments").val().length < 10 )
		{
			$("#danger_texte").show();
			 return true;
		}
		
		
		// control images 
		if( $('#liste_images input').length < 1 )
		{
			$("#danger_fichier").show();
			 return true;
		}
		
		 $('#formulaire').submit();
		
	}
		
		
		
    </script>
</p>
           
</div>


<div id="liste_images" style="display:none">

</div>
	
	
	<div class="action-wrapper">
    <a href="javascript:void(0);" onclick="controller();" class="text-boutton">
      Ajouter panier
    </a>
  </div>
	
	</form>
	
	<?php echo $column_right; ?>
	</div>
</div>
</div>

<?php echo $footer; ?>