<?php echo $header; $display_qte='block';?>
<script src="jquery-3.3.1.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
<div ng-app="myApp" ng-controller="myCtrl" class="container">
  <div class="breadcrumbs">
    <ul>
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <li><a href="<?php echo $breadcrumb['href']; ?>" title="" itemprop="url"><span itemprop="title"><?php echo $breadcrumb['text']; ?></span></a>
        <span>&gt; </span>
      </li>
       <?php } ?>
    </ul>
  </div>
  
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>  
        <div class="row">
            <div class="col-sm-12" >
            <h1 class="titre_produit"><?php echo $heading_title; ?></h1>
                <div class="short-description">
            <div class="col-sm-2">
              <img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" height="46px" width="115px" />
            </div>
            <div class="col-sm-10">
              <p><?php echo $meta_description; ?></p>
            </div>
                </div>
            </div>
        </div><br/>  
        <form id="product" name="produit">
        <div class="row" style="border:solid 1px #e1e1e1">
         <div class="col-sm-8" style="border-right:solid 1px #e1e1e1;padding-top: 40px;">
            <?php if ($column_left || $column_right) { ?>
            <?php $class = 'col-sm-6'; ?>
            <?php } else { ?>
            <?php $class = 'col-sm-6'; ?>
            <?php } ?>
            <div class="<?php echo $class; ?>">
                <?php if ($thumb || $images) { ?>
                <ul class="thumbnails">
                  <?php if ($thumb) { ?>
                  <li><a class="thumbnail" href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>"><img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a></li>
                  <?php } ?>
                  <?php if ($images) { ?>
                  <?php foreach ($images as $image) { ?>
                  <li class="image-additional"><a class="thumbnail" href="<?php echo $image['popup']; ?>" title="<?php echo $heading_title; ?>"> <img src="<?php echo $image['thumb']; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a></li>
                  <?php } ?>
                  <?php } ?>
                </ul>
                <?php } ?>
            </div>
            <?php if ($column_left || $column_right) { ?>
            <?php $class = 'col-sm-6'; ?>
            <?php } else { ?>
            <?php $class = 'col-sm-6'; ?>
            <?php } ?>
            <div class="<?php echo $class; ?>">
              <h2><?php echo $text_option; ?></h2>
                <div class="options-produits">          
                    <div style="text-align:left;" class="form-group-required">
                          <label class="control-label" for="input-option-largeur">Largeur (cm)</label>
                          <input ng-model="Largeur" name="Largeur" max="<?php echo $largeur_maximum; ?>" min="<?php echo $largeur_minimum; ?>" style="width: 88% !important;display: inline-block;" placeholder="Largeur (cm)" id="input-option-largeur" class="form-control" type="number" ng-required="true" /> 
                          <div class="optionSizeQuestion" id="size-input-option-largeur">
                            <span class="questionIco" data-toggle="largeur" data-placement="right" title="Dimensions minimum d'impression <?php echo $largeur_minimum; ?> x <?php echo $hauteur_minimum; ?> cm &#013; Dimensions maximum d'impression <?php echo $largeur_maximum; ?> x <?php echo $hauteur_maximum; ?> cm" >?</span>
                          </div>
                          <span class="text-danger" ng-show="produit.Largeur.$error.required">
                                   Veuillez remplir le champ!</span>
                          <span class="text-danger" ng-show="produit.Largeur.$error.number">
                                Veuillez saisir un nombre!</span>
                          <div class="text-danger" ng-if="produit.Largeur.$error.max || produit.Largeur.$error.min" >La valeur n'est pas dans la plage spécifié</div>
                    </div><br>
                    <div style="text-align:left;" class="form-group-required">
                          <label class="control-label" for="input-option-hauteur">Hauteur (cm)</label>
                          <input ng-model="Hauteur" name="Hauteur" min="<?php echo $hauteur_minimum; ?>" max="2000" style="width: 88% !important;display: inline-block;" placeholder="Hauteur (cm)" id="input-option-hauteur" class="form-control" type="number" ng-required="true" /> 
                          <div class="optionSizeQuestion" id="size-input-option-hauteur">
                            <span class="questionIco" data-toggle="largeur" data-placement="right" title="Dimensions minimum d'impression <?php echo $largeur_minimum; ?> x <?php echo $hauteur_minimum; ?> cm &#013; Dimensions maximum d'impression <?php echo $largeur_maximum; ?> x <?php echo $hauteur_maximum; ?> cm" >?</span>
                          </div>
                          <span class="text-danger" ng-show="produit.Hauteur.$error.required">
                                   Veuillez remplir le champ!</span>
                          <span class="text-danger" ng-show="produit.Hauteur.$error.number">
                                Veuillez saisir un nombre!</span>
                          <div class="text-danger" ng-if="produit.Hauteur.$error.max || produit.Hauteur.$error.min" >La valeur n'est pas dans la plage spécifié</div>
                    </div> <br>
                    <div class="options-produits-wrapper form-group-required">
                          <label class="control-label" for="Orientation">Orientation</label>
                            <select ng-model="Orientation" name="Orientation" ng-options="x.name for x in orientations"  class="form-control" ng-required="true">
                            </select>
                            <span class="text-danger" ng-show="erreur3">Ce champ est obligatoire.</span>
                    </div><br>                 
                    <div class="options-produits-wrapper form-group-required">
                          <label class="control-label" for="Oeillet">Oeillet</label>
                            <select ng-model="oeillet" name="Oeillet" ng-options="x.name for x in oeillets" class="form-control" ng-required="true">
                            </select>
                              <span class="text-danger" ng-show="erreur4">Ce champ est obligatoire.</span>
                    </div><br>
                    <div class="options-produits-wrapper form-group-required">
                          <label class="control-label" for="Soudure">Soudure et renforcement</label>
                            <select ng-model="soudure" name="Soudure"  ng-options="x.name for x in soudures" class="form-control" ng-required="true">
                            </select>
                            <span class="text-danger" ng-show="erreur5">Ce champ est obligatoire.</span>
                    </div><br>
                    <div  style="text-align:left;" class="options-produits-wrapper form-group-required">
                          <label class="control-label" for="Servicepose">Service pose</label>
                            <select ng-model="pose" ng-change="showvisible()" name="Servicepose" ng-options="x.name for x in services" class="form-control" ng-required="true">
                            </select>
                             <span class="text-danger" ng-show="erreur6">Ce champ est obligatoire.</span>
                    </div>
                    <div ng-show="isvisible"  style="text-align:left;" class="options-produits-wrapper form-group-required">
                          <select ng-model="posedepo" name="Pose" ng-options="x.name for x in poses" class="form-control" ng-required="true">
                          </select>
                           <span class="text-danger" ng-show="erreur7">Ce champ est obligatoire.</span>            
                    </div>
                    <div ng-show="isvisible" style="text-align:left;" class="options-produits-wrapper form-group-required">
                            <textarea ng-model="ville" name="Ville" class="form-control" placeholder="Veuillez nous renseignez l'endroit" ng-required="true"></textarea>
                             <span class="text-danger" ng-show="erreur8">Ce champ est obligatoire.</span>
                    </div><br>
                    <div style="text-align:left;" class="options-produits-wrapper form-group-required">
                          <label class="control-label" for="input-option-fourreau">Fourreau</label>
                            <select ng-model="Fourreau" ng-change="showvisible1()" name="Fourreau" ng-options="x.name for x in Fourreaux" id="input-option-fourreau" class="form-control" ng-required="true">
                            </select>
                             <span class="text-danger" ng-show="erreur9">Ce champ est obligatoire.</span>   
                    </div>
                    <div ng-show="isvisible1" style="text-align:left;" class="options-produits-wrapper form-group-required">
                            <select  ng-model= "mettre" name="Mettre" id="input-option-fourreau" ng-options="x.name for x in mettres" class="form-control" ng-required="true">
                            </select>
                             <span class="text-danger" ng-show="erreur10">Ce champ est obligatoire.</span>
                    </div><br>
                     <div style="text-align:left;" class="options-produits-wrapper form-group-required">
                          <label class="control-label" for="bat">BAT en ligne</label>
                            <select  ng-model= "bat" name="bat" id="input-option-bat" ng-options="x.name for x in bats" class="form-control" ng-required="true">
                            </select>
                             <span class="text-danger" ng-show="erreur11">Ce champ est obligatoire.</span>
                    </div><br>
                 </div> <br>
      <div class="clear"></div>        
      <?php if ($recurrings) { ?>
         <hr>
         <h3><?php echo $text_payment_recurring; ?></h3>
            <div style="text-align:left;" class="form-group required">
              <select name="recurring_id" class="form-control">
                 <option value=""><?php echo $text_select; ?></option>
                 <?php foreach ($recurrings as $recurring) { ?>
                   <option value="<?php echo $recurring['recurring_id']; ?>"><?php echo $recurring['name']; ?></option>
                 <?php } ?>
              </select>
              <div class="help-block" id="recurring-description"></div>
            </div>
      <?php } ?>
      <div style='display:<?php echo $display_qte;?>'>
          <span><label class="qty-label required" for="input-quantity"><?php echo $entry_qty; ?> : </label></span>
          <span><input ng-model= "quantity" type="text" name="quantity" value="<?php echo $minimum; ?>" size="1" id="input-quantity"/></span>
      </div>
    </div>
  </div>
        <!--DEV101: ajout 3eme colonne-->
        <div class="col-sm-4">
          <ul class="list-unstyled" style="text-align:center">
                <li class="price-excluding-tax" id="content_prix_ht" style="display:block;" >
                  <h2>
                    <?php $temp = str_word_count($model, 1, 'âàáãç3');?>
                    <?php $x=0; ?>
                    <?php foreach ($temp as  $value) {
                        if($value == 'chute'){
                           $x++;
                        } 
                    } ?>
                    <?php if($x == 0){ ?>
                      <span ng-model='Total_prix'> {{ prixTotal() | currency : "" }} Ar</span> HT
                    <?php }else{ ?>
                      <span ng-model='Total_prix'> {{ prixTotal1() | currency : "" }} Ar</span> HT
                    <?php } ?> 
                  </h2>
                </li>
              <?php if ($points) { ?>
                <li><?php echo $text_points; ?> <?php echo $points; ?></li>
              <?php } ?>
          </ul>
        <?php if($prix_metre_carre_remise!='0.00' && $seuil!='0.00'){ ?>
          <div class="mettreCarrePrice">
            Jusqu'à <span class="price"><?php echo $prix_metre_carre_remise;?></span>  
            <span class="price-no-tax-label">HT</span>
            <span> le m²</span><br>
            <span>(Prix au-delà de <?php echo round($seuil); ?> m²)</span>
          </div>
        <?php } ?>
        <div>  
        <div style="text-align:left;" class="form-group">    
            <?php if ($review_status) { ?>
                <div class="rating" style="text-align:center">
                  <p>
                  <?php for ($i = 1; $i <= 5; $i++) { ?>
                    <?php if ($rating < $i) { ?>
                       <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                    <?php } else { ?>
                       <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                    <?php } ?>
                  <?php } ?>
                  <a href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;"><?php echo $reviews; ?></a> / <a href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;"><?php echo $text_write; ?></a>
                  </p>
                </div>
            <?php } ?>
            <div class="clear"></div>    
              <button type="button"  ng-click="valider()" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary btn-lg btn-block"><?php echo $button_cart; ?></button>
                <ul id="product_page_bloc_livrai_satisf_secu">
                  <li>
                    <span class="camion"><a class="hovered" href="#tab-livraison">Livraison Gratuite</a> partout a Madagascar*</span>
                  </li>
                  <li>
                    <span class="smiley"><a href="#tab-livraison" class="hovered">Satisfait ou remboursé</a> sans condition !</span>
                  </li>
                </ul>
        </div>
        <?php if ($minimum > 1) { ?>
          <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $text_minimum; ?></div>
        <?php } ?>
        </div>
      </div>
        <!--DEV101: ajout 3eme colonne-->  
    </div>
  </form>
    <!-- DEV101 : SI PRODUIT SUR MESURE -->   
    <!--DEV103 continuer ajouter panier-->    
    <div id="divPropose" style="display:none">
      <div class="row">
        <div class="col-sm-12">
          <h3>Ce produit a été ajouté à votre panier :</h3>
          <hr>
          <div class="col-sm-3 image">
            <img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>" class="img-responsive" />
          </div>
          <div class="caption col-sm-9">
            <h4><?php echo $heading_title; ?></h4>
          </div>
        </div>
        <div class="col-sm-12"> 
          <div class="col-md-6">
            <a class="suiteAchat" href="<?php echo $continue; ?>">Continuer mes achats</a>
          </div>
          <div class="col-md-6">
            <a class="confirmPanier" href="<?php echo $checkout; ?>">Voir mon panier</a>
          </div>
        </div>
      </div>
      <?php if (!empty($products_propose)) { ?>
      <div class="row">
        <div class="col-sm-12">
          <h3>Nous vous recommandons avec ce produit :</h3>
          <hr>
            <?php foreach ($products_propose as $product) { ?>
        <div class="col-sm-3 image">
          <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a>
        </div>
        <div class="caption col-sm-9">
          <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
        <?php if ($product['price']) { ?>
        <p class="price">
          <?php if ($product['tax']) { ?>
          <span class="price-tax"><?php echo $product['tax']; ?> HT</span>
          <?php } ?>
        </p>
        <?php } ?>
          <a class="btn btn-default" href="<?php echo $product['href']; ?>">Voir le produit</a>
        </div>
            <?php } ?>
        </div>
      </div>
      <?php } ?>
    </div>
      <!--DEV103 continuer ajouter panier-->
      <?php if ($tags) { ?>
      <p><?php echo $text_tags; ?>
        <?php for ($i = 0; $i < count($tags); $i++) { ?>
        <?php if ($i < (count($tags) - 1)) { ?>
        <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>,
        <?php } else { ?>
        <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>
        <?php } ?>
        <?php } ?>
      </p>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
<script>
  var app = angular.module('myApp', []);
  app.controller('myCtrl',['$scope', '$http',
  function($scope, $http) {
  var valeur = ['9.3','11.9','14.8','18.4','20.4','22.7','26.4','30.5'];
    $scope.oeillets = [
      {val : '0', name: "--- Faites un choix ---"},
      {val : '1', name : "Sans oeillet"},
      {val : '2', name : "Tous les 25 cm"},
      {val : '3', name : "Tous les 50 cm"},
      {val : '4', name : "Tous les 100 cm"},
      {val : '5', name : "Sur les 4 coins"}
    ];
    $scope.orientations = [
      {val : '0', name: "--- Faites un choix ---"},
      {val : '1', name : "Paysage"},
      {val : '2', name : "Portrait"}
    ];
    $scope.soudures = [
      {val : '0', name : "--- Faites un choix ---"},
      {val : '1', name : "Sans soudure"},
      {val : '2', name : "4 cotés perimetral"},
      {val : '3', name : "2 cotés largeur"},
      {val : '4', name : "2 cotés hauteur"}
    ];
    $scope.services = [
      {val : '0', name : "--- Faites un choix ---"},
      {val : '2', name : "Non"},
      {val : '1', name : "Oui"}
    ];
    $scope.poses = [
      {val : '0', name: "--- Faites un choix ---"},
      {val : '1', name : "Pose"},
      {val : '2', name : "Pose et Dépose"}
    ];
    $scope.bats = [
      {val : '0', name: "--- Faites un choix ---"},
      {val : '1', name : "Oui"},
      {val : '2', name : "Non"}
    ];
    $scope.Fourreaux = [
      {val : '0', name : "--- Faites un choix ---"},
      {val : '1', name : "Aucun"},
      {val : '2', name : "Côtés largeur"},
      {val : '3', name : "Côtés hauteur"},
      {val : '4', name : "2 cotés largeur"},
      {val : '5', name : "2 cotés hauteur"}
    ];
    $scope.mettres = [
      {val : '0', name : "--- Faites un choix ---"},
      {val : '3', name : "3 cm"},
      {val : '4', name : "4 cm"},
      {val : '5', name : "5 cm"},
      {val : '6', name : "6 cm"},
      {val : '7', name : "7 cm"},
      {val : '8', name : "8 cm"},
      {val : '9', name : "9 cm"},
      {val : '10', name : "10 cm"}
    ];
    $scope.product_id = '<?php echo $product_id; ?>'
    $scope.isvisible = false;
    $scope.ville = 'Aucun';
    $scope.erreur3 = false;
    $scope.erreur4 = false;
    $scope.erreur5 = false;
    $scope.erreur6 = false;
    $scope.erreur7 = false;
    $scope.erreur8 = false;
    $scope.erreur9 = false;
    $scope.erreur10 = false;
    $scope.verifie = false;
    $scope.isvisible1 = false ;
    $scope.prix_soudure = '<?php echo ($prix_soudure); ?>';
    $scope.marge_soudure = '<?php echo($marge_soudure) ; ?>';
    $scope.prix_pose = '<?php echo ($prix_pose); ?>';
    $scope.prix_oeillet = '<?php echo ($prix_unitaire_oeillet) ; ?>';
    $scope.prix_carre = '<?php echo ($prix_metre_carre) ; ?>';
    $scope.Largeur = '';
    $scope.Hauteur= '';
    $scope.Bat = '<?php echo ($prix_bat) ; ?>'
    $scope.LargeurChute = '0';
    $scope.HauteurChute = '0';
    $scope.MontantChute = '0';
    $scope.MontantTotalChute = '0';
    $scope.LaizeMaxChute = '0';
    $scope.quantity = '1';
    $scope.prix_depose = '<?php echo ($prix_depose); ?>';
    $scope.prix_total_bat = '0';
    $scope.prix_total_pose = '0';
    $scope.prix_total_oeillet = '0';
    $scope.prix_total_soudure = '0';
    $scope.Lar = $scope.Largeur;
    $scope.Hau = $scope.Hauteur;
    $scope.prix_metre_carre = '0';
    $scope.prix_total = '10000';
    $scope.prix_min_pose = '20000';
    $scope.total_min = '10000';
    $scope.total_fourreau = '0';
    function Prixlargeur($x){
      $scope.Lar = $scope.Largeur;
      $scope.Hau = $scope.Hauteur;
      if($scope.Orientation.val == 1){
        if($scope.Largeur < $scope.Hauteur){
            $scope.templarg = $scope.Largeur;
            $scope.Lar = $scope.Hauteur;
            $scope.Hau = $scope.templarg;
        }        
      }else if($scope.Orientation.val == 2){
        if($scope.Largeur > $scope.Hauteur){
            $scope.templarg = $scope.Largeur;
            $scope.La = $scope.Hauteur;
            $scope.Hau = $scope.templarg;
        }  
      }
      return parseInt((($x/100)*($scope.Lar/100)*$scope.prix_carre)+((($scope.Lar*2)/100)*parseInt($scope.prix_soudure)));
    }
    function Prixhauteur($x){
      $scope.Lar = $scope.Largeur;
      $scope.Hau = $scope.Hauteur;
      if($scope.Orientation.val == 1){
        if($scope.Largeur < $scope.Hauteur){
            $scope.templarg = $scope.Largeur;
            $scope.Lar = $scope.Hauteur;
            $scope.Hau = $scope.templarg;
        }        
      }else if($scope.Orientation.val == 2){
        if($scope.Largeur > $scope.Hauteur){
            $scope.templarg = $scope.Largeur;
            $scope.Lar = $scope.Hauteur;
            $scope.Hau = $scope.templarg;
        }  
      }
      return parseInt((($x/100)*($scope.Hau/100)*$scope.prix_carre)+((($scope.Hau*2)/100)*parseInt($scope.prix_soudure)));
    }
    function Prix2coteslargeur($x){
      $scope.Lar = $scope.Largeur;
      $scope.Hau = $scope.Hauteur;
      if($scope.Orientation.val == 1){
        if($scope.Largeur < $scope.Hauteur){
            $scope.templarg = $scope.Largeur;
            $scope.Lar = $scope.Hauteur;
            $scope.Hau = $scope.templarg;
        }        
      }else if($scope.Orientation.val == 2){
        if($scope.Largeur > $scope.Hauteur){
            $scope.templarg = $scope.Largeur;
            $scope.Lar = $scope.Hauteur;
            $scope.Hau = $scope.templarg;
        }  
      }
      return parseInt(((($x/100)*($scope.Lar/100)*$scope.prix_carre)+((($scope.Lar*2)/100)*parseInt($scope.prix_soudure)))*2);
    }
    function Prix2coteshauteur($x){
      $scope.Lar = $scope.Largeur;
      $scope.Hau = $scope.Hauteur;
      if($scope.Orientation.val == 1){
        if($scope.Largeur < $scope.Hauteur){
            $scope.templarg = $scope.Largeur;
            $scope.Lar = $scope.Hauteur;
            $scope.Hau = $scope.templarg;
        }        
      }else if($scope.Orientation.val == 2){
        if($scope.Largeur > $scope.Hauteur){
            $scope.templarg = $scope.Largeur;
            $scope.Lar = $scope.Hauteur;
            $scope.Hau = $scope.templarg;
        }  
      }
      return parseInt(((($x/100)*($scope.Hau/100)*$scope.prix_carre)+((($scope.Hau*2)/100)*parseInt($scope.prix_soudure)))*2);
    }
    $scope.showvisible1 = function(){
      if($scope.Fourreau.val == 2 || $scope.Fourreau.val == 3 || $scope.Fourreau.val == 4 || $scope.Fourreau.val == 5){
          $scope.isvisible1 = $scope.isvisible1 = true ;
      }else{
          $scope.isvisible1 = $scope.isvisible1 = false ;
      }
    }
    $scope.valider = function(){
            if($scope.Largeur == null){
              $scope.Largeur = '';
            }
             if($scope.Hauteur == null){
               $scope.Hauteur = '';
             }
             var data = {
              Quantity : $scope.quantity,
              Total : $scope.prix_total,
              Largeur : $scope.Largeur,
              Hauteur : $scope.Hauteur,
              Orientation : $scope.Orientation.name,
              oeillet : $scope.oeillet.name,
              Soudure : $scope.soudure.name,
              Servicepose : $scope.pose.name,
              Pose : $scope.posedepo.name,
              Ville : $scope.ville,
              Fourreau : $scope.Fourreau.name,
              MettreFourreau : $scope.mettre.name,
              Bat : $scope.bat.name,
              LaizeMaxChute : $scope.LaizeMaxChute,
              Product_id : $scope.product_id
            };
            $http.post('index.php?route=checkout/cart/add1', JSON.stringify(data),{
              transformResponse: [
                function (data) { 
             return data; 
                 }
           ]}).then(function(json){
                  $scope.msg = "Post Data Submitted Successfully!";
                  console.log(data);
                  $json1 = JSON.parse(json['data']);
                  console.log($json1['error']);
                  if($json1['error']){
                    if($json1['error']['option']['Orientation']){
                        $scope.erreur3 = $scope.erreur3 = true;
                    }else if(!$json1['error']['option']['Orientation']){
                      $scope.erreur3 = $scope.erreur3 = false;
                    }
                    if($json1['error']['option']['Oeillet']){
                        $scope.erreur4 = $scope.erreur4 = true;
                    }else if(!$json1['error']['option']['Oeillet']){
                      $scope.erreur4 = $scope.erreur4 = false;
                    }
                    if($json1['error']['option']['Soudure']){
                        $scope.erreur5 = $scope.erreur5 = true;
                    }else if(!$json1['error']['option']['Soudure']){
                      $scope.erreur5 = $scope.erreur5 = false;
                    }
                    if($json1['error']['option']['Servicepose']){
                        $scope.erreur6 = $scope.erreur6 = true;
                    }else if(!$json1['error']['option']['Servicepose']){
                      $scope.erreur6 = $scope.erreur6 = false;
                    }
                    if($json1['error']['option']['Pose']){
                        $scope.erreur7 = $scope.erreur7 = true;
                    }else if(!$json1['error']['option']['Pose']){
                      $scope.erreur7 = $scope.erreur7 = false;
                    }
                    if($json1['error']['option']['Ville']){
                        $scope.erreur8 = $scope.erreur8 = true;
                    }else if(!$json1['error']['option']['Ville']){
                      $scope.erreur8 = $scope.erreur8 = false;
                    }
                    if($json1['error']['option']['Fourreau']){
                        $scope.erreur9 = $scope.erreur9 = true;
                    }else if(!$json1['error']['option']['Fourreau']){
                      $scope.erreur9 = $scope.erreur9 = false;
                    }
                    if($json1['error']['option']['MettreFourreau']){
                        $scope.erreur10 = $scope.erreur10 = true;
                    }else if(!$json1['error']['option']['MettreFourreau']){
                      $scope.erreur10 = $scope.erreur10 = false;
                    }
                    if($json1['error']['option']['bat']){
                        $scope.erreur11 = $scope.erreur11 = true;
                    }else if(!$json1['error']['option']['bat']){
                      $scope.erreur11 = $scope.erreur11 = false;
                    }
                  }
                  if($json1['success']){
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                    $('#panier > a').html('<i class="fa fa-shopping-cart fa-3x"></i><span id="panierTotal">' + $json1['total_produit'] + '</span><p>Mon Panier</p>');
                    $('#panier > ul').load('index.php?route=common/cart/info .wedgetPanier li', function() {
                    autre_proposition() ;
                    });
                  }
            },function(json) {
               console.log(data);
               console.log(json);
                  $scope.msg = "Service not Exists";
            });
    }
    $scope.showvisible = function(){
      if($scope.pose.val == 1){
        $scope.isvisible = $scope.isvisible = true;
      }else{
         $scope.isvisible = $scope.isvisible = false;
      }
    }
    $scope.Fourreau = $scope.Fourreaux[0];
    $scope.mettre = $scope.mettres[0];
    $scope.pose = $scope.services[0];
    $scope.posedepo = $scope.poses[0];
    $scope.Orientation = $scope.orientations[0];
    $scope.soudure = $scope.soudures[0];
    $scope.oeillet = $scope.oeillets[0];
    $scope.bat = $scope.bats[0];
    $scope.prixTotal1 = function() {
      if($scope.Largeur < 160){
         $scope.LaizeMaxChute = '160';
      }
      if($scope.Largeur >= 160){
        $scope.LaizeMaxChute = '320';
      }
      if($scope.bat.val == 1 ){
        $scope.prix_total_bat = $scope.Bat;
      }
      if($scope.bat.val == 0 || $scope.bat.val == 2 ){
        $scope.prix_total_bat = 0;
      }
      $scope.LargeurChute = parseInt($scope.LaizeMaxChute)-parseInt($scope.Largeur);
      $scope.HauteurChute = $scope.Hauteur;
      $scope.prix_metre_carre = ((($scope.Largeur/100)*($scope.Hauteur/100))*parseInt($scope.prix_carre));
      $scope.MontantChute = (($scope.LargeurChute/100)*($scope.HauteurChute/100)*parseInt($scope.prix_carre));
      $scope.MontantTotalChute = parseInt($scope.MontantChute)+parseInt($scope.prix_metre_carre);
        if($scope.oeillet.val == '1'){
          $scope.prix_total_oeillet = '0';
        }else if($scope.oeillet.val == '2'){
          $scope.prix_total_oeillet= (((($scope.Largeur*2)/25)+(($scope.Hauteur*2)/25))*parseInt($scope.prix_oeillet));
        }else if($scope.oeillet.val == '3'){
          $scope.prix_total_oeillet= (((($scope.Largeur*2)/50)+(($scope.Hauteur*2)/50))*parseInt($scope.prix_oeillet));
        }else if($scope.oeillet.val == '4'){
          $scope.prix_total_oeillet = '0';
        }else if($scope.oeillet.val == '5'){
          $scope.prix_total_oeillet = '0';
        }else if($scope.oeillet.val){
          $scope.prix_total_oeillet = '0';
        }
        if($scope.soudure.val == 2){
              $scope.x = (((($scope.Largeur+parseInt($scope.marge_soudure))+($scope.Largeur+parseInt($scope.marge_soudure))+($scope.Hauteur+parseInt($scope.marge_soudure))+($scope.Hauteur+parseInt($scope.marge_soudure)))/100)*parseInt($scope.prix_soudure));
              $scope.y = (((parseInt($scope.marge_soudure)/100)*($scope.Largeur/100))* parseInt($scope.prix_carre))+(((parseInt($scope.marge_soudure)/100)*($scope.Hauteur/100))*parseInt($scope.prix_carre))+(((parseInt($scope.marge_soudure)/100)*(parseInt($scope.marge_soudure)/100))*parseInt($scope.prix_carre));
              $scope.prix_total_soudure = $scope.x + $scope.y;
        }else if($scope.soudure.val == 3){
              $scope.x = ((($scope.Largeur*2)/100)*parseInt($scope.prix_soudure))+(((parseInt($scope.marge_soudure)/100)*(parseInt($scope.marge_soudure)/100))*parseInt($scope.prix_carre));
              $scope.y = (((parseInt($scope.marge_soudure)/100)*($scope.Hauteur/100))*parseInt($scope.prix_carre));
              $scope.prix_total_soudure = $scope.x+ $scope.y;
        }else if($scope.soudure.val == 4){
              $scope.x = ((($scope.Hauteur*2)/100)*parseInt($scope.prix_soudure))+(((parseInt($scope.marge_soudure)/100)*(parseInt($scope.marge_soudure)/100))*parseInt($scope.prix_carre));
              $scope.y = (((parseInt($scope.marge_soudure)/100)*($scope.Largeur/100))*parseInt($scope.prix_carre));
              $scope.prix_total_soudure = $scope.x + $scope.y;
        }else {
              $scope.prix_total_soudure = '0';
        }
        if($scope.pose.val == 1){
         if($scope.posedepo.val == 1){
            $scope.prix_total_pose = (($scope.Largeur/100)*($scope.Hauteur/100))*parseInt($scope.prix_pose);
            if($scope.prix_total_pose < $scope.prix_min_pose){
             $scope.prix_total_pose = $scope.prix_min_pose;
           }
          }else if($scope.posedepo.val == 2){
            $scope.prix_total_pose1 = (($scope.Largeur/100)*($scope.Hauteur/100))*parseInt($scope.prix_pose);
            if($scope.prix_total_pose1 < $scope.prix_min_pose){
             $scope.prix_total_pose1 = $scope.prix_min_pose;
            }
            $scope.prix_total_pose2 = (($scope.Largeur/100)*($scope.Hauteur/100))*parseInt($scope.prix_depose);
            $scope.prix_total_pose = parseInt($scope.prix_total_pose1) + parseInt($scope.prix_total_pose2);
          }else{
            $scope.prix_total_pose = '0';
          }
        }else{
          $scope.prix_total_pose = '0';
        }
        if($scope.Fourreau.val == 2 ){
          if($scope.mettre.val == 3){
            $scope.total_fourreau = Prixlargeur(valeur[0]);
          }else if($scope.mettre.val == 4){
            $scope.total_fourreau = Prixlargeur(valeur[1]);
          }else if($scope.mettre.val == 5){
            $scope.total_fourreau = Prixlargeur(valeur[2]);
          }else if($scope.mettre.val == 6){
            $scope.total_fourreau = Prixlargeur(valeur[3]);
          }else if($scope.mettre.val == 7){
            $scope.total_fourreau = Prixlargeur(valeur[4]);
          }else if($scope.mettre.val == 8){
            $scope.total_fourreau = Prixlargeur(valeur[5]);
          }else if($scope.mettre.val == 9){
            $scope.total_fourreau = Prixlargeur(valeur[6]);
          }else if($scope.mettre.val == 10){
            $scope.total_fourreau = Prixlargeur(valeur[7]);
          }else
            $scope.total_fourreau = '0';
        }else if($scope.Fourreau.val == 3){
          if($scope.mettre.val == 3){
            $scope.total_fourreau = Prixhauteur(valeur[0]);
          }else if($scope.mettre.val == 4){
            $scope.total_fourreau = Prixhauteur(valeur[1]);
          }else if($scope.mettre.val == 5){
            $scope.total_fourreau = Prixhauteur(valeur[2]);
          }else if($scope.mettre.val == 6){
            $scope.total_fourreau = Prixhauteur(valeur[3]);
          }else if($scope.mettre.val == 7){
            $scope.total_fourreau = Prixhauteur(valeur[4]);
          }else if($scope.mettre.val == 8){
            $scope.total_fourreau = Prixhauteur(valeur[5]);
          }else if($scope.mettre.val == 9){
            $scope.total_fourreau = Prixhauteur(valeur[6]);
          }else if($scope.mettre.val == 10){
            $scope.total_fourreau = Prixhauteur(valeur[7]);
          }else
            $scope.total_fourreau = '0';
        }else if($scope.Fourreau.val == 4){
          if($scope.mettre.val == 3){
            $scope.total_fourreau = Prix2coteslargeur(valeur[0]);
          }else if($scope.mettre.val == 4){
            $scope.total_fourreau = Prix2coteslargeur(valeur[1]);
          }else if($scope.mettre.val == 5){
            $scope.total_fourreau = Prix2coteslargeur(valeur[2]);
          }else if($scope.mettre.val == 6){
            $scope.total_fourreau = Prix2coteslargeur(valeur[3]);
          }else if($scope.mettre.val == 7){
            $scope.total_fourreau = Prix2coteslargeur(valeur[4]);
          }else if($scope.mettre.val == 8){
            $scope.total_fourreau = Prix2coteslargeur(valeur[5]);
          }else if($scope.mettre.val == 9){
            $scope.total_fourreau = Prix2coteslargeur(valeur[6]);
          }else if($scope.mettre.val == 10){
            $scope.total_fourreau = Prix2coteslargeur(valeur[7]);
          }else
            $scope.total_fourreau = '0';
        }else if($scope.Fourreau.val == 5){
          if($scope.mettre.val == 3){
            $scope.total_fourreau = Prix2coteshauteur(valeur[0]);
          }else if($scope.mettre.val == 4){
            $scope.total_fourreau = Prix2coteshauteur(valeur[1]);
          }else if($scope.mettre.val == 5){
            $scope.total_fourreau = Prix2coteshauteur(valeur[2]);
          }else if($scope.mettre.val == 6){
            $scope.total_fourreau = Prix2coteshauteur(valeur[3]);
          }else if($scope.mettre.val == 7){
            $scope.total_fourreau = Prix2coteshauteur(valeur[4]);
          }else if($scope.mettre.val == 8){
            $scope.total_fourreau = Prix2coteshauteur(valeur[5]);
          }else if($scope.mettre.val == 9){
            $scope.total_fourreau = Prix2coteshauteur(valeur[6]);
          }else if($scope.mettre.val == 10){
            $scope.total_fourreau = Prix2coteshauteur(valeur[7]);
          }else
            $scope.total_fourreau = '0';
        }else
          $scope.total_fourreau = '0';
        if($scope.soudure.val != 0 && $scope.soudure.val != 1){
          $scope.total_fourreau = $scope.total_fourreau-parseInt($scope.prix_soudure);
        }
        $scope.prix_metre_carre = ((($scope.Largeur/100)*($scope.Hauteur/100))*parseInt($scope.prix_carre));
        $scope.prix_total =  parseInt($scope.MontantTotalChute) + parseInt($scope.prix_total_oeillet) + parseInt($scope.prix_total_soudure) + parseInt($scope.prix_total_pose) + parseInt($scope.total_fourreau) + parseInt($scope.prix_total_bat);
        if(parseInt($scope.total_min) > $scope.prix_total){
          $scope.prix_total = $scope.total_min;
          return $scope.total_min;
        }else if(parseInt($scope.total_min) < $scope.prix_total){
          return $scope.prix_total;
        }else{
          $scope.prix_total = $scope.total_min;
          return $scope.total_min;
        }
    };
    $scope.prixTotal = function() {
        if($scope.bat.val == 1 ){
        $scope.prix_total_bat = $scope.Bat;
      }
      if($scope.bat.val == 0 || $scope.bat.val == 2 ){
        $scope.prix_total_bat = 0;
      }
        if($scope.oeillet.val == '1'){
          $scope.prix_total_oeillet = '0';
        }else if($scope.oeillet.val == '2'){
          $scope.prix_total_oeillet= (((($scope.Largeur*2)/25)+(($scope.Hauteur*2)/25))*parseInt($scope.prix_oeillet));
        }else if($scope.oeillet.val == '3'){
          $scope.prix_total_oeillet= (((($scope.Largeur*2)/50)+(($scope.Hauteur*2)/50))*parseInt($scope.prix_oeillet));
        }else if($scope.oeillet.val == '4'){
          $scope.prix_total_oeillet = '0';
        }else if($scope.oeillet.val == '5'){
          $scope.prix_total_oeillet = '0';
        }else if($scope.oeillet.val){
          $scope.prix_total_oeillet = '0';
        }
        if($scope.soudure.val == 2){
              $scope.x = (((($scope.Largeur+parseInt($scope.marge_soudure))+($scope.Largeur+parseInt($scope.marge_soudure))+($scope.Hauteur+parseInt($scope.marge_soudure))+($scope.Hauteur+parseInt($scope.marge_soudure)))/100)*parseInt($scope.prix_soudure));
              $scope.y = (((parseInt($scope.marge_soudure)/100)*($scope.Largeur/100))* parseInt($scope.prix_carre))+(((parseInt($scope.marge_soudure)/100)*($scope.Hauteur/100))*parseInt($scope.prix_carre))+(((parseInt($scope.marge_soudure)/100)*(parseInt($scope.marge_soudure)/100))*parseInt($scope.prix_carre));
              $scope.prix_total_soudure = $scope.x + $scope.y;
        }else if($scope.soudure.val == 3){
              $scope.x = ((($scope.Largeur*2)/100)*parseInt($scope.prix_soudure))+(((parseInt($scope.marge_soudure)/100)*(parseInt($scope.marge_soudure)/100))*parseInt($scope.prix_carre));
              $scope.y = (((parseInt($scope.marge_soudure)/100)*($scope.Hauteur/100))*parseInt($scope.prix_carre));
              $scope.prix_total_soudure = $scope.x+ $scope.y;
        }else if($scope.soudure.val == 4){
              $scope.x = ((($scope.Hauteur*2)/100)*parseInt($scope.prix_soudure))+(((parseInt($scope.marge_soudure)/100)*(parseInt($scope.marge_soudure)/100))*parseInt($scope.prix_carre));
              $scope.y = ((($scope.marge_soudure/100)*($scope.Largeur/100))*$scope.prix_carre);
              $scope.prix_total_soudure = $scope.x + $scope.y;
        }else {
              $scope.prix_total_soudure = '0';
        }
        if($scope.pose.val == 1){
         if($scope.posedepo.val == 1){
            $scope.prix_total_pose = (($scope.Largeur/100)*($scope.Hauteur/100))*parseInt($scope.prix_pose);
            if($scope.prix_total_pose < $scope.prix_min_pose){
             $scope.prix_total_pose = $scope.prix_min_pose;
           }
          }else if($scope.posedepo.val == 2){
            $scope.prix_total_pose1 = (($scope.Largeur/100)*($scope.Hauteur/100))*parseInt($scope.prix_pose);
            if($scope.prix_total_pose1 < $scope.prix_min_pose){
             $scope.prix_total_pose1 = $scope.prix_min_pose;
            }
            $scope.prix_total_pose2 = (($scope.Largeur/100)*($scope.Hauteur/100))*parseInt($scope.prix_depose);
            $scope.prix_total_pose = parseInt($scope.prix_total_pose1) + parseInt($scope.prix_total_pose2);
          }else{
            $scope.prix_total_pose = '0';
          }
        }else{
          $scope.prix_total_pose = '0';
        }
        if($scope.Fourreau.val == 2 ){
          if($scope.mettre.val == 3){
            $scope.total_fourreau = Prixlargeur(valeur[0]);
          }else if($scope.mettre.val == 4){
            $scope.total_fourreau = Prixlargeur(valeur[1]);
          }else if($scope.mettre.val == 5){
            $scope.total_fourreau = Prixlargeur(valeur[2]);
          }else if($scope.mettre.val == 6){
            $scope.total_fourreau = Prixlargeur(valeur[3]);
          }else if($scope.mettre.val == 7){
            $scope.total_fourreau = Prixlargeur(valeur[4]);
          }else if($scope.mettre.val == 8){
            $scope.total_fourreau = Prixlargeur(valeur[5]);
          }else if($scope.mettre.val == 9){
            $scope.total_fourreau = Prixlargeur(valeur[6]);
          }else if($scope.mettre.val == 10){
            $scope.total_fourreau = Prixlargeur(valeur[7]);
          }else
            $scope.total_fourreau = '0';
        }else if($scope.Fourreau.val == 3){
          if($scope.mettre.val == 3){
            $scope.total_fourreau = Prixhauteur(valeur[0]);
          }else if($scope.mettre.val == 4){
            $scope.total_fourreau = Prixhauteur(valeur[1]);
          }else if($scope.mettre.val == 5){
            $scope.total_fourreau = Prixhauteur(valeur[2]);
          }else if($scope.mettre.val == 6){
            $scope.total_fourreau = Prixhauteur(valeur[3]);
          }else if($scope.mettre.val == 7){
            $scope.total_fourreau = Prixhauteur(valeur[4]);
          }else if($scope.mettre.val == 8){
            $scope.total_fourreau = Prixhauteur(valeur[5]);
          }else if($scope.mettre.val == 9){
            $scope.total_fourreau = Prixhauteur(valeur[6]);
          }else if($scope.mettre.val == 10){
            $scope.total_fourreau = Prixhauteur(valeur[7]);
          }else
            $scope.total_fourreau = '0';
        }else if($scope.Fourreau.val == 4){
          if($scope.mettre.val == 3){
            $scope.total_fourreau = Prix2coteslargeur(valeur[0]);
          }else if($scope.mettre.val == 4){
            $scope.total_fourreau = Prix2coteslargeur(valeur[1]);
          }else if($scope.mettre.val == 5){
            $scope.total_fourreau = Prix2coteslargeur(valeur[2]);
          }else if($scope.mettre.val == 6){
            $scope.total_fourreau = Prix2coteslargeur(valeur[3]);
          }else if($scope.mettre.val == 7){
            $scope.total_fourreau = Prix2coteslargeur(valeur[4]);
          }else if($scope.mettre.val == 8){
            $scope.total_fourreau = Prix2coteslargeur(valeur[5]);
          }else if($scope.mettre.val == 9){
            $scope.total_fourreau = Prix2coteslargeur(valeur[6]);
          }else if($scope.mettre.val == 10){
            $scope.total_fourreau = Prix2coteslargeur(valeur[7]);
          }else
            $scope.total_fourreau = '0';
        }else if($scope.Fourreau.val == 5){
          if($scope.mettre.val == 3){
            $scope.total_fourreau = Prix2coteshauteur(valeur[0]);
          }else if($scope.mettre.val == 4){
            $scope.total_fourreau = Prix2coteshauteur(valeur[1]);
          }else if($scope.mettre.val == 5){
            $scope.total_fourreau = Prix2coteshauteur(valeur[2]);
          }else if($scope.mettre.val == 6){
            $scope.total_fourreau = Prix2coteshauteur(valeur[3]);
          }else if($scope.mettre.val == 7){
            $scope.total_fourreau = Prix2coteshauteur(valeur[4]);
          }else if($scope.mettre.val == 8){
            $scope.total_fourreau = Prix2coteshauteur(valeur[5]);
          }else if($scope.mettre.val == 9){
            $scope.total_fourreau = Prix2coteshauteur(valeur[6]);
          }else if($scope.mettre.val == 10){
            $scope.total_fourreau = Prix2coteshauteur(valeur[7]);
          }else
            $scope.total_fourreau = '0';
        }else
          $scope.total_fourreau = '0';
        if($scope.soudure.val != 0 && $scope.soudure.val != 1){
          $scope.total_fourreau = $scope.total_fourreau-parseInt($scope.prix_soudure);
        }
        $scope.prix_metre_carre = ((($scope.Largeur/100)*($scope.Hauteur/100))*parseInt($scope.prix_carre));
        $scope.prix_total =  parseInt($scope.prix_metre_carre) + parseInt($scope.prix_total_oeillet) + parseInt($scope.prix_total_soudure) + parseInt($scope.prix_total_pose) + parseInt($scope.total_fourreau) + parseInt($scope.prix_total_bat);
        if(parseInt($scope.total_min) > $scope.prix_total){
          $scope.prix_total = $scope.total_min;
          return $scope.total_min;
        }else if(parseInt($scope.total_min) < $scope.prix_total){
          return $scope.prix_total;
        }else{
          $scope.prix_total = $scope.total_min;
          return $scope.total_min;
        }
    };
  }]);
function autre_proposition()
{
    var zContenuProposition = $('#divPropose').html() ;
    //DEV103 continuer ajouter panier
    $('#modal-continuer').remove();
    html  = '<div id="modal-continuer" class="modal">';
    html += '  <div class="modal-dialog">';
    html += '    <div class="modal-content">';
    html += '      <div class="modal-body">';
    html += '        <button type="button" class="close" data-dismiss="modal">&times;</button>';
    html += zContenuProposition;
    html += '      </div>';
    html += '      <div class="modal-footer" >';
    html += '      <img style="width: -moz-available;" src="https://images.easyflyer.fr/banniere-imprimerie/roll-up-eco-prix-bas.jpg" alt="impression roll up pas cher"/>';
    html += '      </div>';
    html += '    </div>';
    html += '  </div>';
    html += '</div> ';
    $('body').append(html);
    $('#modal-continuer').modal('show');
    //DEV103 continuer ajouter panier
}
</script>


