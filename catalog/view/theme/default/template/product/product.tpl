<?php echo $header; $display_qte='block';?>
<div class="container">
	<div class="breadcrumbs">
		<ul>
			<?php foreach ($breadcrumbs as $breadcrumb) { ?>
			<li><a href="<?php echo $breadcrumb['href']; ?>" title="" itemprop="url"><span itemprop="title"><?php echo $breadcrumb['text']; ?></span></a>
				<span>&gt; </span>
			</li>
			 <?php } ?>
		</ul>
	</div>
  
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
    

		<!-- SI ADMONEY -->
		<?php if($easycredit == 1) { ?>  
      <div class="row" style="border:solid 1px #e1e1e1">
        <div class="col-sm-8" style="border-right:solid 1px #e1e1e1">
            <div class="col-sm-12" ><h1 class="titre_produit"><?php echo $heading_title; ?></h1></div>
            <?php if ($column_left || $column_right) { ?>
            <?php $class = 'col-sm-6'; ?>
            <?php } else { ?>
            <?php $class = 'col-sm-6'; ?>
            <?php } ?>
            <div class="<?php echo $class; ?>">
              <?php if ($thumb || $images) { ?>
              <ul class="thumbnails">
                <?php if ($thumb) { ?>
                <li><a class="thumbnail" href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>"><img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a></li>
                <?php } ?>
                <?php if ($images) { ?>
                <?php foreach ($images as $image) { ?>
                <li class="image-additional"><a class="thumbnail" href="<?php echo $image['popup']; ?>" title="<?php echo $heading_title; ?>"> <img src="<?php echo $image['thumb']; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a></li>
                <?php } ?>
                <?php } ?>
              </ul>
              <?php } ?>
            </div>
			
            <?php if ($column_left || $column_right) { ?>
            <?php $class = 'col-sm-6'; ?>
            <?php } else { ?>
            <?php $class = 'col-sm-6'; ?>
            <?php } ?>
            <div class="<?php echo $class; ?>">
				<?php echo $description; ?>
				<?php if ($review_status) { ?>
				<div class="rating">
                <hr>
                <!-- AddThis Button BEGIN -->
                <div class="addthis_toolbox addthis_default_style" data-url="<?php echo $share; ?>"><a class="addthis_button_facebook_like" fb:like:layout="button_count"></a> <a class="addthis_button_tweet"></a> <a class="addthis_button_pinterest_pinit"></a> <a class="addthis_counter addthis_pill_style"></a></div>
                <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-515eeaf54693130e"></script>
                <!-- AddThis Button END -->
              </div>
              <?php } ?>
            </div>
		</div>
			<!--DEV101: ajout 3eme colonne-->
			<div class="col-sm-4">
				<?php
				if ($price) { 
				?>
					<ul class="list-unstyled" style="text-align:center">
					<?php if(isset($old_price)){ ?>
						<li><span style="text-decoration: line-through;"><?php echo $old_price; ?></span> HT</li>
				<?php }
				if ($tax) { ?>
                <li class="price-excluding-tax"><h2><span id='spnPrixHT'><?php echo $tax; ?></span> HT</h2></li>
                <?php } ?>
                <?php if (!$special) { ?>
                <?php if($TVA == 1){ ?>
                <li>
                  Soit le <span id="spnPrixTTC"><?php echo $price; ?></span> TTC
                </li>
                <?php } ?>
                <?php } else { ?>
                <li><span style="text-decoration: line-through;"><?php echo $price; ?></span></li>
                <li>
                  <h2><?php echo $special; ?></h2>
                </li>
                <?php }
				if($tax){
				?>
                <li>
                  Ou <span id="admoney"><?php echo $tax; ?></span> admoney <?php var_dump($TVA); ?>
                </li>
                <?php }
				if ($points) { ?>
                <li><?php echo $text_points; ?> <?php echo $points; ?></li>
                <?php } ?>
				</ul>
              <?php } ?>
        
				<div id="product">
				<div class="col-sm-12">
				<div style="padding-left: 35%;" >
				<div class=""><label class="qty-label" for="input-quantity"><?php echo $entry_qty; ?> : </label>
						</div>
				<div class="">
					<input style="    float: left;width: 50px !important;text-align: center;" type="text"  name="quantity" class="form-control input-number" value="1" min="1" max="10000" />
					
					<input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
					<div style="line-height: 0px; float:left;width:40px">
					<button style="  font-size: 11px;  padding: 0px;width: 40px;    border-radius: 3px;" type="button" class="btn btn-default btn-number" data-type="plus" data-field="quantity">
						<span class="glyphicon glyphicon-plus"></span>
					</button>
                
					<button style="  font-size: 11px;  padding: 0px;width: 40px;    border-radius: 3px;    background: #6c6c6e;" type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="quantity">
                    <span class="glyphicon glyphicon-minus"></span>
					</button>
					</div>
				</div>
				<div class="clearfix"></div>
				</div>
    
            <script>
            //plugin bootstrap minus and plus
            //http://jsfiddle.net/laelitenetwork/puJ6G/
            $('.btn-number').click(function(e){
              //e.preventDefault();
              
              fieldName = $(this).attr('data-field');
              type      = $(this).attr('data-type');
              var input = $("input[name='"+fieldName+"']");
              var currentVal = parseInt(input.val());
              if (!isNaN(currentVal)) {
                if(type == 'minus') {
                  
                  if(currentVal > input.attr('min')) {
                    input.val(currentVal - 1).change();
                  } 
                  if(parseInt(input.val()) == input.attr('min')) {
                    $(this).attr('disabled', true);
                  }

                } else if(type == 'plus') {

                  if(currentVal < input.attr('max')) {
                    input.val(currentVal + 1).change();
                  }
                  if(parseInt(input.val()) == input.attr('max')) {
                    $(this).attr('disabled', true);
                  }

                }
              } else {
                input.val(0);
              }
            });
            
            $('.input-number').focusin(function(){
               $(this).data('oldValue', $(this).val());
            });
            $('.input-number').change(function() {
              
              minValue =  parseInt($(this).attr('min'));
              maxValue =  parseInt($(this).attr('max'));
              valueCurrent = parseInt($(this).val());
              
              name = $(this).attr('name');
              if(valueCurrent >= minValue) {
                $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
              } else {
                alert('Sorry, the minimum value was reached');
                $(this).val($(this).data('oldValue'));
              }
              if(valueCurrent <= maxValue) {
                $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
              } else {
                alert('Sorry, the maximum value was reached');
                $(this).val($(this).data('oldValue'));
              }
              
              
            });
            $(".input-number").keydown(function (e) {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
                   // Allow: Ctrl+A
                  (e.keyCode == 65 && e.ctrlKey === true) || 
                   // Allow: home, end, left, right
                  (e.keyCode >= 35 && e.keyCode <= 39)) {
                     // let it happen, don't do anything
                     return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                  e.preventDefault();
                }
              });

            </script>  
          
				<?php 
					if ($review_status) { ?>
                    <div class="rating" style="text-align:center">
                        <p>
                          <?php for ($i = 1; $i <= 5; $i++) { ?>
                          <?php if ($rating < $i) { ?>
                          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                          <?php } else { ?>
                          <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                          <?php } ?>
                          <?php } ?>
                          <a href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;"><?php echo $reviews; ?></a> / <a href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;"><?php echo $text_write; ?></a>
                        </p>
                    </div>
                    <?php } ?>	
				</div>
				</div>
		
			<div style="text-align:left;" class="form-group">
				<div class="clear"></div>
                <button type="button" id="button-cart" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary btn-lg btn-block"><?php echo $button_cart; ?></button>
                
				<ul id="product_page_bloc_livrai_satisf_secu">
                    <li>
                        <span class="camion"><a class="hovered" href="#tab-livraison">Livraison Gratuite</a> partout a Antananarivo</span>
                    </li>
                    <li>
                        <span class="smiley"><a href="#tab-livraison" class="hovered">Satisfait ou remboursé</a> sans condition !</span>
                    </li>
                    <!-- <li> -->
                        <!-- <span class="cadenas"><a href="#tab-livraison" class="hovered">Paiement sécurisé : </a> cartes bleues...</span> -->
                    <!-- </li> -->
					<ul>
                    </ul>
				</ul>
                
			</div>
			<?php if ($minimum > 1) { ?>
			<div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $text_minimum; ?></div>
			<?php } ?>
				</div>
			
        </div>
		<?php }
		// SI ADMONEY -->
		// DEV101 : SI PRODUIT STANDARD-->
		else if($is_surmesure == 0) {?> 
      <div class="row" style="border:solid 1px #e1e1e1">
        <div class="col-sm-8" style="border-right:solid 1px #e1e1e1">
            <div class="col-sm-12" ><h1 class="titre_produit"><?php echo $heading_title; ?></h1></div>
            <?php if ($column_left || $column_right) { ?>
            <?php $class = 'col-sm-6'; ?>
            <?php } else { ?>
            <?php $class = 'col-sm-6'; ?>
            <?php } ?>
            <div class="<?php echo $class; ?>">
              <?php if ($thumb || $images) { ?>
              <ul class="thumbnails">
                <?php if ($thumb) { ?>
                <li><a class="thumbnail" href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>"><img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a></li>
                <?php } ?>
                <?php if ($images) { ?>
                <?php foreach ($images as $image) { ?>
                <li class="image-additional"><a class="thumbnail" href="<?php echo $image['popup']; ?>" title="<?php echo $heading_title; ?>"> <img src="<?php echo $image['thumb']; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a></li>
                <?php } ?>
                <?php } ?>
              </ul>
              <?php } ?>
              
            </div>
            <?php if ($column_left || $column_right) { ?>
            <?php $class = 'col-sm-6'; ?>
            <?php } else { ?>
            <?php $class = 'col-sm-6'; ?>
            <?php } ?>
            <div class="<?php echo $class; ?>">
              <!--<div class="btn-group">
                <button type="button" data-toggle="tooltip" class="btn btn-default" title="<?php //echo $button_wishlist; ?>" onclick="wishlist.add('<?php //echo $product_id; ?>');"><i class="fa fa-heart"></i></button>
                <button type="button" data-toggle="tooltip" class="btn btn-default" title="<?php //echo $button_compare; ?>" onclick="compare.add('<?php //echo $product_id; ?>');"><i class="fa fa-exchange"></i></button>
              </div>-->
              
              <ul class="list-unstyled">
                <?php if ($manufacturer) { ?>
                <li><?php echo $text_manufacturer; ?> <a href="<?php echo $manufacturers; ?>"><?php echo $manufacturer; ?></a></li>
                <?php } ?>
                <li><?php echo $text_model; ?> <?php echo $model; ?></li>
                <?php if ($reward) { ?>
                <li><?php echo $text_reward; ?> <?php echo $reward; ?></li>
                <?php } ?>
                <li><?php echo $text_stock; ?> <?php echo $stock; ?></li>
              </ul>
              
              <?php if ($review_status) { ?>
              <div class="rating">
                <hr>
                <!-- AddThis Button BEGIN -->
                <div class="addthis_toolbox addthis_default_style" data-url="<?php echo $share; ?>"><a class="addthis_button_facebook_like" fb:like:layout="button_count"></a> <a class="addthis_button_tweet"></a> <a class="addthis_button_pinterest_pinit"></a> <a class="addthis_counter addthis_pill_style"></a></div>
                <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-515eeaf54693130e"></script>
                <!-- AddThis Button END -->
              </div>
              <?php } ?>
            </div>
            
            <div class="col-sm-12">
                <div class="product-designer">
                <!--DEV103 Télécharger Model-->
                    <?php if($code_fichier_model): 
                        $class_fichier_model = 'col-sm-6' ;
                    ?>
                    <div class="uploadFichier col-sm-6">
                        <p class="designTitle"><span class="blue">Transférez</span> vos fichiers</p>
                        <p class="designDescri">
                        Vous avez déjà votre visuel ou vous maitrisez les outils professionnels de création ? Transférez votre fichier lors de la dernière étape de la commande après le paiement.
                        </p>
                        <a href="#tab-gabarit" class="btn btn-primary gabdesign" title="Retrouvez tous les éléments nécessaires pour créer votre maquette">
                            Utiliser nos gabarits
                        </a>
                    </div>
                    <?php else : 
                    $class_fichier_model = 'col-sm-12' ;
                        endif; ?>
                    <!--DEV103 Télécharger Model-->
                    <div class="designerBlock <?php echo $class_fichier_model; ?>">
                        <p class="designTitle">Je n'ai pas<span> de visuel</span></p>
                        <p class="designDescri"> Vous trouverez ci-dessous le(s) gabarit(s) et/ou fiche(s) d'information pour la réalisation de la maquette de votre produit. Privilégiez le format PDF HD, AI, PSD, EPS vectorisé en CMJN (quadri) pour une impression optimale de vos documents.<br><br></p> 
            <?php if( $gabarit != null && $gabarit != '' ) {  ?>
            <a class="btn btn-primary gogalerie" href="/index.php?route=tool/upload/download&code=<?php echo $gabarit ; ?>" title="téléchargez nos gabarits ">
              <?php  } else { ?>
              <a class="btn btn-primary gogalerie" href="#tab-gabarit" title="téléchargez nos gabarits ">
              <?php } ?>
                            Utiliser nos gabarits
                        </a>
                      
                    </div>
          <div class="clear"></div>
        </div>
            </div>
            
        </div>
        <!--DEV101: ajout 3eme colonne-->
        <div class="col-sm-4">
              <?php
			  if ($price) { 
			  ?>
			   <ul class="list-unstyled" style="text-align:center">
			   <?php if(isset($old_price)){ ?>
			    <li><span style="text-decoration: line-through;"><?php echo $old_price; ?></span> HT</li>
			  <?php
			   }
				if ($tax) { ?>
                <li class="price-excluding-tax"><h2><span id='spnPrixHT'><?php echo $tax; ?></span> HT</h2></li>
                <?php } ?>
                <?php if (!$special) { ?>
                <?php if($TVA == 1){ ?>
                <li>
                  Soit <span id="spnPrixTTC"><?php echo $price; ?></span> TTC
                </li>
                <?php } ?>
                <?php } else { ?>
                <li><span style="text-decoration: line-through;"><?php echo $price; ?></span></li>
                <li>
                  <h2><?php echo $special; ?></h2>
                </li>
                <?php }
				if($tax){
				?>
                <li>
                  Ou <span id="admoney"><?php echo $tax; ?></span> admoney
                </li>
                <?php }
				if ($points) { ?>
                <li><?php echo $text_points; ?> <?php echo $points; ?></li>
                <?php } ?>
              </ul>
              <?php } ?>
        
        
         <div id="product">
        <div class="col-sm-12">
        
		
        <div style="padding-left: 35%;" >
           <div class=""><label class="qty-label" for="input-quantity"><?php echo $entry_qty; ?> : </label></div>
          
           <?php /* <input type="number" min="0" name="quantity" value="<?php echo $minimum; ?>" size="1" id="input-quantity"/> */ ?>
            <div class="">
            
            
            <input style="    float: left;width: 50px !important;text-align: center;" type="text"  name="quantity" class="form-control input-number" value="1" min="1" max="100000000000" >
            
            <div style="line-height: 0px; float:left;width:40px">
                <button style="  font-size: 11px;  padding: 0px;width: 40px;    border-radius: 3px;" type="button" class="btn btn-default btn-number" data-type="plus" data-field="quantity">
                    <span class="glyphicon glyphicon-plus"></span>
                </button>
                
                <button style="  font-size: 11px;  padding: 0px;width: 40px;    border-radius: 3px;    background: #6c6c6e;" type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="quantity">
                    <span class="glyphicon glyphicon-minus"></span>
                </button>
                
             </div>
            
            
          </div>
              <div class="clearfix"></div>
        
        </div>
    
                  
                
          
          
            
            <script>
            //plugin bootstrap minus and plus
            //http://jsfiddle.net/laelitenetwork/puJ6G/
            $('.btn-number').click(function(e){
              //e.preventDefault();
              
              fieldName = $(this).attr('data-field');
              type      = $(this).attr('data-type');
              var input = $("input[name='"+fieldName+"']");
              var currentVal = parseInt(input.val());
              if (!isNaN(currentVal)) {
                if(type == 'minus') {
                  
                  if(currentVal > input.attr('min')) {
                    input.val(currentVal - 1).change();
                  } 
                  if(parseInt(input.val()) == input.attr('min')) {
                    $(this).attr('disabled', true);
                  }

                } else if(type == 'plus') {

                  if(currentVal < input.attr('max')) {
                    input.val(currentVal + 1).change();
                  }
                  if(parseInt(input.val()) == input.attr('max')) {
                    $(this).attr('disabled', true);
                  }

                }
              } else {
                input.val(0);
              }
            });
            
            $('.input-number').focusin(function(){
               $(this).data('oldValue', $(this).val());
            });
            $('.input-number').change(function() {
              
              minValue =  parseInt($(this).attr('min'));
              maxValue =  parseInt($(this).attr('max'));
              valueCurrent = parseInt($(this).val());
              
              name = $(this).attr('name');
              if(valueCurrent >= minValue) {
                $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
              } else {
                alert('Sorry, the minimum value was reached');
                $(this).val($(this).data('oldValue'));
              }
              if(valueCurrent <= maxValue) {
                $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
              } else {
                alert('Sorry, the maximum value was reached');
                $(this).val($(this).data('oldValue'));
              }
              
              
            });
            $(".input-number").keydown(function (e) {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
                   // Allow: Ctrl+A
                  (e.keyCode == 65 && e.ctrlKey === true) || 
                   // Allow: home, end, left, right
                  (e.keyCode >= 35 && e.keyCode <= 39)) {
                     // let it happen, don't do anything
                     return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                  e.preventDefault();
                }
              });

            </script>  
          
				<?php 
					$show_discount = false;
						
					if(isset($idExemplaire)){
						foreach ($discounts as $discount) {				
							if(isset($discount['product_exemplaire_id']) && $discount['product_exemplaire_id'] == $idExemplaire ){
								$show_discount = true;
								break;
							}							
						}
					}
					if(!empty($discounts) && $show_discount) { ?>
							<ul class="tier-prices product-pricing" >
								<li class="first"><span style="font-size:11px;">Prix dégressifs (Prix HT/Lot)</span></li>
						<?php 	foreach ($discounts as $discount) {
									if(isset($discount['product_exemplaire_id']) && $discount['product_exemplaire_id'] == $idExemplaire ){
									?>
									<li><ul><li><?php echo $discount['quantity']; ?> et +</li><li><span class="price"><?php echo $discount['price']; ?></span></li></ul></li>
							<?php	}
								}	?>
							</ul>
			<?php	}  	?> 
                <!-- prix degressif -->
                  
                  <?php if ($review_status) { ?>
                    <div class="rating" style="text-align:center">
                        <p>
                          <?php for ($i = 1; $i <= 5; $i++) { ?>
                          <?php if ($rating < $i) { ?>
                          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                          <?php } else { ?>
                          <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                          <?php } ?>
                          <?php } ?>
                          <a href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;"><?php echo $reviews; ?></a> / <a href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;"><?php echo $text_write; ?></a>
                        </p>
                    </div>
                    <?php } ?>
                   
        
        </div>
        
        
        
        
        
             
        
          <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
          
          
                  <?php if( isset($idExemplaire) ){?>
                    <input type="hidden" name="exemplaire_id" value="<?php echo $idExemplaire; ?>" />
                  <?php }?>
        
        
        
                <?php if ($options) { 
        ?>
                <hr>
                <div class="options-produits">
                    <h4><?php echo $text_option; ?></h4>
                    <?php foreach ($options as $option) {
        
          ?>
                    <?php if ($option['type'] == 'select') { ?>
          
          
                
      
      <?php    // dev tiana personnaliser l'option taille 
      
      $quantite_input_apparait = 1 ;
      
          if( stripos($option['name'],'taille') !== false )
          {   
          $quantite_input_apparait = 12 ;
          
          ?>
          
          <input type="hidden" name="product_option_id" value="<?php echo $option['product_option_id']; ?>" >
          
          <div class="options-produits-wrapper form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
            
                      <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>">
            <?php echo $option['name']; ?>
            </label>
            <?php foreach ($option['product_option_value'] as $option_value) { ?>
            <input type="hidden" value="<?php echo $option_value['product_option_value_id']; ?>" name="option_value_ids[]" > 
          
            <ul>
            <li>
            <label> <?php echo $option_value['name']; ?> (Quantité souhaitée) </label>
            <input class="form-control" type="text" value="" name="quantite_option_value_<?php echo $option_value['product_option_value_id']; ?>" >
            </li>
            </ul>
             <?php } ?>
            
            
            
                    </div>
            
          <?php   
          }  
          else
          { ?>
            
               <div class="options-produits-wrapper form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                      <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                      <select <?php if (isset($option['is_livraison']) && $option['is_livraison'] == 1) { ?>id="select_option_livraison_zone"<?php } ?> onchange="changeOptionDynamique() ;" name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control">
                        <option value=""><?php echo $text_select; ?></option>
                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                        <option <?php if (isset($option['is_livraison']) && $option['is_livraison'] == 1) { ?>class="option_livraison_zone"<?php } ?> value="<?php echo $option_value['product_option_value_id']; ?>" data-type-zone="<?php echo (isset($option['zone_livraison']) ? $option_value['zone_livraison'] : 0 ) ; ?>" <?php if(isset($option_value['zone_livraison']) && $option_value['zone_livraison'] == 'B'){?>style="display:none;"<?php } ?>><?php echo $option_value['name']; ?>
                        <?php if ($option_value['price']/* && $option_value['product_option_value_id']!=3754 */) { ?>
                        (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                        <?php } else if ($option_value['price_square']) { ?>
                        (<?php echo ' + '.$option_value['price_square'].' /m²'; ?>)
                        <?php }?>
                        </option>
                        <?php } ?>
                      </select>
                    </div>  
            
          <?php  
          }
          
          // fin personnalisation
          
          ?>
      
      
          
          
                    <?php } ?>
                    <?php if ($option['type'] == 'radio') { ?>
                    <div style="text-align:left;" class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                      <label class="control-label"><?php echo $option['name']; ?></label>
                      <div >
                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                        <div class="radio">
                          <label>
                            <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                            <?php if ($option_value['image']) { ?>
                            <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> 
                            <?php } ?>                    
                            <?php echo $option_value['name']; ?>
                            <?php if ($option_value['price']) { ?>
                            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                            <?php } else if ($option_value['price_square']) { ?>
                        (<?php echo ' + '.$option_value['price_square'].' /m²'; ?>)
                        <?php } ?>
                          </label>
                        </div>
                        <?php } ?>
                      </div>
                    </div>
                    <?php } ?>
                    <?php if ($option['type'] == 'checkbox') { ?>
                    <div style="text-align:left;" class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                      <label class="control-label"><?php echo $option['name']; ?></label>
                      <div id="input-option<?php echo $option['product_option_id']; ?>">
                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                            <?php if ($option_value['image']) { ?>
                            <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> 
                            <?php } ?>
                            <?php echo $option_value['name']; ?>
                            <?php if ($option_value['price']) { ?>
                            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                            <?php } else if ($option_value['price_square']) { ?>
                        (<?php echo ' + '.$option_value['price_square'].' /m²'; ?>)
                        <?php } ?>
                          </label>
                        </div>
                        <?php } ?>
                      </div>
                    </div>
                    <?php } ?>
                    <?php if ($option['type'] == 'text') { ?>
                    <div style="text-align:left;" class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                      <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                      <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                    </div>
                    <?php } ?>
                    <?php if ($option['type'] == 'textarea') { ?>
                    <div style="text-align:left;" class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                      <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                      <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"><?php echo $option['value']; ?></textarea>
                    </div>
                    <?php } ?>
                    <?php if ($option['type'] == 'file') { ?>
                    <div style="text-align:left;" class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                      <label class="control-label"><?php echo $option['name']; ?></label>
                      <button type="button" id="button-upload<?php echo $option['product_option_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default btn-block"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
                      <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id']; ?>" />
                    </div>
                    <?php } ?>
                    <?php if ($option['type'] == 'date') { ?>
                    <div style="text-align:left;" class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                      <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                      <div class="input-group date">
                        <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                        <span class="input-group-btn">
                        <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                        </span></div>
                    </div>
                    <?php } ?>
                    <?php if ($option['type'] == 'datetime') { ?>
                    <div style="text-align:left;" class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                      <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                      <div class="input-group datetime">
                        <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                        <span class="input-group-btn">
                        <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                        </span></div>
                    </div>
                    <?php } ?>
                    <?php if ($option['type'] == 'time') { ?>
                    <div style="text-align:left;" class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                      <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                      <div class="input-group time">
                        <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                        <span class="input-group-btn">
                        <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                        </span></div>
                    </div>
                    <?php } ?>
                    <?php } ?>
                </div>
                <div class="clear"></div>
                <?php } ?>
                <?php if ($recurrings) { ?>
                <hr>
                <h3><?php echo $text_payment_recurring; ?></h3>
                <div style="text-align:left;" class="form-group required">
                  <select name="recurring_id" class="form-control">
                    <option value=""><?php echo $text_select; ?></option>
                    <?php foreach ($recurrings as $recurring) { ?>
                    <option value="<?php echo $recurring['recurring_id']; ?>"><?php echo $recurring['name']; ?></option>
                    <?php } ?>
                  </select>
                  <div class="help-block" id="recurring-description"></div>
                </div>
                <?php } ?>
                
                <div style="text-align:left;" class="form-group">
        
        
        
                    <div class="clear"></div>
                    
                  <button type="button" id="button-cart" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary btn-lg btn-block"><?php echo $button_cart; ?></button>
                
                  <ul id="product_page_bloc_livrai_satisf_secu">
                    <li>
                        <span class="camion"><a class="hovered" href="#tab-livraison">Livraison Gratuite</a> partout a Antananarivo</span>
                    </li>
                    <li>
                        <span class="smiley"><a href="#tab-livraison" class="hovered">Satisfait ou remboursé</a> sans condition !</span>
                    </li>
                    <!-- <li> -->
                        <!-- <span class="cadenas"><a href="#tab-livraison" class="hovered">Paiement sécurisé : </a> cartes bleues...</span> -->
                    <!-- </li> -->
                  <ul>
                    </ul></ul>
                
                </div>
                <?php if ($minimum > 1) { ?>
                <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $text_minimum; ?></div>
                <?php } ?>
              </div>
        </div>
        <!--DEV101: ajout 3eme colonne-->
        
      </div>
      <!-- DEV101 : SI PRODUIT STANDARD -->
      
      <!-- DEV101 : SI PRODUIT SUR MESURE -->
      <?php }
      else {

    ?>    
	<input type="hidden" id="chuteLong" value="0" />
	<input type="hidden" id="chuteLarge" value="0" />
      <div class="row">
        <div class="col-sm-12" >
			<h1 class="titre_produit"><?php echo $heading_title; ?></h1>
            <div class="short-description">
				<div class="col-sm-2">
					<img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" height="46px" width="115px" />
				</div>
				<div class="col-sm-10">
					<p><?php echo $meta_description; ?></p>
				</div>
            </div>
        </div>
      </div><br/>
      
      <form id="product">
      <div class="row" style="border:solid 1px #e1e1e1">
        <div class="col-sm-8" style="border-right:solid 1px #e1e1e1;padding-top: 40px;">
            <?php if ($column_left || $column_right) { ?>
            <?php $class = 'col-sm-6'; ?>
            <?php } else { ?>
            <?php $class = 'col-sm-6'; ?>
            <?php } ?>
            <div class="<?php echo $class; ?>">
              <?php if ($thumb || $images) { ?>
              <ul class="thumbnails">
                <?php if ($thumb) { ?>
                <li><a class="thumbnail" href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>"><img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a></li>
                <?php } ?>
                <?php if ($images) { ?>
                <?php foreach ($images as $image) { ?>
                <li class="image-additional"><a class="thumbnail" href="<?php echo $image['popup']; ?>" title="<?php echo $heading_title; ?>"> <img src="<?php echo $image['thumb']; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a></li>
                <?php } ?>
                <?php } ?>
              </ul>
              <?php } ?>
              
            </div>
            <?php if ($column_left || $column_right) { ?>
            <?php $class = 'col-sm-6'; ?>
            <?php } else { ?>
            <?php $class = 'col-sm-6'; ?>
            <?php } ?>
            <div class="<?php echo $class; ?>">
              <h2><?php echo $text_option; ?></h2>
              <input type='hidden' id='prix_maitre_carre' value='<?php echo $prix_maitre_carre ; ?>'/>
              
              <?php if ($options) { ?>
      
        <div class="options-produits">          
					<?php foreach ($options as $option) {
							if($option['name'] == 'Largeur' || $option['name'] == 'Hauteur'){
					?>
						<div style="text-align:left;" class="form-group <?php echo ($option['required'] ? ' required' : ''); ?>">
							<label class="control-label" for="input-option-<?php echo $option['product_option_id'];?>"><?php echo $option['name'];?> (cm)</label>
							<input name="option[<?php echo $option['product_option_id']; ?>]" style="width: 88% !important;display: inline-block;" onchange="calcuMaitreCarre() ;" onkeypress="return isNumberKey(event)" placeholder="<?php echo $option['name'];?> (cm)" id="input-option-<?php echo strtolower( $option['name']);?>" class="form-control" type="text" /> 
							<div class="optionSizeQuestion" id="size-input-option-<?php echo strtolower( $option['name']);?>">
								<span class="questionIco" data-toggle="largeur" data-placement="right" title="Dimensions minimum d'impression <?php echo $largeur_minimum; ?> x <?php echo $hauteur_minimum; ?> cm &#013; Dimensions maximum d'impression <?php echo $largeur_maximum; ?> x <?php echo $hauteur_maximum; ?> cm" >?</span>
							</div>
						</div>
                    <?php }
					}
					
					foreach ($options as $option) {
            
					if( $option['option_id'] == 213 || $option['name'] == 'Largeur' || $option['name'] == 'Hauteur'){
							continue ;
						}
						
					  ?>
					  <?php
					if ($option['type'] == 'select') {
							if($option['name'] == 'Recto Verso'){
							?>
								<div class="options-produits-wrapper form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
								<label class="control-label" for="input-option-<?php echo str_replace(' ','-',$option['name']) ; ?>"><?php echo $option['name']; ?></label>
								<select disabled onchange="changeOptionDynamique() ;" name="option[<?php echo $option['product_option_id']; ?>]" id="input-option-<?php echo str_replace(' ','-',$option['name']) ; ?>" class="form-control">
									<option value=""><?php echo $text_select; ?></option>
									<?php foreach ($option['product_option_value'] as $option_value) { ?>
									<option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
									<?php 
									}
									?>
								</select>
							</div>
							<?php
							}
							else if($option['name'] == 'Diamètre'){
						?>
							<div class="options-produits-wrapper form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
								<label class="control-label" for="input-option-<?php echo str_replace(' ','-',$option['name']) ; ?>"><?php echo $option['name']; ?></label>
								<select onchange="changeDiametre() ;" name="option[<?php echo $option['product_option_id']; ?>]" id="input-option-<?php echo str_replace(' ','-',$option['name']) ; ?>" class="form-control">
									<option value=""><?php echo $text_select; ?></option>
									<?php foreach ($option['product_option_value'] as $option_value) { 
									?>
									<option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
									<?php if ($option_value['price']) { ?>
									(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
									</option>
									<?php } else if ($option_value['price_square']) { ?>
									(<?php echo ' + '.$option_value['price_square'].' /m²'; ?>)
									<?php }
									}
									?>
								</select>
							</div>
						<?php
						}						
						else{ ?>
							<div class="options-produits-wrapper form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
								<label class="control-label" for="input-option-<?php echo str_replace(' ','-',$option['name']) ; ?>"><?php echo $option['name']; ?></label>
								<select onchange="changeOptionDynamique('<?php echo str_replace(' ','-',$option['name']) ; ?>') ;" name="option[<?php echo $option['product_option_id']; ?>]" id="input-option-<?php echo str_replace(' ','-',$option['name']) ; ?>" class="form-control">
									<option value=""><?php echo $text_select; ?></option>
									<?php foreach ($option['product_option_value'] as $option_value) { 
									?>
									<option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
									<?php if ($option_value['price']) { ?>
                    <?php if($option_value['price_prefix']== '+' || $option_value['price_prefix'] == '-')  { ?>
									   (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                    <?php } ?>
									</option>
									<?php } else if ($option_value['price_square']) { 
										if($option['name']!="Quantité"){
									?>
									(<?php echo ' + '.$option_value['price_square'].' /m²'; ?>)
									<?php }
									}
									}
									?>
								</select>
							</div>
					<?php }
						
						if($option['name'] == 'Oeillet'){
						?>
							 <div class="nombre_oeillet" id="nbr_oeillet" style="color:#bb0c1b;margin-top: -12px;"></div>
						<?php
						}if($option['name'] == 'Tendeurs'){
							?>
							 <div class="nombre_tendeur" id="nbr_tendeur" style="color:#bb0c1b;margin-top: -12px;"></div>
						<?php
						}
					} 
					if ($option['type'] == 'radio') { ?>
                    <div style="text-align:left;" class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                      <label class="control-label"><?php echo $option['name']; ?></label>
                      <div id="input-option-<?php echo str_replace(' ','-',$option['name']) ; ?>">
                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                        <div class="radio">
                          <label>
                            <input type="radio" onchange="changeOptionDynamique()" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                            <?php if ($option_value['image']) { ?>
                            <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> 
                            <?php } ?>                    
                            <span id="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?></span>
                            <?php if ($option_value['price']) { ?>
                            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                            <?php } else if ($option_value['price_square']) { ?>
							(<?php echo ' + '.$option_value['price_square'].' /m²'; ?>)
                        <?php } ?>
                          </label>
                        </div>
                        <?php } ?>
                      </div>
                    </div>
                    <?php } ?>
                    <?php if ($option['type'] == 'checkbox') { ?>
                    <div style="text-align:left;" class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                      <label class="control-label"><?php echo $option['name']; ?></label>
                      <div id="input-option-<?php echo str_replace(' ','-',$option['name']) ; ?>">
                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                            <?php if ($option_value['image']) { ?>
                            <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> 
                            <?php } ?>
                            <?php echo $option_value['name']; ?>
                            <?php if ($option_value['price']) { ?>
                            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                            <?php }  else if ($option_value['price_square']) { ?>
							(<?php echo ' + '.$option_value['price_square'].' /m²'; ?>)
                        <?php }?>
                          </label>
                        </div>
                        <?php } ?>
                      </div>
                    </div>
                    <?php } ?>
                    <?php if ($option['type'] == 'text') { 
						if($option['name'] == 'option_dynamique'){
					?>
						<input type='hidden' id="option_dynamique" name="option[<?php echo $option['product_option_id']; ?>]" value='0'/>
					<?php
						}else{
					?>
						<div style="text-align:left;" class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
						  <label class="control-label" for="input-option-<?php echo str_replace(' ','-',$option['name']) ; ?>"><?php echo $option['name']; ?></label>
						  <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option-<?php echo str_replace(' ','-',$option['name']) ; ?>" class="form-control" />
						</div>
					<?php
					}
					}?>
                    <?php if ($option['type'] == 'textarea') { ?>
                    <div style="text-align:left;" class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                      <label class="control-label" for="input-option-<?php echo str_replace(' ','-',$option['name']) ; ?>"><?php echo $option['name']; ?></label>
                      <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>" id="input-option-<?php echo str_replace(' ','-',$option['name']) ; ?>" class="form-control"><?php echo $option['value']; ?></textarea>
                    </div>
                    <?php } ?>
                    <?php if ($option['type'] == 'file') { ?>
                    <div style="text-align:left;" class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                      <label class="control-label"><?php echo $option['name']; ?></label>
                      <button type="button" id="button-upload<?php echo $option['product_option_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default btn-block"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
                      <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id']; ?>" />
                    </div>
                    <?php } ?>
                    <?php if ($option['type'] == 'date') { ?>
                    <div style="text-align:left;" class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                      <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                      <div class="input-group date">
                        <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                        <span class="input-group-btn">
                        <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                        </span></div>
                    </div>
                    <?php } ?>
                    <?php if ($option['type'] == 'datetime') { ?>
                    <div style="text-align:left;" class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                      <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                      <div class="input-group datetime">
                        <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                        <span class="input-group-btn">
                        <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                        </span></div>
                    </div>
                    <?php } ?>
                    <?php if ($option['type'] == 'time') { ?>
                    <div style="text-align:left;" class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                      <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                      <div class="input-group time">
                        <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                        <span class="input-group-btn">
                        <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                        </span></div>
                    </div>
                    <?php } ?>
                    <?php } ?>
                </div>
                <div class="clear"></div>
                <?php } ?>
                
                <?php if ($recurrings) { ?>
                <hr>
                <h3><?php echo $text_payment_recurring; ?></h3>
                <div style="text-align:left;" class="form-group required">
                  <select name="recurring_id" class="form-control">
                    <option value=""><?php echo $text_select; ?></option>
                    <?php foreach ($recurrings as $recurring) { ?>
                    <option value="<?php echo $recurring['recurring_id']; ?>"><?php echo $recurring['name']; ?></option>
                    <?php } ?>
                  </select>
                  <div class="help-block" id="recurring-description"></div>
                </div>
                <?php } ?>
				<div style='display:<?php echo $display_qte;?>'>
                <span><label class="qty-label required" for="input-quantity"><?php echo $entry_qty; ?> : </label></span>
                  <span><input onchange="changeOptionDynamique() ;" type="text" name="quantity" value="<?php echo $minimum; ?>" size="1" id="input-quantity"/></span>
				
                 </div>
				<input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
				<input type="hidden" id="value_maitre_carre" />
				<input type="text" id="value_oeillet" value="<?php echo $prix_unitaire_oeillet; ?>"/>
        <input type="hidden"
				<input type="hidden" id="value_tendeur" />
				<input type='hidden' id="prix_metre_carre_option" />
                  <!-- prix degressif -->
                  <?php if ($discounts) { ?>
                  <ul class="tier-prices product-pricing">
                    <li class="first"><span style="font-size:11px;">Prix dégressifs (Prix HT/Lot)</span></li>
                    <?php foreach ($discounts as $discount) { ?>
                    <li><ul><li><?php echo $discount['quantity']; ?> et +</li><li><span class="price"><?php echo $discount['price']; ?></span></li></ul></li></li>
                    <?php } ?>
                  </ul>
                  <?php } ?>
                  
                <!-- prix degressif -->
              
            </div>
            
        </div>
        <!--DEV101: ajout 3eme colonne-->

        <div class="col-sm-4">
              <?php if ($price) { ?>
              <ul class="list-unstyled" style="text-align:center">
                <?php if ($tax) { ?>
                <li class="price-excluding-tax" id="content_prix_ht" <?php if(  $is_surmesure == 1 ){ echo 'style="display:block;"'; } ?>>
        <h2>
        <span id='spnPrixHT'> <?php echo $tax; ?></span> HT
        </h2>
        </li>
                <?php } ?>
                
                <!--<?php //if (!$special) { ?>
                <li>
                  Soit <span id="spnPrixTTC"><?php //echo $price; ?></span> TTC
                </li>
                <?php //} else { ?>
                <li><span style="text-decoration: line-through;"><?php //echo $price; ?></span></li>
                <li>
                  <h2><?php //echo $special; ?></h2>
                </li>
                <?php //} ?>-->
                
                <?php if ($points) { ?>
                <li><?php echo $text_points; ?> <?php echo $points; ?></li>
                <?php } ?>
              </ul>
              <?php } 
				if($prix_metre_carre_remise!='0.00' && $seuil!='0.00'){ ?>
				<div class="mettreCarrePrice">
					Jusqu'à <span class="price"><?php echo $prix_metre_carre_remise;?></span>  
					<span class="price-no-tax-label">HT</span>
					<span> le m²</span><br>
					<span>(Prix au-delà de <?php echo round($seuil); ?> m²)</span>
				</div>
				<?php } ?>
				<div>
                
                <div style="text-align:left;" class="form-group">
                  
                  <?php if ($review_status) { ?>
                    <div class="rating" style="text-align:center">
                        <p>
                          <?php for ($i = 1; $i <= 5; $i++) { ?>
                          <?php if ($rating < $i) { ?>
                          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                          <?php } else { ?>
                          <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                          <?php } ?>
                          <?php } ?>
                          <a href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;"><?php echo $reviews; ?></a> / <a href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;"><?php echo $text_write; ?></a>
                        </p>
                    </div>
                    <?php } ?>
                    <div class="clear"></div>
                    
                  <button type="button" id="button-cart" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary btn-lg btn-block"><?php echo $button_cart; ?></button>
                
                  <ul id="product_page_bloc_livrai_satisf_secu">
                    <li>
                        <span class="camion"><a class="hovered" href="#tab-livraison">Livraison Gratuite</a> partout a Madagascar*</span>
                    </li>
                    <li>
                        <span class="smiley"><a href="#tab-livraison" class="hovered">Satisfait ou remboursé</a> sans condition !</span>
                    </li>
                    <!-- <li> -->
                        <!-- <span class="cadenas"><a href="#tab-livraison" class="hovered">Paiement sécurisé : </a> cartes bleues...</span> -->
                    <!-- </li> -->
                  <ul>
                    </ul></ul>
                
                </div>
                <?php if ($minimum > 1) { ?>
                <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $text_minimum; ?></div>
                <?php } ?>
              </div>
        </div>
        <!--DEV101: ajout 3eme colonne-->
        
      </div>
      </form>
      
       <?php } ?>
      <!-- DEV101 : SI PRODUIT SUR MESURE -->
      
      <hr>
      
	  <?php if(!$easycredit){?>
      <!--DEV101 : TABULATIONS -->
      
      <!--TAB LIVRAISON-->
      <div class="row" id="tab-livraison">
        <div class="col-sm-12">
            <ul class="nav nav-tabs navigation_product">
                <li class="active"><a href="#tab-livraison" >Livraison et delai</a></li>
                <!-- <li><a href="#tab-video" >video</a></li> -->
                <li><a href="#tab-gabarit" >Gabarit & consigne</a></li>
                <li><a href="#tab-description" ><?php echo $tab_description; ?></a></li>
            </ul>
            
          <div class="tab-content">
            <div class="tab-pane active">
                <h3>100% SATISFAIT OU REMBOURSE SANS CONDITION</h3>
                <div class="cadre-gris col-sm-12" > 
                    <p>
                    L’ensemble de votre commande doit impérativement arriver en temps et en heure et en parfait état.<br>
                    <br>
                    Si celle-ci est retardée ou abimée, vous pourrez nous demander le remboursement de la commande, un avoir ou le retirage dans les meilleurs délais
                    </p>
                </div>
                <img class="img-responsive" id="img_livraison" src="<?php echo HTTP_IMAGE; ?>bandeau-livraison-gratuite-produit-full.jpg" alt="La livraison est gratuite sur toute la france" height="63" width="894">
            </div>
          </div>
        </div>
      </div>
      <!--TAB LIVRAISON-->
      
      <!--TAB VIDEO
      <div class="row" id="tab-video">
        <div class="col-sm-12">
            <ul class="nav nav-tabs navigation_product">
                <li><a href="#tab-livraison" >Livraison et delai</a></li>
                <li class="active"><a href="#tab-video" >video</a></li>
                <li><a href="#tab-gabarit" >Gabarit & consigne</a></li>
                <li><a href="#tab-description" ><?php echo $tab_description; ?></a></li>
            </ul>
            
          <div class="tab-content">
            <div class="tab-pane active">
                <h2>Vidéo de produit : <?php echo $heading_title; ?></h2>
                <div class="ongletImageVideo col-sm-12">
                    <iframe class="youtube-player" type="text/html" src="http://www.youtube.com/embed/<?php echo $video_url; ?>" frameborder="0">
                    </iframe>
                    <p class="text-center">Vidéo non contractuelle (certains éléments sont susceptibles de changer, couleur, structure, taille,..) - Pour plus de détail merci de nous contacter.</p>
        </div>
            </div>
          </div>
        </div>
      </div>
      <!--TAB VIDEO-->
      
      <!--TAB GABARIT-->
      <div class="row" id="tab-gabarit">
        <div class="col-sm-12">
            <ul class="nav nav-tabs navigation_product">
                <li><a href="#tab-livraison" >Livraison et delai</a></li>
                <!--<li ><a href="#tab-video" >video</a></li>-->
                <li class="active"><a href="#tab-gabarit" >Gabarit & consigne</a></li>
                <li><a href="#tab-description" ><?php echo $tab_description; ?></a></li>
            </ul>
            
          <div class="tab-content">
            <div class="tab-pane active">
                <div class="collateral-box">
                    <div class="cadre-gris col-sm-12">
                        <div class="col-sm-1 col-xs-12" style="text-align:center"><a href="<?php echo $zUrlFichierModel ; ?>"><img src="<?php echo HTTP_IMAGE; ?>imprimerie-en-ligne-gabarit-icon.png" class="left" alt="Gabarit carte de visite format classique 8,5 x 5,4 cm" height="72" width="72"></a></div>
                        <div class="col-sm-11 col-xs-12">
                            <p>
                            Vous trouverez ci-dessous le(s) gabarit(s) et/ou fiche(s) d'information pour la réalisation de la maquette de votre produit.
                            Privilégiez le format PDF HD, AI, PSD, EPS vectorisé en CMJN (quadri) pour une impression optimale de vos documents.
                            </p>
              
      
              <?php if( $gabarit != null && $gabarit != '' ) {  ?>
                 <a class="btn btn-primary " href="/index.php?route=tool/upload/download&code=<?php echo $gabarit ; ?>" title="téléchargez nos gabarits ">
              <?php  } else { ?>
                <a class="btn btn-primary " href="#tab-gabarit" title="téléchargez nos gabarits ">
              <?php } ?>
                Téléchargez nos gabarits
                            </a>
                        </div>
                    </div>  
                </div>
            </div>
          </div>
        </div>
      </div>
      <!--TAB GABARIT-->
      
      <!--TAB DESCRIPTION-->
      <div class="row" id="tab-description">
        <div class="col-sm-12">
            <ul class="nav nav-tabs navigation_product">
                <li><a href="#tab-livraison" >Livraison et delai</a></li>
               <!-- <li><a href="#tab-video" >video</a></li>-->
                <li><a href="#tab-gabarit" >Gabarit & consigne</a></li>
                <li class="active"><a href="#tab-description" ><?php echo $tab_description; ?></a></li>
            </ul>
            
          <div class="tab-content">
            
            <div class="tab-pane active"><?php echo $description; ?></div>
          </div>
        </div>
      </div>
      <!--TAB DESCRIPTION-->
      <!--DEV101 : TABULATIONS -->
     <?php }
	 if ($products) { ?>
      <h3><?php echo $text_related; ?></h3>
      <div class="row">
        <?php $i = 0; ?>
        <?php foreach ($products as $product) { ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-xs-8 col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-xs-6 col-md-4'; ?>
        <?php } else { ?>
        <?php $class = 'col-xs-6 col-sm-3'; ?>
        <?php } ?>
        <div class="<?php echo $class; ?>">
          <div class="product-thumb transition">
            <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
            <div class="caption">
              <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
              <p><?php echo $product['description']; ?></p>
              <?php if ($product['rating']) { ?>
              <div class="rating">
                <?php for ($j = 1; $j <= 5; $j++) { ?>
                <?php if ($product['rating'] < $j) { ?>
                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                <?php } else { ?>
                <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                <?php } ?>
                <?php } ?>
              </div>
              <?php } ?>
              <?php if ($product['price']) { ?>
              <p class="price">
                <?php if (!$product['special']) { ?>
                <?php echo $product['price']; ?>
                <?php } else { ?>
                <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                <?php } ?>
                <?php if ($product['tax']) { ?>
                <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                <?php } ?>
              </p>
              <?php } ?>
            </div>
      
      
            <div class="button-group">
              <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');"><span class="hidden-xs hidden-sm hidden-md"><?php echo $button_cart; ?></span> <i class="fa fa-shopping-cart"></i></button>
              <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart"></i></button>
              <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-exchange"></i></button>
            </div>
      
      
      
          </div>
        </div>
        <?php if (($column_left && $column_right) && (($i+1) % 2 == 0)) { ?>
        <div class="clearfix visible-md visible-sm"></div>
        <?php } elseif (($column_left || $column_right) && (($i+1) % 3 == 0)) { ?>
        <div class="clearfix visible-md"></div>
        <?php } elseif (($i+1) % 4 == 0) { ?>
        <div class="clearfix visible-md"></div>
        <?php } ?>
        <?php $i++; ?>
        <?php } ?>
      </div>
      <?php } ?>
		<!--DEV103 continuer ajouter panier-->		
		<div id="divPropose" style="display:none">
			<div class="row">
				<div class="col-sm-12">
					<h3>Ce produit a été ajouté à votre panier :</h3>
					<hr>
					<div class="col-sm-3 image">
						<img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>" class="img-responsive" />
					</div>
					<div class="caption col-sm-9">
						<h4><?php echo $heading_title; ?></h4>
					</div>
				</div>
				<div class="col-sm-12">	
					<div class="col-md-6">
						<a class="suiteAchat" href="<?php echo $continue; ?>">Continuer mes achats</a>
					</div>
					<div class="col-md-6">
						<a class="confirmPanier" href="<?php echo $checkout; ?>">Voir mon panier</a>
					</div>
				</div>
			</div>
			<?php if (!empty($products_propose)) { ?>
			<div class="row">
				<div class="col-sm-12">
					<h3>Nous vous recommandons avec ce produit :</h3>
					<hr>
            <?php foreach ($products_propose as $product) { ?>
				<div class="col-sm-3 image">
					<a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a>
				</div>
				<div class="caption col-sm-9">
					<h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
				<?php if ($product['price']) { ?>
				<p class="price">
					<?php if ($product['tax']) { ?>
					<span class="price-tax"><?php echo $product['tax']; ?> HT</span>
					<?php } ?>
				</p>
				<?php } ?>
					<a class="btn btn-default" href="<?php echo $product['href']; ?>">Voir le produit</a>
				</div>
            <?php } ?>
				</div>
			</div>
			<?php } ?>
      </div>
      <!--DEV103 continuer ajouter panier-->
      <?php if ($tags) { ?>
      <p><?php echo $text_tags; ?>
        <?php for ($i = 0; $i < count($tags); $i++) { ?>
        <?php if ($i < (count($tags) - 1)) { ?>
        <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>,
        <?php } else { ?>
        <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>
        <?php } ?>
        <?php } ?>
      </p>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php if(!isset($idExemplaire)){ 
  $idExemplaire = -1 ;
  }?>;
<script type="text/javascript"><!--
$(document).ready(function(){
    $('[data-toggle="largeur"]').tooltip().on("mouseenter", function () {
        var $this = $(this),
        tooltip = $this.next(".tooltip");
        tooltip.find(".tooltip-inner").css({
            width: "300px",
        });   
    });
    $('[data-toggle="hauteur"]').tooltip();   
});
$('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){
  $.ajax({
    url: 'index.php?route=product/product/getRecurringDescription',
    type: 'post',
    data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
    dataType: 'json',
    beforeSend: function() {
      $('#recurring-description').html('');
    },
    success: function(json) {
      $('.alert, .text-danger').remove();

      if (json['success']) {
        $('#recurring-description').html(json['success']);
      }
    }
  });
});
<!--DEV101 : ANCRE -->
// Pour tous les liens commençant par #.
$("a[href^='#']").click(function (e) {
    var 
        yPos,
        yInitPos,
        target = ($($(this).attr("href") + ":first"));
        
    // On annule le comportement initial au cas ou la base soit différente de la page courante.
    e.preventDefault(); 
    
    yInitPos = $(window).scrollTop();
    
    // On ajoute le hash dans l'url.
    window.location.hash = $(this).attr("href");
    
    // Comme il est possible que l'ajout du hash perturbe le défilement, on va forcer le scrollTop à son endroit inital.
    $(window).scrollTop(yInitPos);
    
    // On cible manuellement l'ancre pour en extraire sa position.
    // Si c'est un ID on l'obtient.
    target = ($($(this).attr("href") + ":first"));

    // Sinon on cherche l'ancre dans le name d'un a.
    if (target.length == 0) {
        target = ($("a[name=" + $(this).attr("href").replace(/#/gi,"") + "]:first"))
    }
    
    // Si on a trouvé un name ou un id, on défile.
    if (target.length == 1) {
        yPos = target.offset().top; // Position de l'ancre.
    
        // On anime le défilement jusqu'à l'ancre.
        $('html,body').animate({ scrollTop: yPos - 40 }, 1000); // On décale de 40 pixels l'affichage pour ne pas coller le bord haut de l'affichage du navigateur et on défile en 1 seconde jusqu'à l'ancre.
    }
});
<!--DEV101 : ANCRE -->
//--></script>
<script type="text/javascript"><!--
function checkPropose(){
    var zSerializeProductId = '' ;
    $('.checkPropose').each(function(){
        if($(this).is(':checked'))
        {
            var iProduitId = $(this).val() ;
            $('#bouton_propose_' + iProduitId).trigger('click') ;
        }
    }) ;
    $('#modal-continuer').modal('hide') ;
}
function autre_proposition()
{
	var zContenuProposition = $('#divPropose').html() ;
    //DEV103 continuer ajouter panier
    $('#modal-continuer').remove();

    html  = '<div id="modal-continuer" class="modal">';
    html += '  <div class="modal-dialog">';
    
    // html += '      <div class="modal-header">';
    
    // html += '      </div>';
	html += '    <div class="modal-content">';
    html += '      <div class="modal-body">';
	html += '        <button type="button" class="close" data-dismiss="modal">&times;</button>';
    html += zContenuProposition;
    html += '      </div>';
    html += '      <div class="modal-footer" >';
    html += '      <img style="width: -moz-available;" src="https://images.easyflyer.fr/banniere-imprimerie/roll-up-eco-prix-bas.jpg" alt="impression roll up pas cher"/>';
    html += '      </div>';
    html += '    </div>';
    html += '  </div>';
    html += '</div> ';

    $('body').append(html);

    $('#modal-continuer').modal('show');

    //DEV103 continuer ajouter panier
}




$('#button-cart').on('click', function() {     
    var sizelargeur = $('#size-input-option-largeur') ;
    var sizehauteur = $('#size-input-option-hauteur'); 
	
	var min_largeur = parseFloat('<?php echo str_replace(" ","",$largeur_minimum); ?>') ;
    var max_largeur = parseFloat('<?php echo str_replace(" ","",$largeur_maximum); ?>') ;
	
    var min_hauteur = parseFloat('<?php echo str_replace(" ","",$hauteur_minimum); ?>') ;
    var max_hauteur = parseFloat('<?php echo str_replace(" ","",$hauteur_maximum); ?>') ;
	
    
    /*dev101 : controle laize min/max*/
    var largeur = $('#input-option-largeur') ;
    var hauteur = $('#input-option-hauteur') ;
    var error = 0 ;
    if(largeur.val() == ''){
		sizelargeur.next().remove();
        sizelargeur.after('<div class="text-danger">Largeur requise</div>');
        $('.text-danger').parent().addClass('has-error');
        error++ ;
    }
    if(hauteur.val() == ''){
		sizehauteur.next().remove();
        sizehauteur.after('<div class="text-danger">Hauteur requise</div>');
        $('.text-danger').parent().addClass('has-error');
        error++ ;
    }
    if(parseFloat(largeur.val()) < min_largeur || parseFloat(largeur.val()) > max_largeur ){
		sizelargeur.next().remove();
		sizelargeur.after('<div class="text-danger">Laize minimum '+min_largeur+' cm et maximum '+max_largeur+' cm</div>');
        $('.text-danger').parent().addClass('has-error');
        error++ ;
    }
    if(parseFloat(hauteur.val()) < min_hauteur || parseFloat(hauteur.val()) > max_hauteur ){
		sizehauteur.next().remove();
		sizehauteur.after('<div class="text-danger">Laize minimum '+min_hauteur+' cm et maximum '+max_hauteur+' cm</div>');
        $('.text-danger').parent().addClass('has-error');
        error++ ;
    }
    if(error > 0){
        return false; 
    }
    /*dev101 : controle laize min/max*/
        
	/*dev101 : controle laize min/max*/
	var largeur = $('#input-option-largeur') ;
	var hauteur = $('#input-option-hauteur') ;
	var face 	= $('#input-option-Face').find(":selected").text() ;
	var recto_verso = $('#input-option-Recto-Verso');
	
	if(largeur.val() == ''){
		sizelargeur.next().remove();
		sizelargeur.after('<div class="text-danger">Largeur requise</div>');
		$('.text-danger').parent().addClass('has-error');
		return false;
	}
	if(hauteur.val() == ''){
		sizehauteur.next().remove();
		sizehauteur.after('<div class="text-danger">Hauteur requise</div>');
		$('.text-danger').parent().addClass('has-error');
		return false;
	}
	if(parseFloat(largeur.val()) < min_largeur || parseFloat(largeur.val()) > max_largeur ){
		sizelargeur.next().remove();
		sizelargeur.after('<div class="text-danger">Laize minimum '+min_largeur+' cm et maximum '+max_largeur+' cm</div>');
		$('.text-danger').parent().addClass('has-error');
		return false;
	}
	if(parseFloat(hauteur.val()) < min_hauteur || parseFloat(hauteur.val()) > max_hauteur ){
		sizehauteur.next().remove();
		sizehauteur.after('<div class="text-danger">Laize minimum '+min_hauteur+' cm et maximum '+max_hauteur+' cm</div>');
		$('.text-danger').parent().addClass('has-error');
		return false;
	}
	/*dev101 : controle laize min/max*/
	if ( face != null ) {
		
		if(face.includes('Double face') && recto_verso.val()==''){
			recto_verso.next().remove();
			recto_verso.after('<div class="text-danger">Recto Verso requis');
			$('.text-danger').parent().addClass('has-error');
			return false;
		}
	}
	
	<?php if($is_surmesure==1){ ?>
		$.ajax({
			url: 'index.php?route=product/product/testOptionDynamique',
			type: 'post',
			data: 'product_id='+'<?php echo $product_id; ?>',
			dataType: 'text',
			success: function(data) {
			},
			async : false ,
				error: function(xhr, ajaxOptions, thrownError) {
					alert("Error testOptionDynamique:"+"\r\n" +thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	<?php } ?>
	$.ajax({
    url: 'index.php?route=checkout/cart/add',
    type: 'post',
    data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
    dataType: 'json',
    beforeSend: function() {
      $('#button-cart').button('loading');
    },
    complete: function() {
      $('#button-cart').button('reset');
    },
    success: function(json) {
      console.log(json);
      $('.alert, .text-danger').remove();
      $('.form-group').removeClass('has-error');
    if (json['error']) {
        if (json['error']['option']) {
          for (i in json['error']['option']) {
            var element = $('#input-option-' + i.replace(/\ /g , '-'));
            if (element.parent().hasClass('input-group')) {
				element.parent().next().remove();
				element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
            } else {
				element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
            }
          }
        }

        if (json['error']['recurring']) {
          $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
        }

        // Highlight any found errors
        $('.text-danger').parent().addClass('has-error');
    } 
    if (json['success']) {
        $('.breadcrumb').after('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

        $('#panier > a').html('<i class="fa fa-shopping-cart fa-3x"></i><span id="panierTotal">' + json['total_produit'] + '</span><p>Mon Panier</p>');

        $('html, body').animate({ scrollTop: 0 }, 'slow');

        $('#panier > ul').load('index.php?route=common/cart/info .wedgetPanier li', function() {
      //DEV103 continuer ajouter panier
        autre_proposition() ;
      //DEV103 continuer ajouter panier
        });
      }

    }
    ,
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
  });
});
//--></script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
  pickTime: false
});

$('.datetime').datetimepicker({
  pickDate: true,
  pickTime: true
});

$('.time').datetimepicker({
  pickDate: false
});

$('button[id^=\'button-upload\']').on('click', function() {
  var node = this;

  $('#form-upload').remove();

  $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

  $('#form-upload input[name=\'file\']').trigger('click');

  if (typeof timer != 'undefined') {
      clearInterval(timer);
  }

  timer = setInterval(function() {
    if ($('#form-upload input[name=\'file\']').val() != '') {
      clearInterval(timer);

      $.ajax({
        url: 'index.php?route=tool/upload',
        type: 'post',
        dataType: 'json',
        data: new FormData($('#form-upload')[0]),
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function() {
          $(node).button('loading');
        },
        complete: function() {
          $(node).button('reset');
        },
        success: function(json) {
          $('.text-danger').remove();

          if (json['error']) {
            $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
          }

          if (json['success']) {
            alert(json['success']);

            $(node).parent().find('input').val(json['code']);
          }
        },
        error: function(xhr, ajaxOptions, thrownError) {
          alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
      });
    }
  }, 500);
});
//--></script>
<script type="text/javascript"><!--
$('#review').delegate('.pagination a', 'click', function(e) {
    e.preventDefault();

    $('#review').fadeOut('slow');

    $('#review').load(this.href);

    $('#review').fadeIn('slow');
});

$('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

$('#button-review').on('click', function() {
  $.ajax({
    url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
    type: 'post',
    dataType: 'json',
    data: $("#form-review").serialize(),
    beforeSend: function() {
      $('#button-review').button('loading');
    },
    complete: function() {
      $('#button-review').button('reset');
    },
    success: function(json) {
      $('.alert-success, .alert-danger').remove();

      if (json['error']) {
        $('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
      }

      if (json['success']) {
        $('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

        $('input[name=\'name\']').val('');
        $('textarea[name=\'text\']').val('');
        $('input[name=\'rating\']:checked').prop('checked', false);
      }
    }
  });
});

$(document).ready(function() {
  $('.thumbnails').magnificPopup({
    type:'image',
    delegate: 'a',
    gallery: {
      enabled:true
    }
  });
    <?php if($options): ?>
        //changeOptionDynamique() ;
    <?php endif ; ?>
});
//DEV103 Livraison
function showOptionLivraison(_zZone)
{
    $('#select_option_livraison_zone').val('') ;
    $('.option_livraison_zone').hide() ;
    $('.option_livraison_zone').each(function(){
        var zZoneOption = $(this).attr('data-type-zone') ;
        if(zZoneOption == _zZone)
        {
            $(this).show() ;
        }
    });
}
//DEV103 Livraison
//DEV103 changement dynamique d'option
function changeOptionDynamique(option_name)
{	
	getPriceSquare(option_name);
	var face = $('#input-option-Face').find(":selected").text() ;
	var quantite = $('#input-option-Quantité').find(":selected").text() ;
	
	var autoriseChute = false;
	var idChuteChecked = $('#input-option-Chute').find(":selected").text();	
	if (idChuteChecked != ''){
		if(idChuteChecked.includes('Calculer chute')){
			console.log("Autorise ok : "+$('#'+idChuteChecked).text());
			autoriseChute = true;
			$('#input-option-Délais-de-livraison').prop('disabled', false);
		}else if(idChuteChecked.includes('Attendre')){
			console.log("Autorise non : "+$('#'+idChuteChecked).text());
			autoriseChute = false;
			$('#input-option-Délais-de-livraison').parent().removeClass('required');
			$('#input-option-Délais-de-livraison').prop('disabled', true);
		}
	}
	
	
	var totalQte = $('#input-quantity').val() ;
	if( face != ''){
		if(face.includes('Double face')){
			$('#input-option-Recto-Verso').parent().addClass('required');
			$('#input-option-Recto-Verso').prop('disabled', false);
		}else{
			$('#input-option-Recto-Verso').parent().removeClass('required');
			$("#input-option-Recto-Verso").find(":selected").prop("selected", false);
			$('#input-option-Recto-Verso').prop('disabled', true);
		}
	}
	
	calculNombreOeillet();
	calculNombreTendeurs();
	
	var chute = 0;
	if(autoriseChute){
		chute = calculChute();
	}
	
    var idExemplaire = <?php echo $idExemplaire; ?>;
    var iProduitId = <?php echo $product_id ; ?> ;
    var zSerializeOptionId = '' ;
    $('.options-produits-wrapper select').each(function(){
        var iOptionId = ($(this).val()) ? $(this).val() : 0 ;
        zSerializeOptionId += (zSerializeOptionId != '') ? '_' + iOptionId : iOptionId ;
    }) ;
    
    /* dev101 : changer dynamique prix oeillet / calcul prix maitre carre */
    var value_oeillet           = $('#value_oeillet').val() ;
    var value_tendeur           = $('#value_tendeur').val() ;
    var value_maitre_carre      = parseFloat($('#value_maitre_carre').val());
    var prix_unitaire_oeillet   = '<?php echo $prix_unitaire_oeillet ; ?>' ;
    var prix_unitaire_tendeur   = '<?php echo $prix_unitaire_tendeur ; ?>' ;
    var prix_maitre_carre       =  $('#prix_maitre_carre').val() ;
    var prix_metre_carre_option =  $('#prix_metre_carre_option').val() ;
    var seuil       = '<?php echo $seuil ; ?>' ;
    var prix_metre_carre_remise = parseFloat('<?php echo $prix_metre_carre_remise ; ?>'.replace(",","").match(/\d+/));
    var total_prix_oeillet      = 0 ;
    var total_prix_tendeur      = 0 ;
    var total_prix_maitre_carre = 0 ;
    var total_prix_chute = 0 ;
    var qte = 0 ;
	
  if(value_oeillet > 0){
      total_prix_oeillet = parseFloat(prix_unitaire_oeillet) * value_oeillet ;
    }
	if(value_tendeur > 0){
        total_prix_tendeur = parseFloat(prix_unitaire_tendeur) * value_tendeur ;
    }
	
	if(parseInt(quantite) > 0){
		qte = parseInt(quantite);
	}
	if(parseInt(totalQte) > 0){
		qte = parseInt(totalQte);
	}
	
    if(value_maitre_carre > 0 ){
        total_prix_maitre_carre = parseFloat(prix_maitre_carre) * value_maitre_carre ;
        if( prix_metre_carre_remise>0 && parseFloat(value_maitre_carre) > parseFloat(seuil) || parseFloat(value_maitre_carre) == parseFloat(seuil)){
            total_prix_maitre_carre = prix_metre_carre_remise * value_maitre_carre;
            // console.log("prix_metre_carre_remise=>"+prix_metre_carre_remise+" * value_maitre_carre=>"+value_maitre_carre+" = total_prix_maitre_carre=>"+total_prix_maitre_carre) ;
        }
    }
	if(chute > 0){		
		total_prix_chute = parseFloat(prix_maitre_carre) * chute ;
		console.log("total_prix_chute ="+ parseFloat(prix_maitre_carre) +"*"+chute+"="+total_prix_chute);
	}
	
	if(prix_metre_carre_option > 0 ){	
        total_prix_maitre_carre = total_prix_maitre_carre + (parseFloat(prix_metre_carre_option) * value_maitre_carre) ;
    }
	var option_dynamique = total_prix_maitre_carre+total_prix_tendeur+total_prix_oeillet;
	$('#option_dynamique').val(option_dynamique);
	
	// montrer valeur multiplié par la qte)
	if(qte > 0){		
		// Forex PVC Expansé //
		if($('input[name="product_id"]').val()==295){
			var largeur = $('#input-option-largeur').val() ;
			var hauteur = $('#input-option-hauteur').val();
			var largeur_max = parseFloat('<?php echo str_replace(" ","",$largeur_maximum)!=''?str_replace(" ","",$largeur_maximum):'0'; ?>');			
			var hauteur_max =  parseFloat('<?php echo str_replace(" ","",$hauteur_maximum)!=''?str_replace(" ","",$hauteur_maximum):'0'; ?>');	
			// console.log('/// PVC ///');
			// console.log('largeur='+largeur+' longueur='+hauteur+' largeur_max='+largeur_max+' longueur_max='+hauteur_max);
			
			var qte_possible = parseInt(( largeur_max / largeur ) * (hauteur_max / hauteur));
			var qte_possible_long = parseInt(( hauteur_max / largeur ) * (largeur_max / hauteur));
			if(qte_possible_long > qte_possible){
				qte_possible = qte_possible_long;
			}
			
			// console.log('qte_possible='+qte_possible);	
			var nbr_plaque_necessaire =  Math.ceil (qte / qte_possible ) ;
			// console.log('nbr_plaque_necessaire='+nbr_plaque_necessaire);	
			total_prix_maitre_carre = parseFloat('<?php echo $prix_plaque; ?>') * nbr_plaque_necessaire;
			// console.log('/// PVC ///');
		}
		// Forex PVC Expansé //
		
		// Sticker individuel rectangle //
		if($('input[name="product_id"]').val()==597){
			var decoupe = $('#input-option-Découpe').find(":selected").text();	
			if (decoupe != ''){
				if(decoupe.includes('Traçage')){
					largeur = parseFloat($('#input-option-largeur').val())+1 ;
					hauteur = parseFloat($('#input-option-hauteur').val())+1;	
					var surfaceMaitreCarre = (largeur * hauteur) / 10000 ;
					$('#value_maitre_carre').val(surfaceMaitreCarre);
					value_maitre_carre = surfaceMaitreCarre;
					console.log("new surfaace"+value_maitre_carre);
					total_prix_maitre_carre = parseFloat(prix_maitre_carre) * value_maitre_carre ;					
				}else if(decoupe.includes('Découpe complète')){
					largeur = parseFloat($('#input-option-largeur').val())+8 ;
					hauteur = parseFloat($('#input-option-hauteur').val())+8;	
					var surfaceMaitreCarre = (largeur * hauteur) / 10000 ;
					$('#value_maitre_carre').val(surfaceMaitreCarre);
					value_maitre_carre = surfaceMaitreCarre;
					console.log("new surfaace"+value_maitre_carre);
					total_prix_maitre_carre = parseFloat(prix_maitre_carre) * value_maitre_carre ;
				}
			}
			var listeLaize = '<?php echo $laize; ?>' ;
			listeLaize = listeLaize.split(";");
			for(var i=0;i<listeLaize.length;i++){
				var qte_possible = parseInt(( listeLaize[i] / largeur ) * (hauteur / hauteur));
				// console.log('qte_possible='+qte_possible);	
				var nbr_vynile_necessaire =  Math.ceil (qte / qte_possible ) ;
				// console.log('nbr_vynile_necessaire='+nbr_vynile_necessaire);
				var recChute  = (nbr_vynile_necessaire * qte_possible) - qte;
				// console.log(listeLaize[i]+' recChute='+recChute);
				// console.log('///////////////////////');
				
				if(recChute <= chute){
					chute=recChute;
				}
			}
			console.log('chute='+chute);
			
			total_prix_chute = total_prix_maitre_carre * chute ;
		}
		// Sticker individuel rectangle //
		total_prix_maitre_carre = total_prix_maitre_carre * qte;
	}	
    /* dev101 : changer dynamique prix oeillet / calcul prix maitre carre *//*ajout des nombres des exemplaire +'&idexemplaire='+idExemplaire,*/
    $.ajax({
    url: 'index.php?route=product/product/changeOptionDynamique',
    type: 'post',
    data: 'product_id=' + iProduitId + '&serialize_option_id='+ zSerializeOptionId+'&total_prix_oeillet='+total_prix_oeillet+'&total_prix_maitre_carre='+total_prix_maitre_carre+'&idexemplaire='+idExemplaire+'&total_prix_tendeur='+total_prix_tendeur+'&value_metre_carre='+value_maitre_carre+'&qte='+qte+'&total_prix_chute='+total_prix_chute,
    dataType: 'text',
    success: function(data) {
		console.log(data) ;
		var tzPrixAdd = new Array() ;
		tzPrixAdd = data.split('_') ;
		$('#spnPrixHT').html(tzPrixAdd[0]) ;
		$('#admoney').html(tzPrixAdd[0]) ;
		if(parseFloat(tzPrixAdd[2]) > 0){$('#content_prix_ht').css('display','block') ;}
		$('#spnPrixTTC').html(tzPrixAdd[1]) ;
    },
        async : false ,
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
  });
}

function calcuMaitreCarre(){
  
    $('.text-danger').parent().removeClass('has-error');
    $('.text-danger').remove();
	
    var largeur = $('#input-option-largeur') ;
    var sizelargeur = $('#size-input-option-largeur') ;
    var hauteur = $('#input-option-hauteur'); 
    var sizehauteur = $('#size-input-option-hauteur'); 
	
	var min_largeur = parseFloat('<?php echo str_replace(" ","",$largeur_minimum); ?>') ;
    var max_largeur = parseFloat('<?php echo str_replace(" ","",$largeur_maximum); ?>') ;
	
    var min_hauteur = parseFloat('<?php echo str_replace(" ","",$hauteur_minimum); ?>') ;
    var max_hauteur = parseFloat('<?php echo str_replace(" ","",$hauteur_maximum); ?>') ;
	
	if(parseFloat(largeur.val()) < min_largeur || parseFloat(largeur.val()) > max_largeur ){
		sizelargeur.next().remove();
        sizelargeur.after('<div class="text-danger">Laize minimum '+min_largeur+' cm et maximum '+max_largeur+' cm</div>');
        $('.text-danger').parent().addClass('has-error');
        return false;
    }
	
    if(parseFloat(hauteur.val()) < min_hauteur || parseFloat(hauteur.val()) > max_hauteur ){
		sizehauteur.next().remove();
        sizehauteur.after('<div class="text-danger">Laize minimum '+min_hauteur+' cm et maximum '+max_hauteur+' cm</div>');
        $('.text-danger').parent().addClass('has-error');
       return false;
    }
	
	largeur = parseFloat($('#input-option-largeur').val()) ;
    hauteur = parseFloat($('#input-option-hauteur').val());	
	var surfaceMaitreCarre = (largeur * hauteur) / 10000 ;
	$('#value_maitre_carre').val(surfaceMaitreCarre);
	console.log("surfaaaaaace="+$('#value_maitre_carre').val()) ;
	changeOptionDynamique() ;  
}

function calculNombreOeillet() {

	var largeur = parseFloat($('#input-option-largeur').val()) ;
	var hauteur = parseFloat($('#input-option-hauteur').val()) ;
	var oeillet = $('#input-option-Oeillet').find(":selected").text();
 //  console.log(oeillet);
	var quantite = $('#input-quantity').val() ;
	var nb_oeillet = 0 ;
	if( oeillet != '' || oeillet != 1 ){
		if(oeillet.includes("25")){
            nb_oeillet = parseInt(((largeur*2) + (hauteur*2)) / 25) ;
		}
		if(oeillet.includes("50")){
			nb_oeillet = parseInt(((largeur*2) + (hauteur*2)) / 50) ;
		}
		if(oeillet.includes("100")){
			nb_oeillet = parseInt(((largeur*2) + (hauteur*2)) / 100) ;
		}
	}
	nb_oeillet = nb_oeillet * quantite;
	if(nb_oeillet != '' && nb_oeillet > 0 && nb_oeillet < 4) { nb_oeillet = 4;}
	if(nb_oeillet%2 != 0) { nb_oeillet = nb_oeillet+1 ; }

	if(nb_oeillet != '' && nb_oeillet > 0){
       $('#nbr_oeillet').html('Soit un total de '+nb_oeillet+' oeillets') ;
       $('#value_oeillet').val(nb_oeillet);
	      console.log("nbr_oeillet="+$('#value_oeillet').val());
       $('#nbr_oeillet').css('display','block');
 //      changeOptionDynamique() ;
	}
	else{
       $('#nbr_oeillet').css('display','none');
       $('#value_oeillet').val(0);
      // changeOptionDynamique() ;
	}
}

function calculNombreTendeurs() {

	var largeur = parseFloat($('#input-option-largeur').val()) ;
	var hauteur = parseFloat($('#input-option-hauteur').val()) ;
	var tendeur = $('#input-option-Tendeurs').find(":selected").text();
	var nb_tendeur = 0 ;
	var quantite = $('#input-quantity').val() ;
	if( tendeur != '' || tendeur != 1 ){
		if(tendeur.includes("25")){
            nb_tendeur = parseInt(((largeur*2) + (hauteur*2)) / 25) ;
		}
		if(tendeur.includes("50")){
			nb_tendeur = parseInt(((largeur*2) + (hauteur*2)) / 50) ;
		}
		if(tendeur.includes("100")){
			nb_tendeur = parseInt(((largeur*2) + (hauteur*2)) / 100) ;
		}
	}
	nb_tendeur = nb_tendeur*quantite;
	if(nb_tendeur != '' && nb_tendeur > 0 && nb_tendeur < 4) { nb_tendeur = 4;}
	if(nb_tendeur%2 != 0) { nb_tendeur = nb_tendeur+1 ; }

	if(nb_tendeur != '' && nb_tendeur > 0){
       $('#nbr_tendeur').html('Soit un total de '+nb_tendeur+' tendeurs') ;
       $('#value_tendeur').val(nb_tendeur);
	      console.log("nbr_tendeur="+$('#value_tendeur').val());
       $('#nbr_tendeur').css('display','block');
 //      changeOptionDynamique() ;
	}
	else{
       $('#nbr_tendeur').css('display','none');
       $('#value_tendeur').val(0);
      // changeOptionDynamique() ;
	}
}

function isNumberKey(evt) {
	var charCode = (evt.which) ? evt.which : evt.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57)) {
		return false;
	} else {
		return true;
	}
}

function changeDiametre(){
	var diametre = $('#input-option-Diamètre').find(":selected").text();
	diametre = parseInt(diametre);
	var largeur = diametre ;
    var hauteur = diametre; 
	var surfaceMaitreCarre = (largeur * hauteur) / 10000 ;
	$('#value_maitre_carre').val(surfaceMaitreCarre);
	console.log("surface="+surfaceMaitreCarre+" > "+$('#value_maitre_carre').val()) ;
	changeOptionDynamique('Diamètre') ;
}

function getPriceSquare(option_name)
{
	var iProduitId = '<?php echo $product_id; ?>';
	var iOptionId = $('#input-option-'+option_name).val();
	$.ajax({
    url: 'index.php?route=product/product/getPriceSquare',
    type: 'post',
    data: 'product_id=' + iProduitId+'&iOptionId='+iOptionId,
    dataType: 'text',
    success: function(data) {
		if( data!=0 ){
			$('#prix_metre_carre_option').val(data);	
		}
    },
        async : false ,
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
	});
}

function calculChute(){
	var largeur = $('#input-option-largeur').val() ;
	var longueur = $('#input-option-hauteur').val() ;
	var qte = $('#input-quantity').val() ;
	var listeLaize = '<?php echo $laize; ?>' ;
	
	var chute = 0;var chuteLong = 0;
	if(largeur!='' && longueur!=''){
		
		$.ajax({
		url: 'index.php?route=product/product/getChute',
		type: 'post',
		data: 'listeLaize='+listeLaize+'&large='+largeur+'&long='+longueur+'&qte='+qte,
		dataType: 'text',
		success: function(data) {
			$('#chuteLarge').val(data);
		},
			async : false ,
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});	
		
		$.ajax({
		url: 'index.php?route=product/product/getChute',
		type: 'post',
		data: 'listeLaize='+listeLaize+'&large='+longueur+'&long='+largeur+'&qte='+qte,
		dataType: 'text',
		success: function(data) {
			$('#chuteLong').val(data);
		},
			async : false ,
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
		
		if($('#chuteLong').val()!='' && $('#chuteLarge').val()!=''){
			chuteLong = parseFloat($('#chuteLong').val());
			chute = parseFloat($('#chuteLarge').val());
			console.log("chuteLong = "+chuteLong);
			console.log("chuteLarge = "+chute);
			if(chuteLong<chute){
				chute=chuteLong;
			}
		}else if($('#chuteLong').val()=='' && $('#chuteLarge').val()!=''){
			console.log("chuteLarge");			
			chute = parseFloat($('#chuteLarge').val());	
		}else if($('#chuteLarge').val()=='' && $('#chuteLong').val()!=''){
			console.log("chuteLong");			
			chute = parseFloat($('#chuteLong').val());	
		}
	}	

	console.log("chute = "+chute);
	return chute;
}


//DEV103 changement dynamique d'option
//--></script>
<?php echo $footer; ?>
