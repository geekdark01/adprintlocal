 app.controller("myCtrl", function($scope) {
    $scope.oeillet = $('#input-option-Oeillet').find(":selected").text();
    $scope.prix_oeillet = '<?php echo ($prix_unitaire_oeillet) ; ?>';
    $scope.prix_carre = '<?php echo ($prix_metre_carre) ; ?>';
    $scope.Largeur = '';
    $scope.Hauteur= '';
    $scope.prix_total_oillet = '';
    $scope.prix_metre_carre = '';
    $scope.prixTotal = function() {
        if( $scope.oeillet != '' || $scope.oeillet != 1 ){
          if($scope.oeillet.includes("25")){
            $scope.prix_total_oillet = (((($scope.Largeur*2)/25)+(($scope.Hauteur*2)/25))*$scope.prix_oeillet);
          }
          if($scope.oeillet.includes("50")){
            $scope.prix_total_oillet = (((($scope.Largeur*2)/50)+(($scope.Hauteur*2)/50))*$scope.prix_oeillet);
          }else{
            $scope.prix_total_oillet = '';
          }
        }
        $scope.prix_metre_carre = ((($scope.Largeur/100)*($scope.Hauteur/100))*$scope.prix_carre);
        $scope.prix_total = $scope.prix_metre_carre+$scope.prix_total_oillet;
        return $scope.prix_total;
    };
  }); 