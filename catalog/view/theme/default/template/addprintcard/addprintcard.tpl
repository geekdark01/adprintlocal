<?php echo $header; ?>


<div class="container">
	<p><?php echo $msg_solde ?> <strong><?php echo $solde ;?></strong></p>
	<p><strong><?php echo $text_historique ;?></strong></p>
	<p>Vos dépenses:</p>
		<table class="table table-bordered">
			<thead>
				<tr>
					<td><?php echo $text_motant_title;?></td>
					<td><?php echo $text_date_title;?></td>
					<td>Type crédit</td>
				</tr>
			</thead>
				
			<tbody>
				<?php if(count($depenses)>0){
					for($i=0;$i<count($depenses);$i++){ 
					?>
						<tr>
							<td><?php echo $depenses[$i]['montants'] ;?></td>
							<td><?php echo $depenses[$i]['date_action'] ;?></td>
							<td><?php echo $depenses[$i]['type'] ;?></td>
						</tr>
					<?php
					}
				}?>
			</tbody>
		</table>
	<p>Vos ajouts:</p> 
	<table class="table table-bordered">
			<thead>
				<tr>
					<td><?php echo $text_motant_title;?></td>
					<td><?php echo $text_date_title;?></td>
					<td>Type crédit</td>
				</tr>
			</thead>
				
			<tbody>
				<?php if(count($ajouts)>0){
					for($i=0;$i<count($ajouts);$i++){ 
					?>
						<tr>
							<td><?php echo $ajouts[$i]['montants'] ;?></td>
							<td><?php echo $ajouts[$i]['date_action'] ;?></td>
							<td><?php echo $ajouts[$i]['type'] ;?></td>
						</tr>
					<?php
					}
				}?>
			</tbody>
		</table>
</div>

<?php echo $footer; ?>