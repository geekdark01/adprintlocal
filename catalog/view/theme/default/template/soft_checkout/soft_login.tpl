<div class="row">
  <div class="col-sm-4">
    <div class=" connexion-form cart-login-form-box">
      <h3 style="margin-top: 0px; "><?php echo $text_returning_customer; ?></h3>
      <!--p><?php echo $text_i_am_returning_customer; ?></p-->
      <div class="pull-left form-group required connexion-form-content">

        <label class="control-label" for="input-email"><?php echo $entry_email; ?></label>
        <input type="text" name="email" value="" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" />
      </div>
      <div class="pull-left form-group  required connexion-form-content">
        <label class="control-label" for="input-password"><?php echo $entry_password; ?></label>
        <input type="password" name="password" value="" placeholder="<?php echo $entry_password; ?>" id="input-password" class="form-control" />
      </div>
      <div class="buttons-set" style="position: center; margin-left: 40px;">
        <input type="button" data-loading-text="Chargement..." value="Connexion" id="button-login" class="boutton2 boutton2-orange" />        
      </div>
      <div class=" row form-group   connexion-form-content">
          <i class="f-left forgot-password"><a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a></i>
          <!--Add text *Champ obligatoire-->
          <p class="pull-right registerquired">* Champ obligatoires</p>
      </div>
    </div>
  </div>
  &nbsp;
  <div class="col-sm-4">
    <div class="register-form cart-login-form-box">
      <h3 style="margin-top: 0px;"><?php echo $text_new_customer; ?></h3>
      <div class="register-form-content">
          <p><strong>Gagnez du temps !</strong><br>
          Enregistrez-vous et profitez de nombreux avantages :</p>
          <ul class="account-advantages">
            <li>Commandez plus vite et plus facilement</li>
            <li>Consultez et suivez vos commandes</li>
          </ul>
      </div>
      <div class="input-box">
      <div class="buttons-set" style="position: center; margin-left: 40px;">
            <input type="radio" name="account" value="guest" checked="checked" class="hidden" />
      <input type="button" value="S'enregistrer" id="button-account" class="boutton2 boutton2-pink" />
          </div>
      </div>
    </div>
  </div>

<!--Table Panier-->
<div class="col-sm-4">   
    <div><?php echo $soft_panier; ?></div>
</div>



</div>
