<?php echo $header; ?>
<script src="catalog/view/javascript/gestionfichier.js" type="text/javascript"></script>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
        <h2><?php echo $text_gestion_devis ; ?></h2>
        <div class="row">
            <div class="table-responsive">
                <?php if(count($toDevis) < 1): ?>
                    <?php echo $text_vide ; ?>
                <?php else : ?>
                    
                    <table class="table table-bordered table-hover content_order">
                        <thead>
                            <tr>
                                <th>
                                    Ref Order
                                </th>
                                <th>
                                    Fichier
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($toDevis as $toDevis): ?>
                                <tr>
                                    <td>
                                        <?php echo $toDevis['order_id'] ;  ?>
                                    </td>
                                    <td>
                                        <a href="<?php echo $toDevis['devis_fichier'] ; ?>">
                                            <?php echo $toDevis['devis'] ;  ?>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach ; ?>
                        </tbody>
                    </table>
                <?php endif ; ?>
            </div>
        </div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?> 