<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd">
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	</head>
	<body style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000!important;">
		<div style="width: 680px;">
			<a href="<?php echo $store_url; ?>" title="<?php echo $store_name; ?>">
				<img src="<?php echo $logo; ?>" alt="<?php echo $store_name; ?>" style="margin-bottom: 20px; border: none;width: 100%;" />
			</a>
			<p>Cher <?php echo $firstname." ".$lastname?>,</p>
			<p>Votre filleul <?php echo $filleul['firstname'].' '.$filleul['lastname']; ?> a effectué une commande. Vous allez recevoir <b><?php echo $pourc; ?>%</b> du CA HT de la commande.</p>
		</div>
	</body>
</html>