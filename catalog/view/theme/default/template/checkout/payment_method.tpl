
<?php if ($error_warning) { ?>
<div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
<?php } ?>
<?php if ($payment_methods) { ?>
    <div class="sp-methods-choose entry-title">Choisir un mode de paiement</div>
    <div class="sp-methods-info pink">
        <label>Je souhaite commander en indiquant le mode de paiement :</label>
    </div>
    <div class="sp-methods-info-sous-info">
      <label>Votre commande sera traitée à réception de votre paiement</label>
    </div>


<div class="row">  
	<div class="col-sm-8"> 
    <div class="clear"></div>
    <table class="sp-methods-table" cellspacing="0" cellpadding="0" border="0">
      <?php if($_SESSION['var_total'] == 0) { ?>
			<?php foreach ($payment_methods as $payment_method) { ?>	
       <?php  if($payment_method['code'] != 'admoney' && $payment_method['code'] != 'Admoney'){ ?>
			<tr>        
				<td>
					<label>
						  <?php if ($payment_method['code'] == $code || !$code) { ?>
						  <?php $code = $payment_method['code']; ?>
						  <input type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>" checked="checked" />
						  <?php }else{  ?>						  
              <input type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>"/>
            <?php } ?>
						  <?php echo $payment_method['title']; ?>
					</label>
				</td>      
			</tr>
         <?php } ?>
      <?php } ?>
    <?php }else{  ?>
      <?php foreach ($payment_methods as $payment_method) { ?>  
        <?php  if($payment_method['code'] == 'admoney' && $payment_method['title'] == 'Admoney'){ ?>
      <tr>        
        <td>
          <label>
              <?php if ($payment_method['code'] == $code || !$code) { ?>
              <?php $code = $payment_method['code']; ?>
              <input type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>" checked="checked" />
              <?php }  ?>             
              <?php echo $payment_method['title']; ?>
          </label>
        </td>      
      </tr>
      <?php } ?>
    <?php } ?>
  <?php } ?>
	</table>
<?php } ?>
        
        <!--
      <tr>
        <td>
          <div>
                                    <input id="p_method_paymentvirement" value="paymentvirement" name="payment[method]" title="Virement bancaire" onclick="payment.switchMethod('paymentvirement')" class="checkbox" autocomplete="off" type="checkbox">
                                    <label for="p_method_paymentvirement" class="paymentvirement p_method_label">
                                            <div class="image">
                            <img class="p_method_img" src="http://localhost/adprint_opencart/upload/catalog/view/theme/default/image/virement-bancaire.png">
                        </div>
                        <div>
                                                        <span class="p_method_title">Virement bancaire</span>
                                                    </div>
                                                                </label>
                </div>
          </td>
      </tr>
      <tr>
        <td>
          <div>
                                    <input id="p_method_be2bill_several" value="be2bill_several" name="payment[method]" title="Paiement par carte" onclick="payment.switchMethod('be2bill_several')" class="checkbox" autocomplete="off" type="checkbox">
                                    <label for="p_method_be2bill_several" class="be2bill_several p_method_label">
                                            <div class="image">
                            <img class="p_method_img" src="http://localhost/adprint_opencart/upload/catalog/view/theme/default/image/3xsansfrais.png">
                        </div>
                        <div>
                                                        <span class="p_method_title">Paiement par carte</span>
                            <span>en 3 fois sans frais</span>                        </div>
                                                                </label>
                </div>
        </td>
      </tr>
      <tr>
        <td>
          <div>
                                    <input id="p_method_checkmo" value="checkmo" name="payment[method]" title="Chèque" onclick="payment.switchMethod('checkmo')" class="checkbox" autocomplete="off" type="checkbox">
                                    <label for="p_method_checkmo" class="checkmo p_method_label">
                                            <div class="image">
                            <img class="p_method_img" src="http://localhost/adprint_opencart/upload/catalog/view/theme/default/image/cheque.png">
                        </div>
                        <div>
                                                        <span class="p_method_title">Chèque</span>
                                                    </div>
                                                                </label>
                </div>
        </td>
      </tr>
      <tr>
        <td>
          <div>
                                    <input id="p_method_cashondelivery" value="cashondelivery" name="payment[method]" title="Paiement chèque" class="checkbox" autocomplete="off" type="checkbox">
                                    <label for="p_method_cashondelivery" class="cashondelivery p_method_label">
                                            <div class="image">
                            <img class="p_method_img" src="http://localhost/adprint_opencart/upload/catalog/view/theme/default/image/paiement-cheque-3x.png">
                        </div>
                        <div>
                                                        <span class="p_method_title">Paiement chèque</span>
                            <span class="p_method_desc1">en 3 fois sans frais</span>

                        </div>
                                                                </label>
                </div>
        </td>
      </tr>
      <tr>
        <td>
          <div>
                                    <input id="p_method_cashondelivery" value="cashondelivery" name="payment[method]" title="MVola" class="checkbox" checked="checked" checked=""  autocomplete="off" type="checkbox">
                                    <label for="p_method_cashondelivery" class="cashondelivery p_method_label">
                                            <div class="image">
                            <img class="p_method_img" src="http://localhost/adprint_opencart/upload/catalog/view/theme/default/image/mvola.jpg">
                        </div>
                        <div>
                                                        <span class="p_method_title">MVola</span>

                        </div>
                                                                </label>
                </div>
        </td>
      </tr>
      <tr>
        <td>
          <div>
                                    <input id="p_method_cashondelivery" value="cashondelivery" name="payment[method]" title="AirtelMoney" class="checkbox" autocomplete="off" type="checkbox">
                                    <label for="p_method_cashondelivery" class="cashondelivery p_method_label">
                                            <div class="image">
                            <img class="p_method_img" src="http://localhost/adprint_opencart/upload/catalog/view/theme/default/image/airtelMoney.jpg">
                        </div>
                        <div>
                                                        <span class="p_method_title">AirtelMoney</span>

                        </div>
                                                                </label>
                </div>
        </td>
      </tr>--> 

		<div class="sp-method-devis-wrapper">
			<div class="middle">
				<h2 class="entry-title">Création de devis</h2>
				<div class="sp-method-devis">
					<input id="p_method_paymentdevis" value="paymentdevis" name="devis" title="Devis" class="checkbox-devis"  type="checkbox">
					<label id="p_method_label" for="p_method_paymentdevis">Je souhaite créer un Devis à partir de mon panier </label>
				</div>
				<div class="clear"></div>
				<div class="sp-method-devis-info">
					Vous retrouverez votre devis dans votre Tableau 
					de Bord, et vous pourrez le transformer en commande ultérieurement, 
					après validation par nos services                    </div>
			</div>
		</div>

    <?php if($date_payment){?>
    <div class="sp-method-devis-wrapper">
      <div class="middle">
        <h3 class="entry-title">Délai de payment</h3>
        <div class="sp-method-devis">
          <input id="delai-payment" value="paymentdelai" name="payment" title="delai payment" class="checkbox-devis"  type="checkbox">
          <label id="p_method_label" for="delai-payment">Je demande une delai de payment </label>
        </div>
        <div id="delai-value" class="checkbox-delai">
          <input id="delai-15" name="delai" value="15" type="radio"> <label for="delai-15">15 Jours</label>
          <input id="delai-30" name="delai" value="30" type="radio"> <label for="delai-30">30 Jours</label>
          <input id="delai-45" name="delai" value="45" type="radio"> <label for="delai-45">45 Jours</label>
        </div>
        
      </div>
    </div>
    <?php } ?>

	</div>
</div>

<?php if ($text_agree) { ?>
<div class="buttons">
  <div class="pull-right"><?php echo $text_agree; ?>
    <?php if ($agree) { ?>
    <input id="agree" type="checkbox" name="agree" value="1" checked="checked" />
    <?php } else { ?>
    <input id="agree" type="checkbox" name="agree" value="1" />
    <?php } ?>
    &nbsp;
    <input type="button" data-loading-text="Chargement..." value="Payer ma commande" id="button-payment-method"  class="boutton2 boutton2-pink btn-validcommand" />

  </div>
</div>

<?php } else { ?>
<div class="buttons">
  <div class="pull-right">
    <input type="button" data-loading-text="Chargement..." value="Payer ma commande" id="button-payment-method"  class="boutton2 boutton2-pink btn-validcommand" />
  </div>
</div>
<?php } ?>


<script type="text/javascript"><!--
$('input[name=\'payment_address\']').on('change', function() {
  if (this.value == 'new') {
    $('#payment-existing').hide();
    $('#payment-new').show();
  } else {
    $('#payment-existing').show();
    $('#payment-new').hide();
  }
});

//move 1
$('#delai-payment').on('change', function() {
  if($(this).is(':checked')){
    $('#delai-value').show();
  }else{
    $('#delai-value').hide();
    $('input[name=\'delai\']').prop( "checked", false );
  }
});

$('#p_method_paymentdevis').on('change', function() {
	if($('#p_method_paymentdevis').is(':checked')){
		$('#button-payment-method').val("Créer mon devis");
		$('#agree').hide();
		$('#agree').prop('checked', true);
	}else{
		$('#button-payment-method').val("Payer ma commande");
			$('#agree').show();
		$('#agree').prop('checked', false);
	}
});
//--></script>
