<form class="form-horizontal">  
  <!--BEGIN ADD FORM SOFTIMAD-->
  <!--div class="row" style="background: #cccccc;"-->
  <div class="row">
  <div class="col-sm-8">
    <h2 class="entry-title addresses-information">Information de facturation</h2>
    <p class="bold font-size-16">Votre adresse de facturation:</p>

        <?php if ($addresses) { ?>
        <div class="radio">
         <label>
        <input type="radio" name="payment_address" value="existing" checked="checked" /><?php echo $text_address_existing; ?></label>
        </div>
          <div id="payment-existing">
    <select name="address_id" class="form-control">
      <?php foreach ($addresses as $address) { ?>
      <?php if ($address['address_id'] == $address_id) { ?>
      <option value="<?php echo $address['address_id']; ?>" selected="selected"><?php echo $address['firstname']; ?> <?php echo $address['lastname']; ?>, <?php echo $address['address_1']; ?>, <?php echo $address['telephone']; ?>, <?php echo $address['city']; ?>, <?php echo $address['zone']; ?>, <?php echo $address['country']; ?></option>
      <?php } else { ?>
      <option value="<?php echo $address['address_id']; ?>"><?php echo $address['firstname']; ?> <?php echo $address['lastname']; ?>, <?php echo $address['address_1']; ?>, <?php echo $address['telephone']; ?>, <?php echo $address['city']; ?>, <?php echo $address['zone']; ?>, <?php echo $address['country']; ?></option>
      <?php } ?>
      <?php } ?>
    </select>
  </div>
  <div class="radio">
    <label>
      <input type="radio" name="payment_address" value="new" />
      <?php echo $text_address_new; ?></label>
  </div>
  <?php } ?>
        <br />
      
      <div class="new row">
        <div id="payment-new" style="display: <?php echo ($addresses ? 'none' : 'block'); ?>;">
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-payment-firstname">Prénom</label>
            <div class="col-sm-4">
              <input type="text" name="firstname" value="<?php echo $address['firstname']; ?>" placeholder="Prénom" id="input-payment-firstname" class="form-control" />
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-payment-lastname">Nom</label>
            <div class="col-sm-4">
              <input type="text" name="lastname" value="<?php echo $address['lastname']; ?>" placeholder="Nom" id="input-payment-lastname" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-payment-company">Société</label>
            <div class="col-sm-4">
              <input type="text" name="company" value="<?php echo $address['company']; ?>" placeholder="Société" id="input-payment-company" class="form-control" />
            </div>
          </div>

          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-payment-address-1">Adresse</label>
            <div class="col-sm-5">
              <input type="text" name="address_1" value="<?php echo $address['address_1']; ?>" placeholder="N° et voie (rue, allée, avenue, ...)" id="input-payment-address-1" class="form-control" />
            </div>
            <!-- <div class="col-sm-5">
              <input type="text" name="address_2" value="<?php echo $address['address_2']; ?>" placeholder="Bâtiment, résidence, entrée" id="input-payment-address-1" class="form-control" />
            </div> -->
          </div>

          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-payment-postcode">Code postal</label>
            <div class="col-sm-5">
              <input type="text" name="postcode" value="<?php echo $address['postcode']; ?>" placeholder="Code postal" id="input-payment-postcode" class="form-control" />
            </div>
            <label class="col-sm-2 control-label" for="input-payment-country">Pays</label>
            <div class="col-sm-3">
              <select name="country_id" id="input-payment-country" class="form-control">
                <option value=""><?php echo $text_select; ?></option>
                <?php foreach ($countries as $country) { ?>
                <?php if ($country['country_id'] == $country_id) { ?>
                <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                <?php } ?>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="form-group required">
          <label class="col-sm-2 control-label" for="input-telephone">Telephone</label>
            <div class="col-sm-3">
              <input type="text" name="telephone" value="<?php echo $address['telephone']; ?>" placeholder="Telephone" id="input-telephone" class="form-control" />
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label required" for="input-payment-zone"><span data-toggle="tooltip" title="<?php echo $help_zone;?>">Zone <img src="image/iconkely.jpg" width="15 px" /></span></label>
            <div class="col-sm-5">
              <select name="zone_id" id="input-payment-zone" class="form-control">
              </select>
            </div>
            <label class="col-sm-2 control-label" for="input-shipping-city">Ville</label>
              <div class="col-sm-3">
                <select name="city" id="input-shipping-city" class="form-control">
                  <option value="Antananarivo" disabled="disabled">Antananarivo</option>
                  <option value="Antsiranana" disabled="disabled">Antsiranana</option>
                  <option value="Fianarantsoa" disabled="disabled">Fianarantsoa</option>
                  <option value="Mahajanga" disabled="disabled">Mahajanga</option>
                  <option value="Toamasina" disabled="disabled">Toamasina</option>
                  <option value="Toliara" disabled="disabled" >Toliara</option>
                </select>
              </div>
          </div>
          <div class="col-sm-7">
            <p>Service exclusif : l'alerteBAT par SMS (option gratuite) directement éditable depuis votre compte</p>
          </div>
          <p class="pull-right">* Champ obligatoires</p>
        </div>
      </div><!--end new-->

          <!--START Choix adress livraison-->
<!--     <?php if ($addresses) { ?>
    <div class="radio">
      <label>
        <input type="radio" name="shipping_address" value="existing" checked="checked" />
        <?php echo $text_address_existing; ?></label>
    </div>
    <div id="shipping-existing">
      <select name="address_id" class="form-control hidden">
        <?php foreach ($addresses as $address) { ?>
        <?php if ($address['address_id'] == $address_id) { ?>
        <option value="<?php echo $address['address_id']; ?>" selected="selected"><?php echo $address['firstname']; ?> <?php echo $address['lastname']; ?>, <?php echo $address['address_1']; ?>, <?php echo $address['city']; ?>, <?php echo $address['zone']; ?>, <?php echo $address['country']; ?></option>
        <?php } else { ?>
        <option value="<?php echo $address['address_id']; ?>"><?php echo $address['firstname']; ?> <?php echo $address['lastname']; ?>, <?php echo $address['address_1']; ?>, <?php echo $address['city']; ?>, <?php echo $address['zone']; ?>, <?php echo $address['country']; ?></option>
        <?php } ?>
        <?php } ?>
      </select>
    </div>
    <div class="radio">
      <label>
        <input type="radio" name="shipping_address" value="new" />
        <?php echo $text_address_new; ?></label>
    </div>
    <?php } ?> -->
    <br />
    </div><!--col-sm-8-->

    <!--Panier-->
    <div class="col-sm-4">   
      <span><?php echo $soft_panier; ?></span>
    </div><!--col-sm-4-->
    </div>
<br/>
  <!--/divROW-->
  <div class="row">
  <div class="col-sm-12">

   <!--AND Choix adress livraison-->

    <div class="buttons clearfix">
      <div class="buttons-block-left">
        <a href="index.php?route=checkout/cart"><input type="button" value="Retour" class="boutton2 boutton2-gray" /></a>
      </div>
      <div class="pull-right">
        <input type="button" data-loading-text="Chargement..." value="<?php echo $button_continue; ?>" id="button-payment-address" class="boutton2 boutton2-pink" />
      </div>
    </div>   
  </div>
  </div>
</form>


<script type="text/javascript"><!--
$('input[name=\'payment_address\']').on('change', function() {
	if (this.value == 'new') {
		$('#payment-existing').hide();
		$('#payment-new').show();
	} else {
		$('#payment-existing').show();
		$('#payment-new').hide();
	}
});
//--></script>
<script type="text/javascript"><!--
$('select[name=\'zone_id\']').on('change', function() {
  if ((this.value == 1938) || (this.value == 4236) ) {
   $('select[name=\'city\']').val("Antananarivo");
  }
  else if (this.value == 1939) {
   $('select[name=\'city\']').val("Antsiranana");
  }
  else if (this.value == 1940) {
   $('select[name=\'city\']').val("Fianarantsoa");
  }
  else if (this.value == 1941) {
   $('select[name=\'city\']').val("Mahajanga");
  }
  else if (this.value == 1942) {
   $('select[name=\'city\']').val("Toamasina");
  }
  else if (this.value == 1943) {
   $('select[name=\'city\']').val("Toliara");
  }
});
//--></script>
<script type="text/javascript"><!--
// Sort the custom fields
$('#collapse-payment-address .form-group[data-sort]').detach().each(function() {
	if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('#collapse-payment-address .form-group').length-2) {
		$('#collapse-payment-address .form-group').eq(parseInt($(this).attr('data-sort'))+2).before(this);
	}

	if ($(this).attr('data-sort') > $('#collapse-payment-address .form-group').length-2) {
		$('#collapse-payment-address .form-group:last').after(this);
	}

	if ($(this).attr('data-sort') == $('#collapse-payment-address .form-group').length-2) {
		$('#collapse-payment-address .form-group:last').after(this);
	}

	if ($(this).attr('data-sort') < -$('#collapse-payment-address .form-group').length-2) {
		$('#collapse-payment-address .form-group:first').before(this);
	}
});
//--></script>
<script type="text/javascript"><!--
$('#collapse-payment-address button[id^=\'button-payment-custom-field\']').on('click', function() {
	var node = this;

	$('#form-upload').remove();

	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	if (typeof timer != 'undefined') {
    	clearInterval(timer);
	}

	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);

			$.ajax({
				url: 'index.php?route=tool/upload',
				type: 'post',
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$(node).button('loading');
				},
				complete: function() {
					$(node).button('reset');
				},
				success: function(json) {
					$(node).parent().find('.text-danger').remove();

					if (json['error']) {
						$(node).parent().find('input[name^=\'custom_field\']').after('<div class="text-danger">' + json['error'] + '</div>');
					}

					if (json['success']) {
						alert(json['success']);

						$(node).parent().find('input[name^=\'custom_field\']').val(json['code']);
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
//--></script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.time').datetimepicker({
	pickDate: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});
//--></script>
<script type="text/javascript"><!--
$('#collapse-payment-address select[name=\'country_id\']').on('change', function() {
	$.ajax({
		url: 'index.php?route=checkout/checkout/country&country_id=' + this.value,
		dataType: 'json',
		beforeSend: function() {
			$('#collapse-payment-address select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
		},
		complete: function() {
			$('.fa-spin').remove();
		},
		success: function(json) {
			if (json['postcode_required'] == '1') {
				$('#collapse-payment-address input[name=\'postcode\']').parent().parent().addClass('required');
			} else {
				$('#collapse-payment-address input[name=\'postcode\']').parent().parent().removeClass('required');
			}

			html = '<option value=""><?php echo $text_select; ?></option>';

			if (json['zone'] && json['zone'] != '') {
				for (i = 0; i < json['zone'].length; i++) {
					html += '<option value="' + json['zone'][i]['zone_id'] + '"';

					if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
						html += ' selected="selected"';
					}

					html += '>' + json['zone'][i]['name'] + '</option>';
				}
			} else {
				html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
			}

			$('#collapse-payment-address select[name=\'zone_id\']').html(html);
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('#collapse-payment-address select[name=\'country_id\']').trigger('change');
//--></script>
