<?php if (!isset($redirect)) { ?>
<div class="">
  <div class="row"> 
    <div class="col-sm-8">
      <table class="table table-hover">
        <thead>
          <tr>
            <td class="text-left"><?php echo $column_name; ?></td>
            <!--td class="text-left"><?php echo $column_model; ?></td-->
            <td class="text-right"><?php echo $column_price; ?></td>
            <td class="text-right"><?php echo $column_quantity; ?></td>
            <td class="text-right"><?php echo $column_total; ?></td>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($products as $product) { ?>
          <tr>
            <td class="text-left"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
              <?php foreach ($product['option'] as $option) { ?>
              <br />
              &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
              <?php } ?>
              <?php if($product['recurring']) { ?>
              <br />
              <span class="label label-info"><?php echo $text_recurring_item; ?></span> <small><?php echo $product['recurring']; ?></small>
              <?php } ?></td>
            <!--td class="text-left"><?php echo $product['model']; ?></td-->
            <td class="text-right"><?php echo $product['price']; ?></td>
            <td class="text-right"><?php echo $product['quantity']; ?></td>
            <td class="text-right"><?php echo $product['total']; ?></td>
          </tr>
          <?php } ?>
          <?php foreach ($vouchers as $voucher) { ?>
          <tr>
            <td class="text-left"><?php echo $voucher['description']; ?></td>
            <td class="text-left"></td>
            <td class="text-right">1</td>
            <td class="text-right"><?php echo $voucher['amount']; ?></td>
            <td class="text-right"><?php echo $voucher['amount']; ?></td>
          </tr>
          <?php } ?>
        </tbody>
        <!--tfoot>
          <?php foreach ($totals as $total) { ?>
          <tr>
            <td colspan="4" class="text-right"><strong><?php echo $total['title']; ?>:</strong></td>
            <td class="text-right"><?php echo $total['text']; ?></td>
          </tr>
          <?php } ?>
        </tfoot-->
      </table>
      
      <div class="row">
        <div class="col-sm-6">
            <div class="quote-address-review-wrapper quote-address-billing">
                  <h2 class="quote-address-review-billing-title entry-title addresses-information">Adresse de facturation</h2>
                  <div class="quote-address-review-billing">
                      <div class="quote-review-address">
                        <?php echo $payment_address; ?><br>   
                        <?php echo $email; ?><br>
                        Tel : <?php echo $payment_telephone; ?><br>
                        <?php echo $payment_province; ?> <br>
                        <?php echo $payment_pays; ?>                                    
                    </div>
              </div>
            </div>
        </div>
        
        <div class="col-sm-6">
           <div class="quote-address-review-wrapper quote-address-shipping">
                <h2 class="quote-address-review-shipping-title entry-title addresses-information">Adresse de livraison</h2>
                <div class="quote-address-review-shipping">
                    <div class="quote-review-address">
                      <?php echo $shipping_address;?><br>
                      <?php echo $email; ?><br>
                      Tel : <?php echo $shipping_telephone; ?><br>
                      <?php echo $shipping_province; ?> <br>
                      <?php echo $shipping_pays; ?>

                      </div>
                    </div>
                  </div>   
        </div>

      </div>
      <div class="clear"></div>
             

    </div>

    <div class="col-sm-4">
      
      <div id="fake-sidebar-wrapper">
          <div class="fake-sidebar">
            <h2>Votre panier</h2>
             <div id="cart-sidebar-ajaxifier" class="cart-sidebar cart-totals">
                              
            <div class="subtotals-holder">
              <div class="subtotals-separator">
                  <table id="shopping-cart-totals-table">  
                    <tbody>     
                    <?php foreach ($totals as $total) { ?>
                        <?php if ($TVA == 0) { ?> 
                          <?php if($total['title'] != 'TVA') { ?>
                         <?php if($total['title'] == "Total") { ?>
                          <tr>
                            <td style="" class="a-right montant-text" colspan="1">
                                Montant Total 
                            </td>
                            <td style="" class="a-right">
                                <span class="price"><?php echo $total['text']; ?></span>    
                            </td>
                          </tr>
                          <?php } else { ?>
                          <tr>
                            <td style="" class="pull-right" colspan="1">
                                <?php echo $total['title']; ?>
                            </td>
                            <td style="" class="a-right">
                                <span class="price"><?php echo $total['text']; ?></span>    
                            </td>
                          </tr>
                          <?php } ?>
                          <?php } ?>
                        <?php } else { ?>
                          <?php if($total['title'] == "Total") { ?>
                          <tr>
                            <td style="" class="a-right montant-text" colspan="1">
                                Montant Total T.T.C
                            </td>
                            <td style="" class="a-right">
                                <span class="price"><?php echo $total['text']; ?></span>    
                            </td>
                          </tr>
                          <?php } else { ?>
                          <tr>
                            <td style="" class="pull-right" colspan="1">
                                <?php echo $total['title']; ?>
                            </td>
                            <td style="" class="a-right">
                                <span class="price"><?php echo $total['text']; ?></span>    
                            </td>
                          </tr>
                        <?php } ?>
                        <?php } ?>
                    <?php } ?>
                      </tbody>
                  </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>


  </div>
  <div class="row">
    <div class="buttons-block-left-review">
          <a href="index.php?route=checkout/cart"><input type="button" value="Modifiez votre panier" class="boutton2 boutton2-gray" /></a>
       </div>

        <div class="buttons-block-right-review">
          

      <?php echo $payment; ?>
<!--button type="submit" id="submit" title="Passez la commande" class="boutton2 boutton2-pink">
  <span class="left"></span><span>Confirmation panier</span><span class="right"></span>
</button-->

        <span class="please-wait" id="review-please-wait" style="display:none;">
            <img src="payer%20commande_fichiers/opc-ajax-loader.gif" alt="Les informations de commande sont en cours d'envoi..." title="Les informations de commande sont en cours d'envoi..." class="v-middle"> Les informations de commande sont en cours d'envoi...        </span>
    </div>
  </div>

</div>
<?php } else { ?>
<script type="text/javascript"><!--
location = '<?php echo $redirect; ?>';
//--></script>
<?php } ?>
