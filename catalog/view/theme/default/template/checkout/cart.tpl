<?php echo $header; ?>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
<style>

.quantite_ligne{
      width: 85px !important;
}

.boutton{
  height: 17px;
    width: 35px;
}

.texte_1{
    height: 40px;
  padding-top: 5px;
}

.icones{
    font-size: 30px;
      width: 100%;
}

.texte_2{
  height: 150px;
  margin-top: 50px;
    border: 2px solid #dfdfdf;
}

.texte_3{
  height: 150px;
  margin-top: 50px;
  border: 2px solid #dfdfdf;
  padding: 0px;
}


.mes_infos
{
    height: 30px;
    padding: 5px;
  font-family: Arial !important;
    font-size: 1.2em;
    text-transform: uppercase;
    color: #6c6c6c;
    font-weight: 500;
}

.icones_bas{
  font-size: 20px;
  width:50px;
}

.images{
  height:100%;
}



.menu_actif{
  background-color: #fff;
    border-bottom: none;
}

.div_gris{
  background: #F6F6F6;
}

.titre_gris{
  text-align: center;
    font-size: 13px;
    font-weight: 700;
  height: 50px;
  padding-top: 3px;
}

.zero{
    color: #f40088;
    font-size: 45px;
    font-weight: bold;
    text-align: center;
    height: 40px;
}

.zero_2{
      color: #f40088;
    font-weight: bold;
    text-align: center;
}


.content_gris{
   text-align: center;
       height: 70px;
}

.voir_details{
    height: 30px;
    text-align: center;
}

.div_230{
   height: 250px;
}   

.acheter{
      height: 30px;
    padding: 5px;
    text-align: center;
    background: #a3a3a3;
    margin-top: 10px;
    font-size: 13px;
    color: #FFF;
}

.contactez{
  margin-top: 50px;text-align: center;
}

</style>

<div class="container">
  <ul class="breadcrumbs">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php if ($attention) { ?>
  <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $attention; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?>
      </h1>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr>
                <td class="text-center"><?php echo $column_image; ?></td>
                <td class="text-left"><?php echo $column_name; ?></td>
                <td class="text-left"><?php echo $column_model; ?></td>
                <td class="text-left"><?php echo $column_quantity; ?></td>
                <td class="text-right"><?php echo $column_price; ?></td>
                <td class="text-right"><?php echo $column_total; ?></td>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($products as $product) { ?>
              <tr>
                <td class="text-center"><?php if ($product['thumb']) { ?>
                  <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail" /></a>
                  <?php } ?></td>
                <td class="text-left"><a href="<?php echo $product['href']; ?>"><?php echo $product['name'];?></a><br />
                  <?php if (!$product['stock']) { ?>
                  <span class="text-danger">***</span>
                  <span class="text-danger">***</span>
                  <?php } ?>
                  <?php //syrax ?>
                 <?php $temp = str_word_count($product['model'], 1, 'âàáãç3'); ?>
                  <?php if($temp[0] == 'Banderole' || $temp[0] == 'Bache' || $temp[0] == 'Bâche'){ ?>
                          <?php foreach ($product['option'] as $option) { ?>
                             <small>Largeur : <?php echo $option['Largeur']; ?> cm</small><br />
                             <small>Hauteur : <?php echo $option['Hauteur']; ?> cm</small><br />
                             <small>Orientation : <?php echo $option['Orientation']; ?></small><br />
                             <small>Oeillet : <?php echo $option['Oeillet']; ?></small><br />
                             <small>Soudure : <?php echo $option['Soudure']; ?></small><br />
                             <small>Servicepose : <?php echo $option['Servicepose']; ?></small><br />
                             <?php if($option['Servicepose'] == 'Oui'){ ?>
                                <small>Pose : <?php echo $option['Pose']; ?></small><br />
                                <small>Ville : <?php echo $option['Ville']; ?></small><br />
                             <?php } ?>
                             <small>Fourreau : <?php echo $option['Fourreau']; ?></small><br />
                             <?php if($option['Fourreau'] != 'Aucun'){ ?>
                                <small>MettreFourreau : <?php echo $option['MettreFourreau']; ?></small><br />
                             <?php } ?>
                              <small>Envoi du bat: <?php echo $option['Bat']; ?></small><br />
                              <?php if($option['LaizeMaxChute'] != 0){ ?>
                                <small>Laize Max chute: <?php echo $option['LaizeMaxChute']; ?></small><br />
                              <?php } ?>
                          <?php } ?>
                        <?php //syrax ?>
                  <?php }else{ ?>
                    <?php if ($product['option']) { ?>
                      <?php foreach ($product['option'] as $option) { ?>
                       <br />
                       <small><?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
                      <?php } ?>
                    <?php } ?>
                  <?php } ?>
                  <?php if ($product['reward']) { ?>
                  <br />
                  <small><?php echo $product['reward']; ?></small>
                  <?php } ?>
                  <?php if ($product['recurring']) { ?>
                  <br />
                  <span class="label label-info"><?php echo $text_recurring_item; ?></span> <small><?php echo $product['recurring']; ?></small>
                  <?php } ?></td>
                <td class="text-left"><?php echo $product['model']; ?></td>
                <td class="text-left"><div class="input-group btn-block" style="max-width: 200px;">
        
               <input min="1" max="10000" type="text" id="quantity_<?php echo $product['cart_id']; ?>" name="quantity[<?php echo $product['cart_id']; ?>]" value="<?php echo $product['quantity']; ?>" size="1" class="quantite_ligne form-control input-number" />
         
               <input type="hidden" name="larg" value="<?php echo $product['larg']; ?>"/>         
               <input type="hidden" name="long" value="<?php echo $product['long']; ?>"/>         
               <input type="hidden" name="laize" value="<?php echo $product['laize']; ?>"/>
              
        <div style="line-height: 0px; float:left;width:40px">
        
              <button class="boutton"  data-type="plus" type="button"
              onclick="
              
              var type      = $(this).attr('data-type');
              var input = $('#quantity_<?php echo $product['cart_id']; ?>');
              var currentVal = parseInt(input.val());
              
              if (!isNaN(currentVal)) {
                
                if(type == 'minus') {
                  
                  if(currentVal > input.attr('min')) {
                    input.val(currentVal - 1).change();
                  }
                  if(parseInt(input.val()) == input.attr('min')) {
                    // $(this).attr('disabled', true);
                  }

                } else if(type == 'plus') {

                  if(currentVal < input.attr('max')) {
                    input.val(currentVal + 1).change();
                  }
                  if(parseInt(input.val()) == input.attr('max')) {
                    $(this).attr('disabled', true);
                  }

                }
              } else {
                
                input.val(0);
              }
                
                "> <span class="glyphicon glyphicon-plus"></span>
              
              </button>
                
              <button class="boutton" data-type="minus" type="button"
              onclick="
              
              var type      = $(this).attr('data-type');
              var input = $('#quantity_<?php echo $product['cart_id']; ?>');
              var currentVal = parseInt(input.val());
              
              if (!isNaN(currentVal)) {
                
                if(type == 'minus') {
                  
                  if(currentVal > input.attr('min')) {
                    input.val(currentVal - 1).change();
                  }
                  if(parseInt(input.val()) == input.attr('min')) {
                    //$(this).attr('disabled', true);
                  }

                } else if(type == 'plus') {

                  if(currentVal < input.attr('max')) {
                    input.val(currentVal + 1).change();
                  }
                  if(parseInt(input.val()) == input.attr('max')) {
                    $(this).attr('disabled', true);
                  }

                }
              } else {
                
                input.val(0);
              }
                
                "> <span class="glyphicon glyphicon-minus"></span>
                
              </button>  
                
             </div>
         
                    <span class="input-group-btn">
                    <button type="submit" data-toggle="tooltip" title="<?php echo $button_update; ?>" class="btn btn-primary"><i class="fa fa-refresh"></i></button>
                    <button type="button" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger" onclick="remove('<?php echo $product['cart_id']; ?>');"><i class="fa fa-times-circle"></i></button>
                    </span></div></td>
                <?php if($product['is_surmesure'] == 1){ ?>
                  <td class="text-right"><?php echo $product['price'].'/m2';?></td>
                <?php }else{ ?>
                  <td class="text-right"><?php echo $product['price'] ; ?></td>
                <?php }?>
                <td class="text-right"><?php echo $product['total']; ?></td>
              </tr>
              <?php } ?>
              <?php foreach ($vouchers as $voucher) { ?>
              <tr>
                <td></td>
                <td class="text-left"><?php echo $voucher['description']; ?></td>
                <td class="text-left"></td>
                <td class="text-left"><div class="input-group btn-block" style="max-width: 200px;">
                    <input type="text" name="" value="1" size="1" disabled="disabled" class="form-control" />
                    <span class="input-group-btn">
                    <button type="button" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger" onclick="voucher.remove('<?php echo $voucher['key']; ?>');"><i class="fa fa-times-circle"></i></button>
                    </span></div></td>
                <td class="text-right"><?php echo $voucher['amount']; ?></td>
                <td class="text-right"><?php echo $voucher['amount']; ?></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </form>

      <!-- Start Integration statique checkout -->
      <div class="contentCheckout">
		<div class="row">
        <div class="col-md-6">
			<h4>Options & réductions</h4>
				<table class="tableCodePromo" border="1">
				<?php
				foreach($modules as $module){
					?>
					<tr>
						<td> <?php echo $module ; ?></td>
					</tr>
					<?php
				}
				?>
				</table>
		</div>
		<div class="col-md-6">
			<h4>Récapitulatif</h4>
				<table class="table table-bordered prixCheckout">
					<?php foreach ($totals as $total) { ?>
            
          <?php  if ($TVA == 0) { ?>
					<tr>
            
              <?php if($total['title'] != 'TVA') { ?>
                <?php $x=0; ?>
                <?php if($total['title'] == 'Admoney') ?>
                <?php $temp = $total['text']; ?>
                <?php if($total['title'] == 'Total' && $total['text'] == 'Ar0.00'){ ?>
                <?php $x=1; ?>
                <?php } ?>
                <?php $_SESSION['var_total'] = $x; ?>
                <td><strong><?php echo $total['title']; ?></strong></td>
                <td><?php echo $total['text']; ?></td>
             <?php } ?>
            <?php } else { ?>

            
            <?php $x=0; ?>
            <?php if($total['title'] == 'Admoney') ?>
            <?php $temp = $total['text']; ?>
            <?php if($total['title'] == 'Total' && $total['text'] == 'Ar0.00'){ ?>
            <?php $x=1; ?>
            <?php } ?>
            <?php $_SESSION['var_total'] = $x; ?>
						<td><strong><?php echo $total['title']; ?></strong></td>
						<td><?php echo $total['text']; ?></td>
					</tr>
          <?php  } ?>
					<?php } ?>
				</table>
		</div>
        </div>
		<div class="row">
        <div class="col-md-6">
			<a class="suiteAchat" href="<?php echo $continue; ?>">Continuer mes achats</a>
		</div>
        <div class="col-md-6">
			<a class="confirmPanier" href="<?php echo $checkout; ?>">Confirmation panier</a>
		</div>
		</div>
        <div class="clearfix"></div>  
      </div>     
      <!-- End Integration statique checkout -->
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>

<?php echo $footer; ?>
<script>
  function remove(key){
    $.ajax({
      url: 'index.php?route=checkout/cart/remove',
      type: 'post',
      data: 'key=' + key,
      dataType: 'json',
      success: function(json) {
        // Need to set timeout otherwise it wont update the total
        setTimeout(function () {
          $('#panier > a').html('<i class="fa fa-shopping-cart fa-3x"></i><span id="panierTotal">' + json['total_produit'] + '</span><p>Mon Panier</p>');
        }, 100);

        if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
          location = 'index.php?route=checkout/cart';
        } else {
          $('#panier > ul').load('index.php?route=common/cart/info .wedgetPanier li');
        }
      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });
  }

	var conditionalInput = $('div.codePromo');
	var subscribeInput = $('input[name="codePromoBtn"]');
	
	var conditionalInput2 = $('div.codeAdmoney');
	var subscribeInput2 = $('input[name="codeAdmoneyBtn"]');
	
	conditionalInput.hide();
	subscribeInput.on('click', function(){
		if ( $(this).is(':checked') ){
			conditionalInput.show();
			conditionalInput2.hide();
			subscribeInput2.prop('checked',false);
		}else {
			conditionalInput.hide();
		}
	});
  
	conditionalInput2.hide();
	subscribeInput2.on('click', function(){
		if ( $(this).is(':checked') ) {
			conditionalInput2.show();
			conditionalInput.hide();
			subscribeInput.prop('checked',false);
		}
		else {
			conditionalInput2.hide();
		}
	});

</script>