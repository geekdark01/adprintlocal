<?php echo $header; ?>
<script src="catalog/view/javascript/bat.js" type="text/javascript"></script>
<div class="container">
  <ul class="breadcrumbs">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">
        <h2><?php echo $text_gestion_fichier;?></h2>
        <div class="row">
            <div class="col-md-4 col-xs-6">
                <div class="form-group input-group input-group-sm">
                    <label class="input-group-addon"><?php echo $text_choix_commande ; ?>:</label>
                    <select id="input-sort" class="form-control" onchange="showDetailOrder(this.value);">
                        <?php
                            echo $order_id ;
                            $iOrderIdFirst  = 0 ;
                            $iCompteur      = 0 ;
							
							foreach($toOrderAccount as $oOrderAccount): ?>
                            <?php
                                if($iCompteur == 0){
                                    $iOrderIdFirst = ($order_id == 0) ? $oOrderAccount['order_id'] : $order_id ;
                                }
                            ?>
                            <option value="<?php echo $oOrderAccount['order_id'] ; ?>">Order #<?php echo $oOrderAccount['order_id'] ; ?></option>
                            <?php $iCompteur ++  ?>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="table-responsive">
                <?php foreach($toOrderAccount as $oOrderAccount): 
				?>
                    <table class="table table-bordered" id="tableau_<?php echo $oOrderAccount['order_id'] ; ?>">
                        <thead>
                            <tr>
                                <th>
                                    ID
                                </th>
                                <th>
                                    <?php echo $text_titre_produit ; ?>
                                </th>
                                <th>
                                    <?php echo $text_bat_verifier ; ?>
                                </th>
                                <th>
                                    <?php echo $text_titre_action ; ?>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($order_product[$oOrderAccount['order_id']] as $toContenu): ?>
                                <tr>
                                    <td>
                                        <?php echo $toContenu['product_id'] ;  ?>
                                    </td>
                                    <td>
                                        <?php echo $toContenu['name'] ;  ?>
                                    </td>
                                    <td>
                                        <?php if(count($toContenu['bat']) > 0): ?>
                                            <?php foreach($toContenu['bat'] as $toBat): ?>
                                                <a  href="<?php echo $toBat['url_fichier'] ; ?>"><?php echo $toBat['fichier'] ; ?></a>
                                                &nbsp;<span><?php echo $toBat['date_added'] ; ?></span><br/>
                                            <?php endforeach ; ?>
                                        <?php endif ; ?>
                                    </td>
                                    <td>
                                        <?php if($toContenu['bat_status'] == 0): ?>
                                            <span class="lnkverou<?php echo $toContenu['product_id'] . '_' . $toContenu['order_id'] ; ?>_1" style="display:none;"><?php echo $text_valide ; ?></span>
                                            <span class="lnkverou<?php echo $toContenu['product_id'] . '_' . $toContenu['order_id'] ; ?>_2" style="display:none;"><?php echo $text_refuse ; ?></span>
                                            <a class="lnkverou<?php echo $toContenu['product_id'] . '_' . $toContenu['order_id'] ; ?>_0 btn btn-link"  href="javascript:void(0);" onclick="actionBat(<?php echo $toContenu['order_id'] ; ?>, <?php echo $toContenu['product_id'] ; ?>, 1) ;"><?php echo $text_valider ; ?></a>
                                            &nbsp;
                                            <a class="lnkverou<?php echo $toContenu['product_id'] . '_' . $toContenu['order_id'] ; ?>_0 btn btn-link"  href="javascript:void(0);" onclick="actionBat(<?php echo $toContenu['order_id'] ; ?>, <?php echo $toContenu['product_id'] ; ?>, 2) ;"><?php echo $text_refuser ; ?></a>
                                        <?php elseif($toContenu['bat_status'] == 1): ?>
                                            <span class="lnkverou<?php echo $toContenu['product_id'] . '_' . $toContenu['order_id'] ; ?>_1"><?php echo $text_valide ; ?></span>
                                        <?php elseif($toContenu['bat_status'] == 2): ?>
                                            <span class="lnkverou<?php echo $toContenu['product_id'] . '_' . $toContenu['order_id'] ; ?>_2"><?php echo $text_refuse ; ?></span>
                                        <?php else:?>
                                            &nbsp;-&nbsp;
                                        <?php endif ; ?>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            <?php endforeach ; ?>
                        </tbody>
                    </table>
                <?php endforeach ; ?>
            </div>
        </div>
        <?php if($iOrderIdFirst != 0): ?>
            <script type="text/javascript">
                $(document).ready(function(){
                    showDetailOrder(<?php echo $iOrderIdFirst ; ?>) ;
                }) ;
            </script>
        <?php endif ; ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?> 