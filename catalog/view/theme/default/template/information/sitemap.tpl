﻿<?php echo $header; ?>



<style>

.titre_sitemap h1{
	text-align:center;
}

.contenu_titre_sitemap{
	text-align:center;
}

.liste_categorie_sitemap{
	padding-left:5%;
}

.liste_pages{
	padding-left:5%;	
}

.titre_categories{
	text-align:center;
	font-size:25px;
	height: 30px;
    background-color: #f3f3f3;
    padding: 7px;
}

.liste_categorie_sitemap .titre{
	position: relative;
    height: 40px;
    display: block;
    padding-top: 10px;
   
    font-weight: bold;
    text-decoration: none;
    color: #425462;
	font-size: 20px;
}

.liste_categorie_sitemap .liste{
	position: relative;
     height: 25px;
    display: block;
    padding-top: 10px;

    text-decoration: none;
    color: #425462;
}

.liste_categorie_sitemap a:hover {

}



.liste_pages a{
	position: relative;
    height: 40px;
    display: block;
    padding-top: 10px;
    padding-left: 50px;
    font-weight: bold;
    text-decoration: none;
    color: #425462;
}

.liste_pages a:hover {
   
}

</style>


<div class="container">
  <ul class="breadcrumbs">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">
	<?php echo $content_top; ?>
	
	<?php /*
		<div class="titre_sitemap">
	
			<h1><?php echo $heading_title; ?></h1>
			<div class="contenu_titre_sitemap">
		
			<p>Profitez d'une large sélection d'articles avec livraison gratuite en France!</p>
			<p>100% Satisfait ou remboursé sans condition!</p>
		  
			</div>
		  
		</div>
	*/
	?>	
	  
	  
      <div class="row">
	  
	  
        <div class="col-sm-12 liste_categorie_sitemap">
		
				<!--
				<div class="titre_categories" >
					Catégories
				</div>
				-->
				
				  <ul>
					<?php foreach ($categories as $category_1) {
					$i = 1 ;

					?>
					<li class="col-sm-4" style="height:550px"> 
					<a class="titre" href="<?php echo $category_1['href']; ?>">  <?php echo $category_1['name']; ?></a>
					  <?php if ($category_1['children']) { ?>
					  <ul>
						<?php foreach ($category_1['children'] as $category_2) {	
						if( $i < 20 )
						{  ?>
						<li ><a class="liste" href="<?php echo $category_2['href']; ?>">  <?php echo $category_2['name']; ?></a>
						  <?php  if ($category_2['children']) { ?>
						  <ul>
							<?php foreach ($category_2['children'] as $category_3) { ?>
							<?php if( $i < 20 ) { ?>
							<li ><a class="liste" href="<?php echo $category_3['href']; ?>"> &nbsp;&nbsp; <?php echo $category_3['name']; ?></a></li>
							<?php $i++ ; } }?>
							
						  </ul>
						  <?php }  ?>
						</li>
						<?php
						}
						
						?>
						
						<?php $i++ ; } ?>
					  </ul>
					  <?php } ?>
					  
					  <div class="clear"></div>
					</li>
					<?php } ?>
				  </ul>
        </div>
		
		
		
		<?php /*
		
        <div class="col-sm-6 liste_pages">
		
		<div class="titre_categories" >
			Pages
		</div>
		
          <ul>
            <li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
            <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a>
              <ul>
                <li><a href="<?php echo $edit; ?>"><?php echo $text_edit; ?></a></li>
                <li><a href="<?php echo $password; ?>"><?php echo $text_password; ?></a></li>
                <li><a href="<?php echo $address; ?>"><?php echo $text_address; ?></a></li>
                <li><a href="<?php echo $history; ?>"><?php echo $text_history; ?></a></li>
                <li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>
              </ul>
            </li>
            <li><a href="<?php echo $cart; ?>"><?php echo $text_cart; ?></a></li>
            <li><a href="<?php echo $checkout; ?>"><?php echo $text_checkout; ?></a></li>
            <li><a href="<?php echo $search; ?>"><?php echo $text_search; ?></a></li>
            <li><?php // echo $text_information; ?>
              <ul>
                <?php foreach ($informations as $information) { ?>
                <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
                <?php } ?>
                <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
              </ul>
            </li>
          </ul>
        </div>
		
		*/ ?>
		
		
      </div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>