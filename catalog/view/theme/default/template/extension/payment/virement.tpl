﻿<div class="buttons">
	<div class="pull-right">	
		<input type="button" value="<?php echo $button_confirm; ?>" id="button-confirm" class="boutton2 boutton2-pink btn-validcommand" data-loading-text="Chargement..." />
	</div>
</div>

<script type="text/javascript">


$('#button-confirm').on('click', function() {
		$.ajax({
		type: 'get',
		url: 'index.php?route=extension/payment/virement/confirm',
		cache: false,
		beforeSend: function() {
			$('#button-confirm').button('loading');
		},
		complete: function() {
			$('#button-confirm').button('reset');
		},
		success: function() {
			 location = '<?php echo $continue; ?>';
		}
	});
});


</script>
