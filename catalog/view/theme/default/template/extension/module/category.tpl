<style>
.dropdown:hover>.dropdown-menu{display:block;}
.dropdown-submenu{position:relative;}
.dropdown-submenu>.dropdown-menu{top:0;left:100%;margin-top:-6px;margin-left:-1px;}
.dropdown-submenu:hover>.dropdown-menu{display:block;}
.dropdown-submenu>a:after{display:block;content:" ";float:right;width:0;height:0;border-color:transparent;border-style:solid;border-width:5px 0 5px 5px;border-left-color:#cccccc;margin-top:5px;margin-right:-10px;}
.dropdown-submenu:hover>a:after{border-left-color:#ffffff;}
.dropdown-submenu.pull-left{float:none;}.dropdown-submenu.pull-left>.dropdown-menu{left:-100%;margin-left:10px;}
</style>

<nav class="navbar navbar-default" role="navigation">
  <div class="collapse navbar-collapse" id="oNavigation">
        <ul class="nav navbar-nav">
              <?php
              foreach ($categories as $child) { ?>
              <li class="dropdown">
                <?php if ($child['category_id'] == $child_id) { ?>
                    <a href="<?php echo $child['href']; ?>" class="dropdown-toggle list-group-item active" data-toggle="dropdown"><?php echo $child['name']; ?></a>
                <?php } else{ ?>
                    <a href="<?php echo $child['href']; ?>" class="dropdown-toggle list-group-item" data-toggle="dropdown"><?php echo $child['name']; ?></a>
                <?php }?>
                <?php if ($child['children']) { ?>
                <ul class="dropdown-menu">
                  <?php foreach ($child['children'] as $child_2) { ?>
                  <li class="dropdown-submenu">
                   <a href="<?php echo $child_2['href']; ?>" class="list-group-item"><?php echo $child_2['name']; ?></a>
                    <?php if ($child_2['children']) { ?>
                    <ul class="dropdown-menu">
                        <?php foreach ($child_2['children'] as $child_3) { ?>
                          <li class="dropdown-submenu">
                            <a href="<?php echo $child_3['href']; ?>" class="list-group-item"><?php echo $child_3['name']; ?></a>
                            <?php if ($child_3['children']) { ?>
                            <ul class="dropdown-menu">
                                <?php foreach ($child_3['children'] as $child_4) { ?>
                                  <li class="dropdown-submenu">
                                    <a href="<?php echo $child_4['href']; ?>" class="list-group-item"><?php echo $child_4['name']; ?></a>
                                    <?php if ($child_4['children']) { ?>
                                    <ul class="dropdown-menu">
                                        <?php foreach ($child_4['children'] as $child_5) { ?>
                                          <li class="dropdown-submenu">
                                            <a href="<?php echo $child_5['href']; ?>" class="list-group-item"><?php echo $child_5['name']; ?></a>
                                            <?php if ($child_5['children']) { ?>
                                            <ul class="dropdown-menu">
                                                <?php foreach ($child_5['children'] as $child_6) { ?>
                                                  <li class="dropdown-submenu">
                                                    <a href="<?php echo $child_6['href']; ?>" class="list-group-item"><?php echo $child_6['name']; ?></a>
                                                  </li>
                                                  <?php } ?>
                                            </ul>
                                            <?php } ?>
                                          </li>
                                          <?php } ?>
                                    </ul>
                                    <?php } ?>
                                  </li>
                                  <?php } ?>
                            </ul>
                            <?php } ?>
                          </li>
                          <?php } ?>
                    </ul>
                    <?php } ?>
                  </li>
                    <?php } ?>
                </ul>
                 <?php } ?>
              </li>
               <?php } ?>
        </ul>
  </div>
</nav>

<script type="text/javascript">
$('.dropdown').on('show.bs.dropdown', function () {
  $(this).siblings('.open').removeClass('open').find('a.dropdown-toggle').attr('data-toggle', 'dropdown');
  $(this).find('a.dropdown-toggle').removeAttr('data-toggle');
});
</script>