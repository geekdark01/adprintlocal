<div class="separator-big"><h2><span><?php echo $heading_title; ?></span></h2></div>
<div class="row">
    <?php foreach ($categories as $category) { 
		if($category['category_id']==1219){
				continue;
			}
	?>
        <div class="product-layout col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="product-thumb transition">
                <div class="image"><a href="<?php echo $category['href']; ?>"><img src="<?php echo $category['thumb']; ?>" alt="<?php echo $category['name']; ?>" title="<?php echo $category['name']; ?>" class="img-responsive img-categorie" /></a></div>
                <div class="button-group">
                    <button type="button" onclick="document.location='<?php echo $category['href']; ?>';">
                        <span class="hidden-xs hidden-sm hidden-md"><?php echo $category['name']; ?></span>
                    </button>
                </div>
            </div>
        </div>
    <?php } ?>
</div>
    
