<style>
	.btn-addprintcredit{
		background-color 	: #ed008c;
		color 				: #fff;
		border 				: 0px;
		font-weight 		: bold;
		padding 			: 6px 15px 5px;
	}	
</style>
	<?php 
		$disable = $disable_checkbox ? "disabled" : "";
	?>
	<p><input type="checkbox" id="codeAdmoneyBtn" <?php echo $disable;?> name="codeAdmoneyBtn"> <label for="codeAdmoneyBtn">Utiliser Admoney ?</label></p>
	<div class="codeAdmoney">
    	<?php 
    	if(isset($solde)){
    	?>
    		<p><?php echo $text_solde .'<span style="color:#ed008c">'. $solde.' '.$text_addprintcredit.'</span>';?></p>
	      	<div>
	      		<label for="montant"><?php echo $entry_montant; ?></label>
	      		<input id="montant" type="text" name="montant" style="width:147px" value="<?php if(isset($addprintcredit)) echo $addprintcredit ;?>"/>
	      		<input type="button" class="btn-addprintcredit" value="<?php echo $btn_addprintcredit; ?>" id="btn-montant"/>
	      	</div>
	      	</br>
	      	<div>
	      		<p style="float:left"><?php echo $entry_all ;?></p> &nbsp &nbsp 
	      		<input class="btn-addprintcredit" value="<?php echo $btn_addprintcredit; ?>" type="button" id="btn-all-montant"/>
	      	</div>
    	<?php
    	}
    	if(isset($error_compte_null)){
    	?>
    		<p><?php echo $error_compte_null; ?></p>
    	<?php 
    	}
    	if(isset($connexion)){
    		?>
    		<p> <?php echo $text_solde_non_connecter; ?> (<a href="<?php echo $connexion; ?>"><?php echo $text_connexion; ?></a>)</p>
    	<?php
    	}
    	?>
	</div>
      	
      <script type="text/javascript"><!--
$('#btn-montant').on('click', function() {
	if($(".alert")){
		$(".alert").remove() ;
	}
	var montant = $("#montant").val() ;
	if(!Number.isNaN(parseFloat(montant))){
		montant = parseFloat(montant) ;
		$.ajax({
		url: 'index.php?route=extension/total/addprintcredit/utiliserPartie',
		type: 'post',
		data: 'montant='+montant,
		dataType: 'json',
		success: function(json) {
			 if (json['error']) {
			 	$('.breadcrumbs').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

			 	$('html, body').animate({ scrollTop: 0 }, 'slow');
			 }
			  if(json['bon']){
			  	var url = json['bon'] ;
			  	$(location).attr("href", json['bon']) ;
			  }
		}
		
	});
	}
	if(Number.isNaN(parseFloat(montant))){
		$('.breadcrumbs').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_input_montant ;?><button type="button" class="close" data-dismiss="alert">&times;</button></div>');
		$('html, body').animate({ scrollTop: 0 }, 'slow');

	}
	
});
$('#btn-all-montant').on('click', function() {
	
	$.ajax({
	url: 'index.php?route=extension/total/addprintcredit/utiliserTous',
	type: 'post',
	dataType: 'json',
	success: function(json) {
		 if (json['error']) {
		 	$('.breadcrumbs').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

		 	$('html, body').animate({ scrollTop: 0 }, 'slow');
		 }
		  if(json['bon']){
		  	var url = json['bon'] ;
		  	$(location).attr("href", json['bon']) ;
		  }
		}
	});
	
});
//--></script>
    </div>
  </div>
</div>
