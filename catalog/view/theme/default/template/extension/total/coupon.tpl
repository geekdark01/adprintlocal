	<?php 
		$disable = $disable_checkbox ? "disabled" : "";
	?>
	<p><input type="checkbox" id="codePromoBtn" <?php echo $disable; ?> name="codePromoBtn"> <label for="codePromoBtn">Un code Promo ?</label></p>
	<div class="codePromo">
			<p><?php echo $entry_coupon; ?> :  <input type="text" name="coupon" value="<?php echo $coupon; ?>" placeholder="<?php echo $entry_coupon; ?>" id="input-coupon" class="form-control" /></p>
			<button class="valideCode" type="button" id="button-coupon" data-loading-text="<?php echo $text_loading; ?>"><?php echo $button_coupon; ?></button>
	</div>
	<script type="text/javascript"><!--
$('#button-coupon').on('click', function() {
	$.ajax({
		url: 'index.php?route=extension/total/coupon/coupon',
		type: 'post',
		data: 'coupon=' + encodeURIComponent($('input[name=\'coupon\']').val()),
		dataType: 'json',
		beforeSend: function() {
			$('#button-coupon').button('loading');
		},
		complete: function() {
			$('#button-coupon').button('reset');
		},
		success: function(json) {
			$('.alert').remove();

			if (json['error']) {
				$('.breadcrumbs').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

				$('html, body').animate({ scrollTop: 0 }, 'slow');
			}

			if (json['redirect']) {
				location = json['redirect'];
			}
		}
	});
});
//--></script>
    </div>
  </div>
</div>
