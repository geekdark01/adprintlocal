<?php
class ModelAddprintcardAddprintcard extends Model {
	public function addSolde($id_customer, $montant){
		if(isset($id_customer) && isset($montant) && $montant>0){
			$this->db->query("INSERT INTO card_customer SET `customer_id` = '" . (int)$id_customer . "', `ajout` = '" . true . "', `montants` = '" . $montant . "', `date_action` = '" . date("Y-m-d H:i:s") . "'");
		}
		if($montant<0 ){
			return 1 ;
		}
		if(!isset($montant) || $montant == 0){
			return 2 ;
		}
	}

	public function findSolde($id_customer){
		
		$total_add 		= $this->db->query("SELECT sum(montants) as soldeAdd from card_customer where customer_id=".$id_customer." and ajout=true") ;	
		$total_depense 	=  $this->db->query("SELECT sum(montants) as soldeDepense from card_customer where customer_id=".$id_customer." and ajout=false") ;

		$total_add 		= (double)$total_add->row['soldeAdd'] ;
		$total_depense 	= (double)$total_depense->row['soldeDepense'] ;
		$solde 			= $total_add-$total_depense ;
		
		$solde 			+= $this->findSoldeParrainage($id_customer);
		return $solde ;

	}
	
	// Parrainage //
	public function findSoldeParrainage($id_customer){
		
		$total_add 		= $this->db->query("SELECT sum(montants) as soldeAdd from card_customer_parrainage where customer_id=".$id_customer." and ajout=true and date_expiration > '".date('Y-m-d')."' and is_valid=true") ;	
		$total_depense 	=  $this->db->query("SELECT sum(montants) as soldeDepense from card_customer_parrainage where customer_id=".$id_customer." and ajout=false") ;

		$total_add 		= (double)$total_add->row['soldeAdd'] ;
		$total_depense 	= (double)$total_depense->row['soldeDepense'] ;
		$solde 			= $total_add-$total_depense ;
		return $solde ;

	}
	// Parrainage //
	
	public function verifiedCompte($id_customer){
		$compte 		= $this->db->query("SELECT * from card_customer where customer_id=".$id_customer) ;	
		if(empty($compte)){
			return null ;
		}
		if(!empty($compte)){
			return 1 ;
		}
	}
	public function paymentCard($id_customer, $montant){
		$solde = $this->findSolde($id_customer) ;
		$soldeParrainage = $this->findSoldeParrainage($id_customer) ;
		
		if(isset($id_customer) && isset($montant) && $montant>0){

			if($soldeParrainage > 0){
				$date_expiration = new DateTime(date("Y-m-d"));		
				$date_expiration->add(new DateInterval('P365D'));
				if($soldeParrainage>(double)$montant){
					$this->db->query("INSERT INTO card_customer_parrainage SET `customer_id` = '" . (int)$id_customer . "', `ajout` = '" . false . "', `montants` = '" . $montant . "', `is_valid`=1, `date_action` = '" . date("Y-m-d") . "',`date_expiration` = '".$date_expiration->format('Y-m-d')."'");
				}
				else{
					$montant -= $soldeParrainage;
					
					if($solde < (double)$montant){
						return 0 ;
					}
					if($solde >(double)$montant){
						$this->db->query("INSERT INTO card_customer_parrainage SET `customer_id` = '" . (int)$id_customer . "', `ajout` = '" . false . "', `montants` = '" . $soldeParrainage . "',`is_valid`=1, `date_action` = '" . date("Y-m-d") . "'");
						$this->db->query("INSERT INTO card_customer SET `customer_id` = '" . (int)$id_customer . "', `ajout` = '" . false . "', `montants` = '" . $montant . "', `date_action` = '" . date("Y-m-d H:i:s") . "',`date_expiration` = '".$date_expiration->format('Y-m-d')."'");
					}
				}
			}
			else{
				if($solde < (double)$montant){
					return 0 ;
				}
				if($solde>(double)$montant){
					$this->db->query("INSERT INTO card_customer SET `customer_id` = '" . (int)$id_customer . "', `ajout` = '" . false . "', `montants` = '" . $montant . "', `date_action` = '" . date("Y-m-d H:i:s") . "'");
				}
			}
		}
		if($montant<0){
			return 1 ;
		}
		if(!isset($montant)){
			return 2 ;
		}
	}
	public function findDepense($id_customer){
		$query = $this->db->query("SELECT montants,date_action,'Crédit simple' as type from card_customer where customer_id=".$id_customer." and ajout=0 union select montants,date_action,'Crédit parrainage' as type from card_customer_parrainage where customer_id=".$id_customer." and ajout=0") ;
		return $query->rows ;
	}
	public function findAjout($id_customer){
		$query = $this->db->query("SELECT montants,date_action,'Crédit simple' as type from card_customer where customer_id=".$id_customer." and ajout=1 union select montants,date_action,'Crédit parrainage' as type from card_customer_parrainage where customer_id=".$id_customer." and ajout=1 and is_valid=1 and date_expiration > '".date('Y-m-d')."'") ;
		
		return $query->rows ;
	}
	public function findAll($id_customer){		
		$query = $this->db->query("SELECT * from card_customer where customer_id=".$id_customer) ;	
		return $query->rows ;
	}
}