<?php
class ModelExtensionPaymentOrange extends Model {
	
	public function getMethod($address, $total) {
		
		$this->load->language('extension/payment/orange');
				
		$method_data = array(
				'code'       => 'orange',
				'title'      => $this->language->get('text_title'),
				'terms'      => '',
				'sort_order' => $this->config->get('cheque_sort_order')
			);
		return $method_data;
		
	}
	
}

