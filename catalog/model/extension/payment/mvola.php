<?php
class ModelExtensionPaymentMvola extends Model {
	
	public function getMethod($address, $total) {
		
		$this->load->language('extension/payment/mvola');
		
		$method_data = array(
				'code'       => 'mvola',
				'title'      => $this->language->get('text_title'),
				'terms'      => '',
				'sort_order' => $this->config->get('cheque_sort_order')
			);
		return $method_data;
		
	}
	
}

