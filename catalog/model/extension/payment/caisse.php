<?php
class ModelExtensionPaymentcaisse extends Model {
	
	public function getMethod($address, $total) {
		$this->load->language('extension/payment/caisse');
	
		$method_data = array(
				'code'       => 'caisse',
				'title'      => $this->language->get('text_title'),
				'terms'      => '',
				'sort_order' => $this->config->get('cheque_sort_order')
			);
		return $method_data;
		
		
	}
	
}