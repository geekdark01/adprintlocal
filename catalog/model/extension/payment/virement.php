<?php
class ModelExtensionPaymentVirement extends Model {
	
	public function getMethod($address, $total) {
		
		$this->load->language('extension/payment/virement');
				
		$method_data = array(
				'code'       => 'virement',
				'title'      => $this->language->get('text_title'),
				'terms'      => '',
				'sort_order' => $this->config->get('cheque_sort_order')
			);
		return $method_data;
		
	}
	
}

