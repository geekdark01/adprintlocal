<?php
class ModelExtensionPaymentAirtel extends Model {
	
	public function getMethod($address, $total) {
		$this->load->language('extension/payment/airtel');
	
		$method_data = array(
				'code'       => 'airtel',
				'title'      => $this->language->get('text_title'),
				'terms'      => '',
				'sort_order' => $this->config->get('cheque_sort_order')
			);
		return $method_data;
		
		
	}
	
}

