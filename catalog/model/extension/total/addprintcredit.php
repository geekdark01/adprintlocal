<?php
class ModelExtensionTotalAddprintcredit extends Model {
	public function getTotal($total) {
		if (isset($this->session->data['addprintcredit'])) {
			$this->load->language('extension/total/addprintcredit');

			$total['totals'][] = array(
				'code'       => 'addprintcredit',
				'title'      => $this->language->get('text_addprintcredit'),
				'value'      => -($this->session->data['addprintcredit']),
				'sort_order' => 3 
			);
		
			if(empty($total['taxes'])){
				$total['total'] -= $this->session->data['addprintcredit'];
			}
		}
	}
}