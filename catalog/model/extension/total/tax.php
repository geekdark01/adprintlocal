<?php
class ModelExtensionTotalTax extends Model {
	public function getTotal($total) {
		$this->load->model('account/customer');

			//TVA 
			if ($this->customer->isLogged()) {
				$data['TVA'] = $this->model_account_customer->getCustomerByTVA($this->customer->getId());
			}
			else{
				$data['TVA'] = 1;
			}
		foreach ($total['taxes'] as $key => $value) {
			//$value = $total['total'] * 20 / 100;
			if ($value > 0) {
				$total['totals'][] = array(
					'code'       => 'tax',
					//'title'      => $this->tax->getRateName($key),
					'title'      => 'TVA',
					'value'      => $value,
					'sort_order' => $this->config->get('tax_sort_order')
				);
				if ($data['TVA'] == 0) {
					$total['total'] = $total['total'];
				} else {
				$total['total'] += $value;
				}	
			}
		}
	}
}