
<?php 
    class ModelCheckoutParrainage extends Model{
		public function insertCompteCredit($customer_id,$montant){
			$date_expiration = new DateTime(date("Y-m-d"));		
			$date_expiration->add(new DateInterval('P365D'));
            $this->db->query("INSERT INTO card_customer_parrainage SET `customer_id` = '" . (int)$customer_id . "', `ajout` = '" . true . "', `montants` = '" . $montant . "', `date_action` = '" . date("Y-m-d") . "',`is_valid`='" . false . "',`date_expiration` = '".$date_expiration->format('Y-m-d')."'");      
        }
		
		public function getParrain($customer_id){
			$query = $this->db->query("SELECT parrain_id FROM " . DB_PREFIX . "parrainage WHERE customer_id = '" . $customer_id ."'");
			return $query->row;
		}
		
		public function sendMailParrain($filleul_id,$parrain_id,$pourc){
			$this->load->model('account/customer');
			$parrain = $this->model_account_customer->getCustomer($parrain_id);
			$filleul = $this->model_account_customer->getCustomer($filleul_id);
			$mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
			$mail->smtp_username = $this->config->get('config_mail_smtp_username');
			$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
			$mail->smtp_port = $this->config->get('config_mail_smtp_port');
			$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

			$mail->setTo($parrain['email']);
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender(html_entity_decode('AdPrint', ENT_QUOTES, 'UTF-8'));
			$mail->setSubject(html_entity_decode('Parrainage', ENT_QUOTES, 'UTF-8'));
			
			$data['firstname'] = $parrain['firstname'];
			$data['lastname'] = $parrain['lastname'];
			$data['filleul']['firstname']=$filleul['firstname'];
			$data['filleul']['lastname']=$filleul['lastname'];
			$data['pourc']=$pourc;
			$data['logo'] = $this->config->get('config_url') . 'image/' . $this->config->get('config_logo');
			$data['store_name'] = 'AdPrint';
			$data['store_url'] = 'adprint.mg';
			$mail->setHtml($this->load->view('mail/parrainage_gain', $data));
			$mail->setText('Votre filleul '.$filleul['firstname'].' '.$filleul['lastname'].' a effectué une commande. Vous allez recevoir '.$pourc.'% du CA HT de la commande');
			$mail->send();
		}
	
		public function getParrains($customer_id){
			$parrains = array();
            //Niveau 1
			$parrainNiv1 = $this->getParrain($customer_id);
			if(!empty($parrainNiv1)){
				$parrains[] = array('parrain_id'=>$parrainNiv1['parrain_id'],'pourc'=>'6');
				//Niveau 2
				$parrainNiv2 = $this->getParrain($parrainNiv1['parrain_id']);
				if(!empty($parrainNiv2)){
					$parrains[] = array('parrain_id'=>$parrainNiv2['parrain_id'],'pourc'=>'3');
					//Niveau 3
					$parrainNiv3 = $this->getParrain($parrainNiv2['parrain_id']);
					if(!empty($parrainNiv3)){
						$parrains[] = array('parrain_id'=>$parrainNiv3['parrain_id'],'pourc'=>'1');
					}
				}
			}
			return $parrains;
        }
}