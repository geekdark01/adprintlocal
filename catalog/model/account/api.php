<?php
class ModelAccountApi extends Model {
	public function getApiByKey($key = 'q7oIdG6RKSS4tnniSGt0z7wpcSSyiMjZkRCTCtsrlLPHx0K6MOQpQj8utkwK08Mjn8D4ePoFngqadnhnpD0P4x4Qrforll2nTVxF5KC8QOffyiztdlRh9oFrlFaVnyWrNkYVL5ZprCSsiOm21rjFmLQyfy4ChYr8MF9lV9d6uSK7gBFGw18HKXnSNYLzmzKT6MamVEkJXGwWkcQaQox81RCOPWXpTetei3WO3YGIEmDTlDbFN0fdgX4QA0Rm4F4y') {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "api` WHERE `key` = '" . $this->db->escape($key) . "' AND status = '1'");

		return $query->row;
	}

	public function addApiSession($api_id, $session_id, $ip) {
		$token = token(32);

		$this->db->query("INSERT INTO `" . DB_PREFIX . "api_session` SET api_id = '" . (int)$api_id . "', token = '" . $this->db->escape($token) . "', session_id = '" . $this->db->escape($session_id) . "', ip = '" . $this->db->escape($ip) . "', date_added = NOW(), date_modified = NOW()");

		return $token;
	}

	public function getApiIps($api_id) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "api_ip` WHERE api_id = '" . (int)$api_id . "'");

		return $query->rows;
	}
}
