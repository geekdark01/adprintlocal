<?php
class ModelAccountAvantage extends Model {
	public function sendMail($contact,$msg,$client){
		
		$data['msg'] = $msg;
		$data['logo'] = $this->config->get('config_url') . 'image/' . $this->config->get('config_logo');
		$data['store_name'] = 'AdPrint';
		$data['store_url'] = 'adprint.mg';
		$data['client'] = $client;
		
		$this->load->model('account/customer');
		$mail = new Mail();
		$mail->protocol = $this->config->get('config_mail_protocol');
		$mail->parameter = $this->config->get('config_mail_parameter');
		$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
		$mail->smtp_username = $this->config->get('config_mail_smtp_username');
		$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
		$mail->smtp_port = $this->config->get('config_mail_smtp_port');
		$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
		$mail->setTo($contact);
		$mail->setFrom($this->config->get('config_email'));
		$mail->setSender(html_entity_decode('AdPrint', ENT_QUOTES, 'UTF-8'));
		$mail->setSubject(html_entity_decode('Parrainage', ENT_QUOTES, 'UTF-8'));
		$mail->setHtml($this->load->view('mail/parrainage', $data));
		$mail->send();
	}
}
