<?php
class ModelCatalogCategory extends Model {
	
	public function getCategories_by_name( $mot ) {
	
	$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE cd.name like '".$mot."%' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "'  AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)");

		return $query->rows;
	}
	
	
	public function getCategories_by_name_interieur( $mot ) {
	
	$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE cd.name like '%".$mot."%' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "'  AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)");

		return $query->rows;
	}
	
	public function getCategories_by_product_affinity( $product_affinity_id ) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.product_affinity_id = '" . (int)$product_affinity_id . "' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND c.status = '1'");

		return $query->rows;
	}
	
	public function getCategory($category_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.category_id = '" . (int)$category_id . "' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND c.status = '1'");

		return $query->row;
	}

	public function getCategories($parent_id = 0) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.parent_id = '" . (int)$parent_id . "' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "'  AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)");//echo "SELECT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.parent_id = '" . (int)$parent_id . "' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "'  AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)"."<br>";//exit;echo '<pre>';		print_r($query->rows);						echo '</pre>'; exit;						
		return $query->rows;
	}
    
    public function getCategoriesUp($parent_id = 0) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.sous_menu = 1 AND c.parent_id = '" . (int)$parent_id . "' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "'  AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)");

		return $query->rows;
	}
    
    /*dev101*/
    public function getAllCategories() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE cd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "'  AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)");

		return $query->rows;
	}
    
    public function getPrixMinCategorie($category_id) {
		$query = $this->db->query("SELECT MIN(price) as prix_minimum FROM `" . DB_PREFIX . "product_to_category` LEFT JOIN " . DB_PREFIX . "product ON " . DB_PREFIX . "product.product_id = " . DB_PREFIX . "product_to_category.`product_id` WHERE category_id = " . $category_id . "");

		return $query->rows;
	}
    
    public function getDossierByIdCategory($category_id) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "dossier` WHERE category_id = ". $category_id );
		return $query->rows;
	}
    
    public function getSerializeParent($category_id) {
        $toSerializeParent = array() ;
        $oCategorieId = $category_id ;
        while(1<2){
            $query  = $this->db->query("SELECT parent_id FROM `" . DB_PREFIX . "category` WHERE category_id = ".$oCategorieId."");
            $row    = $query->rows;
            if(isset($row) && $row != null){
                if($row[0]['parent_id'] != 0){
                    array_push($toSerializeParent, $row[0]['parent_id'] ) ;
                    $oCategorieId     = $row[0]['parent_id'] ; 
                }
                else{break;}
            }
            else{break;}
        }
        $serializeParent = "" ;
        
        for($i = count($toSerializeParent)-1 ; $i >= 0 ; $i--){
            if($i == count($toSerializeParent)-1){
                $serializeParent .= $toSerializeParent[$i] ;
            }else{
                $serializeParent .= "_".$toSerializeParent[$i] ;
            }
        }
        $serializeParent .= "_".$category_id ;
        
		return $serializeParent;
	}
    
    /*dev101*/

	public function getCategoryFilters($category_id) {
		$implode = array();

		$query = $this->db->query("SELECT filter_id FROM " . DB_PREFIX . "category_filter WHERE category_id = '" . (int)$category_id . "'");

		foreach ($query->rows as $result) {
			$implode[] = (int)$result['filter_id'];
		}

		$filter_group_data = array();

		if ($implode) {
			$filter_group_query = $this->db->query("SELECT DISTINCT f.filter_group_id, fgd.name, fg.sort_order FROM " . DB_PREFIX . "filter f LEFT JOIN " . DB_PREFIX . "filter_group fg ON (f.filter_group_id = fg.filter_group_id) LEFT JOIN " . DB_PREFIX . "filter_group_description fgd ON (fg.filter_group_id = fgd.filter_group_id) WHERE f.filter_id IN (" . implode(',', $implode) . ") AND fgd.language_id = '" . (int)$this->config->get('config_language_id') . "' GROUP BY f.filter_group_id ORDER BY fg.sort_order, LCASE(fgd.name)");

			foreach ($filter_group_query->rows as $filter_group) {
				$filter_data = array();

				$filter_query = $this->db->query("SELECT DISTINCT f.filter_id, fd.name FROM " . DB_PREFIX . "filter f LEFT JOIN " . DB_PREFIX . "filter_description fd ON (f.filter_id = fd.filter_id) WHERE f.filter_id IN (" . implode(',', $implode) . ") AND f.filter_group_id = '" . (int)$filter_group['filter_group_id'] . "' AND fd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY f.sort_order, LCASE(fd.name)");

				foreach ($filter_query->rows as $filter) {
					$filter_data[] = array(
						'filter_id' => $filter['filter_id'],
						'name'      => $filter['name']
					);
				}

				if ($filter_data) {
					$filter_group_data[] = array(
						'filter_group_id' => $filter_group['filter_group_id'],
						'name'            => $filter_group['name'],
						'filter'          => $filter_data
					);
				}
			}
		}

		return $filter_group_data;
	}

	public function getCategoryLayoutId($category_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_to_layout WHERE category_id = '" . (int)$category_id . "' AND store_id = '" . (int)$this->config->get('config_store_id') . "'");

		if ($query->num_rows) {
			return $query->row['layout_id'];
		} else {
			return 0;
		}
	}

	public function getTotalCategoriesByCategoryId($parent_id = 0) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.parent_id = '" . (int)$parent_id . "' AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND c.status = '1'");

		return $query->row['total'];
	}
    //DEV103 Les Indispensables
    public function getCategoriesChoixIndispensables()
    {
        $sql = "SELECT cp.path_id , cp.category_id AS category_id, GROUP_CONCAT(cd1.name ORDER BY cp.level SEPARATOR '&nbsp;&nbsp;&gt;&nbsp;&nbsp;') AS name, c1.parent_id, c1.sort_order FROM " . DB_PREFIX . "category_path cp LEFT JOIN " . DB_PREFIX . "category c1 ON (cp.category_id = c1.category_id) LEFT JOIN " . DB_PREFIX . "category c2 ON (cp.path_id = c2.category_id) LEFT JOIN " . DB_PREFIX . "category_description cd1 ON (cp.path_id = cd1.category_id) LEFT JOIN " . DB_PREFIX . "category_description cd2 ON (cp.category_id = cd2.category_id) WHERE cd1.language_id = '" . (int)$this->config->get('config_language_id') . "' AND cd2.language_id = '" . (int)$this->config->get('config_language_id') . "'" ;
		$sql .= " GROUP BY cp.category_id";
		$sql .= " ORDER BY sort_order DESC";
        $query = $this->db->query($sql);
		return $query->rows;
    }
    public function getCategoriesPath($_iCategorieId)
    {
        $sql = "SELECT category_id FROM " . DB_PREFIX . "category_path WHERE category_id = " . $_iCategorieId . " AND path_id = " . $_iCategorieId ;		
        $query = $this->db->query($sql);
        $toCategoriesPath = array() ;
        foreach($query->rows as $toCategoriePath)
        {
            array_push($toCategoriesPath, $toCategoriePath['category_id']) ;
        }
		return $toCategoriesPath ;
    }
    public function loadIndispensable($_iCategorieId)
    {
        $sql = "SELECT * FROM " . DB_PREFIX . "category_indispensable ci  WHERE ci.category_id = '" . (int)$_iCategorieId . "'" ;
	    $query = $this->db->query($sql);
		return $query->rows;
    }
    //DEV103 Les Indispensables
	
	public function categoriesImbriques($_iCategorieId)
    {
        $this->load->model('catalog/category') ;
        $toCategories       = $this->getCategoriesChoixIndispensables() ;
        $toCategoriesPathId = $this->getCategoriesPath($_iCategorieId) ;
        foreach($toCategories as $iKey=>$toCategorie)
        {
            if(!in_array($toCategorie['category_id'], $toCategoriesPathId))
            {
                unset($toCategories[$iKey]) ;
            }
        }
        return $toCategories ;
    }
	
	public function calculPrixMin($category_id){
		$prix_minimum = 0;
		$toCategorieImplode = $this->categoriesImbriques($category_id) ;
		
		$toCategorieToImpode = array() ;
		foreach($toCategorieImplode as $toCategorie)
		{
			array_push($toCategorieToImpode, $category_id );
		}
		
		if(count($toCategorieToImpode) > 0)
		{
			$prix_minimum = $this->model_catalog_product->getPriceMinPerCategories($toCategorieToImpode) ;
		}
		
		$filles = $this->getCategories($category_id);
		// considerer filles dans le calcul prix minimum //
		
		if(!empty($filles)){
			foreach($filles as $fille){
				$min = $this->calculPrixMin($fille['category_id']) ;
				if($prix_minimum == 0 || ($prix_minimum > $min && $min!=0)){
					$prix_minimum = $min;
				}
			}
		}
		// considerer filles dans le calcul prix minimum //
		return $prix_minimum;
	}
	
	public function getAllProducts($category_id){
		$this->load->model('catalog/product');
		$filles = $this->getCategories($category_id);
		$rep=array();
		if(!empty($filles)){
			foreach($filles as $fille){
				$rec = $this->getAllProducts($fille['category_id']); 
				if(!empty($rec)){
					foreach($rec as $r){
						$rep[] = $r;
					}
				}
			}
		}else{
			$products = $this->model_catalog_product->getProducts(array("filter_category_id" => $category_id));
			foreach($products as $product){
				$rep[] = $product;
			}
		}
		return $rep;
	}
}