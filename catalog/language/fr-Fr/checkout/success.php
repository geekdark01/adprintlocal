<?php

// Heading

$_['heading_title']        ='Votre commande a bien été reçue!';
$_['heading_title_devis']  ='Votre devis a bien été reçue!';


// Text

$_['text_basket']          ='Panier';
$_['text_checkout']        ='Check-out';
$_['text_success']         ='Succès';
$_['text_customer_caisse'] ='<p> Votre commande a été traitée avec succès! </p>  <p> Vous pouvez afficher votre historique des commandes en allant dans la page <a href="%s"> Mon compte </a> et en cliquant sur <a href="%s"> Mes commandes </a> .</p><p>Veuillez passer à la caisse pour effectuer votre payement, votre commande : <b> "%s" </b>" sera traité dès que possible.</p> <p> Merci de faire vos achats en ligne avec nous! </p> ';
$_['text_customer_admoney'] ='<p> Votre commande a été traitée avec succès! </p>  <p> Vous pouvez afficher votre historique des commandes en allant dans la page <a href="%s"> Mon compte </a> et en cliquant sur <a href="%s"> Mes commandes </a> .</p><p>Vous avez utiliser l\'admoney en tant que méthode de payement, votre commande : <b> "%s" </b>" sera traité dès que possible.</p> <p> Merci de faire vos achats en ligne avec nous! </p> ';

$_['text_customer_airtel'] ='<p> Votre commande a été traitée avec succès! </p>  <p> Vous pouvez afficher votre historique des commandes en allant dans la page <a href="%s"> Mon compte </a> et en cliquant sur <a href="%s"> Mes commandes </a> . </p>  <p> Veuillez régler votre paiement par Airtel money au numéro 0 33 44 374 47 </p> <p>Afin de faciliter la prise en compte de votre paiement, nous vous remercions par avance de bien vouloir indiquer votre n° de commande : <b> "%s" </b> dans votre objet.</p> <p> Merci de faire vos achats en ligne avec nous! </p> ';

$_['text_customer_orange'] ='<p> Votre commande a été traitée avec succès! </p>  <p> Vous pouvez afficher votre historique des commandes en allant dans la page <a href="%s"> Mon compte </a> et en cliquant sur <a href="%s"> Mes commandes </a> . </p>  <p> Veuillez régler votre paiement par Orange money au numéro 0 32 44 374 47 </p> <p>Afin de faciliter la prise en compte de votre paiement, nous vous remercions par avance de bien vouloir indiquer votre n° de commande : <b> "%s" </b> dans votre objet.</p> <p> Merci de faire vos achats en ligne avec nous! </p> ';

$_['text_customer_mvola'] ='<p> Votre commande a été traitée avec succès! </p>  <p> Vous pouvez afficher votre historique des commandes en allant dans la page <a href="%s"> Mon compte </a> et en cliquant sur <a href="%s"> Mes commandes </a> . </p>  <p> Veuillez régler votre paiement par MVola au numéro 0 34 44 374 47 </p> <p>Afin de faciliter la prise en compte de votre paiement, nous vous remercions par avance de bien vouloir indiquer votre n° de commande : <b> "%s" </b> dans votre objet.</p> <p> Merci de faire vos achats en ligne avec nous! </p> ';

$_['text_customer_cheque'] ='<p> Votre commande a été traitée avec succès! </p>  <p> Vous pouvez afficher votre historique des commandes en allant dans la page <a href="%s"> Mon compte </a> et en cliquant sur <a href="%s"> Mes commandes </a> . </p>  <p> Veuillez régler votre paiement en envoyant le chèque à l\'adresse suivante : II I A bis T Alarobia Amboniloha.</p> <p> Merci de faire vos achats en ligne avec nous! </p> ';

$_['text_customer_virement'] ='<p> Votre commande a été traitée avec succès! </p>  <p> Vous pouvez afficher votre historique des commandes en allant dans la page <a href="%s"> Mon compte </a> et en cliquant sur <a href="%s"> Mes commandes </a> . </p>  <p> Veuillez régler votre paiement pour valider votre commande.</p> <p>Afin de faciliter la prise en compte de votre paiement, nous vous remercions par avance de bien vouloir indiquer votre n° de commande : <b> "%s" </b> dans votre ordre de virement.</p> <p> Merci de faire vos achats en ligne avec nous! </p> <img src="http://adprint-dev.com/image/catalog/rib.jpg" />';

$_['text_customer_cheque'] ='<p> Votre commande a été traitée avec succès! </p>  <p> Vous pouvez afficher votre historique des commandes en allant dans la page <a href="%s"> Mon compte </a> et en cliquant sur <a href="%s"> Mes commandes </a> . </p>  <p> Veuillez régler votre paiement en envoyant le chèque à l\'adresse suivante : II I A bis T Alarobia Amboniloha.</p> <p> Merci de faire vos achats en ligne avec nous! </p> ';

$_['text_devis_customer']  ='<p>Votre devis a été traité avec succès! </ p> <p> Vous pouvez consulter votre historique de soumission en accédant à la page <a href="%s"> mon compte </a> et en cliquant sur <a href = "% s "> Historique des devis </a>. </ p> <p> Merci de faire vos achats en ligne avec nous! </p>';

$_['text_guest']           ='  <p> Votre commande a été traitée avec succès! </p>  <p> Veuillez adresser vos questions au <a href="%s"> propriétaire du magasin </a> . </p>  <p> Merci d`avoir acheté avec nous en ligne! </p> ';