<?php
// Heading
$_['heading_title']         = 'Gestion Fichier';

// Text
$_['text_gestion_fichier']  = 'Gestion Fichier';
$_['text_choix_commande']   = 'Choisir commande';
$_['text_titre_produit']    = 'Produit';
$_['text_titre_statut']     = 'Statut';
$_['text_titre_action']     = 'Action';
$_['button_upload']         = 'Envoi fichier';
$_['text_verouiller']       = 'Verouiller';
$_['text_verouille']        = 'Verouillé';
$_['button_upload']         = 'Envoi fichier';
$_['text_loading']          = 'Chargement en cours...';