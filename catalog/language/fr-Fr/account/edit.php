<?php

// Heading

$_['heading_title']      ='Informations sur mon compte';


// Text

$_['text_account']       ='Compte';
$_['text_edit']          ='Modifier l`information';
$_['text_your_details']  ='Vos informations personnelles';
$_['text_success']       ='Succès: votre compte a été mis à jour avec succès.';


// Entry

$_['entry_firstname']    ='Prénom';
$_['entry_lastname']     ='Nom de famille';
$_['entry_email']        ='Email';
$_['entry_telephone']    ='Téléphone';



// Error

$_['error_exists']                ='Attention: l`adresse e-mail est déjà enregistrée!';
$_['error_firstname']             ='Le prénom doit être compris entre 1 et 32 ​​caractères!';
$_['error_lastname']              ='Le nom de famille doit être compris entre 1 et 32 ​​caractères!';
$_['error_email']                 ='L`adresse e-mail ne semble pas être valide!';
$_['error_telephone']             ='Le téléphone doit avoir entre 3 et 32 ​​caractères!';
$_['error_NIF']            		  ='Le NIF ne semble pas être valide!';
$_['error_STAT']           		  ='Le STAT ne semble pas être valide!';
$_['error_secteur']        		  ='Sélectionnez un secteur';
$_['error_company']       		  ='Le nom de société doit être compris entre 1 et 50 ​​caractères!';
$_['error_custom_field']          ='%s requis!';