<?php
// Heading
$_['heading_title']         = 'Mes fichiers BAT';

// Text
$_['text_account']          = 'Account';
$_['text_gestion_fichier']  = 'Mes fichiers BAT';
$_['text_date_bat']         = 'Date BAT';
$_['text_bat_verifier']     = 'BAT à vérifier';
$_['text_valider']          = 'Valider';
$_['text_refuser']          = 'Refuser';
$_['text_valide']           = 'Validé';
$_['text_refuse']           = 'Refusé';
$_['text_choix_commande']   = 'Choisir commande';
$_['text_titre_produit']    = 'Produit';
$_['text_titre_statut']     = 'Statut';
$_['text_titre_action']     = 'Action';
$_['button_upload']         = 'Envoi fichier';
$_['text_verouiller']       = 'Verouiller';
$_['text_verouille']        = 'Verouillé';
$_['button_upload']         = 'Envoi fichier';
$_['text_loading']          = 'Chargement en cours...';