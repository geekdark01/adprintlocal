<?php
// Text
$_['text_title']				= 'Chèque';
$_['text_instruction']			= 'Chèque Instructions';
$_['text_payable']				= 'Make Payable To: ';
$_['text_address']				= 'Send To: ';
$_['text_payment']				= 'Your order will not ship until we receive payment.';