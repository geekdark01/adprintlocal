<?php 
//Heading
$_['heading_title']						= 'Satisfaction client';
//Text
$_['text_account']						= 'Account';
$_['text_information']						= 'Vos informations';
$_['text_success']						= 'Success : Your password has been successfully upadted';
//Entry
$_['input_firstname']						= 'Nom';
$_['tel_confirm']						= 'Telephone';
$_['adresse']						= 'Mail';
$_['societe']						= 'societe';
$_['sujetdmd']						= 'Sujet de votre demande';
$_['text_order']						= 'Order information';
$_['text_order_detail']						= 'Order details';
$_['text_invoice_no']						= 'Invoice No. : ';
$_['text_order_id']						= 'Order ID : ';
$_['text_date_added']						= 'Date Added';
$_['text_shipping_address']						= 'Shipping Address';
$_['text_shipping_method']						= 'Shipping Method';
$_['text_payment_address']						= 'Payment Address';
$_['text_payment_method']						= 'Payment Method';
$_['text_comment']						= 'Order comments';
$_['text_history']						= 'Order History';
$_['text_success']						= 'Success : You have added <a href="%s">%s</a> ';
$_['text_empty']						= 'You have not made any previous order';
$_['text_error']						= 'The order you requested could not be found';
$_['selectioncmd']						= 'Selectionner une commande';
$_['motifclient']						= 'Motif du contact auprès du service satisfaction client';
$_['idateajouthistorique']						= "Date de l'ajout";
$_['souhait']						= "Votre souhait";
$_['commentaire']						= "Votre commentaire";
$_['zNomsaisie']						= "Nom saisie";
$_['zMailsaisie']						= "Mail saisie";
$_['zTelsaisie']						= "Tel saisie";
$_['zSocietesaisie']						= "Societe saisie";
$_['zCommentsaisie']						= "Commentaire saisie";
$_['zsujetdmdsaisie']						= "sujet demande saisie";
$_['Extension_correcte']						= "Extension correcte";
$_['Extension_incorrecte']						= "Extension incorrecte";

//Column
$_['column_order_id']						= "Order ID";
$_['column_customer']						= "Customer";
$_['column_product']						= "No. of Products";
$_['column_name']						= "Product name";
$_['column_model']						= "Model";
$_['column_quantity']						= "Quantity";
$_['column_price']						= "Price";
$_['column_total']						= "Total";
$_['column_action']						= "Action";
$_['column_date_added']						= "Date added";
$_['column_status']						= "Status";
$_['column_comment']						= "Comment";
$_['resul']						= "Resultat";
$_['action']						= "recup";
$_['nom']						= "nom";

//Error
$_['error_reorder']						= "%s is not currently available to be reordered.";