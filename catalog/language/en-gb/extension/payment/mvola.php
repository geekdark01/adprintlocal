<?php
// Text
$_['text_title']				= 'Commande MVola';
$_['text_instruction']			= 'MVola instructions';
$_['text_payable']				= 'Make Payable To: ';
$_['text_address']				= 'Send To: ';
$_['text_payment']				= 'Your order will not ship until we receive payment.';