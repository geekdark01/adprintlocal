<?php
// Heading
$_['heading_title'] = 'Utiliser Admoney';

// Text
$_['text_addprintcredit']   = 'Admoney';
$_['text_solde']  			= 'Vous disposez de ';
$_['text_success']  		= 'Success: votre payment par Admoney est fait';
$_['btn_addprintcredit']  	= 'Ok';
$_['text_solde_non_connecter'] = 'Pour pouvoir utiliser <b>Admoney</b> vuillez vous connecter';
$_['text_connexion'] = 'se connecter';


// Entry
$_['entry_montant']  = 'Utiliser ';
$_['entry_all']		 = 'Utiliser tous mes Admoney';


// Error
$_['error_montant_insuffisant']  = 'Votre montant est insuffisant';
$_['error_empty']   = 'Vous avez 0 Admoney';
$_['error_input_montant']   = 'La valeur que vous avez inseré sur le montant utiliser est un text';