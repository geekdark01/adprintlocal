<?php
class ControllerCommonHeader extends Controller {
  
  
  public function index() {
    
  
  
    // Analytics
    $this->load->model('extension/extension');
        $this->load->model('tool/image');

    $data['analytics'] = array();

  
     
    $analytics = $this->model_extension_extension->getExtensions('analytics');

    foreach ($analytics as $analytic) {
      if ($this->config->get($analytic['code'] . '_status')) {
        $data['analytics'][] = $this->load->controller('extension/analytics/' . $analytic['code'], $this->config->get($analytic['code'] . '_status'));
      }
    }

    if ($this->request->server['HTTPS']) {
      $server = $this->config->get('config_ssl');
    } else {
      $server = $this->config->get('config_url');
    }

    if (is_file(DIR_IMAGE . $this->config->get('config_icon'))) {
      $this->document->addLink($server . 'image/' . $this->config->get('config_icon'), 'icon');
    }
	
    $data['title'] = $this->document->getTitle();
  
    $data['base'] = $server;
    $data['description'] = $this->document->getDescription();
    $data['keywords'] = $this->document->getKeywords();
    $data['links'] = $this->document->getLinks();
    $data['styles'] = $this->document->getStyles();
    $data['scripts'] = $this->document->getScripts();
    $data['lang'] = $this->language->get('code');
    $data['direction'] = $this->language->get('direction');

    $data['name'] = $this->config->get('config_name');

    if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
      $data['logo'] = $server . 'image/' . $this->config->get('config_logo');
    } else {
      $data['logo'] = '';
    }

    $this->load->language('common/header');

    $data['text_home'] = $this->language->get('text_home');

    // Wishlist
    if ($this->customer->isLogged()) {
      $this->load->model('account/wishlist');

      $data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), $this->model_account_wishlist->getTotalWishlist());
    } else {
      $data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
    }

    $data['text_shopping_cart'] = $this->language->get('text_shopping_cart');
    $data['text_logged'] = sprintf($this->language->get('text_logged'), $this->url->link('account/account', '', true), $this->customer->getFirstName(), $this->url->link('account/logout', '', true));

    $data['text_account'] = $this->language->get('text_account');
    $data['text_register'] = $this->language->get('text_register');
    $data['text_login'] = $this->language->get('text_login');
    $data['text_order'] = $this->language->get('text_order');
    $data['text_transaction'] = $this->language->get('text_transaction');
    $data['text_download'] = $this->language->get('text_download');
    $data['text_logout'] = $this->language->get('text_logout');
        //DEV103 Validation BAT
        $data['text_fichier_bat'] = $this->language->get('text_fichier_bat');
        //DEV103 Validation BAT
    $data['text_checkout'] = $this->language->get('text_checkout');
    $data['text_category'] = $this->language->get('text_category');
    $data['text_all'] = $this->language->get('text_all');
    $data['text_gestion_fichier'] = $this->language->get('text_gestion_fichier');

    $data['url_gestion_fichier'] = $this->url->link('account/account/gestionfichier', '', true);
        $data['home'] = $this->url->link('common/home');
    $data['wishlist'] = $this->url->link('account/wishlist', '', true);
    $data['logged'] = $this->customer->isLogged();
    $data['account'] = $this->url->link('account/account', '', true);
    $data['register'] = $this->url->link('account/register', '', true);
    $data['login'] = $this->url->link('account/login', '', true);
    $data['order'] = $this->url->link('account/order', '', true);
    $data['transaction'] = $this->url->link('account/transaction', '', true);
    $data['download'] = $this->url->link('account/download', '', true);
    $data['logout'] = $this->url->link('account/logout', '', true);
        //DEV103 Validation BAT
    $data['bat'] = $this->url->link('bat/bat', '', true);
        //DEV103 Validation BAT
    $data['shopping_cart'] = $this->url->link('checkout/cart');
    $data['checkout'] = $this->url->link('checkout/checkout', '', true);
    $data['contact'] = $this->url->link('information/contact');
    $data['telephone'] = $this->config->get('config_telephone');

    // Menu
    $this->load->model('catalog/category');

    $this->load->model('catalog/product');

    $data['categories'] = array();

    $categories = $this->model_catalog_category->getCategories(0);

    foreach ($categories as $iKey=>$category) {		
            //DEV103 Les Indispensables
            $toCategorieIndispensables = $this->model_catalog_category->loadIndispensable($category['category_id']) ;
           /* foreach($toCategorieIndispensables as $iKey=>$toCategorieIndispensable)
            {
                $toCategorieIndispensables[$iKey]['type_text'] = ($toCategorieIndispensable['type'] == 'c') ? $this->language->get('text_type_cat') :  $this->language->get('text_type_prd') ;
                if($toCategorieIndispensable['type'] == 'p')
                {
                    $toProduct = $this->model_catalog_product->getProduct($toCategorieIndispensable['item_id']) ;
                    $toCategorieIndispensables[$iKey]['name']       = $toProduct['name'] ;
                    $toCategorieIndispensables[$iKey]['meta_title'] = $toProduct['meta_title'] ;
                    $toCategorieIndispensables[$iKey]['price']      = $this->currency->format($toProduct['price'], $this->session->data['currency']) ;
                    $toCategorieIndispensables[$iKey]['href']       = $this->url->link('product/product', 'product_id=' . $toCategorieIndispensable['item_id']) ;
                }
                else
                {
                    $toCategorieIndispensables[$iKey]['name']       = '' ;
                    $toCategorieIndispensables[$iKey]['meta_title'] = '' ;
                            
                    $toCategorie = $this->model_catalog_category->getCategory($toCategorieIndispensable['item_id']) ;
                    if(isset($toCategorie) && $toCategorie != null){
                            $toCategorieIndispensables[$iKey]['name']       = ($toCategorie['name'] != '') ? $toCategorie['name'] : '' ;
                            $toCategorieIndispensables[$iKey]['meta_title'] = ($toCategorie['meta_title'] != '') ? $toCategorie['meta_title'] : '' ;
                    }
                    $toCategorieIndispensables[$iKey]['href']       = $this->url->link('product/category', 'path=' .$category['category_id'] .'_'. $toCategorieIndispensable['item_id']) ;
					$toCategorieIndispensables[$iKey]['price']  	= $this->model_catalog_category->calculPrixMin($toCategorieIndispensable['item_id']) ;
                    $toCategorieIndispensables[$iKey]['price'] = $this->currency->format($toCategorieIndispensables[$iKey]['price'], $this->session->data['currency']) ;
                }
            }*/
			
            //DEV103 Les Indispensables
            //DEV103 pour image changée onmouseover
            if ($category['image']) {
				//$data['categories'][$iKey]['thumb'] = $this->model_tool_image->resize($category['image'], $this->config->get($this->config->get('config_theme') . '_image_category_width'), $this->config->get($this->config->get('config_theme') . '_image_category_height'));
				$categories[$iKey]['thumb'] = $this->model_tool_image->resize($category['image'], 125, 125);
			} else {
				//$data['categories'][$iKey]['thumb'] = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_category_width'), $this->config->get($this->config->get('config_theme') . '_image_category_height'));
				$categories[$iKey]['thumb'] = $this->model_tool_image->resize('placeholder.png', 125, 125);
			}
			
            //DEV103 pour image changée onmouseover
            //DEV103 Image promo
            if ($category['image_promo']) {
				$categories[$iKey]['thumb_promo'] = $this->model_tool_image->resize($category['image_promo'], 125, 125);
			} else {
				$categories[$iKey]['thumb_promo'] = $this->model_tool_image->resize('placeholder.png', 125, 125);
			}
			$categories[$iKey]['link_thumb_promo'] = $category['lien_image_promo'];
            //DEV103 Image promo
			
			if ($category['top']) {
			// Level 2
			$children_data = array();

			// $children = $this->model_catalog_category->getCategories($category['category_id']);
			$children = $this->model_catalog_category->getCategoriesUp($category['category_id']);
			
			foreach ($children as $child) {
				
				$filter_data = array(
				'filter_category_id'  => $child['category_id'],
				'filter_sub_category' => true
			);
                    
			//DEV103 pour image changée onmouseover
			$zChildCategorieImage = '' ;
			if ($child['image']) {
				$zChildCategorieImage = $this->model_tool_image->resize($child['image'], 125, 125);
			} else {
				//$data['categories'][$iKey]['thumb'] = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_category_width'), $this->config->get($this->config->get('config_theme') . '_image_category_height'));
				$zChildCategorieImage = $this->model_tool_image->resize('placeholder.png', 125, 125);
			}
			
			$children_data[] = array(
				//'name'  => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
				'name'  => $child['name'] ,
				'href'  => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id']),
				'thumb'  => $zChildCategorieImage,
				'category_id'  => $child['category_id']
			);
			
			// if($child['category_id'] == 740){
			// echo "<pre>";
			// print_r($children_data);
			// echo "</pre>";
			// exit;
		//}
			//DEV103 pour image changée onmouseover
        }
		
        // Level 1
        $data['categories'][] = array(
		//DEV103 Les Indispensables
			//'indispensable' => $toCategorieIndispensables,
		//DEV103 Les Indispensables
		//DEV103 Image promo
			'thumb_promo'   => $categories[$iKey]['thumb_promo'],
			'link_thumb_promo'   => $categories[$iKey]['link_thumb_promo'],
		//DEV103 Image promo
		//DEV103 pour image changée onmouseover
			'thumb'         => $categories[$iKey]['thumb'],
			'category_id'   => $category['category_id'],
		//DEV103 pour image changée onmouseover
			'name'     => $category['name'],
			'children' => $children_data,
			'column'   => $category['column'] ? $category['column'] : 1,
			'href'     => $this->url->link('product/category', 'path=' . $category['category_id'])
        );	
      }
    }
	
	//DEV103 Les Indispensables
	$data['text_from'] = $this->language->get('text_from');
	//DEV103 Les Indispensables
    $data['language'] = $this->load->controller('common/language');
    $data['currency'] = $this->load->controller('common/currency');
    $data['search'] = $this->load->controller('common/search');
    $data['cart'] = $this->load->controller('common/cart');
 

    // For page specific css
    if (isset($this->request->get['route'])) {
      if (isset($this->request->get['product_id'])) {
        $class = '-' . $this->request->get['product_id'];
      } elseif (isset($this->request->get['path'])) {
        $class = '-' . $this->request->get['path'];
      } elseif (isset($this->request->get['manufacturer_id'])) {
        $class = '-' . $this->request->get['manufacturer_id'];
      } elseif (isset($this->request->get['information_id'])) {
        $class = '-' . $this->request->get['information_id'];
      } else {
        $class = '';
      }

      $data['class'] = str_replace('/', '-', $this->request->get['route']) . $class;
    } else {
      $data['class'] = 'common-home';
    }
 
    if( isset( $_GET['path'] ) ) {
     $idcateg = intval( $_GET['path'] ); 
     $data['categorieID'] = $idcateg ;
	}
    return $this->load->view('common/test', $data);
  }
  
    //DEV103 Les Indispensables
    public function categoriesImbriques($_iCategorieId)
    {
        $this->load->model('catalog/category') ;
        $toCategories       = $this->model_catalog_category->getCategoriesChoixIndispensables() ;
        $toCategoriesPathId = $this->model_catalog_category->getCategoriesPath($_iCategorieId) ;
        foreach($toCategories as $iKey=>$toCategorie)
        {
            if(!in_array($toCategorie['category_id'], $toCategoriesPathId))
            {
                unset($toCategories[$iKey]) ;
            }
        }
        return $toCategories ;
    }
    //DEV103 Les Indispensables
}
