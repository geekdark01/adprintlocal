<?php
class ControllerCommonCart extends Controller {
	public function index() {
		$this->load->language('common/cart');
		$this->load->model('account/customer');

			//TVA 
			if ($this->customer->isLogged()) {
				$data['TVA'] = $this->model_account_customer->getCustomerByTVA($this->customer->getId());
			}
			else{
				$data['TVA'] = 1;
			}
		// Totals
		$this->load->model('extension/extension');

		$totals = array();
		$taxes = $this->cart->getTaxes();
		$total = 0;

		// Because __call can not keep var references so we put them into an array.
		$total_data = array(
			'totals' => &$totals,
			'taxes'  => &$taxes,
			'total'  => &$total
		);
			
		// Display prices
		if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
			$sort_order = array();

			$results = $this->model_extension_extension->getExtensions('total');

			foreach ($results as $key => $value) {
				$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
			}

			array_multisort($sort_order, SORT_ASC, $results);

			foreach ($results as $result) {
				if ($this->config->get($result['code'] . '_status')) {
					$this->load->model('extension/total/' . $result['code']);

					// We have to put the totals in an array so that they pass by reference.
					$this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
				}
			}

			$sort_order = array();

			foreach ($totals as $key => $value) {
				$sort_order[$key] = $value['sort_order'];
			}

			array_multisort($sort_order, SORT_ASC, $totals);
		}

		$data['text_empty'] = $this->language->get('text_empty');
		$data['text_cart'] = $this->language->get('text_cart');
		$data['text_checkout'] = $this->language->get('text_checkout');
		$data['text_recurring'] = $this->language->get('text_recurring');
		$data['text_items'] = sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), $this->currency->format($total, $this->session->data['currency']));
		$data['text_loading'] = $this->language->get('text_loading');

		$data['button_remove'] = $this->language->get('button_remove');

		$this->load->model('tool/image');
		$this->load->model('tool/upload');

		$data['products'] = array();

		foreach ($this->cart->getProducts() as $product) {
			if ($product['image']) {
				$image = $this->model_tool_image->resize($product['image'], $this->config->get($this->config->get('config_theme') . '_image_cart_width'), $this->config->get($this->config->get('config_theme') . '_image_cart_height'));
			} else {
				$image = '';
			}

			$option_data = array();
			foreach ($product['option'] as $option) {
				if($option['name'] == 'option_dynamique'){
					continue;
				}
				if ($option['type'] != 'file') {
					$value = $option['value'];
				} 
				else {
					$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

					if ($upload_info) {
						$value = $upload_info['name'];
					} else {
						$value = '';
					}
				}

				$option_data[] = array(
					'name'  => $option['name'],
					'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value),
					'type'  => $option['type']
				);
			}
			$this->load->model('catalog/product');
			$product1 = $this->model_catalog_product->getProduct($product['product_id']);
			// Display prices
			$total1 =  $this->model_catalog_product->getTotals($product['product_id'], $product['cart_id']);
			if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {			
					if($product1['is_surmesure'] == 0){
						$unit_price = $product1['price'];
						$price = $this->currency->format($unit_price, $this->session->data['currency']);
						$total = $this->currency->format($product['total'], $this->session->data['currency']);
					}else if($product1['is_surmesure'] == 1){
						$unit_price = $product1['prix_metre_carre'];
						$product['total'] = floatval($total1['totalprix']);
						$price = $this->currency->format($unit_price, $this->session->data['currency']);
						$total = $this->currency->format($product['total'], $this->session->data['currency']);			
					}			
			} else {
				$price = false;
				$total = false;
			}
			$data['products'][] = array(
				'cart_id'   => $product['cart_id'],
				'thumb'     => $image,
				'name'      => $product['name'],
				'model'     => $product['model'],
				'option'    => $option_data,
				'recurring' => ($product['recurring'] ? $product['recurring']['name'] : ''),
				'quantity'  => $product['quantity'],
				'price'     => $price,
				'total'     => $total,
				'href'      => $this->url->link('product/product', 'product_id=' . $product['product_id'])
			);
		}

		// Gift Voucher
		$data['vouchers'] = array();

		if (!empty($this->session->data['vouchers'])) {
			foreach ($this->session->data['vouchers'] as $key => $voucher) {
				$data['vouchers'][] = array(
					'key'         => $key,
					'description' => $voucher['description'],
					'amount'      => $this->currency->format($voucher['amount'], $this->session->data['currency'])
				);
			}
		}

		$data['totals'] = array();

		foreach ($totals as $total) {
			$data['totals'][] = array(
				'title' => $total['title'],
				'text'  => $this->currency->format($total['value'], $this->session->data['currency']),
			);
		}
		$data['logged'] = $this->customer->isLogged();
		$data['logout'] = $this->url->link('account/logout', '', true);
		$data['mon_compte'] = $this->url->link('account/account', '', true);
		$data['mes_commandes'] = $this->url->link('account/order', '', true);
		$data['mes_devis'] = $this->url->link('account/devis', '', true);
		$data['transfert_fichier'] = $this->url->link('account/account/gestionfichier', '', true);
		$data['bat'] = $this->url->link('account/account/gestionfichier#content_bat', '', true);
		$data['parrainage'] = $this->url->link('account/avantage', '', true);
		$data['login'] = $this->url->link('account/login', '', true);
		$data['register'] = $this->url->link('account/register', '', true);
		$data['infographie'] = $this->url->link('servicemodification/index', '', true);

		$data['cart'] = $this->url->link('checkout/cart');
		$data['checkout'] = $this->url->link('checkout/checkout', '', true);

		return $this->load->view('common/cart', $data);
	}
	
	public function info() {
		$this->response->setOutput($this->index());
	}
}
