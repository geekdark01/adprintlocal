<?php
class ControllerCheckoutCart extends Controller {
	
	
	public function index() {
		$this->load->language('checkout/cart');
		$this->load->model('account/customer');

			//TVA 
			if ($this->customer->isLogged()) {
				$data['TVA'] = $this->model_account_customer->getCustomerByTVA($this->customer->getId());
			}
			else{
				$data['TVA'] = 1;
			}

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'href' => $this->url->link('common/home'),
			'text' => $this->language->get('text_home')
		);

		$data['breadcrumbs'][] = array(
			'href' => $this->url->link('checkout/cart'),
			'text' => $this->language->get('heading_title')
		);

		if ($this->cart->hasProducts() || !empty($this->session->data['vouchers'])) {
			$data['heading_title'] = $this->language->get('heading_title');

			$data['text_recurring_item'] = $this->language->get('text_recurring_item');
			$data['text_next'] = $this->language->get('text_next');
			$data['text_next_choice'] = $this->language->get('text_next_choice');

			$data['column_image'] = $this->language->get('column_image');
			$data['column_name'] = $this->language->get('column_name');
			$data['column_model'] = $this->language->get('column_model');
			$data['column_quantity'] = $this->language->get('column_quantity');
			$data['column_price'] = $this->language->get('column_price');
			$data['column_total'] = $this->language->get('column_total');

			$data['button_update'] = $this->language->get('button_update');
			$data['button_remove'] = $this->language->get('button_remove');
			$data['button_shopping'] = $this->language->get('button_shopping');
			$data['button_checkout'] = $this->language->get('button_checkout');

			if (!$this->cart->hasStock() && (!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning'))) {
				$data['error_warning'] = $this->language->get('error_stock');
			} elseif (isset($this->session->data['error'])) {
				$data['error_warning'] = $this->session->data['error'];

				unset($this->session->data['error']);
			} else {
				$data['error_warning'] = '';
			}

			if ($this->config->get('config_customer_price') && !$this->customer->isLogged()) {
				$data['attention'] = sprintf($this->language->get('text_login'), $this->url->link('account/login'), $this->url->link('account/register'));
			} else {
				$data['attention'] = '';
			}
			if (isset($this->session->data['success'])) {
				$data['success'] = $this->session->data['success'];

				unset($this->session->data['success']);
			} else {
				$data['success'] = '';
			}

			$data['action'] = $this->url->link('checkout/cart/edit', '', true);

			if ($this->config->get('config_cart_weight')) {
				$data['weight'] = $this->weight->format($this->cart->getWeight(), $this->config->get('config_weight_class_id'), $this->language->get('decimal_point'), $this->language->get('thousand_point'));
			} else {
				$data['weight'] = '';
			}

			$this->load->model('tool/image');
			$this->load->model('tool/upload');

			$data['products'] = array();

			$products = $this->cart->getProducts();
			
			$this->load->model('catalog/product');
			foreach ($products as $product) {
				$product_total = 0;
				//$product_info = $this->model_catalog_product->getProduct( $product['product_id']);
				
				foreach ($products as $product_2) {
					if ($product_2['product_id'] == $product['product_id']) {
						$product_total += $product_2['quantity'];
					}
				}
				if ($product['minimum'] > $product_total) {
					$data['error_warning'] = sprintf($this->language->get('error_minimum'), $product['name'], $product['minimum']);
				}

				if ($product['image']) {
					$image = $this->model_tool_image->resize($product['image'], $this->config->get($this->config->get('config_theme') . '_image_cart_width'), $this->config->get($this->config->get('config_theme') . '_image_cart_height'));
				} else {
					$image = '';
				}
				$total1 =  $this->model_catalog_product->getTotals($product['product_id'], $product['cart_id']);
				$product1 = $this->model_catalog_product->getProduct($product['product_id']);
				$option_data = array();		
				$model =  str_word_count($product1['model'], 1, 'âàáãç3'); 
		    	if($model[0] == 'Banderole' || $model[0] == 'Bache' || $model[0] == 'Bâche'){
					$optiontotal = $this->model_catalog_product->getOptions($product['product_id'],$product['cart_id']);
					foreach($optiontotal as $option){
							$options  = $option['option'];
							$options1= json_decode($options, TRUE);
							$option_data[] = array(
								'Largeur'  => $options1['Largeur'],
								'Hauteur'  => $options1['Hauteur'],
								'Orientation'  => $options1['Orientation'],
								'Soudure'  => $options1['Soudure'],
								'Oeillet'  => $options1['Oeillet'],
								'Servicepose'  => $options1['Servicepose'],
								'Pose'  => $options1['Pose'],
								'Ville'  => $options1['Ville'],
								'Fourreau'  => $options1['Fourreau'],
								'MettreFourreau'  => $options1['MettreFourreau'],
								'Bat' => $options1['Bat'],
								'LaizeMaxChute' => $options1['LaizeMaxChute'],
							);
					}
				}else{
					foreach ($product['option'] as $option) {
						if($option['name'] == 'option_dynamique'){
							continue;
						}
						if ($option['type'] != 'file') {
							$value = $option['value'];
						} else {
							$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);
							if ($upload_info) {
								$value = $upload_info['name'];
							} else {
								$value = '';
							}
						}
						$option_data[] = array(
							'name'  => $option['name'],
							'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
						);
					}
				}
				// Display prices
				if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					if($product1['is_surmesure'] == 0){
						$unit_price = $product1['price'];
						$price = $this->currency->format($unit_price, $this->session->data['currency']);
						$total = $this->currency->format($product['total'], $this->session->data['currency']);
					}else if($product1['is_surmesure'] == 1){
						$unit_price = $product1['prix_metre_carre'];
						$product['total'] = floatval($total1['totalprix']);
						$price = $this->currency->format($unit_price, $this->session->data['currency']);
						$total = $this->currency->format($product['total'], $this->session->data['currency']);
					}
				} else {
					$price = false;
					$total = false;
				}
				$recurring = '';
	
				if ($product['recurring']) {
					$frequencies = array(
						'day'        => $this->language->get('text_day'),
						'week'       => $this->language->get('text_week'),
						'semi_month' => $this->language->get('text_semi_month'),
						'month'      => $this->language->get('text_month'),
						'year'       => $this->language->get('text_year'),
					);

					if ($product['recurring']['trial']) {
						$recurring = sprintf($this->language->get('text_trial_description'), $this->currency->format($this->tax->calculate($product['recurring']['trial_price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']), $product['recurring']['trial_cycle'], $frequencies[$product['recurring']['trial_frequency']], $product['recurring']['trial_duration']) . ' ';
					}

					if ($product['recurring']['duration']) {
						$recurring .= sprintf($this->language->get('text_payment_description'), $this->currency->format($this->tax->calculate($product['recurring']['price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']), $product['recurring']['cycle'], $frequencies[$product['recurring']['frequency']], $product['recurring']['duration']);
					} else {
						$recurring .= sprintf($this->language->get('text_payment_cancel'), $this->currency->format($this->tax->calculate($product['recurring']['price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']), $product['recurring']['cycle'], $frequencies[$product['recurring']['frequency']], $product['recurring']['duration']);
					}
				}
			
				$data['products'][] = array(
					'cart_id'   => $product['cart_id'],
					'thumb'     => $image,
					'laize'     => $product['width'],
					'thumb'     => $image,
					'model'		=> $model,
					'name'      => $product['name'],
					'model'     => $product['model'],
					'option'    => $option_data,
					'is_surmesure' => $product1['is_surmesure'],
					'recurring' => $recurring,
					'quantity'  => $product['quantity'],
					'stock'     => $product['stock'] ? true : !(!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning')),
					'reward'    => ($product['reward'] ? sprintf($this->language->get('text_points'), $product['reward']) : ''),
					'price'     => $price,
					'total'     => $total,

					'href'      => $this->url->link('product/product', 'product_id=' . $product['product_id'])
				);
			}

			// Gift Voucher
			$data['vouchers'] = array();

			if (!empty($this->session->data['vouchers'])) {
				foreach ($this->session->data['vouchers'] as $key => $voucher) {
					$data['vouchers'][] = array(
						'key'         => $key,
						'description' => $voucher['description'],
						'amount'      => $this->currency->format($voucher['amount'], $this->session->data['currency']),
						'remove'      => $this->url->link('checkout/cart', 'remove=' . $key)
					);
				}
			}

			// Totals
			$this->load->model('extension/extension');

			
			$totals = array();
			$taxes = $this->cart->getTaxes();
			$total = 0;
			
			// Because __call can not keep var references so we put them into an array. 			
			$total_data = array(
				'totals' => &$totals,
				'taxes'  => &$taxes,
				'total'  => &$total
			);

			// Display prices
			if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
				$sort_order = array();

				$results = $this->model_extension_extension->getExtensions('total');

				foreach ($results as $key => $value) {
					$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
					
				}

				array_multisort($sort_order, SORT_ASC, $results);
				
				if(isset($this->session->data['addprintcredit'])){
					$data['addprintcredit'] = $this->session->data['addprintcredit'];
					$this->config->set('addprintcredit_status', 1) ;
					if ($this->config->get('addprintcredit_status', 1)) {
						$this->load->model('extension/total/addprintcredit');
						// We have to put the totals in an array so that they pass by reference.
						$this->{'model_extension_total_addprintcredit'}->getTotal($total_data);
					}
				}
				
				if(isset($this->session->data['coupon'])){
					$data['coupon'] = $this->session->data['coupon'];
				}
				
				foreach ($results as $result) {
					// begin dev 101SFM120417 
					/* if(isset($this->session->data['addprintcredit']) && $result['code'] == 'addprintcredit'){
						$this->config->set('addprintcredit_status', 1) ;
					} */
					// end dev 101SFM120417
						
					if ($this->config->get($result['code'] . '_status')) {
						$this->load->model('extension/total/' . $result['code']);
						// We have to put the totals in an array so that they pass by reference.
						$this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
					}
				}
  /* echo 'Totals<br><pre>';
print_r($totals);
echo '</pre>';
exit; */  		
				$sort_order = array();
				foreach ($totals as $key => $value) {
					$sort_order[$key] = $value['sort_order'];
				}
				array_multisort($sort_order, SORT_ASC, $totals);
			}

			$data['totals'] = array();
			
			
			foreach ($totals as $total) {
				$data['totals'][] = array(
					'title' => $total['title'],
					'text'  => $this->currency->format($total['value'], $this->session->data['currency'])
				);
			}
			 
			// begin dev 101SFM120417 recuperation du total pour la comparaison dans addprintcredit
			$this->session->data['totalsForAddprintCredit'] = $totals[0]['value'];
			//echo $totals[0]['value'];
			// ending dev 101SFM120417 


			$data['continue'] = $this->url->link('common/home');

			$data['checkout'] = $this->url->link('checkout/checkout', '', true);

			$this->load->model('extension/extension');

			$data['modules'] = array();
			
			$files = glob(DIR_APPLICATION . '/controller/extension/total/*.php');

			if ($files) {
				foreach ($files as $file) {
					$result = $this->load->controller('extension/total/' . basename($file, '.php'));
					if ($result) {
						$data['modules'][] = $result;
					}
				}
			}

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('checkout/cart', $data));
		} else {
			$data['heading_title'] = $this->language->get('heading_title');

			$data['text_error'] = $this->language->get('text_empty');

			$data['button_continue'] = $this->language->get('button_continue');

			$data['continue'] = $this->url->link('common/home');

			unset($this->session->data['success']);

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('error/not_found', $data));
		}
	}

	public function add() {
		$this->load->language('checkout/cart');
		$this->load->model('account/customer');
			//TVA 
			if ($this->customer->isLogged()) {
				$data['TVA'] = $this->model_account_customer->getCustomerByTVA($this->customer->getId());
			}
			else{
				$data['TVA'] = 1;
			}

		$json = array();

		if (isset($this->request->post['product_id'])) {
			$product_id = (int)$this->request->post['product_id'];
		} else {
			$product_id = 0;
		}
		
		$this->load->model('catalog/product');

		$product_info = $this->model_catalog_product->getProduct($product_id);
		//$product_info['price']=$product_id;

		if ($product_info) {
			if (isset($this->request->post['quantity']) && ((int)$this->request->post['quantity'] >= $product_info['minimum'])) {
				$quantity = (int)$this->request->post['quantity'];
			} else {
				$quantity = $product_info['minimum'] ? $product_info['minimum'] : 1;
			}

			if (isset($this->request->post['option'])) {
				$option = array_filter($this->request->post['option']);
			} else {
				$option = array();
			}
			
			/*var_dump($option);
			exit();*/

			if( !isset( $this->request->post['product_option_id'] ) )
			{
				$product_options = $this->model_catalog_product->getProductOptions($this->request->post['product_id']);
				
				foreach ($product_options as $product_option) {
					if ($product_option['required'] && empty($option[$product_option['product_option_id']])) {
						$json['error']['option'][$product_option['name']] = sprintf($this->language->get('error_required'), $product_option['name']);
					}
				}
			}
			
			if (isset($this->request->post['recurring_id'])) {
				$recurring_id = $this->request->post['recurring_id'];
			} else {
				$recurring_id = 0;
			}

			$recurrings = $this->model_catalog_product->getProfiles($product_info['product_id']);

			if ($recurrings) {
				$recurring_ids = array();

				foreach ($recurrings as $recurring) {
					$recurring_ids[] = $recurring['recurring_id'];
				}

				if (!in_array($recurring_id, $recurring_ids)) {
					$json['error']['recurring'] = $this->language->get('error_recurring_required');
				}
			}
			/*beging dev101SFM180417 exemplaire*/
			$exemplaire_id = null ;

			if(isset($this->request->post['exemplaire_id'])){
				$exemplaire_id = $this->request->post['exemplaire_id'] ;
			}
			/*ending dev101SFM180417 exemplaire*/
			if (!$json) {
				
				// dev tiana
				if(isset($this->request->post['price_uniter'])){
					$this->cart->add($this->request->post['product_id'], $quantity, $option, $recurring_id, $this->request->post['price_uniter']);					
				}			
				elseif( isset( $this->request->post['product_option_id'] ) ) // dev tiana si
				{					
					$option_id = $this->request->post['product_option_id'] ; // option id								
					$option_value_ids = $this->request->post['option_value_ids'] ; // liste ids
					foreach($option_value_ids as $id_option_value )
					{
						$quantite = intval( $this->request->post['quantite_option_value_'.$id_option_value] );
						$option = array( $option_id => $id_option_value );					
						$this->cart->add( $this->request->post['product_id'] , $quantite , $option, $recurring_id, $exemplaire_id);
					}								
				}
				else
				{
					$this->cart->add($this->request->post['product_id'], $quantity, $option, $recurring_id, $exemplaire_id);
				}
				// dev tiana 
				$json['success'] = sprintf($this->language->get('text_success'), $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']), $product_info['name'], $this->url->link('checkout/cart'));

				// Unset all shipping and payment methods
				unset($this->session->data['shipping_method']);
				unset($this->session->data['shipping_methods']);
				unset($this->session->data['payment_method']);
				unset($this->session->data['payment_methods']);

				// Totals
				$this->load->model('extension/extension');

				$totals = array();
				$taxes = $this->cart->getTaxes();
				$total = 0;
		
				// Because __call can not keep var references so we put them into an array. 			
				$total_data = array(
					'totals' => &$totals,
					'taxes'  => &$taxes,
					'total'  => &$total
				);

				// Display prices
				if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					$sort_order = array();

					$results = $this->model_extension_extension->getExtensions('total');

					foreach ($results as $key => $value) {
						$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
					}

					array_multisort($sort_order, SORT_ASC, $results);

					foreach ($results as $result) {
						if ($this->config->get($result['code'] . '_status')) {
							$this->load->model('extension/total/' . $result['code']);

							// We have to put the totals in an array so that they pass by reference.
							$this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
						}
					}

					$sort_order = array();

					foreach ($totals as $key => $value) {
						$sort_order[$key] = $value['sort_order'];
					}

					array_multisort($sort_order, SORT_ASC, $totals);
				}
				$json['total_produit'] =  $this->cart->countProducts();
				$json['total'] = sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), $this->currency->format($total, $this->session->data['currency']));
			} else {
				$json['redirect'] = str_replace('&amp;', '&', $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']));
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
		
	}
	public function add1() {
		$this->load->language('checkout/cart');
		$this->load->model('account/customer');

		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$Product_id = $request->Product_id;
		$Quantity =  $request->Quantity;
		$Largeur = $request->Largeur;
		$Hauteur = $request->Hauteur;
		$oeillet = $request->oeillet;
		$Orientation = $request->Orientation;
		$Soudure = $request->Soudure;
		$Servicepose = $request->Servicepose;
        $Pose = $request->Pose;
        $Ville = $request->Ville;
        $Fourreau = $request->Fourreau;
        $MettreFourreau = $request->MettreFourreau;
        $Total = $request->Total;
        $Bat =  $request->Bat;
        $LaizeMaxChute = $request->LaizeMaxChute;
		$option = array(
			'Largeur' => $Largeur,
			'Hauteur' => $Hauteur,
			'Orientation' => $Orientation, 
            'Oeillet' => $oeillet,
            'Soudure' => $Soudure,
            'Servicepose' => $Servicepose,
            'Pose'  => $Pose,
            'Ville'  => $Ville,
            'Fourreau' => $Fourreau,
            'MettreFourreau' => $MettreFourreau,
            'Bat' => $Bat,
            'LaizeMaxChute' => $LaizeMaxChute,
		);
		if ($this->customer->isLogged()) {
			$data['TVA'] = $this->model_account_customer->getCustomerByTVA($this->customer->getId());
		} else{
			$data['TVA'] = 1;
		}
		$json = array();
		
		
		$this->load->model('catalog/product');
		$product_info = $this->model_catalog_product->getProduct($Product_id);

		if ($product_info) {
			if ($Quantity && ($Quantity >= $product_info['minimum'])) {
				$quantity = $Quantity;
			} else {
				$quantity = $product_info['minimum'] ? $product_info['minimum'] : 1;
			}
			if (isset($this->request->post['recurring_id'])) {
				$recurring_id = $this->request->post['recurring_id'];
			} else {
				$recurring_id = 0;
			}

			if (empty($Largeur)) {
				$json['error']['option']['Largeur'] = sprintf($this->language->get('error_required'), 'Largeur');
			}
			if (empty($Hauteur)) {
				$json['error']['option']['Hauteur'] = sprintf($this->language->get('error_required'), 'Hauteur');
			}
			if ($oeillet == "--- Faites un choix ---"){
				$json['error']['option']['Oeillet'] =  sprintf($this->language->get('error_required'), 'Oeillet');
			}	
			if ($Orientation == "--- Faites un choix ---"){
				$json['error']['option']['Orientation'] = sprintf($this->language->get('error_required'), 'Orientation');
			}	
			if ($Soudure == "--- Faites un choix ---"){
				$json['error']['option']['Soudure'] = sprintf($this->language->get('error_required'), 'Soudure');
			}	
			if ($Servicepose == "--- Faites un choix ---"){
				$json['error']['option']['Servicepose'] = sprintf($this->language->get('error_required'), 'Service pose');
			}
			if($Servicepose == "Oui"){
				if($Pose == "--- Faites un choix ---"){
					$json['error']['option']['Pose'] = sprintf($this->language->get('error_required'), 'Pose');
				}
				if($Ville == "Aucun"){
					$json['error']['option']['Ville'] = sprintf($this->language->get('error_required'), 'Ville');
				}
			}

			if($Fourreau == "--- Faites un choix ---"){
				$json['error']['option']['Fourreau'] = sprintf($this->language->get('error_required'), 'Fourreau');
			}
			if($Fourreau == "Côtés largeur" || $Fourreau == "Côtés hauteur" || $Fourreau == '2 cotés largeur' || $Fourreau == '2 cotés Hauteur'){
				if($MettreFourreau == "--- Faites un choix ---"){
					$json['error']['option']['MettreFourreau'] = sprintf($this->language->get('error_required'), 'Mettre fourreau');
				}
			}
			if ($Bat == "--- Faites un choix ---"){
				$json['error']['option']['bat'] = sprintf($this->language->get('error_required'), 'BAT');
			}
   			$recurrings = $this->model_catalog_product->getProfiles($product_info['product_id']);
			if ($recurrings) {
				$recurring_ids = array();
				foreach ($recurrings as $recurring) {
				$recurring_ids[] = $recurring['recurring_id'];
				}
				if (!in_array($recurring_id, $recurring_ids)) {
					$json['error']['recurring'] = $this->language->get('error_recurring_required');
				}
			}
			if(!$json){
			$this->cart->add($Product_id , $quantity , $option, $recurring_id , $Total);
			$json['success'] = sprintf($this->language->get('text_success'), $this->url->link('product/product', 'product_id=' . $Product_id), $product_info['name'], $this->url->link('checkout/cart'));
			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
			unset($this->session->data['payment_method']);
			unset($this->session->data['payment_methods']);
			$this->load->model('extension/extension');
				$totals = array();
				$taxes = $this->cart->getTaxes();
				$total = 0;
			// Because __call can not keep var references so we put them into an array. 			
			$total_data = array(
				'totals' => &$totals,
				'taxes'  => &$taxes,
				'total'  => &$total,
			);
			if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
				$sort_order = array();

				$results = $this->model_extension_extension->getExtensions('total');

					foreach ($results as $key => $value) {
						$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
					}

					array_multisort($sort_order, SORT_ASC, $results);

					foreach ($results as $result) {
						if ($this->config->get($result['code'] . '_status')) {
							$this->load->model('extension/total/' . $result['code']);
							// We have to put the totals in an array so that they pass by reference.
							$this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
						}
					}

					$sort_order = array();

					foreach ($totals as $key => $value) {
						$sort_order[$key] = $value['sort_order'];
					}

					array_multisort($sort_order, SORT_ASC, $totals);
				}
			$json['total_produit'] =  $this->cart->countProducts();
			$json['total'] = sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), $this->currency->format($Total, $this->session->data['currency']));
			}else {
				$json['redirect'] = str_replace('&amp;', '&', $this->url->link('product/product', 'product_id=' . $Product_id));
			}
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function edit() {
		$this->load->language('checkout/cart');
		$this->load->model('account/customer');

			//TVA 
			if ($this->customer->isLogged()) {
				$data['TVA'] = $this->model_account_customer->getCustomerByTVA($this->customer->getId());
			}
			else{
				$data['TVA'] = 1;
			}

		$json = array();

		// Update
		if (!empty($this->request->post['quantity'])) {
			foreach ($this->request->post['quantity'] as $key => $value) {
				$this->cart->update($key, $value);
			}

			$this->session->data['success'] = $this->language->get('text_remove');

			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
			unset($this->session->data['payment_method']);
			unset($this->session->data['payment_methods']);
			unset($this->session->data['reward']);

			$this->response->redirect($this->url->link('checkout/cart'));
		}
		$json['total_produit'] =  $this->cart->countProducts();
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function remove() {
		$this->load->language('checkout/cart');
		$this->load->model('account/customer');

			//TVA 
			if ($this->customer->isLogged()) {
				$data['TVA'] = $this->model_account_customer->getCustomerByTVA($this->customer->getId());
			}
			else{
				$data['TVA'] = 1;
			}

		$json = array();

		// Remove
		if (isset($this->request->post['key'])) {
			$this->cart->remove($this->request->post['key']);

			unset($this->session->data['vouchers'][$this->request->post['key']]);

			$json['success'] = $this->language->get('text_remove');

			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
			unset($this->session->data['payment_method']);
			unset($this->session->data['payment_methods']);
			unset($this->session->data['reward']);

			// Totals
			$this->load->model('extension/extension');

			$totals = array();
			$taxes = $this->cart->getTaxes();
			$total = 0;

			// Because __call can not keep var references so we put them into an array. 			
			$total_data = array(
				'totals' => &$totals,
				'taxes'  => &$taxes,
				'total'  => &$total
			);

			// Display prices
			if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
				$sort_order = array();

				$results = $this->model_extension_extension->getExtensions('total');

				foreach ($results as $key => $value) {
					$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
				}

				array_multisort($sort_order, SORT_ASC, $results);

				foreach ($results as $result) {
					if ($this->config->get($result['code'] . '_status')) {
						$this->load->model('extension/total/' . $result['code']);

						// We have to put the totals in an array so that they pass by reference.
						$this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
					}
				}

				$sort_order = array();

				foreach ($totals as $key => $value) {
					$sort_order[$key] = $value['sort_order'];
				}

				array_multisort($sort_order, SORT_ASC, $totals);
			}
			$json['total_produit'] =  $this->cart->countProducts();
			$json['total'] = sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), $this->currency->format($total, $this->session->data['currency']));
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
    
    /*DEV101 : add option fictif*/
    public function add_option() {
		
		
        $this->load->model('catalog/product');
        
        $value_maitre_carre =   $this->request->post['value_maitre_carre'] ;
        $prix_maitre_carre  =   $this->request->post['prix_maitre_carre'] ;
        $_idProduct         =   $this->request->post['id_produit'] ;
        //$_idOption        =   62 ; // adynamiser
		$_idOption          =   213 ; 
		
        $_price             = floatval($value_maitre_carre) * floatval($prix_maitre_carre) ;
        
        $json = $this->model_catalog_product->insertProduitFictif($_idProduct, $_idOption, $_price);
        
        $retour = array() ;
        $retour['option'] = $json[0] ;
        $retour['option_value'] = $json[1] ;
        
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($retour));
		
	
	}
    /*DEV101 : add option fictif*/
}
