<?php
class ControllerAccountAvantage extends Controller {
	private $success = array();
	public function index() {
		if (!$this->customer->isLogged()) {
		  $this->session->data['redirect'] = $this->url->link('account/avantage', '', true);

		  $this->response->redirect($this->url->link('account/login', '', true));
		}
		
		if(($this->request->server['REQUEST_METHOD'] == 'POST')&& $this->validate())
		{
			$this->response->redirect($this->url->link('account/avantage', '', true));
		}
		$this->document->setTitle("Mes avantages");
		$data['action'] = $this->url->link('account/avantage', '', true);
		$data['code'] 	= $this->customer->getEmail();
		$data['client'] = $this->customer->getLastName().' '.$this->customer->getFirstName();
		
		if (isset($this->success['success'])) {
		  $data['success'] = $this->success['success'];
		} else {
		  $data['success'] = '';
		}
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		$this->response->setOutput($this->load->view('account/avantage', $data));
	}
	
	protected function validate() {
		$this->load->model('account/avantage');
		$nb 	= $this->request->post['nb_mail'];
		$msg 	= $this->request->post['msg'];
		$client = $this->customer->getLastName().' '.$this->customer->getFirstName();
		$nb_sent = 0;
		for($i=1;$i<=$nb;$i++){
			$contact = $this->request->post['email-'.$i]; 
			if($contact!=''){
				
				$this->model_account_avantage->sendMail($contact,$msg,$client);
				$nb_sent++;
			}
		}
		$this->success['success'] = "Votre message a été envoyé avec succès à ".$nb_sent." contacts de votre réseau. Toute l'équipe d'AdPrint vous remercie pour votre confiance.";
	}
}