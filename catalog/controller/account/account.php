<?php

class ControllerAccountAccount extends Controller {
	public function gestionfichier()
	{
		if(isset($this->request->get['order_id'])){
			$data['iOrderIdFirst'] = $this->request->get['order_id'];
		}
		

		$data['footer'] = $this->load->controller('common/footer');

		$data['header'] = $this->load->controller('common/header');	

		$this->load->model('account/customer');
		$this->load->language('bat/bat');
		

		$data['firstname'] = $this->session->data['payment_address']['firstname'];

		$data['lastname'] = $this->session->data['payment_address']['lastname'];

		$data['email'] = "clienttemporaire@gmail.com";

		// copie code de rachefo  ===========

        $_iOrderId = 0 ;

        $toRequest  = $this->request->request ;

        $zRoute     = $toRequest['route'] ;

        $tzRoute    = explode('/', $zRoute) ;

        $zDerniereColonne = end($tzRoute) ;

        if($zDerniereColonne != 'index')
        {
            $_iOrderId = intval($zDerniereColonne) ;
        }

		$this->load->language('gestionfichier/gestionfichier');

		$this->document->setTitle($this->language->get('heading_title'));
      
        $this->load->model('account/order');

        $toOrderAccount = $this->model_account_order->getOrders() ;

        $data['order_product'] = array() ;

        foreach($toOrderAccount as $oOrderAccount)
        {
            $data['order_product'][$oOrderAccount['order_id']] = $this->model_account_order->getOrderProducts($oOrderAccount['order_id']) ;
			$toContenuOrder = $data['order_product'][$oOrderAccount['order_id']] ;
			
            foreach($toContenuOrder as $iKey=>$toContenu)
            {
                $iProduitId = $toContenu['product_id'] ;
                $iOrderId   = $oOrderAccount['order_id'] ;
                $toContenuOrder[$iKey]['bat'] = $this->model_account_order->loadBatPerProduct($iOrderId, $iProduitId) ;
                $toContenuOrder[$iKey]['bat_status'] = 3 ;
                foreach($toContenuOrder[$iKey]['bat'] as $iKeyBat=>$toBatPerProduct)
                {
                    $tzFichierBat = $this->model_tool_upload->getUploadByCode($toBatPerProduct['code_fichier']);
                    $toContenuOrder[$iKey]['bat_status']                    = $toBatPerProduct['status'] ;
                    $toContenuOrder[$iKey]['bat'][$iKeyBat]['fichier']      = $tzFichierBat['name'] ;
                    $toContenuOrder[$iKey]['bat'][$iKeyBat]['url_fichier']  = $this->url->link('tool/upload/download', 'code=' . $toBatPerProduct['code_fichier'], true) ;
                    $toContenuOrder[$iKey]['bat'][$iKeyBat]['date_added']   = date($this->language->get('datetime_format'), strtotime($toBatPerProduct['date_added'])) ;
                }
            }
            $data['order_product'][$oOrderAccount['order_id']] = $toContenuOrder ;
        }

        $data['order_id']       = $_iOrderId ;
        $data['toOrderAccount'] = $toOrderAccount ;        

		$data['heading_title']          = $this->language->get('heading_title');

		$data['text_gestion_fichier']   = $this->language->get('text_gestion_fichier');

		$data['text_choix_commande']    = $this->language->get('text_choix_commande');

		$data['text_titre_produit']     = $this->language->get('text_titre_produit');

		$data['text_titre_statut']      = $this->language->get('text_titre_statut');

		$data['text_titre_action']      = $this->language->get('text_titre_action');

		$data['text_loading']           = $this->language->get('text_loading');

		$data['text_envoyer']           = $this->language->get('text_envoyer');

		$data['text_envoyé']           = $this->language->get('text_envoyé');
		
		$data['text_valider']           = $this->language->get('text_valider');
        $data['text_refuser']           = $this->language->get('text_refuser');
        $data['text_valide']           = $this->language->get('text_valide');
        $data['text_refuse']           = $this->language->get('text_refuse');
		
		$data['button_upload']          = $this->language->get('button_upload');

        $data['column_left'] = $this->load->controller('common/column_left');

        $data['column_right'] = $this->load->controller('common/column_right');

        $data['content_top'] = $this->load->controller('common/content_top');

        $data['content_bottom'] = $this->load->controller('common/content_bottom');

		$data['footer'] = $this->load->controller('common/footer');

		$data['header'] = $this->load->controller('common/header');	

		$this->response->setOutput($this->load->view('account/gestion_fichier', $data));

		// fin copie code de rachefo  ===========
	}

	public function index()
	{
		if (!$this->customer->isLogged()) {

			$this->session->data['redirect'] = $this->url->link('account/account', '', true);
			$this->response->redirect($this->url->link('account/login', '', true));
		}
		
		$this->load->language('account/account');
		
		$this->document->setTitle($this->language->get('heading_title'));

		$data['footer'] = $this->load->controller('common/footer');

		$data['header'] = $this->load->controller('common/header');

		$this->load->model('account/customer');

		$customer_info = $this->model_account_customer->getCustomer($this->customer->getId());
		
		$data['firstname'] = $customer_info['firstname'];

		$data['lastname'] = $customer_info['lastname'];

		$data['email'] = $customer_info['email'];
		
		// orders 
		$this->load->model('account/order');
		$results = $this->model_account_order->getOrders( 0 , 1 );

		$data['orders'] = $results ;

		if(!empty($results)){

		$result = $results[0];
		$products = $this->model_account_order->getOrderProducts( $result['order_id'] );
		}else{
			$products = array();
		}
		
		$produits_reel = array();

		foreach( $products as $product ){
			$result_produit  =   $this->model_catalog_product->getProduct(  $product['product_id']  );

			$produits_reel[] = array(

					'name'       => $result_produit['name']       ,

					'product_id' => $result_produit['product_id'] , 

					'image' => $result_produit['image'] ) ;

					

		}

		$data['produits'] = $produits_reel ; 
		$data['detailsAdmoney'] = $this->url->link('addprintcard/addprintcard', '', true);
		$data['achatAdmoney'] = $this->url->link('product/category&path=1219', '', true);
		$data['ajoutFilleuils'] = $this->url->link('account/avantage', '', true);
		
		$this->load->model('addprintcard/addprintcard');
		$data['addprintcredit'] = $this->model_addprintcard_addprintcard->findSolde($this->customer->getId());
		
		$this->response->setOutput($this->load->view('account/mon_compte', $data));

	}

	public function index_avant() {

		if (!$this->customer->isLogged()) {

			$this->session->data['redirect'] = $this->url->link('account/account', '', true);
			$this->response->redirect($this->url->link('account/login', '', true));

		}

		$this->load->language('account/account');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(

			'text' => $this->language->get('text_home'),

			'href' => $this->url->link('common/home')

		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),

			'href' => $this->url->link('account/account', '', true)
		);

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		} 

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_my_account'] = $this->language->get('text_my_account');

		$data['text_my_orders'] = $this->language->get('text_my_orders');

		$data['text_my_newsletter'] = $this->language->get('text_my_newsletter');

		$data['text_edit'] = $this->language->get('text_edit');

		$data['text_password'] = $this->language->get('text_password');

		$data['text_address'] = $this->language->get('text_address');

		$data['text_credit_card'] = $this->language->get('text_credit_card');

		$data['text_wishlist'] = $this->language->get('text_wishlist');

		$data['text_order'] = $this->language->get('text_order');

		$data['text_download'] = $this->language->get('text_download');

		$data['text_reward'] = $this->language->get('text_reward');

		$data['text_return'] = $this->language->get('text_return');

		$data['text_transaction'] = $this->language->get('text_transaction');

		$data['text_newsletter'] = $this->language->get('text_newsletter');

		$data['text_recurring'] = $this->language->get('text_recurring');

		$data['edit'] = $this->url->link('account/edit', '', true);

		$data['password'] = $this->url->link('account/password', '', true);

		$data['address'] = $this->url->link('account/address', '', true);

		$data['credit_cards'] = array();

		$files = glob(DIR_APPLICATION . 'controller/extension/credit_card/*.php');

		foreach ($files as $file) {
			$code = basename($file, '.php');

			if ($this->config->get($code . '_status') && $this->config->get($code . '_card')) {

				$this->load->language('extension/credit_card/' . $code);

				$data['credit_cards'][] = array(

					'name' => $this->language->get('heading_title'),

					'href' => $this->url->link('extension/credit_card/' . $code, '', true)
				);
			}
		}

		$data['wishlist'] = $this->url->link('account/wishlist');

		$data['order'] = $this->url->link('account/order', '', true);

		$data['download'] = $this->url->link('account/download', '', true);

		if ($this->config->get('reward_status')) {

			$data['reward'] = $this->url->link('account/reward', '', true);
		} else {
			$data['reward'] = '';
		}		

		$data['return'] = $this->url->link('account/return', '', true);

		$data['transaction'] = $this->url->link('account/transaction', '', true);

		$data['newsletter'] = $this->url->link('account/newsletter', '', true);

		$data['recurring'] = $this->url->link('account/recurring', '', true);

		$data['column_left'] = $this->load->controller('common/column_left');

		$data['column_right'] = $this->load->controller('common/column_right');

		$data['content_top'] = $this->load->controller('common/content_top');

		$data['content_bottom'] = $this->load->controller('common/content_bottom');

		$data['footer'] = $this->load->controller('common/footer');

		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('account/account', $data));
	}

	public function country() {
		$json = array();

		$this->load->model('localisation/country');

		$country_info = $this->model_localisation_country->getCountry($this->request->get['country_id']);

		if ($country_info) {

			$this->load->model('localisation/zone');

			$json = array(

				'country_id'        => $country_info['country_id'],

				'name'              => $country_info['name'],

				'iso_code_2'        => $country_info['iso_code_2'],

				'iso_code_3'        => $country_info['iso_code_3'],

				'address_format'    => $country_info['address_format'],

				'postcode_required' => $country_info['postcode_required'],

				'zone'              => $this->model_localisation_zone->getZonesByCountryId($this->request->get['country_id']),

				'status'            => $country_info['status']
			);
		}

		$this->response->addHeader('Content-Type: application/json');

		$this->response->setOutput(json_encode($json));
	}
}

