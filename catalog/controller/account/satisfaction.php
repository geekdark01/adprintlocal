<?php
class ControllerAccountSatisfaction extends Controller {
	private $error = array();
	public function index()
	{
		if ($this->customer->isLogged()) {			
			$data['customer_group_id'] =  $this->customer->getGroupId();
			$this->load->language('account/satisfaction');
			$this->document->setTitle($this->language->get('heading_title'));
			if ($this->request->server['HTTPS']) {
				$data['base'] = $this->config->get('config_ssl');
			} else {
				$data['base'] = $this->config->get('config_url');
			}
			$data['heading_title'] = $this->language->get('heading_title') ;
			$data['text_information'] = $this->language->get('text_information') ;
			$data['input_firstname'] = $this->language->get('input_firstname') ;
			$data['tel_confirm'] = $this->language->get('tel_confirm') ;
			$data['adresse'] = $this->language->get('Mail') ;
			$data['societe'] = $this->language->get('societe') ;
			$data['sujetdmd'] = $this->language->get('sujetdmd') ;
			$data['selectioncmd'] = $this->language->get('selectioncmd') ;
			$data['motifclient'] = $this->language->get('motifclient') ;
			$data['resul'] = $this->language->get('resul') ;
			$data['souhait'] = $this->language->get('souhait') ;
			$data['commentaire'] = $this->language->get('Votre question/commentaire, indiquez le(s) produit(s) concerné(s) par votre requête') ;
			if (isset($this->request->post['choixcommande'])){
				$data['zchoixcommande'] = str_replace('&lt;=&gt;', '<=>', $this->request->post['choixcommande']);
			}
			if (isset($this->request->post['nom'])){
				$data['zNomsaisie'] = ($this->request->post['nom']);
			}
			if (isset($this->request->post['mail'])){
				$data['zMailsaisie'] = ($this->request->post['mail']);
			}
			if (isset($this->request->post['tel'])){				$data['zTelsaisie'] = ($this->request->post['tel']);
			}
			if (isset($this->request->post['societe'])){
				$data['zSocietesaisie'] = ($this->request->post['societe']);			}
			if (isset($this->request->post['comment'])){
				$data['zCommentsaisie'] = ($this->request->post['comment']);
			}
			if (isset($this->request->post['sujetdmd'])){				$data['zSujetdmdsaisie'] = ($this->request->post['sujetdmd']);
			}
			if (isset($this->request->post['motifclientsaisie'])){				$data['motifclientsaisie'] = ($this->request->post['motifclientsaisie']);
			}
			if (isset($this->request->post['chx'])){
				$data['chx'] = ($this->request->post['chx']);
			}
			$Path = '';
			$zLiensImage = '';
			if ($this->request->server['HTTPS']) {				$base = $this->config->get('config_ssl');
			} else {				$base = $this->config->get('config_url');			
			}
			$data['liens'] =  $base . "uploads/uploads/" ;
			// $data['liens'] = $base . 'image/servicesatisfaction/';
			if (isset($this->request->post['uploader_0_name'])){
				$data['image1'] = ($this->request->post['uploader_0_name']);
				$Path = "<img src='" . $data['liens'] . $data['image1'] . "'/>";
				$zLiensImage = $data['liens'] . $data['image1'] ;
			}
			if (isset($this->request->post['uploader_1_name'])){
				$data['image2'] = ($this->request->post['uploader_1_name']);
				$Path = $Path . "<img src='" . $data['liens'] . $data['image2'] . "'/>";
				$zLiensImage = $zLiensImage . "|" . $data['liens'] . $data['image2'] ;
			}
			if (isset($this->request->post['uploader_2_name'])){
				$data['image3'] = ($this->request->post['uploader_2_name']);
				$Path = $Path . "<img src='" . $data['liens'] . $data['image3'] . "'/>";
				$zLiensImage = $zLiensImage . "|" . $data['liens'] . $data['image3'] ;
			}
			if (isset($this->request->post['uploader_3_name'])){
				$data['image4'] = ($this->request->post['uploader_3_name']);
				$Path = $Path . "<img src='" . $data['liens'] . $data['image4'] . "'/>";
				$zLiensImage = $zLiensImage . "|" . $data['liens'] . $data['image4'] ;
			}
			if (isset($this->request->post['tel'])){
				$tabinfo = array(
					($this->request->post['nom']), 
					($this->request->post['mail']),
					($this->request->post['tel']),
					($this->request->post['societe']),
					($this->request->post['sujetdmd']),
					($this->request->post['choixcommande']),
					($this->request->post['motifclientsaisie']),
					($this->request->post['chx']),
					($this->request->post['comment']),
					($zLiensImage),
					($Path)
				);
				//envoi mail
				$passage_ligne = "\r\n";
				
				$boundary = "-----=" . md5(uniqid(rand()));
				$data['to'] 		= "radomiarintsoa.teko@gmail.com";
				$message = 
				"<html><body>
					<table style='border-collapse:collapse; width:100%; border-top: 1px solid #DDDDDD; border-left: 1px solid #DDDDDD; margin-bottom: 20px; padding:10px;'>
						<CAPTION style='font-size: 15px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #00BFFF; font-weight: bold; text-align: left; padding: 7px; color: #222222;'>
							<h2>Bonjour, </h2>
							</br>C'est " . $data['zNomsaisie'] . 
						"</CAPTION>
						<tr>
							<td style='font-size: 15px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;'>
								<p> Contact : </p></td>
							<td style='font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 5px; color: #708090;'>
								<p> ". $data['zTelsaisie'] ." </p></td>
						</tr>
						<tr>
							<td style='font-size: 15px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;'>
								<p> De la société : </p></td>
							<td style='font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 5px; color: #708090;'>
								<p> ". $data['zSocietesaisie'] ." </p></td>
						</tr>
						<tr>
							<td style='font-size: 15px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;'>								<p> Service satisfaction de la commande de : </p></td>
							<td style='font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 5px; color: #708090;'>
								<p> ". $data['zchoixcommande'] ." </p></td>
						</tr>
						<tr>
							<td style='font-size: 15px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;'>
								<p> Motif : </p></td>
							<td style='font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 5px; color: #708090;'>
								<p> ". $data['motifclientsaisie'] ." </p></td>
						</tr>
						<tr>
							<td style='font-size: 15px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;'>
								<p> Commentaire : </p></td>
							<td style='font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 5px; color: #708090;'>
								<p> ". $data['zCommentsaisie'] ." </p></td>
						</tr>
						<tr>
							<td style='font-size: 15px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;'>
								<p> Souhait : </p></td>
							<td style='font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 5px; color: #708090;'>
								<p> ". $data['chx'] ." </p></td>
						</tr></br>
					</table>
				<h3> Photos représentatrice de la requette SAV : </h3><br />" . $Path . "</body></html>"
				. $passage_ligne;
				$this->load->model('account/satisfaction');
				$this->model_account_satisfaction->informationclientsatisfaction($tabinfo);
				$mail = new Mail();
				$mail->protocol = $this->config->get('config_mail_protocol');
				$mail->parameter = $this->config->get('config_mail_parameter');
				$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
				$mail->smtp_username = $this->config->get('config_mail_smtp_username');
				$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
				$mail->smtp_port = $this->config->get('config_mail_smtp_port');
				$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');  		
				$mail->setTo("miangalyravaka.teko@gmail.com");				
				$mail->setFrom($data['zNomsaisie']);
				$mail->setSender($this->request->post['mail']);
				$mail->setSubject("[" . $data['zSujetdmdsaisie'] . "]");
				$mail->setHtml($message);
				$mail->send();
				//fin envoi mail
			}
			$this->load->model('account/satisfaction');			
			$this->load->model('account/order');
			$data['result'] = $this->model_account_order->getOrders();
			$data['action'] = $this->url->link('account/satisfaction', '', true);
			$data['back'] = $this->url->link('account/account', '', true);
			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');
			
			$this->load->model('account/customer');
			$customer_info = $this->model_account_customer->getCustomer($this->customer->getId());
		
			$data['firstname'] = $customer_info['firstname'];
			$data['lastname'] = $customer_info['lastname'];
			$data['email'] = $customer_info['email'];
			
			$this->response->setOutput($this->load->view('account/satisfaction', $data) );
		}
		else {
			$this->session->data['redirect'] = $this->url->link('account/satisfaction', '', true);
			$this->response->redirect($this->url->link('account/login', '', true));
		}
		
	}
	
	
	function upload_files(){
		if (empty($_FILES) || $_FILES['file']['error']) {
		  die('{"OK": 0, "info": "Failed to move uploaded file."}');
		}
		 
		$chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
		$chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
		 
		$fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : $_FILES["file"]["name"];
		// $filePath = "uploads/$fileName";
		if ($this->request->server['HTTPS']) {
			$base = $this->config->get('config_ssl');
		} else {
			$base = $this->config->get('config_url');
		}
		$filePath = $base . 'image/servicesatisfaction/$fileName';
		
		echo "path : " . $filePath;
		 
		 
		// Open temp file
		$out = @fopen($filePath . ".part", $chunk == 0 ? "wb" : "ab");
		if ($out) {
		  // Read binary input stream and append it to temp file
		  $in = @fopen($_FILES['file']['tmp_name'], "rb");
		 
		  if ($in) {
			while ($buff = fread($in, 4096))
			  fwrite($out, $buff);
		  } else
			die('{"OK": 0, "info": "Failed to open input stream."}');
		 
		  @fclose($in);
		  @fclose($out);
		 
		  @unlink($_FILES['file']['tmp_name']);
		} else
		  die('{"OK": 0, "info": "Failed to open output stream."}');
		 
		 
		// Check if file has been uploaded
		if (!$chunks || $chunk == $chunks - 1) {
		  // Strip the temp .part suffix off
		  rename($filePath . ".part", $filePath);
		}
		 exit();
		die('{"OK": 1, "info": "Upload successful."}');
		
	}
	
	
	/**
     * 批量图片上传
     * 上传的图片名称用'_'隔开
     * 例如 test_abc_image.jpg
     * 规则 目录名称_商品型号_图片名称
     * by yuansir 
     */
    public function pupload() {
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

		// Settings
        //$targetDir = ini_get("upload_tmp_dir") . DIRECTORY_SEPARATOR . "plupload";
        $tmp = array();
        $tmp = explode("_", $_REQUEST["name"]);

        if (count($tmp) <= 2) {
            die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "上传图片命名格式应该是：目录名称_商品型号_图片名称，例：test_abc_image.jpg"}, "id" : "id"}');
        }

		if ($this->request->server['HTTPS']) {
			$base = $this->config->get('config_ssl');
		} else {
			$base = $this->config->get('config_url');
		}
        $targetDir = $base . 'image/servicesatisfaction/' . $tmp[0];

        $cleanupTargetDir = true; // Remove old files
        $maxFileAge = 5 * 3600; // Temp file age in seconds
		// 5 minutes execution time
        @set_time_limit(5 * 60);

		// Uncomment this one to fake upload time
		// usleep(5000);
		// Get parameters
        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
        $fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : '';

		// Clean the fileName for security reasons
        $fileName = preg_replace('/[^\w\._]+/', '_', $fileName);

		// Make sure the fileName is unique but only if chunking is disabled
        if ($chunks < 2 && file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName)) {
            $ext = strrpos($fileName, '.');
            $fileName_a = substr($fileName, 0, $ext);
            $fileName_b = substr($fileName, $ext);

            $count = 1;
            while (file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName_a . '_' . $count . $fileName_b))
                $count++;

            $fileName = $fileName_a . '_' . $count . $fileName_b;
        }

        $filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;

		// Create target dir
        if (!file_exists($targetDir))
            @mkdir($targetDir);

		// Remove old temp files	
        if ($cleanupTargetDir && is_dir($targetDir) && ($dir = opendir($targetDir))) {
            while (($file = readdir($dir)) !== false) {
                $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

                // Remove temp file if it is older than the max age and is not the current file
                if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge) && ($tmpfilePath != "{$filePath}.part")) {
                    @unlink($tmpfilePath);
                }
            }

            closedir($dir);
        } else
            die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');


		// Look for the content type header
        if (isset($_SERVER["HTTP_CONTENT_TYPE"]))
            $contentType = $_SERVER["HTTP_CONTENT_TYPE"];

        if (isset($_SERVER["CONTENT_TYPE"]))
            $contentType = $_SERVER["CONTENT_TYPE"];

		// Handle non multipart uploads older WebKit versions didn't support multipart in HTML5
        if (strpos($contentType, "multipart") !== false) {
            if (isset($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {
                // Open temp file
                $out = fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
                if ($out) {
                    // Read binary input stream and append it to temp file
                    $in = fopen($_FILES['file']['tmp_name'], "rb");

                    if ($in) {
                        while ($buff = fread($in, 4096))
                            fwrite($out, $buff);
                    } else
                        die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
                    fclose($in);
                    fclose($out);
                    @unlink($_FILES['file']['tmp_name']);
                } else
                    die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
            } else
                die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
        } else {
            // Open temp file
            $out = fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
            if ($out) {
                // Read binary input stream and append it to temp file
                $in = fopen("php://input", "rb");

                if ($in) {
                    while ($buff = fread($in, 4096))
                        fwrite($out, $buff);
                } else
                    die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');

                fclose($in);
                fclose($out);
            } else
                die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
        }

		// Check if file has been uploaded
        if (!$chunks || $chunk == $chunks - 1) {
            // Strip the temp .part suffix off 
            rename("{$filePath}.part", $filePath);
        }


        /**
         *将上传好的图片保存在session中 
         */
        $this->session->data[$tmp[1]][] = array(
            'image' => 'data/' . current(explode('_', $fileName)) . '/' . $fileName,
            'sort_order' => 0
        );
        
        //var_dump($this->session->data[$tmp[1]]);


		// Return JSON-RPC response
        // die('{"jsonrpc" : "2.0", "result" : null, "id" : "id"}');
    }
	
}
