<?php
class ControllerDevisDevis extends Controller {
	
	
	public function transformer()
	{
		
		if ( isset( $this->request->get['order_id'] ) ) {
			
				
			// code order info ====================
			if (isset($this->request->get['order_id'])) {
				
			$order_id = $this->request->get['order_id'];
		
			} else {
				
				$order_id = 0;
			
			}

			if (!$this->customer->isLogged()) {
				
				$this->session->data['redirect'] = $this->url->link('account/order/info', 'order_id=' . $order_id, true);

				$this->response->redirect($this->url->link('account/login', '', true));
				
			}
			
			$this->load->model('account/order');
			$this->model_account_order->transformer( $this->request->get['order_id'] ) ;	
		
			$this->response->redirect($this->url->link('account/account', '', true));
		
		}
			
		
	}
	

	public function imprimerdevis()
	{
		
	if ( isset( $this->request->get['order_id'] ) ) {
				
			// code order info ====================
			if (isset($this->request->get['order_id'])) {
			$order_id = $this->request->get['order_id'];
		} else {
			$order_id = 0;
		}

		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/order/info', 'order_id=' . $order_id, true);

			$this->response->redirect($this->url->link('account/login', '', true));
		}

		$this->load->model('account/order');

		$order_info = $this->model_account_order->getOrder($order_id, true) ;

		
		
		$order_info = $this->model_account_order->getOrder($order_id, true) ;
		
		/*
		echo " ===== eto 1==== ".$order_id;
		var_dump( $order_info );
		echo " ===== eto 2==== ";
			*/
        
        $this->document->setTitle($this->language->get('text_order')) ;
       
        
        if ($order_info['invoice_no']) {
            $data['invoice_no'] = $order_info['invoice_prefix'] . $order_info['invoice_no'];
        } else {
            $data['invoice_no'] = '';
        }

        $data['order_id'] = $this->request->get['order_id'];
        $data['date_added'] = date($this->language->get('date_format_short'), strtotime($order_info['date_added']));

        if ($order_info['payment_address_format']) {
            $format = $order_info['payment_address_format'];
        } else {
            $format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
        }

        $find = array(
            '{firstname}',
            '{lastname}',
            '{company}',
            '{address_1}',
            '{address_2}',
            '{city}',
            '{postcode}',
            '{zone}',
            '{zone_code}',
            '{country}'
        );

        $replace = array(
            'firstname' => $order_info['payment_firstname'],
            'lastname'  => $order_info['payment_lastname'],
            'company'   => $order_info['payment_company'],
            'address_1' => $order_info['payment_address_1'],
            'address_2' => $order_info['payment_address_2'],
            'city'      => $order_info['payment_city'],
            'postcode'  => $order_info['payment_postcode'],
            'zone'      => $order_info['payment_zone'],
            'zone_code' => $order_info['payment_zone_code'],
            'country'   => $order_info['payment_country']
        );

        $data['payment_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

        $data['payment_method'] = $order_info['payment_method'];

        if ($order_info['shipping_address_format']) {
            $format = $order_info['shipping_address_format'];
        } else {
            $format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
        }

        $find = array(
            '{firstname}',
            '{lastname}',
            '{company}',
            '{address_1}',
            '{address_2}',
            '{city}',
            '{postcode}',
            '{zone}',
            '{zone_code}',
            '{country}'
        );

        $replace = array(
            'firstname' => $order_info['shipping_firstname'],
            'lastname'  => $order_info['shipping_lastname'],
            'company'   => $order_info['shipping_company'],
            'address_1' => $order_info['shipping_address_1'],
            'address_2' => $order_info['shipping_address_2'],
            'city'      => $order_info['shipping_city'],
            'postcode'  => $order_info['shipping_postcode'],
            'zone'      => $order_info['shipping_zone'],
            'zone_code' => $order_info['shipping_zone_code'],
            'country'   => $order_info['shipping_country']
        );

        $data['shipping_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

        $data['shipping_method'] = $order_info['shipping_method'];

        $this->load->model('catalog/product');
        $this->load->model('tool/upload');

        // Products
        $data['products'] = array();

        $products = $this->model_account_order->getOrderProducts($this->request->get['order_id']);

        foreach ($products as $product) {
            $option_data = array();

            $options = $this->model_account_order->getOrderOptions($this->request->get['order_id'], $product['order_product_id']);

            foreach ($options as $option) {
                if ($option['type'] != 'file') {
                    $value = $option['value'];
                } else {
                    $upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

                    if ($upload_info) {
                        $value = $upload_info['name'];
                    } else {
                        $value = '';
                    }
                }

                $option_data[] = array(
                    'name'  => $option['name'],
                    'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
                );
            }

            $product_info = $this->model_catalog_product->getProduct($product['product_id']);

            if ($product_info) {
                $reorder = $this->url->link('account/order/reorder', 'order_id=' . $order_id . '&order_product_id=' . $product['order_product_id'], true);
            } else {
                $reorder = '';
            }

			
			
			
			
			
			
            $data['products'][] = array(
                'name'     => $product['name'],
                'model'    => $product['model'],
                'option'   => $option_data,
                'quantity' => $product['quantity'],
                'price'    => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
                'total'    => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']),
                'reorder'  => $reorder,
                'return'   => $this->url->link('account/return/add', 'order_id=' . $order_info['order_id'] . '&product_id=' . $product['product_id'], true)
            );
        }

        // Voucher
        $data['vouchers'] = array();

        $vouchers = $this->model_account_order->getOrderVouchers($this->request->get['order_id']);

        foreach ($vouchers as $voucher) {
            $data['vouchers'][] = array(
                'description' => $voucher['description'],
                'amount'      => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value'])
            );
        }

        // Totals
        $data['totals'] = array();

        $totals = $this->model_account_order->getOrderTotals($this->request->get['order_id']);
        //DEV103 gestion livraison
        foreach ($totals as $total) {
            $data['totals'][] = array(
                'title' => $total['title'],
                // 'text'  => $this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']),
                'text'  => ((is_float($total['value'])) ? $this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']) : $total['value']),
            );
        }
        //DEV103 gestion livraison
        $data['comment'] = nl2br($order_info['comment']);

        // History
        $data['histories'] = array();

        $results = $this->model_account_order->getOrderHistories($this->request->get['order_id']);

        foreach ($results as $result) {
            $data['histories'][] = array(
                'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
                'status'     => $result['status'],
                'comment'    => $result['notify'] ? nl2br($result['comment']) : ''
            );
        }
		
		
			// code order info ====================
			
			$date_commande = $data['date_added'] ;
			$order_id    = $data['order_id'] ;
			
			$date_lettre = "";
			$tab_dates = explode( "/" , $date_commande );
			
			$french_months = array('','Janvier', 'F&eacute;vrier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Ao&ucirc;t', 'Septembre', 'Octobre', 'Novembre', 'D&eacute;cembre');
			$date_lettres = $tab_dates[0]." ".$french_months[ intval($tab_dates[1]) ]." ".$tab_dates[2];
			
			
			$html_produits = "" ;
			
			 foreach ( $data['products'] as $product ) { 
             $html_produits .= '<tr>';
             $html_produits .= ' <td style="border-right:1px solid #dddddd ;" >'.$product['name'] ;
                foreach ($product['option'] as $option) {
                $html_produits .= ' <br /> ' ;
				$html_produits .= ' &nbsp;<small> - '.$option['name'].': '.$option['value'].' </small> ' ;
                }
			  
			  $html_produits .= ' </td> ' ;
              $html_produits .= ' <td style="border-right:1px solid #dddddd ;">'.$product['model'].' </td> ' ;
              $html_produits .= ' <td style="border-right:1px solid #dddddd ;">'.$product['price'].' </td> ' ;
              $html_produits .= ' <td style="border-right:1px solid #dddddd ;">'.$product['quantity'].' </td> ' ;
              $html_produits .= ' <td style="border-right:1px solid #dddddd ;">'.$product['total'].' </td> </tr> ';
            
            }

			$html_totals = "" ;
			foreach ( $data['totals'] as $total) {
              $html_totals .=' <tr>  <td></td><td></td>'; 
              $html_totals .=' <td colspan="2"> '.$total['title'].' </td> ' ;
              $html_totals .=' <td >  '.$total['text'] .' </td> ' ;
              $html_totals .=' </tr> ' ;
			} 
			
		require_once(DIR_DOMPDF."dompdf_config.inc.php");
		$dompdf = new DOMPDF();
		$html ='			
				<!DOCTYPE html>
				<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
				<head>
				</head>
				<body>
				
				<div style="width:100%;height: 30px;">
					<div style="width:20%;float:left;font-weight: bold;">
					'.$date_commande.'						
					</div>
					
					<div style="width:80%;float:left;text-align:center;font-weight: bold;">
					Imprimer le devis N&deg; '.$order_id.'						
					</div>
					<div style="clear: both;"></div>
				</div>
			
			<div style="margin-top:10px;height:30px;width: 100%;font-size:11px;text-transform: uppercase;font-family: sans-serif;font-weight: bold;color: #7b7a7a;">
			imprimerie en ligne - carte de visite - flyers - depliants - plaquettes - stickers - totems - stands - baches 
			</div>	
						
			<div style="width:100%">
				<div style="width:100%;height:50px;">
					<img style="width:300px" src="'.HTTP_SERVER.'image/catalog/logo.png" >
				</div>
				<div style="width:100%;text-align:right">
					<div style="font-size: 20px;" > 04 94 56 79 20  </div>
					<div style="" > adprint-dev.com </div>
				</div>
			</div>	
			<div style="color:#adabab;width:100%;margin-top:10px;margin-bottom:10px;height:25px">								   	
				<div style="float:left;width:70%;text-align:left;font-size:22px;"> 
					N&deg; '.$order_id.'  
				</div>												   
				<div style="float:left;width:30%;text-align:right;font-size:22px;">
					Date : '.$date_lettres.'
				</div>
				<div style="clear: both;"></div>
			</div>
			<div style="color:#adabab;width:100%;margin-top:5px;height:20px">
				Articles
			</div>
			
			<div style="width:100%">
			
			<table style="width:100%">
			<tr>
			<td style="border-bottom:1px solid #dddddd ;width:300px">Nom de produit</td>
			<td style="border-bottom:1px solid #dddddd;width:100px">Model</td>
			<td style="border-bottom:1px solid #dddddd;width:100px">Prix</td>
			<td style="border-bottom:1px solid #dddddd;width:100px">Qte</td>
			<td style="border-bottom:1px solid #dddddd;width:100px">Sous-total</td>
			</tr>
			'.$html_produits.'
			'.$html_totals.'
			</table>	
			</div>
			<div style="margin-top:40px;width:100%;color:#adabab;font-size:18px">
				<div style="width:50%;float:left;">
				Antananarivo Alarobia
				</div>
				<div style="width:50%;float:left;">
				Adprint soci&eacute;t&eacute; au capital de 2 000 000 Ar
				</div>
			</div>	
</body>
</html>
';


        $dompdf->load_html( $html );
	    $dompdf->set_paper( "04" , "portrait" );
	    $dompdf->render();
	    $dompdf->stream("devis.pdf", array("Attachment" => false ));
	    exit(0);

		
		
	}		
}


	
	public function listedevis()
	{
		
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/account', '', true);

			$this->response->redirect($this->url->link('account/login', '', true));
		}
		
		
		
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/order', '', true);

			$this->response->redirect($this->url->link('account/login', '', true));
		}

		
		    
    $this->load->model('account/customer');
    
    $customer_info = $this->model_account_customer->getCustomer($this->customer->getId());
    
    $data['firstname'] = $customer_info['firstname'];
    $data['lastname'] = $customer_info['lastname'];
    $data['email'] = $customer_info['email'];
    
		
		
		$this->load->language('devis/devis');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$url = '';

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', true)
		);
		
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('account/order', $url, true)
		);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_empty'] = $this->language->get('text_devis_empty');

		$data['column_order_id'] = $this->language->get('column_order_id');
		$data['column_customer'] = $this->language->get('column_customer');
		$data['column_product'] = $this->language->get('column_product');
		$data['column_total'] = $this->language->get('column_total');
		$data['column_status'] = $this->language->get('column_status');
		$data['column_date_added'] = $this->language->get('column_date_added');

		$data['button_view'] = $this->language->get('button_view');
		$data['button_continue'] = $this->language->get('button_continue');

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$data['orders'] = array();

		$this->load->model('account/order');
		$this->load->model('catalog/product');
	    $this->load->model('tool/upload');	
			  

		$order_total = $this->model_account_order->getTotalOrders();

		$results = $this->model_account_order->getDevis( ($page - 1) * 10, 10);
		
		foreach ($results as $result) {
			
			$product_total = $this->model_account_order->getTotalOrderProductsByOrderId($result['order_id']);
			
			$voucher_total = $this->model_account_order->getTotalOrderVouchersByOrderId($result['order_id']);
			
			$products = $this->model_account_order->getOrderProducts( $result['order_id'] );
			 
			$produits_reel = array();
			   
			foreach ($products as $product) {
				$option_data = array();
				$options = $this->model_account_order->getOrderOptions(  $result['order_id'] , $product['order_product_id']);

				foreach ($options as $option) {
					if($option['name'] == 'option_dynamique'){
						continue;
					}
					if ($option['type'] != 'file') {
						$value = $option['value'];
					} else {
						$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

						if ($upload_info) {
							$value = $upload_info['name'];
						} else {
							$value = '';
						}
					}

					$option_data[] = array(
						'name'  => $option['name'],
						'value' => (utf8_strlen($value) > 50 ? utf8_substr($value, 0, 50) . '..' : $value)
					);
				}

				$product_info = $this->model_catalog_product->getProduct($product['product_id']);

				if ($product_info) {
					$reorder = $this->url->link('account/order/reorder', 'order_id=' . $result['order_id'] . '&order_product_id=' . $product['order_product_id'], true);
				} else {
					$reorder = '';
				}
				
//$order_info = $this->model_account_order->getOrder( $result['order_id'] );
				$order_info = $this->model_account_order->getOneDevis( $result['order_id'] );
				
				$result_produit  =   $this->model_catalog_product->getProduct(  $product['product_id']  );
				
				$produits_reel[] = array(
					'name'       => $product['name']       ,
					'product_id' => $product['product_id'] , 
					'image' => $result_produit['image'] , 
					'model'    => $product['model'],
					'option'   => $option_data,
					'quantity' => $product['quantity'],
					'price'    => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
					'total'    => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']),
					'reorder'  => $reorder,
					'return'   => $this->url->link('account/return/add', 'order_id=' . $order_info['order_id'] . '&product_id=' . $product['product_id'], true)
				);
			}
			
			
		
			
			
			// adresse , livraison , ....
			
				// $order_info = $this->model_account_order->getOrder( $result['order_id'] );
				

					if ($order_info['payment_address_format']) {
						$format = $order_info['payment_address_format'];
					} else {
						$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
					}

					$find = array(
						'{firstname}',
						'{lastname}',
						'{company}',
						'{address_1}',
						'{address_2}',
						'{city}',
						'{postcode}',
						'{zone}',
						'{zone_code}',
						'{country}'
					);

					$replace = array(
						'firstname' => $order_info['payment_firstname'],
						'lastname'  => $order_info['payment_lastname'],
						'company'   => $order_info['payment_company'],
						'address_1' => $order_info['payment_address_1'],
						'address_2' => $order_info['payment_address_2'],
						'city'      => $order_info['payment_city'],
						'postcode'  => $order_info['payment_postcode'],
						'zone'      => $order_info['payment_zone'],
						'zone_code' => $order_info['payment_zone_code'],
						'country'   => $order_info['payment_country']
					);

					$payment_address = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

					$payment_method = $order_info['payment_method'];

					if ($order_info['shipping_address_format']) {
						$format = $order_info['shipping_address_format'];
					} else {
						$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
					}

					$find = array(
						'{firstname}',
						'{lastname}',
						'{company}',
						'{address_1}',
						'{address_2}',
						'{city}',
						'{postcode}',
						'{zone}',
						'{zone_code}',
						'{country}'
					);

					$replace = array(
						'firstname' => $order_info['shipping_firstname'],
						'lastname'  => $order_info['shipping_lastname'],
						'company'   => $order_info['shipping_company'],
						'address_1' => $order_info['shipping_address_1'],
						'address_2' => $order_info['shipping_address_2'],
						'city'      => $order_info['shipping_city'],
						'postcode'  => $order_info['shipping_postcode'],
						'zone'      => $order_info['shipping_zone'],
						'zone_code' => $order_info['shipping_zone_code'],
						'country'   => $order_info['shipping_country']
					);

					$shipping_address = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

					$shipping_method = $order_info['shipping_method'];
			
			
			// adresse , livraison , ...
			
			
			
			// total 
			$totals_vues = array();

			$totals = $this->model_account_order->getOrderTotals(  $result['order_id']  );
            //DEV103 gestion livraison
			foreach ($totals as $total) {
				$totals_vues[] = array(
					'title' => $total['title'],
					// 'text'  => $this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']),
					'text'  => ((is_float($total['value'])) ? $this->currency->format($total['value'],  $result['currency_code'] ,$result['currency_value'] ) : $total['value']),
				);
			}
			
			// total fin
			
			
			  
			$data['orders'][] = array(
				'order_id'   => $result['order_id'],
				'name'       => $result['firstname'] . ' ' . $result['lastname'],
				'status'     => $result['status'],
				'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
				'products'   => ($product_total + $voucher_total),
				'total'      => $this->currency->format($result['total'], $result['currency_code'], $result['currency_value']),
				'view'       => $this->url->link('account/order/info', 'order_id=' . $result['order_id'], true),
				'produits'   =>  $produits_reel,
				'payment_address'  => $payment_address  ,
				'payment_method'   => $payment_method   ,
				'shipping_address' => $shipping_address ,
				'shipping_method'  => $shipping_method  ,
				'totals'		   => $totals_vues 		
			);
			
		}

		$pagination = new Pagination();
		$pagination->total = $order_total;
		$pagination->page = $page;
		$pagination->limit = 10;
		$pagination->url = $this->url->link('account/order', 'page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($order_total) ? (($page - 1) * 10) + 1 : 0, ((($page - 1) * 10) > ($order_total - 10)) ? $order_total : ((($page - 1) * 10) + 10), $order_total, ceil($order_total / 10));

		$data['continue'] = $this->url->link('account/account', '', true);

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('account/order_list_devis', $data));
		
		
		
	}
	
	
	public function index() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/account', '', true);

			$this->response->redirect($this->url->link('account/login', '', true));
		}
        $_iOrderId = 0 ;
        
        $toRequest  = $this->request->request ;
        $zRoute     = $toRequest['route'] ;
        $tzRoute    = explode('/', $zRoute) ;
        $zDerniereColonne = end($tzRoute) ;
        if(is_int($zDerniereColonne))
        {
            $_iOrderId = $zDerniereColonne ;
        }
        
        
		
		$this->load->language('devis/devis');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_gestion_devis'),
			'href' => $this->url->link('devis/devis', '', true)
		);
        
        $this->load->model('account/order');
        
        $toDevis = array() ;
        $toDevis = $this->model_account_order->loadDevis() ;
        
        if ($this->request->server['HTTPS']) {
			$server = $this->config->get('config_ssl');
		} else {
			$server = $this->config->get('config_url');
		}
        
        foreach($toDevis as $iKey=>$toDevi)
        {
            $toDevis[$iKey]['devis_fichier']    = $this->url->link('devis/devis/download', 'fichier=' . $toDevi['devis'], true) ;
        }
        
        $data['toDevis'] = $toDevis ;
        
        
		$data['heading_title']          = $this->language->get('heading_title');
		$data['text_vide']              = $this->language->get('text_vide');
		$data['text_gestion_devis']     = $this->language->get('text_gestion_devis');
		$data['text_choix_commande']    = $this->language->get('text_choix_commande');
		$data['text_titre_produit']     = $this->language->get('text_titre_produit');
		$data['text_titre_statut']      = $this->language->get('text_titre_statut');
		$data['text_titre_action']      = $this->language->get('text_titre_action');
		$data['text_loading']           = $this->language->get('text_loading');
		$data['text_verouiller']           = $this->language->get('text_verouiller');
		$data['text_verouille']           = $this->language->get('text_verouille');
		$data['button_upload']          = $this->language->get('button_upload');
        
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		
		$this->response->setOutput($this->load->view('devis/devis', $data));
	}
    public function generer()
    {
        $this->load->language('account/order');
		
		if (isset($this->request->get['order_id'])) {
			$order_id = $this->request->get['order_id'];
		} else {
			$order_id = 0;
		}
        $this->load->model('account/order');

		$order_info = $this->model_account_order->getOrder($order_id, true) ;
		
		/*
		echo " ===== eto 1==== ".$order_id;
		var_dump( $order_info );
		echo " ===== eto 2==== ";
			*/
        
        $this->document->setTitle($this->language->get('text_order')) ;
        
        $data['heading_title'] = $this->language->get('text_order');

        $data['text_order_detail'] = $this->language->get('text_order_detail');
        $data['text_invoice_no'] = $this->language->get('text_invoice_no');
        $data['text_order_id'] = $this->language->get('text_order_id');
        $data['text_date_added'] = $this->language->get('text_date_added');
        $data['text_shipping_method'] = $this->language->get('text_shipping_method');
        $data['text_shipping_address'] = $this->language->get('text_shipping_address');
        $data['text_payment_method'] = $this->language->get('text_payment_method');
        $data['text_payment_address'] = $this->language->get('text_payment_address');
        $data['text_history'] = $this->language->get('text_history');
        $data['text_comment'] = $this->language->get('text_comment');
        $data['text_no_results'] = $this->language->get('text_no_results');

        $data['column_name'] = $this->language->get('column_name');
        $data['column_model'] = $this->language->get('column_model');
        $data['column_quantity'] = $this->language->get('column_quantity');
        $data['column_price'] = $this->language->get('column_price');
        $data['column_total'] = $this->language->get('column_total');
        $data['column_action'] = $this->language->get('column_action');
        $data['column_date_added'] = $this->language->get('column_date_added');
        $data['column_status'] = $this->language->get('column_status');
        $data['column_comment'] = $this->language->get('column_comment');

        $data['button_reorder'] = $this->language->get('button_reorder');
        $data['button_return'] = $this->language->get('button_return');
        $data['button_continue'] = $this->language->get('button_continue');
        
        if ($order_info['invoice_no']) {
            $data['invoice_no'] = $order_info['invoice_prefix'] . $order_info['invoice_no'];
        } else {
            $data['invoice_no'] = '';
        }

        $data['order_id'] = $this->request->get['order_id'];
        $data['date_added'] = date($this->language->get('date_format_short'), strtotime($order_info['date_added']));

        if ($order_info['payment_address_format']) {
            $format = $order_info['payment_address_format'];
        } else {
            $format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
        }

        $find = array(
            '{firstname}',
            '{lastname}',
            '{company}',
            '{address_1}',
            '{address_2}',
            '{city}',
            '{postcode}',
            '{zone}',
            '{zone_code}',
            '{country}'
        );

        $replace = array(
            'firstname' => $order_info['payment_firstname'],
            'lastname'  => $order_info['payment_lastname'],
            'company'   => $order_info['payment_company'],
            'address_1' => $order_info['payment_address_1'],
            'address_2' => $order_info['payment_address_2'],
            'city'      => $order_info['payment_city'],
            'postcode'  => $order_info['payment_postcode'],
            'zone'      => $order_info['payment_zone'],
            'zone_code' => $order_info['payment_zone_code'],
            'country'   => $order_info['payment_country']
        );

        $data['payment_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

        $data['payment_method'] = $order_info['payment_method'];

        if ($order_info['shipping_address_format']) {
            $format = $order_info['shipping_address_format'];
        } else {
            $format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
        }

        $find = array(
            '{firstname}',
            '{lastname}',
            '{company}',
            '{address_1}',
            '{address_2}',
            '{city}',
            '{postcode}',
            '{zone}',
            '{zone_code}',
            '{country}'
        );

        $replace = array(
            'firstname' => $order_info['shipping_firstname'],
            'lastname'  => $order_info['shipping_lastname'],
            'company'   => $order_info['shipping_company'],
            'address_1' => $order_info['shipping_address_1'],
            'address_2' => $order_info['shipping_address_2'],
            'city'      => $order_info['shipping_city'],
            'postcode'  => $order_info['shipping_postcode'],
            'zone'      => $order_info['shipping_zone'],
            'zone_code' => $order_info['shipping_zone_code'],
            'country'   => $order_info['shipping_country']
        );

        $data['shipping_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

        $data['shipping_method'] = $order_info['shipping_method'];

        $this->load->model('catalog/product');
        $this->load->model('tool/upload');

        // Products
        $data['products'] = array();

        $products = $this->model_account_order->getOrderProducts($this->request->get['order_id']);

        foreach ($products as $product) {
            $option_data = array();

            $options = $this->model_account_order->getOrderOptions($this->request->get['order_id'], $product['order_product_id']);

            foreach ($options as $option) {
                if ($option['type'] != 'file') {
                    $value = $option['value'];
                } else {
                    $upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

                    if ($upload_info) {
                        $value = $upload_info['name'];
                    } else {
                        $value = '';
                    }
                }

                $option_data[] = array(
                    'name'  => $option['name'],
                    'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
                );
            }

            $product_info = $this->model_catalog_product->getProduct($product['product_id']);

            if ($product_info) {
                $reorder = $this->url->link('account/order/reorder', 'order_id=' . $order_id . '&order_product_id=' . $product['order_product_id'], true);
            } else {
                $reorder = '';
            }

			
			
			
			
			
			
            $data['products'][] = array(
                'name'     => $product['name'],
                'model'    => $product['model'],
                'option'   => $option_data,
                'quantity' => $product['quantity'],
                'price'    => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
                'total'    => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']),
                'reorder'  => $reorder,
                'return'   => $this->url->link('account/return/add', 'order_id=' . $order_info['order_id'] . '&product_id=' . $product['product_id'], true)
            );
        }

        // Voucher
        $data['vouchers'] = array();

        $vouchers = $this->model_account_order->getOrderVouchers($this->request->get['order_id']);

        foreach ($vouchers as $voucher) {
            $data['vouchers'][] = array(
                'description' => $voucher['description'],
                'amount'      => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value'])
            );
        }

        // Totals
        $data['totals'] = array();

        $totals = $this->model_account_order->getOrderTotals($this->request->get['order_id']);
        //DEV103 gestion livraison
        foreach ($totals as $total) {
            $data['totals'][] = array(
                'title' => $total['title'],
                // 'text'  => $this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']),
                'text'  => ((is_float($total['value'])) ? $this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']) : $total['value']),
            );
        }
        //DEV103 gestion livraison
        $data['comment'] = nl2br($order_info['comment']);

        // History
        $data['histories'] = array();

        $results = $this->model_account_order->getOrderHistories($this->request->get['order_id']);

        foreach ($results as $result) {
            $data['histories'][] = array(
                'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
                'status'     => $result['status'],
                'comment'    => $result['notify'] ? nl2br($result['comment']) : ''
            );
        }

        $data['continue'] = $this->url->link('account/order', '', true);

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        if ($this->request->server['HTTPS']) {
			$server = $this->config->get('config_ssl');
		} else {
			$server = $this->config->get('config_url');
		}


		if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
			$data['logo'] = $server . 'image/' . $this->config->get('config_logo');
		} else {
			$data['logo'] = '';
		}
        $zPath = DIR_UPLOAD . 'devis' ; 
        $path_ok = true ;
        if (!file_exists($zPath)) {
            if (!mkdir($zPath, 0777, true)) {
                $path_ok = false;
            }
        }
        if(!$path_ok)
        {
            return false ;
        }
        require_once("dompdf/dompdf_config.inc.php");
        $zContenu = $this->load->view('devis/detail_devis', $data) ;
	    
	    $dompdf = new DOMPDF();
        
	    
	    $dompdf->load_html($zContenu);
	    
        $dompdf->set_paper('a4','landscape');
	    $dompdf->render();
        //$dompdf->stream("exemple.pdf", array("Attachment" => false));
        // $dompdf->stream("exemple.pdf");
        $data = $dompdf->output();
        $zFileName = "devis_" . $this->request->get['order_id'] . "_" . date('YmdHis') . ".pdf" ;
        $zFilePath = $zPath . "/" . $zFileName ;
        $fh = fopen($zFilePath, "w");
        if(fputs($fh, $data))
        {
            fclose($fh);
        }
        $this->model_account_order->updateOrderDevis($this->request->get['order_id'], $zFileName) ;
    }
	
	
    public function download() {
		$this->load->model('tool/upload');

		if (isset($this->request->get['fichier'])) {
			$zFichier = $this->request->get['fichier'];
		} else {
			$zFichier = 0;
		}


    
        $file = DIR_UPLOAD . 'devis/' . $zFichier ;
        $mask = basename($zFichier);

        if (!headers_sent()) {
            if (is_file($file)) {
                header('Content-Type: application/octet-stream');
                header('Content-Description: File Transfer');
                header('Content-Disposition: attachment; filename="' . ($mask ? $mask : basename($zFichier)) . '"');
                header('Content-Transfer-Encoding: binary');
                header('Expires: 0');
                header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                header('Pragma: public');
                header('Content-Length: ' . filesize($file));

                readfile($file, 'rb');
                exit;
            } else {
                exit('Error: Could not find file ' . $file . '!');
            }
        } else {
            exit('Error: Headers already sent out!');
        }
		
	}
}
