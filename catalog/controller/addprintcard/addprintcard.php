<?php
class ControllerAddprintcardAddprintcard extends Controller {
  public function index(){
    $this->load->model('addprintcard/addprintcard') ;
    $this->load->language('addprintcard/addprintcard') ;
    
    if (!$this->customer->isLogged()) {
      $this->session->data['redirect'] = $this->url->link('account/account', '', true);
      $this->response->redirect($this->url->link('account/login', '', true));
    }
    
    // recuperer ma solde
    $customer_id     = $this->session->data['customer_id'];
    $solde       = $this->findSolde($customer_id ) ;
    // faire une paymentCard
     // $payment     = $this->payementByCard(20, -1000) ;
    // Ajouter un solde dans le compte addprintcard
    //$this->addSolde($customer_id, 200) ;
    //recuperer l'historique
    // $historique   = $this->addprintcard->findHistorique(1) ;
    $depense     = $this->model_addprintcard_addprintcard->findDepense($customer_id) ;
    $ajout       = $this->model_addprintcard_addprintcard->findAjout($customer_id) ;


    $compte = $this->verifiedCompte($customer_id);
    $data['solde']       = $solde ;
    $data['compte']      = $compte ;
    $data['depenses']    = $depense ;
    $data['ajouts']      = $ajout ;

    $data['msg_solde']       = $this->language->get('msg_solde') ;
    $data['text_historique']   = $this->language->get('text_historique') ;
    $data['text_motant_title']   = $this->language->get('text_motant_title') ;
    $data['text_date_title']   = $this->language->get('text_date_title') ;


    $data['header']     = $this->load->controller('common/header');
    $data['footer']     = $this->load->controller('common/footer');
    
    //svar_dump($data) ;
    

    $this->response->addHeader($this->request->server['SERVER_PROTOCOL']);
    $this->response->setOutput($this->load->view('addprintcard/addprintcard', $data));
  }
  public function addsoldes(){
    $this->addSolde(2,100);
  }
  public function verifiedCompte($id_customer){
    $compte = $this->model_addprintcard_addprintcard->verifiedCompte($id_customer) ;
    if($compte == null){
      return   $this->language->get('montant_vide') ;
    }
    else{
      return 1 ;
    }
    
  }
  public function payementByCard($id_customer, $montant){
    $payed = $this->model_addprintcard_addprintcard->paymentCard($id_customer, $montant);
    if(isset($payed)){
      if($payed == 0){
      
        return $this->language->get('solde_insuffisant') ;
      }
      if($payed == 1){
        return $this->language->get('montant_negatif') ; 
      }
      if($payed == 2){
        return $this->language->get('montant_vide') ;
      }
    }
  }
  public function findSolde($id_customer){
    return $this->model_addprintcard_addprintcard->findSolde($id_customer);
  }
  public function addSolde($id_customer, $montant){
    $addSolde = $this->model_addprintcard_addprintcard->addSolde($id_customer, $montant);  
    if(isset($addSolde)){
      if($addSolde == 1){
        return $this->language->get('montant_negatif') ;
      }
      if($addSolde == 2){
        return $this->language->get('montant_vide') ; 
      }
    }
  }
  public function findHistorique($id_customer){
    return $this->model_addprintcard_addprintcard->findAll($id_customer) ;
  }
  public function sendMail(){

    $data['footer']     = $this->load->controller('common/footer');
    $data['header']     = $this->load->controller('common/header');
    // echo "<pre>" ;
    // echo "<pre>" ;
    // var_dump("test");
    // echo "</pre>" ;
    $this->response->addHeader($this->request->server['SERVER_PROTOCOL']);
    $this->response->setOutput($this->load->view('addprintcard/sendMail', $data));
  }
}