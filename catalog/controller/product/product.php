<?php
class ControllerProductProduct extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('product/product');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$this->load->model('catalog/category');
		$this->load->model('account/customer');

		//TVA 
		if ($this->customer->isLogged()) {
			$data['TVA'] = $this->model_account_customer->getCustomerByTVA($this->customer->getId());
		}
		else{
			$data['TVA'] = 1;
		}

		if (isset($this->request->get['path'])) {
			$path = '';

			$parts = explode('_', (string)$this->request->get['path']);

			$category_id = (int)array_pop($parts);

			foreach ($parts as $path_id) {
				if (!$path) {
					$path = $path_id;
				} else {
					$path .= '_' . $path_id;
				}

				$category_info = $this->model_catalog_category->getCategory($path_id);

				if ($category_info) {
					$data['breadcrumbs'][] = array(
						'text' => $category_info['name'],
						'href' => $this->url->link('product/category', 'path=' . $path)
					);
				}
			}

			// Set the last category breadcrumb
			$category_info = $this->model_catalog_category->getCategory($category_id);

			if ($category_info) {
				$url = '';

				if (isset($this->request->get['sort'])) {
					$url .= '&sort=' . $this->request->get['sort'];
				}

				if (isset($this->request->get['order'])) {
					$url .= '&order=' . $this->request->get['order'];
				}

				if (isset($this->request->get['page'])) {
					$url .= '&page=' . $this->request->get['page'];
				}

				if (isset($this->request->get['limit'])) {
					$url .= '&limit=' . $this->request->get['limit'];
				}

				$data['breadcrumbs'][] = array(
					'text' => $category_info['name'],
					'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url)
				);
			}
		}

		$this->load->model('catalog/manufacturer');

		if (isset($this->request->get['manufacturer_id'])) {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_brand'),
				'href' => $this->url->link('product/manufacturer')
			);

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$manufacturer_info = $this->model_catalog_manufacturer->getManufacturer($this->request->get['manufacturer_id']);

			if ($manufacturer_info) {
				$data['breadcrumbs'][] = array(
					'text' => $manufacturer_info['name'],
					'href' => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . $url)
				);
			}
		}

		if (isset($this->request->get['search']) || isset($this->request->get['tag'])) {
			$url = '';

			if (isset($this->request->get['search'])) {
				$url .= '&search=' . $this->request->get['search'];
			}

			if (isset($this->request->get['tag'])) {
				$url .= '&tag=' . $this->request->get['tag'];
			}

			if (isset($this->request->get['description'])) {
				$url .= '&description=' . $this->request->get['description'];
			}

			if (isset($this->request->get['category_id'])) {
				$url .= '&category_id=' . $this->request->get['category_id'];
			}

			if (isset($this->request->get['sub_category'])) {
				$url .= '&sub_category=' . $this->request->get['sub_category'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_search'),
				'href' => $this->url->link('product/search', $url)
			);
		}

		if (isset($this->request->get['product_id'])) {
			$product_id = (int)$this->request->get['product_id'];
		} else {
			$product_id = 0;
		}

		$this->load->model('catalog/product');

		$product_info = $this->model_catalog_product->getProduct($product_id);
		if ($product_info) {
			
			
			// gabarit
			$data['gabarit'] = $product_info['gabarit'] ;
			
			
			/*beging dev101SFM180417 exemplaire*/
			if(isset($this->request->get['idexemplaire'])){
				$this->load->model("catalog/exemplaire") ;
				$data['idExemplaire'] = $this->request->get['idexemplaire'] ;
				$exemplaire = $this->model_catalog_exemplaire->findExemplaireById($this->request->get['idexemplaire']);
				$product_info['price'] = $exemplaire['prix_product_ex'] ;
				if($exemplaire['activation_remise'] == 1 ){
					$data['old_price'] = $this->currency->format($exemplaire['prix_product_ex'],$this->session->data['currency']) ;

					$product_info['price'] = $exemplaire['prix_remise'] ;
				}
				$product_info['name'] = $product_info['name']. " ".$exemplaire['nombre_ex']." ex";
			}
			/*ending dev101SFM180417 exemplaire*/
			$url = '';

			if (isset($this->request->get['path'])) {
				$url .= '&path=' . $this->request->get['path'];
			}

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['manufacturer_id'])) {
				$url .= '&manufacturer_id=' . $this->request->get['manufacturer_id'];
			}

			if (isset($this->request->get['search'])) {
				$url .= '&search=' . $this->request->get['search'];
			}

			if (isset($this->request->get['tag'])) {
				$url .= '&tag=' . $this->request->get['tag'];
			}

			if (isset($this->request->get['description'])) {
				$url .= '&description=' . $this->request->get['description'];
			}

			if (isset($this->request->get['category_id'])) {
				$url .= '&category_id=' . $this->request->get['category_id'];
			}

			if (isset($this->request->get['sub_category'])) {
				$url .= '&sub_category=' . $this->request->get['sub_category'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['breadcrumbs'][] = array(
				'text' => $product_info['name'],
				'href' => $this->url->link('product/product', $url . '&product_id=' . $this->request->get['product_id'])
			);

			$this->document->setTitle($product_info['meta_title']);
			$this->document->setDescription($product_info['meta_description']);
			$this->document->setKeywords($product_info['meta_keyword']);
			$this->document->addLink($this->url->link('product/product', 'product_id=' . $this->request->get['product_id']), 'canonical');
			$this->document->addScript('catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js');
			$this->document->addStyle('catalog/view/javascript/jquery/magnific/magnific-popup.css');
			$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment.js');
			$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
			$this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');

			$data['heading_title'] = $product_info['name'];
			
			// Forex PVC Expansé //
			if($product_info['product_id'] == 295){
				$data['prix_plaque'] 	= $product_info['price'];
			}
			// Forex PVC Expansé //
			
            /*DEV101:recuperation type produit : standard/sur mesure */
            $data['is_surmesure'] = $product_info['is_surmesure'];
            /*DEV101:recuperation type produit : standard/sur mesure */
			$data['easycredit'] = $product_info['easycredit'];
            //DEV101 : choix oeillet
            $data['choix_oeillet'] = $product_info['choix_oeillet'];
            
			$data['text_select'] = $this->language->get('text_select');
			$data['text_manufacturer'] = $this->language->get('text_manufacturer');
			$data['text_model'] = $this->language->get('text_model');
			$data['text_reward'] = $this->language->get('text_reward');
			$data['text_points'] = $this->language->get('text_points');
			$data['text_stock'] = $this->language->get('text_stock');
			$data['text_discount'] = $this->language->get('text_discount');
			$data['text_tax'] = $this->language->get('text_tax');
			$data['text_option'] = $this->language->get('text_option');
			$data['text_minimum'] = sprintf($this->language->get('text_minimum'), $product_info['minimum']);
			$data['text_write'] = $this->language->get('text_write');
			$data['text_login'] = sprintf($this->language->get('text_login'), $this->url->link('account/login', '', true), $this->url->link('account/register', '', true));
			$data['text_note'] = $this->language->get('text_note');
			$data['text_tags'] = $this->language->get('text_tags');
			$data['text_related'] = $this->language->get('text_related');
			$data['text_payment_recurring'] = $this->language->get('text_payment_recurring');
			$data['text_loading'] = $this->language->get('text_loading');

			$data['entry_qty'] = $this->language->get('entry_qty');
			$data['entry_name'] = $this->language->get('entry_name');
			$data['entry_review'] = $this->language->get('entry_review');
			$data['entry_rating'] = $this->language->get('entry_rating');
			$data['entry_good'] = $this->language->get('entry_good');
			$data['entry_bad'] = $this->language->get('entry_bad');
            //DEV103 Télécharger Model
			$data['text_telecharger_model'] = $this->language->get('text_telecharger_model');
            $data['code_fichier_model']     = $product_info['code_fichier_model'] ;
            $data['zUrlFichierModel']       = $this->url->link('tool/upload/download', 'code=' . $data['code_fichier_model'], true) ; 
            //DEV103 Télécharger Model
            $data['video_url'] = $product_info['video_url'] ;
			$data['button_cart'] = $this->language->get('button_cart');
			$data['button_wishlist'] = $this->language->get('button_wishlist');
			$data['button_compare'] = $this->language->get('button_compare');
			$data['button_upload'] = $this->language->get('button_upload');
			$data['button_continue'] = $this->language->get('button_continue');

			$this->load->model('catalog/review');

			$data['tab_description'] = $this->language->get('tab_description');
			$data['tab_attribute'] = $this->language->get('tab_attribute');
			$data['tab_review'] = sprintf($this->language->get('tab_review'), $product_info['reviews']);

			$data['product_id'] = (int)$this->request->get['product_id'];
			$data['manufacturer'] = $product_info['manufacturer'];
			$data['manufacturers'] = $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $product_info['manufacturer_id']);
			$data['model'] = $product_info['model'];
			$data['reward'] = $product_info['reward'];
			$data['points'] = $product_info['points'];
			$data['description'] = html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8');
			$data['meta_description'] = html_entity_decode($product_info['meta_description'], ENT_QUOTES, 'UTF-8');
            
            if (isset($this->request->post['prix_metre_carre'])) {
                $data['prix_metre_carre'] = $this->request->post['prix_metre_carre'];
            } elseif (!empty($product_info)) {
                $data['prix_metre_carre'] = $product_info['prix_metre_carre'];
            } else {
                $data['prix_metre_carre'] = '0.00';
            }
            
            if (isset($this->request->post['seuil'])) {
                $data['seuil'] = $this->request->post['seuil'];
            } elseif (!empty($product_info)) {
                $data['seuil'] = $product_info['seuil'];
            } else {
                $data['seuil'] = '0.00';
            }
            
            if (isset($this->request->post['prix_metre_carre_remise'])) {
                //$data['prix_metre_carre_remise'] = $this->request->post['prix_metre_carre_remise'];
				$data['prix_metre_carre_remise'] = $this->currency->format($this->request->post['prix_metre_carre_remise'], $this->session->data['currency']);
            } elseif (!empty($product_info)) {
               // $data['prix_metre_carre_remise'] = $product_info['prix_metre_carre_remise'];
				$data['prix_metre_carre_remise'] = $this->currency->format($product_info['prix_metre_carre_remise'], $this->session->data['currency']);
            } else {
                $data['prix_metre_carre_remise'] = '0.00';
            }
            
            //LARGEUR MAX
            if (isset($this->request->post['largeur_minimum'])) {
                $data['largeur_minimum'] = $this->request->post['largeur_minimum'];
            } elseif (!empty($product_info)) {
                $data['largeur_minimum'] = $product_info['largeur_minimum'];
            } else {
                $data['largeur_minimum'] = '0.00';
            }
            
            //LARGEUR MIN
            if (isset($this->request->post['largeur_maximum'])) {
                $data['largeur_maximum'] = $this->request->post['largeur_maximum'];
            } elseif (!empty($product_info)) {
                $data['largeur_maximum'] = $product_info['largeur_maximum'];
            } else {
                $data['largeur_maximum'] = '0.00';
            }
            
            //HAUTEUR MAX
            if (isset($this->request->post['hauteur_minimum'])) {
                $data['hauteur_minimum'] = $this->request->post['hauteur_minimum'];
            } elseif (!empty($product_info)) {
                $data['hauteur_minimum'] = $product_info['hauteur_minimum'];
            } else {
                $data['hauteur_minimum'] = '0.00';
            }
            
            //HAUTEUR MIN
            if (isset($this->request->post['hauteur_maximum'])) {
                $data['hauteur_maximum'] = $this->request->post['hauteur_maximum'];
            } elseif (!empty($product_info)) {
                $data['hauteur_maximum'] = $product_info['hauteur_maximum'];
            } else {
                $data['hauteur_maximum'] = '0.00';
            }
            
			$data['laize'] = $product_info['laize'];
			
			if ($product_info['quantity'] <= 0) {
				$data['stock'] = $product_info['stock_status'];
			} elseif ($this->config->get('config_stock_display')) {
				$data['stock'] = $product_info['quantity'];
			} else {
				$data['stock'] = $this->language->get('text_instock');
			}

			$this->load->model('tool/image');

			if ($product_info['image']) {
				$data['popup'] = $this->model_tool_image->resize($product_info['image'], $this->config->get($this->config->get('config_theme') . '_image_popup_width'), $this->config->get($this->config->get('config_theme') . '_image_popup_height'));
			} else {
				$data['popup'] = '';
			}

			if ($product_info['image']) {
				$data['thumb'] = $this->model_tool_image->resize($product_info['image'], $this->config->get($this->config->get('config_theme') . '_image_thumb_width'), $this->config->get($this->config->get('config_theme') . '_image_thumb_height'));
			} else {
				$data['thumb'] = '';
			}

			$data['images'] = array();

			$results = $this->model_catalog_product->getProductImages($this->request->get['product_id']);

			foreach ($results as $result) {
				$data['images'][] = array(
					'popup' => $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_popup_width'), $this->config->get($this->config->get('config_theme') . '_image_popup_height')),
					'thumb' => $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_additional_width'), $this->config->get($this->config->get('config_theme') . '_image_additional_height'))
				);
			}
			
			if($product_info['easycredit'] == 1){
					$product_info['price'] -= ($product_info['price']*$product_info['easycredit_reduction']/100);
				}
			
			if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
				$data['price'] = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
			} else {
				$data['price'] = false;
			}

			if ((float)$product_info['special']) {
				$data['special'] = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
			} else {
				$data['special'] = false;
			}

			if ($this->config->get('config_tax')) {
				$data['tax'] = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price'], $this->session->data['currency']);
			} else {
				$data['tax'] = false;
			}

			$discounts = $this->model_catalog_product->getProductDiscounts($this->request->get['product_id']);
			
			$data['discounts'] = array();
			foreach ($discounts as $discount) {
				$data['discounts'][] = array(
					'product_exemplaire_id' => $discount['product_exemplaire_id'],
					'quantity' => $discount['quantity'],
					'price'    => $this->currency->format($this->tax->calculate($discount['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'])
				);
			}

			$data['options'] = array();
            
			foreach ($this->model_catalog_product->getProductOptions($this->request->get['product_id']) as $option) {
			
				$product_option_value_data = array();

				foreach ($option['product_option_value'] as $option_value) {
					if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
						if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
							
							$price = $this->currency->format($this->tax->calculate($option_value['price'], $product_info['tax_class_id'], $this->config->get('config_tax') ? 'P' : false), $this->session->data['currency']);
							$price_square = false;
						}
						else if((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price_square']){
							$price_square = $this->currency->format($this->tax->calculate($option_value['price_square'], $product_info['tax_class_id'], $this->config->get('config_tax') ? 'P' : false), $this->session->data['currency']);
							$price = false;
						}
						else {
							$price = false;
							$price_square = false;
						}
                        //DEV103 Livraison
                        if($option['is_livraison'])
                        {
                            $tzZone = $this->model_catalog_product->getZoneLivraison($this->request->get['product_id'], $option_value['product_option_value_id']) ;
                        }
                        //DEV103 Livraison
						$product_option_value_data[] = array(
							'product_option_value_id' => $option_value['product_option_value_id'],
							'option_value_id'         => $option_value['option_value_id'],
							'name'                    => $option_value['name'],
							'image'                   => $this->model_tool_image->resize($option_value['image'], 50, 50),
							'price'                   => $price,
							'price_square'            => $price_square,
							'price_prefix'            => $option_value['price_prefix']
						);
					}
				}

				$data['options'][] = array(
					'product_option_id'    => $option['product_option_id'],
					'product_option_value' => $product_option_value_data,
					'option_id'            => $option['option_id'],
					'name'                 => $option['name'],
					'type'                 => $option['type'],
					'value'                => $option['value'],
					'required'             => $option['required']
				);
			}

			if ($product_info['minimum']) {
				$data['minimum'] = $product_info['minimum'];
			} else {
				$data['minimum'] = 1;
			}

			$data['review_status'] = $this->config->get('config_review_status');

			if ($this->config->get('config_review_guest') || $this->customer->isLogged()) {
				$data['review_guest'] = true;
			} else {
				$data['review_guest'] = false;
			}

			if ($this->customer->isLogged()) {
				$data['customer_name'] = $this->customer->getFirstName() . '&nbsp;' . $this->customer->getLastName();
			} else {
				$data['customer_name'] = '';
			}
			
			$data['reviews'] = sprintf($this->language->get('text_reviews'), (int)$product_info['reviews']);
			$data['rating'] = (int)$product_info['rating'];

			// Captcha
			if ($this->config->get($this->config->get('config_captcha') . '_status') && in_array('review', (array)$this->config->get('config_captcha_page'))) {
				$data['captcha'] = $this->load->controller('extension/captcha/' . $this->config->get('config_captcha'));
			} else {
				$data['captcha'] = '';
			}

			$data['share'] = $this->url->link('product/product', 'product_id=' . (int)$this->request->get['product_id']);

			$data['attribute_groups'] = $this->model_catalog_product->getProductAttributes($this->request->get['product_id']);

			$data['products'] = array();

            /*DEV101 : prix 1 oeillet + prix unitaire*/
            $oeillets1 = $this->model_catalog_product->getPriceOptionStaticProduit($this->request->get['product_id'],'218');
            //$toOeillets = $this->model_catalog_product->getProduct('584') ;
            $toTendeurs = $this->model_catalog_product->getProduct('585') ;
			$toProduct = $this->model_catalog_product->getProduct($this->request->get['product_id']) ;
		/*echo '<pre>';
		print_r($toTendeurs);
		echo '</pre>';
		exit;
			
		*/	$model = $this->model_catalog_product->getProductModel($this->request->get['product_id']);
			$Depose = $this->model_catalog_product->getPriceOption('Dépose');
			$Bat = $this->model_catalog_product->getPriceOption('Envoi du BAT');
			$Pose = $this->model_catalog_product->getPriceOption('Pose');
			$Oeillet = $this->model_catalog_product->getPriceOption('Oeillet');
			$Soudure = $this->model_catalog_product->getPriceOption('Soudure');
			$data['prix_bat'] = $Bat['price'];
			$data['prix_soudure']  = $Soudure['price'];
			$data['prix_pose']  = $Pose['price'];
			$data['prix_depose']  = $Depose['price'];
 			$data['marge_soudure'] = 10;
            $data['prix_unitaire_oeillet'] = $Oeillet['price'];
            $data['prix_unitaire_tendeur'] = $toTendeurs['price'] ;
            $data['prix_maitre_carre']     = $toProduct['prix_metre_carre'] ;
            

            
            /*DEV101 : prix 1 oeillet + prix unitaire*/
            
			$results = $this->model_catalog_product->getProductRelated($this->request->get['product_id']);
			$related = array();
			foreach ($results as $result) {
				$related[] = $result;
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_related_width'), $this->config->get($this->config->get('config_theme') . '_image_related_height'));
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_related_width'), $this->config->get($this->config->get('config_theme') . '_image_related_height'));
				}
				
				if($result['easycredit'] == 1){
					$result['price'] -= ($result['price']*$result['easycredit_reduction']/100);
					//echo $result['price'].'-'.($result['price']*$result['easycredit_reduction']/100).'='.$price;exit;
				}
				
				if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$price = false;
				}

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = (int)$result['rating'];
				} else {
					$rating = false;
				}
				$data['products'][] = array(
					'product_id'  => $result['product_id'],
					'thumb'       => $image,
					'name'        => $result['name'],
					'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
					'price'       => $price,
					'special'     => $special,
					'tax'         => $tax,
					'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
					'rating'      => $rating,
					'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id'])
				);
			}
			
			//continuer ajouter panier
			/*$result = '';
			$data['products_propose'] = array();
			// get from products related
			if(!empty($related)){
				$result = $related[rand(0,count($related)-1)];
			}
				
            // sinon get from product affinity
			else if($this->customer->getProductAffinity()!=''){
				$affinities = explode(",",$this->customer->getProductAffinity());
				$products = array();
				foreach($affinities as $affinity){
					$categories = $this->model_catalog_category->getCategories_by_product_affinity($affinity);
					foreach($categories as $category){
						$products = $this->model_catalog_category->getAllProducts($category['category_id']);
					}
				}
				
				if(!empty($products)){
					$result = $products[rand(0,count($products)-1)];
				}
			}
            //$result = $this->model_catalog_product->getProductPropose($this->request->get['product_id'])[0];

			// foreach ($results as $result) {
			if($result != '') {
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_related_width'), $this->config->get($this->config->get('config_theme') . '_image_related_height'));
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_related_width'), $this->config->get($this->config->get('config_theme') . '_image_related_height'));
				}
				
				
				if($result['easycredit'] == 1){
					$result['price'] -= ($result['price']*$result['easycredit_reduction']/100);
					//echo $result['price'].'-'.($result['price']*$result['easycredit_reduction']/100).'='.$price;exit;
				}
				
				if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$price = false;
				}

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = (int)$result['rating'];
				} else {
					$rating = false;
				}

				$data['products_propose'][] = array(
					'product_id'  => $result['product_id'],
					'thumb'       => $image,
					'name'        => $result['name'],
					'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
					'price'       => $price,
					'special'     => $special,
					'tax'         => $tax,
					'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
					'rating'      => $rating,
					'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id'])
				);
			}
			// }*/
            //continuer ajouter panier
			
			$data['tags'] = array();

			if ($product_info['tag']) {
				$tags = explode(',', $product_info['tag']);

				foreach ($tags as $tag) {
					$data['tags'][] = array(
						'tag'  => trim($tag),
						'href' => $this->url->link('product/search', 'tag=' . trim($tag))
					);
				}
			}
			$data['recurrings'] = $this->model_catalog_product->getProfiles($this->request->get['product_id']);
			$this->model_catalog_product->updateViewed($this->request->get['product_id']);
			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');
			$data['model'] = $product_info['model'];
			$data['continue'] = $this->url->link('common/home');
			$data['checkout'] = $this->url->link('checkout/cart');
			$temp = str_word_count($product_info['name'], 1, 'âàáãç3');
			//$data['temp'] = $temp;
			if($temp[0] === 'Banderole' || $temp[0] === 'Bache' || $temp[0] === 'Bâche'){
				$this->response->setOutput($this->load->view('product/product_mesure', $data));
			}else{
				$this->response->setOutput($this->load->view('product/product', $data));
			}
		} else {
			$url = '';

			if (isset($this->request->get['path'])) {
				$url .= '&path=' . $this->request->get['path'];
			}

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['manufacturer_id'])) {
				$url .= '&manufacturer_id=' . $this->request->get['manufacturer_id'];
			}

			if (isset($this->request->get['search'])) {
				$url .= '&search=' . $this->request->get['search'];
			}

			if (isset($this->request->get['tag'])) {
				$url .= '&tag=' . $this->request->get['tag'];
			}

			if (isset($this->request->get['description'])) {
				$url .= '&description=' . $this->request->get['description'];
			}

			if (isset($this->request->get['category_id'])) {
				$url .= '&category_id=' . $this->request->get['category_id'];
			}

			if (isset($this->request->get['sub_category'])) {
				$url .= '&sub_category=' . $this->request->get['sub_category'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_error'),
				'href' => $this->url->link('product/product', $url . '&product_id=' . $product_id)
			);

			$this->document->setTitle($this->language->get('text_error'));

			$data['heading_title'] = $this->language->get('text_error');

			$data['text_error'] = $this->language->get('text_error');

			$data['button_continue'] = $this->language->get('button_continue');

			$data['continue'] = $this->url->link('common/home');

			$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('error/not_found', $data));
		}
	}

	public function review() {
		$this->load->language('product/product');

		$this->load->model('catalog/review');

		$data['text_no_reviews'] = $this->language->get('text_no_reviews');

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$data['reviews'] = array();

		$review_total = $this->model_catalog_review->getTotalReviewsByProductId($this->request->get['product_id']);

		$results = $this->model_catalog_review->getReviewsByProductId($this->request->get['product_id'], ($page - 1) * 5, 5);

		foreach ($results as $result) {
			$data['reviews'][] = array(
				'author'     => $result['author'],
				'text'       => nl2br($result['text']),
				'rating'     => (int)$result['rating'],
				'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added']))
			);
		}

		$pagination = new Pagination();
		$pagination->total = $review_total;
		$pagination->page = $page;
		$pagination->limit = 5;
		$pagination->url = $this->url->link('product/product/review', 'product_id=' . $this->request->get['product_id'] . '&page={page}');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($review_total) ? (($page - 1) * 5) + 1 : 0, ((($page - 1) * 5) > ($review_total - 5)) ? $review_total : ((($page - 1) * 5) + 5), $review_total, ceil($review_total / 5));

		$this->response->setOutput($this->load->view('product/review', $data));
	}

	public function write() {
		$this->load->language('product/product');

		$json = array();

		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 25)) {
				$json['error'] = $this->language->get('error_name');
			}

			if ((utf8_strlen($this->request->post['text']) < 25) || (utf8_strlen($this->request->post['text']) > 1000)) {
				$json['error'] = $this->language->get('error_text');
			}

			if (empty($this->request->post['rating']) || $this->request->post['rating'] < 0 || $this->request->post['rating'] > 5) {
				$json['error'] = $this->language->get('error_rating');
			}

			// Captcha
			if ($this->config->get($this->config->get('config_captcha') . '_status') && in_array('review', (array)$this->config->get('config_captcha_page'))) {
				$captcha = $this->load->controller('extension/captcha/' . $this->config->get('config_captcha') . '/validate');

				if ($captcha) {
					$json['error'] = $captcha;
				}
			}

			if (!isset($json['error'])) {
				$this->load->model('catalog/review');

				$this->model_catalog_review->addReview($this->request->get['product_id'], $this->request->post);

				$json['success'] = $this->language->get('text_success');
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function getRecurringDescription() {
		$this->load->language('product/product');
		$this->load->model('catalog/product');

		if (isset($this->request->post['product_id'])) {
			$product_id = $this->request->post['product_id'];
		} else {
			$product_id = 0;
		}

		if (isset($this->request->post['recurring_id'])) {
			$recurring_id = $this->request->post['recurring_id'];
		} else {
			$recurring_id = 0;
		}

		if (isset($this->request->post['quantity'])) {
			$quantity = $this->request->post['quantity'];
		} else {
			$quantity = 1;
		}

		$product_info = $this->model_catalog_product->getProduct($product_id);
		$recurring_info = $this->model_catalog_product->getProfile($product_id, $recurring_id);

		$json = array();

		if ($product_info && $recurring_info) {
			if (!$json) {
				$frequencies = array(
					'day'        => $this->language->get('text_day'),
					'week'       => $this->language->get('text_week'),
					'semi_month' => $this->language->get('text_semi_month'),
					'month'      => $this->language->get('text_month'),
					'year'       => $this->language->get('text_year'),
				);

				if ($recurring_info['trial_status'] == 1) {
					$price = $this->currency->format($this->tax->calculate($recurring_info['trial_price'] * $quantity, $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					$trial_text = sprintf($this->language->get('text_trial_description'), $price, $recurring_info['trial_cycle'], $frequencies[$recurring_info['trial_frequency']], $recurring_info['trial_duration']) . ' ';
				} else {
					$trial_text = '';
				}

				$price = $this->currency->format($this->tax->calculate($recurring_info['price'] * $quantity, $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);

				if ($recurring_info['duration']) {
					$text = $trial_text . sprintf($this->language->get('text_payment_description'), $price, $recurring_info['cycle'], $frequencies[$recurring_info['frequency']], $recurring_info['duration']);
				} else {
					$text = $trial_text . sprintf($this->language->get('text_payment_cancel'), $price, $recurring_info['cycle'], $frequencies[$recurring_info['frequency']], $recurring_info['duration']);
				}

				$json['success'] = $text;
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
    //DEV103 changement dynamique d'option
    public function changeOptionDynamique()
    {
        $this->load->model('catalog/product');
        $this->load->model('catalog/exemplaire') ;
        
        $zSerializeOptionId         = $this->request->post['serialize_option_id'] ;
        $iProduitId                 = $this->request->post['product_id'] ;
        $tzSerializeOptionId        = explode('_', $zSerializeOptionId) ;
        $product_info               = $this->model_catalog_product->getProduct($iProduitId);
        /*beging dev101SFM180417 exemplaire*/
        $iExemplaireId               = $this->request->post['idexemplaire'] ;
        $exemplaire 				= null ;
		
		// Forex PVC Expansé //
		if($iProduitId==295){
			$product_info['price']=0;
		}
		// Forex PVC Expansé //
		
        if($iExemplaireId != -1){
        	$exemplaire = $this->model_catalog_exemplaire->findExemplaireById($iExemplaireId);
        	$product_info['price'] = $exemplaire['prix_product_ex'] ;
        	if($exemplaire['activation_remise'] == 1 ){
					$product_info['price'] = $exemplaire['prix_remise'] ;
			}
        }
        /*ending dev101SFM180417 exemplaire*/ 
        $total_prix_chute         = ($this->request->post['total_prix_chute']) ? $this->request->post['total_prix_chute'] : 0 ;
	
        $total_prix_oeillet         = ($this->request->post['total_prix_oeillet'] > 0 ) ? $this->request->post['total_prix_oeillet'] : 0 ;
        $total_prix_tendeur         = ($this->request->post['total_prix_tendeur'] > 0 ) ? $this->request->post['total_prix_tendeur'] : 0 ;
        $total_prix_maitre_carre    = ($this->request->post['total_prix_maitre_carre'] > 0 ) ? $this->request->post['total_prix_maitre_carre'] : 0 ;
        $iPrixHT    = 0 ;
        $iPrixTTC   = 0 ;
        foreach($tzSerializeOptionId as $iOptionId)
        {
            $tzPrixPlus = array(
                'price' => 0, 
                'price_ttc' => 0, 
                'prefix' => '+'
            ) ;
            if($iOptionId)
            {
                $tzPrixPlus = $this->model_catalog_product->getPriceOptionProduit($iProduitId, $iOptionId);
				
                if (!$tzPrixPlus['subtract'] || ($tzPrixPlus['quantity'] > 0)) {
                    if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$tzPrixPlus['price']) {						
                        $price = $this->tax->calculate($tzPrixPlus['price'], $product_info['tax_class_id'], $this->config->get('config_tax') ? 'P' : false);
                    }
					else {
                        $price = false;
                    }
                    $tzPrixPlus = array(
                        'price' => $tzPrixPlus['price'], 
                        'price_ttc' => $price, 
                        'prefix' => $tzPrixPlus['price_prefix']
                    ) ;
                }
            }
            if($tzPrixPlus['prefix'] == '+')
            {
                $iPrixHT    = $iPrixHT + $tzPrixPlus['price'] ;
                $iPrixTTC   = $iPrixTTC + $tzPrixPlus['price_ttc'] ;
            }
            elseif($tzPrixPlus['prefix'] == '-')
            {
                $iPrixHT    = $iPrixHT - $tzPrixPlus['price'] ;
                $iPrixTTC   = $iPrixTTC - $tzPrixPlus['price_ttc'] ;
            }
        }
			
		if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
            $data['price'] = $this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax'));
            $data['price'] = $data['price'] + $iPrixTTC + $total_prix_oeillet + $total_prix_maitre_carre + $total_prix_tendeur  + $total_prix_chute;
        } else {
            $data['price'] = false;
        }


        if ($this->config->get('config_tax')) {
            $data['tax'] = (float)$product_info['special'] ? $product_info['special'] : $product_info['price'] ;
            $data['tax'] = $data['tax'] + $iPrixHT + $total_prix_oeillet + $total_prix_maitre_carre+ $total_prix_tendeur + $total_prix_chute;
        } else {
            $data['tax'] = false;
        }
        echo $this->currency->format($data['tax'] , $this->session->data['currency']) . '_' . $this->currency->format($data['price'], $this->session->data['currency']) ;
    }
    //DEV103 changement dynamique d'option
	
	// get prix metre carre par option
	public function getPriceSquare(){
		$this->load->model('catalog/product');
		$iProduitId = $this->request->post['product_id'] ;
		$iOptionId = $this->request->post['iOptionId'] ;
		
		$tzPrixPlus = $this->model_catalog_product->getPriceOptionProduit($iProduitId, $iOptionId);
		$price = 0;
		if((float)$tzPrixPlus['price_square'] > 0){
			$price = $tzPrixPlus['price_square'];	
		}
		echo $price; 
	}
	
	//get chute avec laize ideal par rapport au mesure
	public function getChute(){
		$listeLaize = explode(";",$this->request->post['listeLaize']) ;
		$large = $this->request->post['large'] ;
		$long = $this->request->post['long'] ;
		$qte = $this->request->post['qte'] ;
		$chute='';
		if(!empty($listeLaize)){			
			if(!($large > $listeLaize[0] && $long < $listeLaize[0])){
				$chute = $this->calculChute($large,$long,$qte,$listeLaize[0]);
			}
			foreach($listeLaize as $laize){
				if(!($large > $laize && $long < $laize)){
					$recup = $this->calculChute($large,$long,$qte,$laize);
					
					if($recup < $chute){				
						$chute = $recup;
					}
				}
			}
		}
		
		echo $chute;
	}
	
	function calculChute($large,$long,$qte,$laize){	
		$chute = 0;
		if($large!='' && $laize!=0){
			$large = floatval($large);
			$long  = floatval($long);
			$qte   = intval($qte);
			$laize = floatval($laize);
			
			$reste = ($large%$laize)* $qte;
			$chute = $laize - $reste;
			//echo "->large:".$large.", laize:".$laize.", reste:".$reste.", qte:".$qte."<br>";
			if($reste > $laize){			
				$reste2 = $reste%$laize;
				$chute = $laize - $reste2;
			}		
		}	
		if($chute==$laize){
			$chute=0;
		}
		//echo ">>>".$laize.">".$chute."<br>";
		$chute = ($chute * $long) / 10000 ;
		return $chute;
	}
	
	function testOptionDynamique(){
		$product_id = $this->request->post['product_id'] ;
		$this->load->model('catalog/product');
		$options = $this->model_catalog_product->getProductOptions($product_id);
		
		$rec = false;
		foreach($options as $option){
			if($option['option_id']==229){ //option_dynamique id
				$rec = true;
				return true;
			}		
		}
		$this->model_catalog_product->addOptionDynamique($product_id);
		return true;
	}
}
