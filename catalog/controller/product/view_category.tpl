<?php echo $header; ?>
<div class="container content_container">
  <?php if($child_id == 0) { ?>
        <div class="row">
            <?php foreach ($categories as $category) { ?>
                <div class="product-layout col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="product-thumb transition">
                        <div class="image"><a href="<?php echo $category['href']; ?>"><img src="<?php echo $category['thumb']; ?>" alt="<?php echo $category['name']; ?>" title="<?php echo $category['name']; ?>" class="img-responsive" /></a></div>
                        <div class="button-group">
                            <button type="button" onclick="document.location='<?php echo $category['href']; ?>';">
                                <span class="hidden-xs hidden-sm hidden-md"><?php echo $category['name']; ?></span>
                            </button>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
  <?php } ?>
  <!-- AFFICHAGE EN TABLEAU DEUXIEME RANG CATEGORIE -->
  <?php if($child_id != 0) { ?>
      <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
            <div class="page-title-category col-sm-12">
                <div class="breadcrumbs">
                     <ul>
                         <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                         <li><a href="<?php echo $breadcrumb['href']; ?>" title="" itemprop="url"><span itemprop="title"><?php echo $breadcrumb['text']; ?></span></a>
                            <span>&gt; </span>
                         </li>
                         <?php } ?>
                     </ul>
                  </div>
                 <h1><?php foreach ($breadcrumbs as $breadcrumb) { echo $breadcrumb['text']."&nbsp;" ; } ?></h1> 
            </div>
          <!--dev101-->
          <div class="cms_block" style="margin-bottom:10px;">
            <div class="experience-big col-sm-12">
                <div class="img_left col-sm-6 col-xs-12">
                    <!--<img src="<?php //echo HTTP_IMAGE; ?>brochure-livre-pas-cher.png" class="image-encart" alt="" height="93" width="93">-->
                    
                    <?php if ($thumb) { ?>
                        <img src="<?php echo $thumb; ?>"  title="<?php echo $heading_title; ?>" class="image-exp-big img-responsive thumbnail"  alt="<?php echo $heading_title; ?>" height="250" width="250" <!--height="100%" width="90%" --> >
                    <?php }
                    else{ ?>
                        <img src="<?php echo $default_img_category; ?>"  title="<?php echo $heading_title; ?>" class="image-exp-big img-responsive thumbnail"  alt="<?php echo $heading_title; ?>" height="250" width="250" <!--height="100%" width="90%" --> >
                    <?php } ?>
                </div>
                <div class="right col-sm-3 col-xs-12">
                    <ul>
                        <li><h3><?php echo $heading_title; ?></h3></li>
                        <?php if ($description) { ?>
                            <?php echo $description; // <li>?>
                        <?php } 
                        else{ ?>
                            <?php echo "Aucune description"; // <li>?>
                      <?php } ?>
                    <li><img src="<?php echo HTTP_IMAGE; ?>vos-options-ci-dessous.png" style="float:right;margin-right:40px;" alt="Option produit brochures A6"></li>
                    </ul>
                </div>
            </div>
            
            <!--tableau-->
            <?php if ( isset($toCategoriesNiveau1) && $toCategoriesNiveau1 != array() ) { ?>
            <div class="category-inpage-navigation">
                <div class="cat-content">
                    <div class="cat-listing">
                        <div class="nav-header">
                            <div class="nav-header-left">
                                <div class="nav-header-right">
                                    <span class="products-header-delimeter">&nbsp;&nbsp;&nbsp;</span>
                                    Notre gamme Brochure Livre                            </div>
                            </div>
                        </div>
                        <div class="nav-main-content">
                            <div class="nav-main-content-left-corner">
                                <div class="nav-main-content-right-corner">
                                    <ul>
                                        <?php foreach( $toCategoriesNiveau1 as $oCategoriesNiveau1 ) { 
                                            if($oCategoriesNiveau1['category_id'] == $child_id) {
                                                echo '<li class="current" style="height:37px;"><a href="'.$oCategoriesNiveau1['href'].'" class="current">'.$oCategoriesNiveau1['name'].' </a></li>' ;
                                                echo '<li class="separator current" style="height:37px;"></li>' ;
                                            }
                                            else{
                                                echo '<li class="" style="height:37px;"><a href="'.$oCategoriesNiveau1['href'].'">'.$oCategoriesNiveau1['name'].' </a></li>' ;
                                                echo '<li class="separator" style="height:37px;"></li>' ;
                                            }
                                            
                                        } ?>
                                    </ul>
                                    <div style="clear: both;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
            <?php if ( isset($toCategoriesNiveau2) && $toCategoriesNiveau2 != array() ) { ?>
            <div class="category-inpage-navigation">
                <div class="cat-content">
                    <div class="cat-listing">
                        <div class="nav-header">
                            <div class="nav-header-left">
                                <div class="nav-header-right">
                                    <span class="products-header-delimeter">&nbsp;&nbsp;&nbsp;</span>
                                    Notre gamme Brochure Livre                            </div>
                            </div>
                        </div>
                        <div class="nav-main-content">
                            <div class="nav-main-content-left-corner">
                                <div class="nav-main-content-right-corner">
                                    <ul>
                                        <?php foreach( $toCategoriesNiveau2 as $oCategoriesNiveau2 ) { 
                                            if($oCategoriesNiveau2['category_id'] == $child_id_1) {
                                                echo '<li class="current" style="height:37px;"><a href="'.$oCategoriesNiveau2['href'].'" class="current">'.$oCategoriesNiveau2['name'].' </a></li>' ;
                                                echo '<li class="separator current" style="height:37px;"></li>' ;
                                            }
                                            else{
                                                echo '<li class="" style="height:37px;"><a href="'.$oCategoriesNiveau2['href'].'">'.$oCategoriesNiveau2['name'].' </a></li>' ;
                                                echo '<li class="separator" style="height:37px;"></li>' ;
                                            }
                                            
                                        } ?>
                                    </ul>
                                    <div style="clear: both;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
            <?php if ( isset($toCategoriesNiveau3) && $toCategoriesNiveau3 != array() || $child_id_2 != 0 ) { 
                $parts = explode('_', (string)$toCategoriesNiveau3[0]['href']);
                $display = " " ;
                if (isset($parts[3])) {
                    $display = ( $parts[3] == 0 ) ? "style='display:none'" : " " ;
                }
            ?>
            <div class="category-inpage-navigation last" <?php echo $display ;?>>
                <div class="cat-content">
                    <div class="cat-listing">
                        <div class="nav-header">
                            <div class="nav-header-left">
                                <div class="nav-header-right">
                                    <span class="products-header-delimeter">&nbsp;&nbsp;&nbsp;</span>
                                    Notre gamme Brochure Livre                            </div>
                            </div>
                        </div>
                        <div class="nav-main-content">
                            <div class="nav-main-content-left-corner">
                                <div class="nav-main-content-right-corner">
                                    <ul>
                                        <?php foreach( $toCategoriesNiveau3 as $oCategoriesNiveau3 ) { 
                                            if($oCategoriesNiveau3['category_id'] == $child_id_2) {
                                                echo '<li class="current" style="height:37px;"><a href="'.$oCategoriesNiveau3['href'].'" class="current">'.$oCategoriesNiveau3['name'].' </a></li>' ;
                                                echo '<li class="separator current" style="height:37px;"></li>' ;
                                            }
                                            else{
                                                echo '<li class="" style="height:37px;"><a href="'.$oCategoriesNiveau3['href'].'">'.$oCategoriesNiveau3['name'].' </a></li>' ;
                                                echo '<li class="separator" style="height:37px;"></li>' ;
                                            }
                                            
                                        } ?>
                                    </ul>
                                    <div style="clear: both;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
            <!--tableau-->
            
            <div class="clear:both;"></div>
          </div>
          <!--dev101-->
                 <?php if ($products) { ?>
                 <div class="category-products">
                        <h3 class="step-choose">Votre produit</h3>
                        <div class="tva-switcher" style="float:right;margin-right:5px;">
                            <script type="text/javascript">
                                    $(document).ready(function(){
                                        $('.tvaswitch').change(function(){
                                            var value = $(this).val();
                                            if(value== 1){
                                                $('.price-including-tax').hide();
                                                $('.price-excluding-tax').fadeIn();

                                            }else if(value == 2){
                                                $('.price-excluding-tax').hide();
                                                $('.price-including-tax').fadeIn();
                                            }
                                        });
                                        $("input[name=tvaswitch][value=1]").attr('checked', 'checked');
                                    });
                            </script>

                            <label>Prix Ht</label>
                            <input name="tvaswitch" class="tvaswitch" value="1" type="radio">
                            <label>Prix TTC</label>
                            <input name="tvaswitch" class="tvaswitch" value="2" type="radio">
                        </div>
                        <div class="products-containe">
                                <ol class="products-list" id="products-list">
                                        <?php foreach ($products as $product) { ?>
                                            <li class="item">
                                                <div class="product-shop">
                                                    <div class="product-name">
                                                        <a href="<?php echo $product['href']; ?>" title="<?php echo $product['name']; ?>">
                                                            <?php echo $product['name']; ?>
                                                        </a>
                                                        <?php if ($product['price']) { ?>
                                                        <div class="price-box">
                                                            <?php if ($product['tax']) { ?>
                                                                 
                                                                   <span class="price-excluding-tax" style="display: block;">
                                                                        <span class="price" ><?php echo $product['tax']; ?></span><span class="label">HT</span>
                                                                   </span>
                                                            <?php } ?>
                                                             <?php if (!$product['special']) { ?>
                                                                <span class="price-including-tax" style="display: none;">
                                                                    <span class="label" style="display:inline-block;"></span>
                                                                    <span class="price" > <?php echo $product['price']; ?></span><span class="label">TTC</span>
                                                                </span>
                                                              <?php } else { ?>
                                                                <span class="price-including-tax" style="display: none;">
                                                                    <span class="label" style="display:inline-block;"></span>
                                                                    <span class="price" > <?php echo $product['special']; ?></span><span class="label">TTC</span>
                                                                </span>
                                                              <?php } ?>
                                                        <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        <?php }?> 
                                  </ol>
                                  <div class="list-produit-end"></div>
                                  <div class="list-category-description">
                                       <h2>Impression personnalisée de brochures A6 20 pages </h2>
                                        <p><b>Le livre 100% personnalisable</b> imprimé sur papier 135g est le support de communication indispensable à la réalisation de catalogue produit, carnet de voyage, livret sponsor, bulletin municipal... Réalisez votre support de communication en haute définition sur papier 135g couché brillant ou demi-mat (au choix), pour promouvoir votre activité, votre entreprise ou vos solutions.</p>
                                        <p>Impression offset quadri sur brochure format : A6 fermé (10,5 x 15 cm) // A5 ouvert (15 x 21 cm) sens de lecture en portrait (à la Française). Livraison gratuite partout en France hors Corse en J+6 ouvrés. Possibilité de livrer en J+4.</p>
                                  </div>
                            </div>
                    </div>
                    <?php } 
                   else { ?>
                   <p><?php echo $text_empty; ?></p>
                   <?php }?> 
          
        </div>
        <?php echo $column_right; ?>
        </div>
    <?php } ?>
  <!-- AFFICHAGE EN TABLEAU DEUXIEME RANG CATEGORIE -->
  <?php echo $content_bottom; ?>
</div>

<?php echo $footer; ?>
