<?php
class ControllerProductCategory extends Controller {
	
	public function index() {
		$this->load->language('product/category');

		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		$this->load->model('account/customer');

		//TVA 
		if ($this->customer->isLogged()) {
			$data['TVA'] = $this->model_account_customer->getCustomerByTVA($this->customer->getId());
		}
		else{
			$data['TVA'] = 1;
		}

        //DEV 101 config chemin img
        if ($this->request->server['HTTPS']) {
			$server = $this->config->get('config_ssl');
		} else {
			$server = $this->config->get('config_url');
		}
        $data['default_img_category'] = $server . 'image/default_category.jpg';
        //DEV 101 config chemin img
        
		if (isset($this->request->get['filter'])) {
			$filter = $this->request->get['filter'];
		} else {
			$filter = '';
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'p.sort_order';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		if (isset($this->request->get['limit'])) {
			$limit = (int)$this->request->get['limit'];
		} else {
			$limit = $this->config->get($this->config->get('config_theme') . '_product_limit');
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

        $id_category_dossier = 0 ;
        
		if (isset($this->request->get['path'])) {
			
			
			
			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$path = '';

			$parts = explode('_', (string)$this->request->get['path']);
			
			$category_id = (int)array_pop($parts);

			foreach ($parts as $path_id) {
			
				if (!$path) {
					$path = (int)$path_id;
				} else {
					$path .= '_' . (int)$path_id;
				}

				$category_info = $this->model_catalog_category->getCategory($path_id);
				
				if ($category_info) {
				
					// changer le style des pages carte de visite , flyers , depliant plaquette 
					if( $path_id == 70 || $path_id == 73 || $path_id == 75 || $path_id == 81  )   // carte visite //stickers
					{
						
						$data['breadcrumbs'][] = array(
							'text' => $category_info['name'],
							'href' => $this->url->link('product/category', 'path=' . $path . $url."&st=1&me=1")
						);
					}
					else
					{
						$data['breadcrumbs'][] = array(
							'text' => $category_info['name'],
							'href' => $this->url->link('product/category', 'path=' . $path . $url)
						);						
					}				
				}				
			}
            $id_category_dossier = $category_id ;
		} else {
			$category_id = 0;
            $id_category_dossier = $path_id ;
		}
        /*dev101*/
        $url = '';

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}
        
        if (isset($this->request->get['path'])) {
			$parts = explode('_', (string)$this->request->get['path']);
		} else {
			$parts = array();
		}

		if (isset($parts[0])) {
			$data['category_id'] = $parts[0];
		} else {
			$data['category_id'] = 0;
		}
        
        if (isset($parts[1])) {
			$data['child_id'] = $parts[1];
		} else {
			$data['child_id'] = 0;
		}
        
        if (isset($parts[2])) {
			$data['child_id_1'] = $parts[2];
		} else {
			$data['child_id_1'] = 0;
		}
        
        if (isset($parts[3])) {
			$data['child_id_2'] = $parts[3];
		} else {
			$data['child_id_2'] = 0;
		}
        
        if (isset($parts[4])) {
			$data['child_id_3'] = $parts[4];
		} else {
			$data['child_id_3'] = 0;
		}
        
        if (isset($parts[5])) {
			$data['child_id_4'] = $parts[5];
		} else {
			$data['child_id_4'] = 0;
		}
        
        if (isset($parts[6])) {
			$data['child_id_5'] = $parts[6];
		} else {
			$data['child_id_5'] = 0;
		}
        
        if (isset($parts[7])) {
			$data['child_id_6'] = $parts[7];
		} else {
			$data['child_id_6'] = 0;
		}
        
        if (isset($parts[8])) {
			$data['child_id_7'] = $parts[8];
		} else {
			$data['child_id_7'] = 0;
		}
        
        /*DEV101:AFFICHAGE DOSSIER*/
        $toDossier          = $this->model_catalog_category->getDossierByIdCategory($id_category_dossier);
		
        $affichageDossier = 0 ;
		
		$liste_categorie_fille = $this->model_catalog_category->loadIndispensable($id_category_dossier);
		
        if(isset($toDossier) && $toDossier != null){
            
            foreach($toDossier as $keyDossier => $oDossier){
                if($oDossier['dossier_serialize_item_id'] != ''){
                    $category_info_dossier[$keyDossier]['nom_dossier'] = $oDossier['dossier_nom'] ;
                    $toIdDossier = explode("_",$oDossier['dossier_serialize_item_id']) ;
                    foreach($toIdDossier as $key => $idDossier){
                        $detailDossier = $this->model_catalog_category->getCategory($idDossier);
                        $serializeParent = '' ;
                        if($idDossier!=''){
                            $serializeParent = $this->model_catalog_category->getSerializeParent($idDossier);
                        }
						if(!empty($detailDossier)){
							$detailDossier['href'] = $this->url->link('product/category', 'path='. $serializeParent) ;
							$category_info_dossier[$keyDossier]['fils'][$key]        = $detailDossier ;
						}						
                    }
                    $affichageDossier++ ;
					
                }
            }
            
            if($affichageDossier > 0)
            {$data['category_info_dossier'] = $category_info_dossier ;}
        }
        
        $data['affichageDossier']      = $affichageDossier ;
        
        /*DEV101:AFFICHAGE DOSSIER*/
        
        //NIVEAU 1
        $toCategoriesNiveau1 = $this->model_catalog_category->getCategories($data['category_id']);
        $data['toCategoriesNiveau1'] = array() ;
        foreach ($toCategoriesNiveau1 as $oCategoriesNiveau1) {
				$data['toCategoriesNiveau1'][] = array(
                    'category_id' => $oCategoriesNiveau1['category_id'] ,
					'name' => $oCategoriesNiveau1['name'] ,
					'href' => $this->url->link('product/category', 'path='.$data['category_id'] .'_'. $oCategoriesNiveau1['category_id'] . $url)
				);
                
		}
        
        //NIVEAU 2
        $toCategoriesNiveau2 = $this->model_catalog_category->getCategories($data['child_id']);
        $data['toCategoriesNiveau2'] = array() ;
        foreach ($toCategoriesNiveau2 as $oCategoriesNiveau2) {
				$data['toCategoriesNiveau2'][] = array(
                    'category_id' => $oCategoriesNiveau2['category_id'] ,
					'name' => $oCategoriesNiveau2['name'] ,
					'href' => $this->url->link('product/category', 'path='.$data['category_id'] .'_'. $data['child_id'] .'_'. $oCategoriesNiveau2['category_id'] . $url)
				);
                if($data['category_id'] == 0 || $data['child_id'] == 0){
                    unset($data['toCategoriesNiveau2']) ;
                }
		}
        
        //NIVEAU 3
        $toCategoriesNiveau3 = $this->model_catalog_category->getCategories($data['child_id_1']);
        $data['toCategoriesNiveau3'] = array() ;
        foreach ($toCategoriesNiveau3 as $oCategoriesNiveau3) {
				$data['toCategoriesNiveau3'][] = array(
                    'category_id' => $oCategoriesNiveau3['category_id'] ,
					'name' => $oCategoriesNiveau3['name'] ,
					'href' => $this->url->link('product/category', 'path='.$data['category_id'] .'_'. $data['child_id'] .'_'. $data['child_id_1'] .'_'. $oCategoriesNiveau3['category_id'] . $url)
				);
                if($data['category_id'] == 0 || $data['child_id'] == 0 || $data['child_id_1'] == 0){
                    unset($data['toCategoriesNiveau3']) ;
                }
		}
        
        //NIVEAU 4
        $toCategoriesNiveau4 = $this->model_catalog_category->getCategories($data['child_id_2']);
        $data['toCategoriesNiveau4'] = array() ;
        foreach ($toCategoriesNiveau4 as $oCategoriesNiveau4) {
				$data['toCategoriesNiveau4'][] = array(
                    'category_id' => $oCategoriesNiveau4['category_id'] ,
					'name' => $oCategoriesNiveau4['name'] ,
					'href' => $this->url->link('product/category', 'path='.$data['category_id'] .'_'. $data['child_id'] .'_'. $data['child_id_1'] .'_'. $data['child_id_2'] .'_'. $oCategoriesNiveau4['category_id'] . $url)
				);
                if($data['category_id'] == 0 || $data['child_id'] == 0 || $data['child_id_1'] == 0 || $data['child_id_2'] == 0){
                    unset($data['toCategoriesNiveau4']) ;
                }
		}
        
        //NIVEAU 5
        $toCategoriesNiveau5 = $this->model_catalog_category->getCategories($data['child_id_3']);
        
        $data['toCategoriesNiveau5'] = array() ;
        foreach ($toCategoriesNiveau5 as $oCategoriesNiveau5) {
				$data['toCategoriesNiveau5'][] = array(
                    'category_id' => $oCategoriesNiveau5['category_id'] ,
					'name' => $oCategoriesNiveau5['name'] ,
					'href' => $this->url->link('product/category', 'path='.$data['category_id'] .'_'. $data['child_id'] .'_'. $data['child_id_1'] .'_'. $data['child_id_2'] .'_'. $data['child_id_3'] .'_'. $oCategoriesNiveau5['category_id'] . $url)
				);
                if($data['category_id'] == 0 || $data['child_id'] == 0 || $data['child_id_1'] == 0 || $data['child_id_2'] == 0 || $data['child_id_3'] == 0){
                    unset($data['toCategoriesNiveau5']) ;
                }
		}
        
        //NIVEAU 6
        $toCategoriesNiveau6 = $this->model_catalog_category->getCategories($data['child_id_4']);
        $data['toCategoriesNiveau6'] = array() ;
        foreach ($toCategoriesNiveau6 as $oCategoriesNiveau6) {
				$data['toCategoriesNiveau6'][] = array(
                    'category_id' => $oCategoriesNiveau6['category_id'] ,
					'name' => $oCategoriesNiveau6['name'] ,
					'href' => $this->url->link('product/category', 'path='.$data['category_id'] .'_'. $data['child_id'] .'_'. $data['child_id_1'] .'_'. $data['child_id_2'] .'_'. $data['child_id_3'] .'_'. $data['child_id_4'] .'_'. $oCategoriesNiveau6['category_id'] . $url)
				);
                if($data['category_id'] == 0 || $data['child_id'] == 0 || $data['child_id_1'] == 0 || $data['child_id_2'] == 0 || $data['child_id_3'] == 0 || $data['child_id_4'] == 0){
                    unset($data['toCategoriesNiveau6']) ;
                }
		}
        
        //NIVEAU 7
        $toCategoriesNiveau7 = $this->model_catalog_category->getCategories($data['child_id_5']);
        $data['toCategoriesNiveau7'] = array() ;
        foreach ($toCategoriesNiveau7 as $oCategoriesNiveau7) {
				$data['toCategoriesNiveau7'][] = array(
                    'category_id' => $oCategoriesNiveau7['category_id'] ,
					'name' => $oCategoriesNiveau7['name'] ,
					'href' => $this->url->link('product/category', 'path='.$data['category_id'] .'_'. $data['child_id'] .'_'. $data['child_id_1'] .'_'. $data['child_id_2'] .'_'. $data['child_id_3'] .'_'. $data['child_id_4'] .'_'. $data['child_id_5'] .'_'. $oCategoriesNiveau7['category_id'] . $url)
				);
                if($data['category_id'] == 0 || $data['child_id'] == 0 || $data['child_id_1'] == 0 || $data['child_id_2'] == 0 || $data['child_id_3'] == 0 || $data['child_id_4'] == 0 || $data['child_id_5'] == 0){
                    unset($data['toCategoriesNiveau7']) ;
                }
		}
        
        //NIVEAU 8
        $toCategoriesNiveau8 = $this->model_catalog_category->getCategories($data['child_id_6']);
        $data['toCategoriesNiveau8'] = array() ;
        foreach ($toCategoriesNiveau8 as $oCategoriesNiveau8) {
				$data['toCategoriesNiveau8'][] = array(
                    'category_id' => $oCategoriesNiveau8['category_id'] ,
					'name' => $oCategoriesNiveau8['name'] ,
					'href' => $this->url->link('product/category', 'path='.$data['category_id'] .'_'. $data['child_id'] .'_'. $data['child_id_1'] .'_'. $data['child_id_2'] .'_'. $data['child_id_3'] .'_'. $data['child_id_4'] .'_'. $data['child_id_5'] .'_'. $data['child_id_6'] .'_'. $oCategoriesNiveau8['category_id'] . $url)
				);
                if($data['category_id'] == 0 || $data['child_id'] == 0 || $data['child_id_1'] == 0 || $data['child_id_2'] == 0 || $data['child_id_3'] == 0 || $data['child_id_4'] == 0 || $data['child_id_5'] == 0 || $data['child_id_6'] == 0){
                    unset($data['toCategoriesNiveau8']) ;
                }
		}
        
        /*dev101*/
        
		$category_info = $this->model_catalog_category->getCategory($category_id);

		if ($category_info) {
			$this->document->setTitle($category_info['meta_title']);
			$this->document->setDescription($category_info['meta_description']);
			$this->document->setKeywords($category_info['meta_keyword']);

			$data['heading_title'] = $category_info['name'];

			$data['text_refine'] = $this->language->get('text_refine');
			$data['text_empty'] = $this->language->get('text_empty');
			$data['text_quantity'] = $this->language->get('text_quantity');
			$data['text_manufacturer'] = $this->language->get('text_manufacturer');
			$data['text_model'] = $this->language->get('text_model');
			$data['text_price'] = $this->language->get('text_price');
			$data['text_tax'] = $this->language->get('text_tax');
			$data['text_points'] = $this->language->get('text_points');
			$data['text_compare'] = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));
			$data['text_sort'] = $this->language->get('text_sort');
			$data['text_limit'] = $this->language->get('text_limit');

			$data['button_cart'] = $this->language->get('button_cart');
			$data['button_wishlist'] = $this->language->get('button_wishlist');
			$data['button_compare'] = $this->language->get('button_compare');
			$data['button_continue'] = $this->language->get('button_continue');
			$data['button_list'] = $this->language->get('button_list');
			$data['button_grid'] = $this->language->get('button_grid');

			
			if( $category_info['category_id'] == 70 || $category_info['category_id'] == 73 || $category_info['category_id'] == 75 || $category_info['category_id'] == 81)   // carte visite //stickers
					{
										
							$data['breadcrumbs'][] = array(
								'text' => $category_info['name'],
								'href' => $this->url->link('product/category', 'path=' . $this->request->get['path']."&st=1&me=1")
							);
								
					}
					else
					{
						
						
						$data['breadcrumbs'][] = array(
								'text' => $category_info['name'],
								'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'])
							);		
					}
					/*echo '<pre>';print_r($data['breadcrumbs']);echo '</pre>';exit;
					
			// Set the last category breadcrumb
			$data['breadcrumbs'][] = array(
				'text' => $category_info['name'],
				'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'])
			);
			*/

			if ($category_info['image']) {
				$data['thumb'] = $this->model_tool_image->resize($category_info['image'], $this->config->get($this->config->get('config_theme') . '_image_category_width'), $this->config->get($this->config->get('config_theme') . '_image_category_height'));
			} else {
				$data['thumb'] = '';
			}

			$data['description'] = html_entity_decode($category_info['description'], ENT_QUOTES, 'UTF-8');
			$data['compare'] = $this->url->link('product/compare');
			$data['affichage_tableau'] = $category_info['affichage_tableau'];

			$url = '';

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['categories'] = array();

			$results = $this->model_catalog_category->getCategories($category_id);

            
        
			foreach ($results as $result) {
                if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
				}
                
				$filter_data = array(
					'filter_category_id'  => $result['category_id'],
					'filter_sub_category' => true
				);
				
				$prix_minimum = $this->model_catalog_category->calculPrixMin($result['category_id']) ;
							
				$filles = $this->model_catalog_category->getCategories($result['category_id']);
				
				$prix_minimum = $prix_minimum!=0 ? $this->currency->format($prix_minimum, $this->session->data['currency']) : '' ;
				 
                $data['categories'][] = array(
					'category_id' => $result['category_id'] ,
					'name' => $result['name'] ,
					'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '_' . $result['category_id'] . $url),
                    'thumb'       	=> $image,
                    'prix_minimum' 	=> $prix_minimum,
					'filles' => $filles
				);
			}
			
			$zListeCategoriesMeres =  $data['categories'] ;
			
			$data['categories_bas'] = $zListeCategoriesMeres ;
			
					
			$data['products'] = array();

			$filter_data = array(
				'filter_category_id' => $category_id,
				'filter_filter'      => $filter,
				'sort'               => $sort,
				'order'              => $order,
				'start'              => ($page - 1) * $limit,
				'limit'              => $limit
			);

			$product_total = $this->model_catalog_product->getTotalProducts($filter_data);

			$results = $this->model_catalog_product->getProducts($filter_data);

			foreach ($results as $result) {
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
				}

				if($result['easycredit'] == 1){
					$result['price'] -= ($result['price']*$result['easycredit_reduction']/100);
				}
				
				if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$price = false;
				}

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = (int)$result['rating'];
				} else {
					$rating = false;
				}
				/*ending dev101SFM180417 exemplaire*/
				//recuperation de l'exemplaire de chaque produit
				$this->load->model("catalog/exemplaire") ;

				$exemplaires  			= $this->model_catalog_exemplaire->getExemplaireByProduct($result['product_id']);
				//echo '<pre>';print_r($result);echo '</pre>';exit;
				    
				if (empty($exemplaires)) {
					$data['products'][] = array(
						'product_id'  => $result['product_id'],
						'thumb'       => $image,
						'name'        => $result['name'],
						'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
						'price'       => $price,
						'special'     => $special,
						'tax'         => $tax,
						'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
						'rating'      => $result['rating'],
						'recommander' => 0,
						'prix_remise' => 0,
						'activation_remise'=> false,
						'href'        => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id'] . $url)
					);
				}
				else if(!empty($exemplaires)){
					asort ($exemplaires) ;
					foreach ($exemplaires as $exemplaire) {
						$price_remise = null ;
						if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
							$price_ex = $this->currency->format($this->tax->calculate($exemplaire['prix_product_ex'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
							if($exemplaire['activation_remise'] == 1){
								$price_remise = $this->currency->format($this->tax->calculate($exemplaire['prix_remise'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
								$price_ex     = $price_remise ;
								$price_remise = $this->currency->format($this->tax->calculate($exemplaire['prix_product_ex'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
							}
						} else {
							$price_ex = false;
							$price_remise 	= false;
						}
						if ($this->config->get('config_tax')) {
							$tax_ex = $this->currency->format((float)$result['special'] ? $result['special'] : $exemplaire['prix_product_ex'], $this->session->data['currency']);
							if($exemplaire['activation_remise'] == 1){
								$tax_ex     = $this->currency->format((float)$result['special'] ? $result['special'] : $exemplaire['prix_remise'], $this->session->data['currency']); ;
							}
						} else {
							$tax_ex = false;
						}
						
						
						// prendre is_recommande dans product description 
						
						
						
						
						//fin prendre is_recommande dans product description 
						
						$data['products'][] = array( 
							'product_id'  => $result['product_id'],
							'thumb'       => $image,
							'name'        => $result['name']." ".$exemplaire['nombre_ex']." ex",
							'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
							'price'       => $price_ex,
							'special'     => $special,
							'tax'         => $tax_ex,
							'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
							'rating'      => $result['rating'],
							'recommander'=> $exemplaire['recommander'],
							'prix_remise' => $price_remise,
							'activation_remise'=> $exemplaire['activation_remise'],
							'href'        => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id'] .',&idexemplaire='.$exemplaire['product_exemplaire_id']. $url),
							'texterecommandation' => $exemplaire['texterecommandation']
						);
					}
				}
				/*ending dev101SFM180417 exemplaire*/
			}

			$url = '';

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['sorts'] = array();

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_default'),
				'value' => 'p.sort_order-ASC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.sort_order&order=ASC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_name_asc'),
				'value' => 'pd.name-ASC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=pd.name&order=ASC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_name_desc'),
				'value' => 'pd.name-DESC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=pd.name&order=DESC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_price_asc'),
				'value' => 'p.price-ASC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.price&order=ASC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_price_desc'),
				'value' => 'p.price-DESC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.price&order=DESC' . $url)
			);

			if ($this->config->get('config_review_status')) {
				$data['sorts'][] = array(
					'text'  => $this->language->get('text_rating_desc'),
					'value' => 'rating-DESC',
					'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=rating&order=DESC' . $url)
				);

				$data['sorts'][] = array(
					'text'  => $this->language->get('text_rating_asc'),
					'value' => 'rating-ASC',
					'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=rating&order=ASC' . $url)
				);
			}

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_model_asc'),
				'value' => 'p.model-ASC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.model&order=ASC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_model_desc'),
				'value' => 'p.model-DESC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.model&order=DESC' . $url)
			);

			$url = '';

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			$data['limits'] = array();

			$limits = array_unique(array($this->config->get($this->config->get('config_theme') . '_product_limit'), 25, 50, 75, 100));

			sort($limits);

			foreach($limits as $value) {
				$data['limits'][] = array(
					'text'  => $value,
					'value' => $value,
					'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url . '&limit=' . $value)
				);
			}

			$url = '';

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$pagination = new Pagination();
			$pagination->total = $product_total;
			$pagination->page = $page;
			$pagination->limit = $limit;
			$pagination->url = $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url . '&page={page}');

			$data['pagination'] = $pagination->render();

			$data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($product_total - $limit)) ? $product_total : ((($page - 1) * $limit) + $limit), $product_total, ceil($product_total / $limit));

			// http://googlewebmastercentral.blogspot.com/2011/09/pagination-with-relnext-and-relprev.html
			if ($page == 1) {
			    $this->document->addLink($this->url->link('product/category', 'path=' . $category_info['category_id'], true), 'canonical');
			} elseif ($page == 2) {
			    $this->document->addLink($this->url->link('product/category', 'path=' . $category_info['category_id'], true), 'prev');
			} else {
			    $this->document->addLink($this->url->link('product/category', 'path=' . $category_info['category_id'] . '&page='. ($page - 1), true), 'prev');
			}

			if ($limit && ceil($product_total / $limit) > $page) {
			    $this->document->addLink($this->url->link('product/category', 'path=' . $category_info['category_id'] . '&page='. ($page + 1), true), 'next');
			}

			/*beging dev101SFM180417 exemplaire*/
			$server = "" ;
			if ($this->request->server['HTTPS']) {
				$server = $this->config->get('config_ssl');
			} else {
				$server = $this->config->get('config_url');
			}
			$data['text_recommandation'] 	= $this->language->get('text_recommandation') ;
			$data['img_recommander'] 		= $server.'image/cache/recommande.jpg' ;
			/*ending dev101SFM180417 exemplaire*/

			$data['sort'] = $sort;
			$data['order'] = $order;
			$data['limit'] = $limit;

			$data['continue'] = $this->url->link('common/home');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');
		
			// redirect si produit sur mesure
			
			$this->load->model('catalog/product');
			
			if(  $data['affichage_tableau'] == 0 )
			{
				if( !isset($data['categories_bas'] ) || $data['categories_bas'] == null )
				{
					if( $data['products'] )
					{
						if( count( $data['products'] ) > 0 )
						{
							$liste = $data['products'];
							$temp = $liste[0];
							$result_produit  =   $this->model_catalog_product->getProduct( $temp['product_id'] );
							if( $result_produit["is_surmesure"] == 1 || $result_produit["easycredit"] == 1 )
							{
								$this->response->redirect($this->url->link('product/product&path=' . $this->request->get['path'] .'&product_id='.$temp['product_id'], '', true));
					
							}
						}
					}
				}
			}	
						
			// prendre l'image , le lien , le texte 
// echo '<pre>';			
			// var_dump( $category_info ) ;
			// echo '</pre>';
			// exit ;
			if( $category_info['image_menu'] != null && $category_info['image_menu'] != '' )
			{
				$data['image_menu'] 		= $category_info['image_menu'] ;	
			}
			
			
			if( $category_info['lien_image_menu'] != null && $category_info['lien_image_menu'] != '' )
			{
				$data['lien_image_menu'] 		= $category_info['lien_image_menu'] ;	
			}
			
			// prendre l'image , le lien , le texte  
			
			// fin redirect si produit sur mesure 
			// forcer le changement de style si isset  $_GET['st']
			if( isset( $_GET['st'] ) )
				$data['style'] = 2 ;
		
			$_GET['me'] = 1 ;
			
			if( isset( $_GET['me'] ) && ( $_GET['path'] == 70 || $_GET['path'] == 73 || $_GET['path'] == 620 || $_GET['path'] == 618 || $_GET['path'] == 690 || $_GET['path'] == 76 || $_GET['path'] == 75 || $_GET['path'] == 77 || $_GET['path'] == 67 || $_GET['path'] == 78 || $_GET['path'] == 79 || $_GET['path'] == 80 || $_GET['path'] == 81 || $_GET['path'] == 83 || $_GET['path'] == 84   )   ) 
			{
				$this->response->setOutput($this->load->view('common/accueil_menu/accueil_carte_visite', $data));
			}	
			else
			{
				$this->response->setOutput($this->load->view('product/category', $data));	
			}
		
		} else {
			$url = '';

			if (isset($this->request->get['path'])) {
				$url .= '&path=' . $this->request->get['path'];
			}

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_error'),
				'href' => $this->url->link('product/category', $url)
			);

			$this->document->setTitle($this->language->get('text_error'));

			$data['heading_title'] = $this->language->get('text_error');

			$data['text_error'] = $this->language->get('text_error');

			$data['button_continue'] = $this->language->get('button_continue');

			$data['continue'] = $this->url->link('common/home');

			$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('error/not_found', $data));
		}
	}
}
