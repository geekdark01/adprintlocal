﻿<?php
// catalog/controller/ajax/index.php
class ControllerAjaxIndex extends Controller {
	
  public function index() {
   
    //  $this->load->language('ajax/index');
    // $this->load->model('catalog/product');
     
    //$this->document->setTitle($this->language->get('heading_title'));
     
    // load all products
    //$products = $this->model_catalog_product->getProducts();
    $data['test'] = "test";
     
	
    $this->response->setOutput($this->load->view('ajax/index', $data));
	
  }
  
  
    public function rechercher() {
   
    //  $this->load->language('ajax/index');
    $this->load->model('catalog/category');
     
    //$this->document->setTitle($this->language->get('heading_title'));
    
	if( strlen($_POST['mot']) > 0 )
	{
			
		// load all products
		$categories = $this->model_catalog_category->getCategories_by_name( $_POST['mot'] );
		
	    
		if( count($categories) > 0  )
		{
			$data['categories'] = $categories ;
		}
		else
		{
			$categories = $this->model_catalog_category->getCategories_by_name_interieur( $_POST['mot'] );
			if( count($categories) > 0  )
			{
				$data['categories'] = $categories ;
			}
			else{
					$data['categories'] = "" ;
			}

		
		}
	
	}
	else{
		
		$data['categories'] = "" ;
	
	}
		
		
	$data['mot'] = 	$_POST['mot'] ;

	
    $this->response->setOutput($this->load->view('ajax/resultat', $data));
	
  }
  
  
	public function rechercher_produit() 
	{
    
	
		//  $this->load->language('ajax/index');
		$this->load->model('catalog/product');
		 
		//$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/category'); 	
		
		// envoyer le categorie cliqué 
		
		$id_categorie =  $_POST['id_categorie'] ;
		
		$categorie = $this->model_catalog_category->getCategory( $id_categorie );
		
		$this->load->model('tool/image');
		if( isset( $categorie['image'] ) &&  strlen( $categorie['image'] ) > 5 )
		{
				$categorie['thumb'] = $this->model_tool_image->resize( $categorie['image'] , '70', '70');
		}
		
		$data['categorie'] = $categorie ;
		
		
		
		
		
		// envoyer les categories filles
		$zoSous_categories =  $this->model_catalog_category->getCategories($id_categorie);
		
		
		
		foreach( $zoSous_categories as &$cat ) 
		{
			
			if ( strlen($cat['image']) > 5 ) {
					$cat['thumb'] = $this->model_tool_image->resize($cat['image'], '70', '70');
				} 
			
		}
		
		
		if( count($zoSous_categories) > 3 )
		{
			$tab_indice =  array_rand($zoSous_categories , 3 );
			$data['categories'] = array( $zoSous_categories[$tab_indice[0]] ,$zoSous_categories[$tab_indice[1]] ,$zoSous_categories[$tab_indice[2]]  );
		}   		
		else
			$data['categories'] =  $zoSous_categories;
		
		
		
		//var_dump( $data['categories'] );
		
		
		
		// avant 
		
		/*
		$filtre = array( 'filter_category_id' => $id_categorie , 'filter_sub_category' => true ) ;
		$result =   $this->model_catalog_product->getProducts($filtre);
		if( count($result) > 4 )
		{
			$tab_indice =  array_rand($result , 4 );
			$data['produits'] = array( $result[$tab_indice[0]] ,$result[$tab_indice[1]] ,$result[$tab_indice[2]] ,$result[$tab_indice[3]]  );
			
		}		
		else
			$data['produits'] =  $result;
		
		
		
		$temp_array = $data['produits'] ; 
		
		foreach( $temp_array as &$product_info )
		{
			
			if ($product_info['image']) {
					$product_info['thumb'] = $this->model_tool_image->resize($product_info['image'], '70', '70');
				} 
			
		}
		
		$data['produits'] = $temp_array ;
		*/
		
		
		
		
		$this->response->setOutput($this->load->view('ajax/resultat_produit', $data));
	
	}
  
  
  
}