<?php
class ControllerExtensionTotalAddprintcredit extends Controller {
	public function index() {
		$this->load->language('extension/total/addprintcredit');

		$this->load->model('addprintcard/addprintcard') ;
		$data['heading_title'] 			= $this->language->get('heading_title');
		
		

		if(isset($this->session->data['customer_id'])){
			
			$customer_id = (int)$this->session->data['customer_id'] ;

			
			$compte = $this->model_addprintcard_addprintcard->verifiedCompte($customer_id) ;
			if($compte == null){
				$data['error_compte_null'] 	= $this->language->get('error_compte_null');
			}
			else{
				$data['btn_addprintcredit'] 	= $this->language->get('btn_addprintcredit');
				$data['text_addprintcredit'] 	= $this->language->get('text_addprintcredit');
				$data['text_solde'] 			= $this->language->get('text_solde');
				$data['entry_montant'] 			= $this->language->get('entry_montant');
				$data['entry_all'] 			 	= $this->language->get('entry_all');
				$data['error_input_montant'] 	= $this->language->get('error_input_montant');
				$data['solde']		   			= $this->model_addprintcard_addprintcard->findSolde($customer_id) ;
			}
			
		}
		else{

			$data['text_solde_non_connecter'] 	= $this->language->get('text_solde_non_connecter');
			$data['text_connexion'] 			= $this->language->get('text_connexion');
			$data['connexion'] 					= $this->url->link('account/login') ;


		}
		$data['disable_checkbox'] = isset($this->session->data['addprintcredit']) || isset($this->session->data['coupon']);
		return $this->load->view('extension/total/addprintcredit', $data);
	}
	public function verifiedCompte($id_customer){
		$compte = $this->model_addprintcard_addprintcard->verifiedCompte($id_customer) ;
		if($compte == null){
			return 	$this->language->get('montant_vide') ;
		}
		else{
			return 1 ;
		}
		
	}
	public function utiliserPartie() {
		$this->load->language('extension/total/addprintcredit');
		$customer_id = (int)$this->session->data['customer_id'] ;

		$json = array();

		$this->load->model('addprintcard/addprintcard');
		
		$solde 				= $this->model_addprintcard_addprintcard->findSolde($customer_id);

		$montantAPaye 		= $this->request->post['montant'] ;
		if($solde < $montantAPaye){
			$json['error'] = $this->language->get('error_montant_insuffisant') ;
		}
		
		else{
			if($montantAPaye > $this->session->data['totalsForAddprintCredit']){
				$montantAPaye = $this->session->data['totalsForAddprintCredit'] ;
			}
			
			$this->session->data['addprintcredit'] =  $montantAPaye;
			$json['bon'] = $this->url->link('checkout/cart') ;
		}
		unset($this->session->data['totalsForAddprintCredit']) ;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
		
	}
	public function killSession(){
		if(isset($this->session->data['addprintcredit'])){
			$this->load->model('addprintcard/addprintcard');
			$this->load->model('checkout/order') ;
			$customer_id = (int)$this->session->data['customer_id'] ;
			$this->model_addprintcard_addprintcard->paymentCard($customer_id, $this->session->data['addprintcredit']) ;
			unset($this->session->data['addprintcredit']) ;
			unset($this->session->data['totalsForAddprintCredit']) ;
		}
		$json['exception'] = "passe dans addprintcredit" ;
		$this->response->addHeader('Content-Type: application/json');
	/* 	echo '<pre>';
		print_r(json_encode($json));
		echo '</pre>'; */
		$this->response->setOutput(json_encode($json));
	}
	public function utiliserTous(){
		$this->load->language('extension/total/addprintcredit');
		$customer_id = (int)$this->session->data['customer_id'] ;

		$json = array();
		$this->load->model('addprintcard/addprintcard');
		
		$solde 				= $this->model_addprintcard_addprintcard->findSolde($customer_id);
		//$montantAPayer 		= 0 ;
		if($solde <= 0){
			$json['error'] = $this->language->get('error_solde_0') ;
		}
		if(isset($this->session->data['totalsForAddprintCredit'])){
			if($solde > $this->session->data['totalsForAddprintCredit']){
				$this->session->data['addprintcredit'] = $this->session->data['totalsForAddprintCredit'];
				$json['bon'] = $this->url->link('checkout/cart') ;

			}
			if($solde < $this->session->data['totalsForAddprintCredit']){
				$this->session->data['addprintcredit'] = $solde;
				$json['bon'] = $this->url->link('checkout/cart') ;
			}
			unset($this->session->data['totalsForAddprintCredit']) ;
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
