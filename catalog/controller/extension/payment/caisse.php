<?php	
class ControllerExtensionPaymentcaisse extends Controller {
		
		
		
	public function index() {
		
		$this->load->language('extension/payment/caisse');
		
		$data['text_instruction'] = $this->language->get('text_instruction');
		/*
		$data['text_payable'] = $this->language->get('text_payable');
		$data['text_address'] = $this->language->get('text_address');
		$data['text_payment'] = $this->language->get('text_payment');
		$data['text_loading'] = $this->language->get('text_loading');
		*/
		
		$data['button_confirm'] = $this->language->get('button_confirm');
		$data['devis'] 			= isset($this->session->data['devis']);
		
		//$data['payable'] = $this->config->get('cheque_payable');
		//$data['address'] = nl2br($this->config->get('config_address'));
		$data['continue'] = $this->url->link('checkout/success');
		
		return $this->load->view('extension/payment/caisse', $data);
		
	}	
		
	
	public function confirm() {
				
				//	echo "debut";
		
		if ($this->session->data['payment_method']['code'] == 'caisse') {
			
			//		echo "debut 2";
			$this->load->language('extension/payment/caisse');
			
			$this->load->model('checkout/order');
			
					
			$this->session->data['order_id'] = $this->model_checkout_order->addOrder($this->session->data['order_data']);
			
			$comment = "Le payement a été effectué à la caisse , votre commande : <b> ".$this->session->data['order_id'] ."</b> sera traité dès que possible.";
				
			$customer_id = (int)$this->session->data['customer_id'] ;
			
			if(isset($this->session->data['devis'])){
				$this->model_checkout_order->toDevis($this->session->data['order_id']);
			}else{
				$this->model_checkout_order->addOrderHistory( $this->session->data['order_id'] , 1 , $comment, true );
			
				if(isset($this->session->data['addprintcredit'])){
					$this->load->model('addprintcard/addprintcard');
					
					$this->model_addprintcard_addprintcard->paymentCard($customer_id, $this->session->data['addprintcredit']) ;
					unset($this->session->data['addprintcredit']) ;
					unset($this->session->data['totalsForAddprintCredit']) ;
				} 
			}
			
			$this->load->model('account/customer');
			$nb_order = $this->model_account_customer->getCountOrders($customer_id);
			
			if ( $nb_order == 1 ) { // premiere commande qui vient d'etre inseree
				$order = $this->model_checkout_order->getOrder($this->session->data['order_id']);
				
				$this->load->model('checkout/parrainage');
				
				$parrains = $this->model_checkout_parrainage->getParrains($customer_id);
				
				foreach($parrains as $parrain){
					$montant = ( floatval($parrain['pourc']) * floatval($order['total']) )/100;
					$this->model_checkout_parrainage->insertCompteCredit($parrain['parrain_id'],$montant);
					$this->model_checkout_parrainage->sendMailParrain($parrain['parrain_id'],$parrain['pourc']);
				}
				
				$this->model_checkout_parrainage->insertCompteCredit($customer_id,10);
			}
		}
	}
	
	
	
}