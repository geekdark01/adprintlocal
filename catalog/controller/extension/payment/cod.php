<?php
class ControllerExtensionPaymentCod extends Controller {
	public function index() {
		$data['button_confirm'] = $this->language->get('button_confirm');

		$data['text_loading'] = $this->language->get('text_loading');

		$data['continue'] = $this->url->link('checkout/success');

		return $this->load->view('extension/payment/cod', $data);
	}

	public function confirm() {
				
				//	echo "debut";
		
		if ($this->session->data['payment_method']['code'] == 'cod') {
			
			//		echo "debut 2";
			$this->load->language('extension/payment/cod');
			
			$this->load->model('checkout/order');
			
			$comment = "Payement par cod : ".$_GET['ref'];
						
			$this->session->data['order_id'] = $this->model_checkout_order->addOrder($this->session->data['order_data']);
			
				
			$customer_id = (int)$this->session->data['customer_id'] ;
			
			if(isset($this->session->data['devis'])){
				$this->model_checkout_order->toDevis($this->session->data['order_id']);
			}else{
				$this->model_checkout_order->addOrderHistory( $this->session->data['order_id'] , 1 , $comment, true );
			
				if(isset($this->session->data['addprintcredit'])){
					$this->load->model('addprintcard/addprintcard');
					
					$this->model_addprintcard_addprintcard->paymentCard($customer_id, $this->session->data['addprintcredit']) ;
					unset($this->session->data['addprintcredit']) ;
					unset($this->session->data['totalsForAddprintCredit']) ;
				} 
			}
			
			$this->load->model('account/customer');
			$nb_order = $this->model_account_customer->getCountOrders($customer_id);
			
			if ( $nb_order == 1 ) { // premiere commande qui vient d'etre inseree
				$order = $this->model_checkout_order->getOrder($this->session->data['order_id']);
				
				$this->load->model('checkout/parrainage');
				
				$parrains = $this->model_checkout_parrainage->getParrains($customer_id);
				
				foreach($parrains as $parrain){
					$montant = ( floatval($parrain['pourc']) * floatval($order['total']) )/100;
					$this->model_checkout_parrainage->insertCompteCredit($parrain['parrain_id'],$montant);
					$this->model_checkout_parrainage->sendMailParrain($parrain['parrain_id'],$parrain['pourc']);
				}
				
				$this->model_checkout_parrainage->insertCompteCredit($customer_id,10);
			}
		}
	}
	
	
	
}
