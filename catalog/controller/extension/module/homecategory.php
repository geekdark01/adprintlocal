<?php
class ControllerExtensionModuleHomecategory extends Controller {
	public function index($setting) {
		$this->load->language('extension/module/homecategory');

		$data['heading_title'] = $this->language->get('heading_title');

		
        $this->load->model('catalog/category');

        $this->load->model('tool/image');

		$data['categories'] = array();

		$data['categories'] = $this->model_catalog_category->getCategories(0);

        foreach($data['categories']  as $iKey=>$category){			
            if ($category['image']) {
				//$data['categories'][$iKey]['thumb'] = $this->model_tool_image->resize($category['image'], $this->config->get($this->config->get('config_theme') . '_image_category_width'), $this->config->get($this->config->get('config_theme') . '_image_category_height'));
				$data['categories'][$iKey]['thumb'] = $this->model_tool_image->resize($category['image'], $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
			} else {
				//$data['categories'][$iKey]['thumb'] = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_category_width'), $this->config->get($this->config->get('config_theme') . '_image_category_height'));
				$data['categories'][$iKey]['thumb'] = $this->model_tool_image->resize('placeholder.png',$this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
			}
            $data['categories'][$iKey]['href']      = $this->url->link('product/category', 'path=' . $category['category_id']) ;
        }

		return $this->load->view('extension/module/homecategory', $data);
	}
}