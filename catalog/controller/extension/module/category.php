<?php
class ControllerExtensionModuleCategory extends Controller {
	public function index() {
		$this->load->language('extension/module/category');

		$data['heading_title'] = $this->language->get('heading_title');

		if (isset($this->request->get['path'])) {
			$parts = explode('_', (string)$this->request->get['path']);
		} else {
			$parts = array();
		}

		if (isset($parts[0])) {
			$data['category_id'] = $parts[0];
		} else {
			$data['category_id'] = 0;
		}

		if (isset($parts[1])) {
			$data['child_id'] = $parts[1];
		} else {
			$data['child_id'] = 0;
		}

		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$data['categories'] = array();

		$categories = $this->model_catalog_category->getCategories($data['category_id']);

		//B1
        foreach ($categories as $category) {
			$children_data_b1 = array();

            $children = $this->model_catalog_category->getCategories($category['category_id']);

            //B2
            foreach($children as $child) {
                
                $children_data_b2 = array();
                $children_b2 = $this->model_catalog_category->getCategories($child['category_id']);
                
                //B3
                foreach($children_b2 as $child_2) {
                
                    $children_data_b3 = array();
                    $children_b3 = $this->model_catalog_category->getCategories($child_2['category_id']);
                    
                    //B4
                    foreach($children_b3 as $child_3) {
                
                        $children_data_b4 = array();
                        $children_b4 = $this->model_catalog_category->getCategories($child_3['category_id']);

                        //B5
                        foreach($children_b4 as $child_4) {
                    
                            $children_data_b5 = array();
                            $children_b5 = $this->model_catalog_category->getCategories($child_4['category_id']);
                            
                            $filter_data = array('filter_category_id' => $child_4['category_id'], 'filter_sub_category' => true);

                            //B6
                            foreach($children_b5 as $child_5) {
                    
                                $children_data_b6 = array();
                                $children_b6 = $this->model_catalog_category->getCategories($child_5['category_id']);
                                
                                $filter_data = array('filter_category_id' => $child_5['category_id'], 'filter_sub_category' => true);

                                //B7
                                foreach($children_b6 as $child_6) {
                        
                                    $children_data_b7 = array();
                                    $children_b7 = $this->model_catalog_category->getCategories($child_6['category_id']);
                                    
                                    $filter_data = array('filter_category_id' => $child_6['category_id'], 'filter_sub_category' => true);

                                    //B8
                                    foreach($children_b7 as $child_7) {
                            
                                        $children_data_b8 = array();
                                        $children_b8 = $this->model_catalog_category->getCategories($child_7['category_id']);
                                        
                                        $filter_data = array('filter_category_id' => $child_7['category_id'], 'filter_sub_category' => true);

                                        $children_data_b7[] = array(
                                            'category_id' => $child_7['category_id'],
                                            'name' => $child_7['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
                                            'href' => $this->url->link('product/category', 'path=' . $data['category_id'] . '_' . $category['category_id'] .'_'. $child['category_id'] . '_' . $child_2['category_id']. '_' . $child_3['category_id'] .'_'. $child_4['category_id'] .'_'. $child_5['category_id']. '_' . $child_6['category_id']. '_' . $child_7['category_id'])
                                        );
                                    }
                                    
                                    $children_data_b6[] = array(
                                        'category_id' => $child_6['category_id'],
                                        'name' => $child_6['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
                                        'children'    => $children_data_b7,
                                        'href' => $this->url->link('product/category', 'path=' . $data['category_id'] . '_' . $category['category_id'] .'_'. $child['category_id'] . '_' . $child_2['category_id']. '_' . $child_3['category_id'] .'_'. $child_4['category_id'] .'_'. $child_5['category_id']. '_' . $child_6['category_id'])
                                    );
                                }
                                
                                $children_data_b5[] = array(
                                    'category_id' => $child_5['category_id'],
                                    'name' => $child_5['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
                                    'children'    => $children_data_b6,
                                    'href' => $this->url->link('product/category', 'path=' . $data['category_id'] . '_' . $category['category_id'] .'_'. $child['category_id'] . '_' . $child_2['category_id']. '_' . $child_3['category_id'] .'_'. $child_4['category_id'] .'_'. $child_5['category_id'])
                                );
                            }
                            
                            $children_data_b4[] = array(
                                'category_id' => $child_4['category_id'],
                                'name' => $child_4['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
                                'children'    => $children_data_b5,
                                'href' => $this->url->link('product/category', 'path=' . $data['category_id'] . '_' . $category['category_id'] .'_'. $child['category_id'] . '_' . $child_2['category_id']. '_' . $child_3['category_id'] .'_'. $child_4['category_id'])
                            );
                        }
                        
                        $filter_data = array('filter_category_id' => $child_3['category_id'], 'filter_sub_category' => true);

                        $children_data_b3[] = array(
                            'category_id' => $child_3['category_id'],
                            'name' => $child_3['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
                            'children'    => $children_data_b4,
                            'href' => $this->url->link('product/category', 'path=' . $data['category_id'] . '_' . $category['category_id'] .'_'. $child['category_id'] . '_' . $child_2['category_id']. '_' . $child_3['category_id'])
                        );
                    }
                    
                    $filter_data = array('filter_category_id' => $child_2['category_id'], 'filter_sub_category' => true);

                    $children_data_b2[] = array(
                        'category_id' => $child_2['category_id'],
                        'name' => $child_2['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
                        'children'    => $children_data_b3,
                        'href' => $this->url->link('product/category', 'path=' . $data['category_id'] . '_' . $category['category_id'] .'_'. $child['category_id'] . '_' . $child_2['category_id'])
                    );
                }
                
                $filter_data = array('filter_category_id' => $child['category_id'], 'filter_sub_category' => true);

                $children_data_b1[] = array(
                    'category_id' => $child['category_id'],
                    'name' => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
                    'children'    => $children_data_b2,
                    'href' => $this->url->link('product/category', 'path=' . $data['category_id'] . '_' . $category['category_id'] .'_'. $child['category_id'])
                );
            }

			$filter_data = array(
				'filter_category_id'  => $category['category_id'],
				'filter_sub_category' => true
			);

			$data['categories'][] = array(
				'category_id' => $category['category_id'],
				'name'        => $category['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
				'children'    => $children_data_b1,
				'href'        => $this->url->link('product/category', 'path=' . $data['category_id'] .'_'. $category['category_id'])
			);
		}

		return $this->load->view('extension/module/category', $data);
	}
}