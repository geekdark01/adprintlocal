﻿<?php
// catalog/controller/ajax/index.php
class ControllerServicemodificationIndex extends Controller { 
	
  public function index() {
   
    //  $this->load->language('ajax/index');
    // $this->load->model('catalog/product');
     
    //$this->document->setTitle($this->language->get('heading_title'));
     
    // load all products
    //$products = $this->model_catalog_product->getProducts();
    $data['test'] = "test" ;
	
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
	
     $this->response->setOutput($this->load->view('servicemodification/index', $data ) );
	
  }
  
  
  public function commencer()
  {
		
		if ($this->request->server['HTTPS']) {
			$data['base'] = $this->config->get('config_ssl');
		} else {
			$data['base'] = $this->config->get('config_url');
		}
		
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
	
     $this->response->setOutput($this->load->view('servicemodification/commencer', $data ) );  
	  
  }
  
  public function sauvermodification()
  {
	  
	$this->cart->add( 551 , 1 );
	
	$texte = " <span style='text-decoration: underline;' > Types de modifications souhaités </span> :  </br> ";
	$texte = $texte." <ul> ";

	// options
	if( isset($_POST['option1']) )
		$texte = $texte." <li> Modification du texte, logo ou arrière-plan </li>  ";
	if( isset($_POST['option2']) )
		$texte = $texte." <li> Modification d'un modèle  </li> ";
	if( isset($_POST['option3']) )
		$texte = $texte." <li> Correction d'image  </li> ";
	if( isset($_POST['option4']) )
		$texte = $texte." <li> Autres modifications  </li> ";

	$texte = $texte." </ul> ";

	$texte = $texte." </br> ";
	
	// texte 
	$texte = $texte." <span style='text-decoration: underline;' > Indications sur les modifications à apporter : </span> </br> ";
	$texte = $texte." ".$_POST['comments']." </br> ";
	$texte = $texte." </br> ";
	// image 
	
	// texte 
	$texte = $texte." <span style='text-decoration: underline;' > Images ou fichiers joints : </span> </br> ";
	foreach( $_POST['images'] as $image  )
	{
		$texte = $texte." <a target='_blank' href='http://adprint-dev.com/image/servicemodification/files/".$image."'> http://adprint-dev.com/image/servicemodification/files/".$image." </a> </br> ";
	}
	
	
	// sauver les données dans la session

	$this->session->data['texte_service_modification'] = $texte ;
	
	// redirection vers page commande
	$this->redirect($this->url->link('checkout/cart'));
	 
	
	  
  }
  
  
  
  /*
   public function rechercher() {
   
    //  $this->load->language('ajax/index');
     $this->load->model('catalog/category');
     
    //$this->document->setTitle($this->language->get('heading_title'));
     
	 
	 
	if( strlen($_POST['mot']) > 0 )
	{
		
			
		// load all products
		$categories = $this->model_catalog_category->getCategories_by_name( $_POST['mot'] );
		$data['categories'] = $categories ;
	}
	else
		$data['categories'] = "" ;
		
	$data['mot'] = 	$_POST['mot'] ;

	
    $this->response->setOutput($this->load->view('ajax/resultat', $data));
	
  }
  
  
    public function rechercher_produit() {
    
    //  $this->load->language('ajax/index');
    $this->load->model('catalog/product');
     
    //$this->document->setTitle($this->language->get('heading_title'));
     
	
	 
	$id_categorie =  $_POST['id_categorie'] ;
		
			
	$filtre = array( 'filter_category_id' => $id_categorie , 'filter_sub_category' => true ) ;
	

	$result =   $this->model_catalog_product->getProducts($filtre);
	if( count($result) > 4 )
	{
		$tab_indice =  array_rand($result , 4 );
		$data['produits'] = array( $result[$tab_indice[0]] ,$result[$tab_indice[1]] ,$result[$tab_indice[2]] ,$result[$tab_indice[3]]  );
		
	}		
	else
		$data['produits'] =  $result;
	
	
	
	$temp_array = $data['produits'] ; 
	
	$this->load->model('tool/image');
	foreach( $temp_array as &$product_info )
	{
		
		if ($product_info['image']) {
				$product_info['thumb'] = $this->model_tool_image->resize($product_info['image'], '70', '70');
			} 
		
	}
	
	$data['produits'] = $temp_array ;
	
	
    $this->response->setOutput($this->load->view('ajax/resultat_produit', $data));
	
	}
  
  */
  
}
