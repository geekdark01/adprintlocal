<?php
class ControllerCheckoutShippingMethod extends Controller {
	public function index() {
		$this->load->language('checkout/checkout');

		if (isset($this->session->data['shipping_address'])) {
			// Shipping Methods
            $this->load->model('checkout/order');
			//DEV103 gestion livraison
            $toShippings = $this->model_checkout_order->getShippingAdprint() ;
            
            //DEV103 gestion livraison
            
			// Shipping Methods
			$method_data = array();
            $iCompteur  = 0 ;
            foreach($toShippings as $toShipping)
            {
                $iCompteur ++ ;
                $quote_data = array();

                $quote_data['flat' . $iCompteur] = array(
                    'code'         => $toShipping['code'],
                    'title'        => $toShipping['title'],
                    'cost'         => $toShipping['cost'],
                    'tax_class_id' => $toShipping['tax_class_id'],
                    'text'         => $toShipping['text']
                );
                
                $method_data_temp = array(
                    'code'       => 'flat' . $iCompteur,
                    'title'      => $toShipping['localisation'],
                    'quote'      => $quote_data,
                    'sort_order' => 1,
                    'error'      => false
                );
                $method_data['flat' . $iCompteur] = array(
                    'title'      => $method_data_temp['title'],
                    'quote'      => $method_data_temp['quote'],
                    'sort_order' => $method_data_temp['sort_order'],
                    'error'      => $method_data_temp['error']
                );
            }
            
            // $quote_data = array();

			// $quote_data['flat1'] = array(
				// 'code'         => 'flat1.flat1',
				// 'title'        => 'text description1',
				// 'cost'         => 'tafita',
				// 'tax_class_id' => 0,
				// 'text'         => 'text fotsiny1'
			// );

			// $method_data1 = array(
				// 'code'       => 'flat1',
				// 'title'      => 'titre eto koa',
				// 'quote'      => $quote_data,
				// 'sort_order' => 1,
				// 'error'      => false
			// );
            
            // $quote_data = array();

			// $quote_data['flat2'] = array(
				// 'code'         => 'flat2.flat2',
				// 'title'        => 'text description2',
				// 'cost'         => 5,
				// 'tax_class_id' => 0,
				// 'text'         => 'text fotsiny2'
			// );

			// $method_data2 = array(
				// 'code'       => 'flat2',
				// 'title'      => 'titre eto koa2',
				// 'quote'      => $quote_data,
				// 'sort_order' => 1,
				// 'error'      => false
			// );
            
            // $method_data['flat1'] = array(
                // 'title'      => $method_data1['title'],
                // 'quote'      => $method_data1['quote'],
                // 'sort_order' => $method_data1['sort_order'],
                // 'error'      => $method_data1['error']
            // );
            
            // $method_data['flat2'] = array(
                // 'title'      => $method_data2['title'],
                // 'quote'      => $method_data2['quote'],
                // 'sort_order' => $method_data2['sort_order'],
                // 'error'      => $method_data2['error']
            // );

			// $this->load->model('extension/extension');

			// $results = $this->model_extension_extension->getExtensions('shipping');

			// foreach ($results as $result) {
				// if ($this->config->get($result['code'] . '_status')) {
					// $this->load->model('extension/shipping/' . $result['code']);

					// $quote = $this->{'model_extension_shipping_' . $result['code']}->getQuote($this->session->data['shipping_address']);

					// if ($quote) {
						// $method_data[$result['code']] = array(
							// 'title'      => $quote['title'],
							// 'quote'      => $quote['quote'],
							// 'sort_order' => $quote['sort_order'],
							// 'error'      => $quote['error']
						// );
					// }
				// }
			// }

			$sort_order = array();

			foreach ($method_data as $key => $value) {
				$sort_order[$key] = $value['sort_order'];
			}

			array_multisort($sort_order, SORT_ASC, $method_data);

			$this->session->data['shipping_methods'] = $method_data;
		}

		$data['text_shipping_method'] = $this->language->get('text_shipping_method');
		$data['text_comments'] = $this->language->get('text_comments');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['button_continue'] = $this->language->get('button_continue');

		if (empty($this->session->data['shipping_methods'])) {
			$data['error_warning'] = sprintf($this->language->get('error_no_shipping'), $this->url->link('information/contact'));
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['shipping_methods'])) {
			$data['shipping_methods'] = $this->session->data['shipping_methods'];
		} else {
			$data['shipping_methods'] = array();
		}

		if (isset($this->session->data['shipping_method']['code'])) {
			$data['code'] = $this->session->data['shipping_method']['code'];
		} else {
			$data['code'] = '';
		}

		if (isset($this->session->data['comment'])) {
			$data['comment'] = $this->session->data['comment'];
		} else {
			$data['comment'] = '';
		}

		$this->response->setOutput($this->load->view('checkout/shipping_method', $data));
	}

	public function save() {
		$this->load->language('checkout/checkout');

		$json = array();

		// Validate if shipping is required. If not the customer should not have reached this page.
		if (!$this->cart->hasShipping()) {
			$json['redirect'] = $this->url->link('checkout/checkout', '', true);
		}

		// Validate if shipping address has been set.
		if (!isset($this->session->data['shipping_address'])) {
			$json['redirect'] = $this->url->link('checkout/checkout', '', true);
		}

		// Validate cart has products and has stock.
		if ((!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout'))) {
			$json['redirect'] = $this->url->link('checkout/cart');
		}

		// Validate minimum quantity requirements.
		$products = $this->cart->getProducts();

		foreach ($products as $product) {
			$product_total = 0;

			foreach ($products as $product_2) {
				if ($product_2['product_id'] == $product['product_id']) {
					$product_total += $product_2['quantity'];
				}
			}

			if ($product['minimum'] > $product_total) {
				$json['redirect'] = $this->url->link('checkout/cart');

				break;
			}
		}

		if (!isset($this->request->post['shipping_method'])) {
			$json['error']['warning'] = $this->language->get('error_shipping');
		} else {
			$shipping = explode('.', $this->request->post['shipping_method']);

			if (!isset($shipping[0]) || !isset($shipping[1]) || !isset($this->session->data['shipping_methods'][$shipping[0]]['quote'][$shipping[1]])) {
				$json['error']['warning'] = $this->language->get('error_shipping');
			}
		}

		if (!$json) {
			$this->session->data['shipping_method'] = $this->session->data['shipping_methods'][$shipping[0]]['quote'][$shipping[1]];

			$this->session->data['comment'] = strip_tags($this->request->post['comment']);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}