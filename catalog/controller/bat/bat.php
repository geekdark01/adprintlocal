<?php
class ControllerBatBat extends Controller {
	public function index() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/account', '', true);

			$this->response->redirect($this->url->link('account/login', '', true));
		}
        $data = array() ;
        

		$this->load->language('bat/bat');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

        $data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', true)
		);
        
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_gestion_fichier'),
			'href' => $this->url->link('bat/bat', '', true)
		);
        
        $this->load->model('account/order');
        $this->load->model('tool/upload');
        $toOrderAccount = $this->model_account_order->getOrders() ;
		
        $data['order_product'] = array() ;
        foreach($toOrderAccount as $oOrderAccount)
        {
            $data['order_product'][$oOrderAccount['order_id']] = $this->model_account_order->getOrderProducts($oOrderAccount['order_id']) ;
            $toContenuOrder = $data['order_product'][$oOrderAccount['order_id']] ;
            foreach($toContenuOrder as $iKey=>$toContenu)
            {
                $iProduitId = $toContenu['product_id'] ;
                $iOrderId   = $oOrderAccount['order_id'] ;
                $toContenuOrder[$iKey]['bat'] = $this->model_account_order->loadBatPerProduct($iOrderId, $iProduitId) ;
                $toContenuOrder[$iKey]['bat_status'] = 3 ;
                foreach($toContenuOrder[$iKey]['bat'] as $iKeyBat=>$toBatPerProduct)
                {
                    $tzFichierBat = $this->model_tool_upload->getUploadByCode($toBatPerProduct['code_fichier']);
                    $toContenuOrder[$iKey]['bat_status']                    = $toBatPerProduct['status'] ;
                    $toContenuOrder[$iKey]['bat'][$iKeyBat]['fichier']      = $tzFichierBat['name'] ;
                    //$toContenuOrder[$iKey]['bat'][$iKeyBat]['url_fichier']  = $this->url->link('tool/upload/download', 'token=' . $this->session->data['token'] . '&code=' . $toBatPerProduct['code_fichier'], true) ;
                    $toContenuOrder[$iKey]['bat'][$iKeyBat]['url_fichier']  = $this->url->link('tool/upload/download', 'code=' . $toBatPerProduct['code_fichier'], true) ;
                    $toContenuOrder[$iKey]['bat'][$iKeyBat]['date_added']   = date($this->language->get('datetime_format'), strtotime($toBatPerProduct['date_added'])) ;
                }
            }
            $data['order_product'][$oOrderAccount['order_id']] = $toContenuOrder ;
        }
        
        
        $data['toOrderAccount'] = $toOrderAccount ;
        
		$data['heading_title']          = $this->language->get('heading_title');
		$data['text_gestion_fichier']   = $this->language->get('text_gestion_fichier');
		$data['text_choix_commande']    = $this->language->get('text_choix_commande');
		$data['text_titre_produit']     = $this->language->get('text_titre_produit');
		$data['text_titre_statut']      = $this->language->get('text_titre_statut');
        $data['text_avis_pao']          = $this->language->get('text_avis_pao');
		$data['text_titre_action']      = $this->language->get('text_titre_action');
		$data['text_loading']           = $this->language->get('text_loading');
        $data['text_date_bat']          = $this->language->get('text_date_bat');
        $data['text_bat_verifier']      = $this->language->get('text_bat_verifier');
        $data['text_valider']           = $this->language->get('text_valider');
        $data['text_refuser']           = $this->language->get('text_refuser');
        $data['text_valide']           = $this->language->get('text_valide');
        $data['text_refuse']           = $this->language->get('text_refuse');

		$data['text_envoyer']           = $this->language->get('text_envoyer');
		$data['text_verouille']           = $this->language->get('text_verouille');
		$data['button_upload']          = $this->language->get('button_upload');
        
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		
		$this->response->setOutput($this->load->view('bat/bat', $data));
	}
    public function actionBat()
    {
        $json = array();
        $this->load->model('bat/bat');
        
        $iOrderId       = $this->request->post['order_id'] ;
        $iProductId     = $this->request->post['product_id'] ;
        $iStatus        = $this->request->post['status'] ;
        
        $this->model_bat_bat->actionBat($iOrderId, $iProductId, $iStatus) ;
    }
}
