<?php
class ControllerGestionfichierGestionfichier extends Controller {
	public function index() {
		
        $_iOrderId = 0 ;
        
        $toRequest  = $this->request->request ;
        
        $zRoute     = $toRequest['route'] ;
        $tzRoute    = explode('/', $zRoute) ;
        $zDerniereColonne = end($tzRoute) ;
        
        if($zDerniereColonne != 'index')
        {
            
            $_iOrderId = intval($zDerniereColonne) ;
        }
        
        

		$this->load->language('gestionfichier/gestionfichier');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_gestion_fichier'),
			'href' => $this->url->link('gestionfichier/gestionfichier', '', true)
		);
        
        $this->load->model('account/order');
        $toOrderAccount = $this->model_account_order->getOrders() ;
        $data['order_product'] = array() ;
        foreach($toOrderAccount as $oOrderAccount)
        {
            $data['order_product'][$oOrderAccount['order_id']] = $this->model_account_order->getOrderProducts($oOrderAccount['order_id']) ;
        }
        
        //echo $_iOrderId . '==><br/>';
        $data['order_id']       = $_iOrderId ;
        $data['toOrderAccount'] = $toOrderAccount ;
        
		$data['heading_title']          = $this->language->get('heading_title');
		$data['text_gestion_fichier']   = $this->language->get('text_gestion_fichier');
		$data['text_choix_commande']    = $this->language->get('text_choix_commande');
		$data['text_titre_produit']     = $this->language->get('text_titre_produit');
		$data['text_titre_statut']      = $this->language->get('text_titre_statut');
       // $data['text_avis_pao']          = $this->language->get('text_avis_pao');
		$data['text_titre_action']      = $this->language->get('text_titre_action');
		$data['text_loading']           = $this->language->get('text_loading');
		$data['text_envoyer']        = $this->language->get('text_envoyer');
		$data['text_envoyé']         = $this->language->get('text_envoyé');
		$data['button_upload']          = $this->language->get('button_upload');
        
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		
		$this->response->setOutput($this->load->view('gestionfichier/gestionfichier', $data));
	}
    public function enregistrementfichier()
    {

		$json = array();
        $this->load->model('gestionfichier/gestionfichier');
        
        $iOrderId       = $this->request->post['order_id'] ;
        $iProductId     = $this->request->post['product_id'] ;
        $zCodeFichier   = $this->request->post['code_fichier'] ;
        
        $this->model_gestionfichier_gestionfichier->enregistrementfichier($iOrderId, $iProductId, $zCodeFichier) ;

		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
    }
	
    public function recuperationFichier()
    {
        $json = array();
        $this->load->model('gestionfichier/gestionfichier');
        
        $zCodeFichier   = $this->request->post['code'] ;
        // echo $zCodeFichier ;
        // exit() ;
        $toFichier = $this->model_gestionfichier_gestionfichier->recuperationFichier($zCodeFichier) ;
        
        $json['name'] = $toFichier[0]['nom'] ;
        echo $json['name'] ;
    }
	
    public function actionFichier()
    {
        $json = array();
        $this->load->model('gestionfichier/gestionfichier');
        
        $iOrderId       = $this->request->post['order_id'] ;
        $iProductId     = $this->request->post['product_id'] ;
        $zAction   = $this->request->post['action'] ;
        
        $this->model_gestionfichier_gestionfichier->actionFichier($iOrderId, $iProductId, $zAction) ;
    }

}
