<?php
class ModelCatalogAffinity extends Model {
	public function getAffinity() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "affinity");
		return $query->rows;
	}
}