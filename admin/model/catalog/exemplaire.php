<?php
class ModelCatalogExemplaire extends Model {
	public function addExemplaire($data) {
		// print_r("INSERT INTO " ."oc_product_exemplaire SET product_id = " . $this->db->escape($data['product_id']) . ", nombre_ex = '" . $this->db->escape($data['nombre_ex']) . "', prix_product_ex = '" . $this->db->escape($data['prix_product_ex']) . "', recommander = " . $this->db->escape($data['recommander']));
		$this->db->query("INSERT INTO " ."oc_product_exemplaire SET product_id = " . $this->db->escape($data['product_id']) . ", nombre_ex = '" . $this->db->escape($data['nombre_ex']) . "', prix_product_ex = '" . $this->db->escape($data['prix_product_ex']) . "', recommander = " . $this->db->escape($data['recommander']). ",activation_remise =".$this->db->escape($data['activation_remise']). ",prix_remise='".$this->db->escape($data['prix_remise'])."'  ,  texterecommandation = '".$this->db->escape($data['texterecommandation'])."'    ");
	}
	public function modifierExemplaire($data) {
		
		$this->db->query("UPDATE " . "oc_product_exemplaire SET nombre_ex = '" . $this->db->escape($data['nombre_ex']) ."', prix_product_ex = '". $this->db->escape($data['prix_product_ex']) . "', recommander = " . $this->db->escape($data['recommander']) . " WHERE product_exemplaire_id = " . $this->db->escape($data['product_exemplaire_id']));
	} 
	public function getExemplaireByProduct($product_id) {
		$query = $this->db->query("SELECT * FROM " . "oc_product_exemplaire WHERE product_id = " . (int)$product_id );

		return $query->rows;
	}
	public function deleteExemplaire($exemplaire_id) {

		$this->db->query("DELETE FROM `" . "oc_product_exemplaire` WHERE product_exemplaire_id = '" . (int)$exemplaire_id. "'");
	}
}