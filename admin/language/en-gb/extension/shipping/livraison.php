<?php
// Heading
$_['heading_title']    = 'Shipping Based zone';

// Text
$_['text_shipping']    = 'Shipping';
$_['text_success']     = 'Success: You have modified shipping based zone!';
$_['text_edit']        = 'Edit Shipping based zone';

// Entry
$_['entry_rate']       = 'Rates';
$_['entry_tax_class']  = 'Tax Class';
$_['entry_geo_zone']   = 'Geo Zone';
$_['entry_status']     = 'Status';
$_['entry_sort_order'] = 'Sort Order';

// Help
$_['help_rate']        = 'Veuillez insérer comme suit: 5:10.00,7:12.00 Correspondant au suivant-> Poids:Prix,Poids:Prix, etc... Chaque Poids Correspond au poids max supporté par un véhicule';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify shipping based zone!';