<?php
// Heading
$_['heading_title']    = 'Home Category';

// Text
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Success: You have modified home category module!';
$_['text_edit']        = 'Edit home category Module';

// Entry
$_['entry_status']     = 'Status';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify category module!';