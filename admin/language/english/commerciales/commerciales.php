<?php
// Heading
$_['heading_title']     	= 'Liste commerciales';

// Text
// Column
$_['error_warning']     	= 'Warning: Please check the form carefully for errors!';
$_['error_permission']  	= 'Warning: You do not have permission to modify headermenu!';
$_['error_title']       	= 'headermenu Title must be between 3 and 64 characters!';
$_['error_description'] 	= 'Description must be more than 3 characters!';
$_['error_account']     	= 'Warning: This headermenu page cannot be deleted as it is currently assigned as the store account terms!';
$_['error_checkout']    	= 'Warning: This headermenu page cannot be deleted as it is currently assigned as the store checkout terms!';
$_['error_affiliate']   	= 'Warning: This headermenu page cannot be deleted as it is currently assigned as the store affiliate terms!';
$_['error_store']       	= 'Warning: This headermenu page cannot be deleted as its currently used by %s stores!';

//Real column title
$_['commerciale_name']      = 'Nom Commerciale';
$_['customer_name']      	= 'Nom Client';
$_['commerciale_mail']      = 'Adresse email';
$_['chiffre']       		= 'Chiffre d’affaire';
$_['details']       	= 'Détails';
$_['nombreclient']			= 'Nombre Clients';
$_['commerciale_details']	= 'Commerciale Détails';
$_['text_commerciale']		= 'Commerciale';
$_['listeclient']			= 'Liste clients';
$_['Action']				= 'Action';
?>