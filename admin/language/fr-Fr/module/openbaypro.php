<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

$_['heading_title']		= 'OpenBay Pro';

$_['text_module']		= 'Modules';
$_['text_installed']	= 'Le module OpenBay Pro est désormais installé. Il est accessible sous Extensions -> OpenBay Pro';
?>