<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// Heading
$_['heading_title']			= 'Bienvenue';

// Text
$_['text_module']			= 'Modules';
$_['text_success']			= 'Félicitations, vous avez modifié le module <b>Bienvenue</b> avec succès !';
$_['text_content_top']		= 'En-tête';
$_['text_content_bottom']	= 'Pied de page';
$_['text_column_left']		= 'Colonne de gauche';
$_['text_column_right']		= 'Colonne de droite';

// Entry
$_['entry_description']		= 'Administrateur de la boutique seulement :';
$_['entry_layout']			= 'Disposition :';
$_['entry_position']		= 'Emplacement :';
$_['entry_status']			= 'État :';
$_['entry_sort_order']		= 'Classement :';

// Error
$_['error_permission']		= 'Attention, vous n’avez pas la permission de modifier le module <b>Bienvenue</b> !';
?>