<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// Heading
$_['heading_title']			= 'Google Talk';

// Text
$_['text_module']			= 'Modules';
$_['text_success']			= 'Félicitations, vous avez modifié le module <b>Google Talk</b> avec succès !';
$_['text_content_top']		= 'En-tête';
$_['text_content_bottom']	= 'Pied de page';
$_['text_column_left']		= 'Colonne de gauche';
$_['text_column_right']		= 'Colonne de droite';

// Entry
$_['entry_code']			= 'Code Google Talk :<br /><span class="help">Aller sur <a onclick="window.open(’http://www.google.com/talk/service/badge/New’);"><u>Créer un badge Google Talk Chatback</u></a> et copier/coller le code généré dans la zone de texte.</span>';
$_['entry_layout']			= 'Disposition';
$_['entry_position']		= 'Emplacement';
$_['entry_status']			= 'État';
$_['entry_sort_order']		= 'Classement';

// Error
$_['error_permission']		= 'Attention, vous n’avez pas la permission de modifier <b>Google Talk</b> !';
$_['error_code']			= 'Attention, le code  est requis !';
?>