<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// Heading
$_['heading_title']		= 'Frais Klarna';

// Text
$_['text_total']		= 'Total de la commande';
$_['text_success']		= 'Succès: Vous avez modifié le total des frais Klarna!';
$_['text_sweden']		= 'Suéde';
$_['text_norway']		= 'Norvège';
$_['text_finland']		= 'Finlande';
$_['text_denmark']		= 'Danemark';
$_['text_germany']		= 'Allemagne';
$_['text_netherlands']	= 'Pays-Bas';

// Entry
$_['entry_total']		= 'Total de la commande :';
$_['entry_fee']			= 'Frais :';
$_['entry_tax_class']	= 'Classe de TVA :';
$_['entry_status']		= 'État :';
$_['entry_sort_order']	= 'Classement :';

// Error
$_['error_permission']	= 'Attention, vous n’avez pas la permission de modifier le total des frais Klarna!';
?>