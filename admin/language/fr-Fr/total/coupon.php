<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// Heading
$_['heading_title']		= 'Bons de réduction';

// Text
$_['text_total']		= 'Totaux Commande';
$_['text_success']		= 'Félicitations, vous avez modifié les <b>Bons de réduction</b> avec succès !';

// Entry
$_['entry_status']		= 'État :';
$_['entry_sort_order']	= 'Classement :';

// Error
$_['error_permission']	= 'Attention, vous n’avez pas la permission de modifier les <b>Bons de réduction</b> !';
?>