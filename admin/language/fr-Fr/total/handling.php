<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// Heading
$_['heading_title']		= 'Frais de manutention';

// Text
$_['text_total']		= 'Totaux commande';
$_['text_success']		= 'Félicitations, vous avez modifié les <b>Frais de manutention</b> avec succès !';

// Entry
$_['entry_total']		= 'Total commande :';
$_['entry_fee']			= 'Frais :';
$_['entry_tax_class']	= 'Classe de taxe :';
$_['entry_status']		= 'État :';
$_['entry_sort_order']	= 'Classement :';

// Error
$_['error_permission']	= 'Attention, vous n’avez pas la permission de modifier les <b>Frais de manutention</b> !';
?>