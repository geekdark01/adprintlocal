<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// Heading
$_['heading_title']		= 'Livraison';

// Text
$_['text_total']		= 'Totaux Commande';
$_['text_success']		= 'Félicitations, vous avez modifié la <b>Livraison</b> avec succès !';

// Entry
$_['entry_estimator']	= 'Coût estimatif de livraison :';
$_['entry_status']		= 'État :';
$_['entry_sort_order']	= 'Classement :';

// Error
$_['error_permission']	= 'Attention, vous n’avez pas la permission de modifier la <b>Livraison</b> !';
?>