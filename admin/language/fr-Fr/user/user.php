<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// Heading
$_['heading_title']		= 'Utilisateur';

// Text
$_['text_success']		= 'Félicitations, vous avez modifié cet <b>Utilisateur</b> avec succès !';

// Column
$_['column_username']	= 'Nom d’utilisateur';
$_['column_status']		= 'État';
$_['column_date_added']	= 'Date';
$_['column_action']		= 'Action';

// Entry
$_['entry_username']	= 'Nom d’utilisateur :';
$_['entry_password']	= 'Mot de passe :';
$_['entry_confirm']		= 'Confirmation :';
$_['entry_firstname']	= 'Prénom :';
$_['entry_lastname']	= 'Nom :';
$_['entry_email']		= 'Courriel :';
$_['entry_user_group']	= 'Groupe d’utilisateurs :';
$_['entry_status']		= 'État :';
$_['entry_captcha']		= 'Entrer le code dans la zone ci-dessous';

// Error
$_['error_permission']	= 'Attention, vous n’avez pas la permission de modifier les <b>Utilisateurs</b> !';
$_['error_account']		= 'Attention, vous ne pouvez pas supprimer votre propre compte !';
$_['error_exists']		= 'Attention, le nom d’utilisateur est déj&agrave utilisé !';
$_['error_username']	= 'Le <b>Nom d’utilisateur</b> doit être composé de 3 à 20 caractères !';
$_['error_password']	= 'Le <b>Mot de passe</b> doit être composé de 4 à 20 caractères !';
$_['error_confirm']		= 'Le Mot de passe et la Confirmation ne correspondent pas !';
$_['error_firstname']	= 'Le <b>Prénom</b> doit être composé de 1 à 32 caractères !';
$_['error_lastname']	= 'Le <b>Nom</b> doit être composé de 1 à 32 caractères !';
$_['error_captcha']		= 'Le code de vérification ne correspond pas à l’image !';
?>