<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// Heading
$_['heading_title']		= 'Groupe Utilisateurs';

// Text
$_['text_success']		= 'Félicitations, vous avez modifié le <b>Groupe utilisateurs</b> avec succès !';

// Column
$_['column_name']		= 'Nom du Groupe Utilisateurs';
$_['column_action']		= 'Action';

// Entry
$_['entry_name']		= 'Nom du Groupe Utilisateurs :';
$_['entry_access']		= 'Droits en Lecture :';
$_['entry_modify']		= 'Droits en écriture :';

// Error
$_['error_permission']	= 'Attention, vous n’avez pas la permission de modifier le <b>Groupe Utilisateurs</b> !';
$_['error_name']		= 'Le <b>Nom du Groupe Utilisateurs</b> doit être composé de 3 à 64 caractères !';
$_['error_user']		= 'Attention, ce Groupe Utilisateurs ne peut être supprimé car il est assigné à %s utilisateurs !';
?>