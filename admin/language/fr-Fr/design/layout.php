<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//--------------------------------//

// Heading
$_['heading_title']		= 'Dispositions';

// Text
$_['text_success']		= 'Félicitations, vous avez sauvegardé les <b>Dispositions</b> avec succès !';
$_['text_default']		= 'Défaut';

// Column
$_['column_name']		= 'Nom de la disposition';
$_['column_action']		= 'Action';

// Entry
$_['entry_name']		= 'Nom de la disposition :';
$_['entry_store']		= 'Boutique';
$_['entry_route']		= 'Chemin';

// Error
$_['error_permission']	= 'Attention, vous n’avez pas la permission de modifier les <b>Dispositions</b> !';
$_['error_name']		= 'Le <b>Nom de la disposition</b> doit être composé de 3 à 64 caractères !';
$_['error_default']		= 'Attention, cette disposition ne peut pas être supprimée car elle est actuellement affectée comme disposition par d&eacutefaut pour cette boutique !';
$_['error_store']		= 'Attention, cette disposition ne peut pas être supprimée car elle est actuellement affectée à %s boutiques !';
$_['error_product']		= 'Attention, cette disposition ne peut pas être supprimée car elle est actuellement affectée à %s produits !';
$_['error_category']	= 'Attention, cette disposition ne peut pas être supprimée car elle est actuellement affectée à %s cat&eacutegories !';
$_['error_information']	= 'Attention, cette disposition ne peut pas être supprimée car elle est actuellement affectée à %s pages d’information !';
?>