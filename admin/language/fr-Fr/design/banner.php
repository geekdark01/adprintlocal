<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//--------------------------------//

// Heading
$_['heading_title']			= 'Bannières';

// Text
$_['text_success']			= 'Félicitations, vous avez sauvegardé les <b>Bannières</b> avec succès !';
$_['text_default']			= 'D&eacutefaut';
$_['text_image_manager']	= 'Gestionnaire d’images';
$_['text_browse']			= 'Parcourir les fichiers';
$_['text_clear']			= 'Nettoyer l’image';

// Column
$_['column_name']			= 'Nom de la bannière';
$_['column_status']			= 'État';
$_['column_action']			= 'Action';

// Entry
$_['entry_name']			= 'Nom de la bannière :';
$_['entry_title']			= 'Titre de la bannière';
$_['entry_link']			= 'Lien';
$_['entry_image']			= 'Image';
$_['entry_status']			= 'État :';

// Error
$_['error_permission']		= 'Attention, vous n’avez pas la permission de modifier les <b>Bannières</b> !';
$_['error_name']			= 'Le <b>Nom de la bannière</b> doit être composé de 3 à 64 caractères !';
$_['error_title']			= 'Le <b>Titre de la bannière</b> doit être composé de 2 à 64 caractères !';
$_['error_default']			= 'Attention, cette disposition ne peut pas être supprimée car elle est actuellement affectée comme disposition par d&eacutefaut pour cette boutique !';
$_['error_product']			= 'Attention, cette disposition ne peut pas être supprimée car elle est actuellement affectée à %s produits !';
$_['error_category']		= 'Attention, cette disposition ne peut pas être supprimée car elle est actuellement affectée à %s cat&eacutegories !';
$_['error_information']		= 'Attention, cette disposition ne peut pas être supprimée car elle est actuellement affectée à %s pages d’information !';
?>