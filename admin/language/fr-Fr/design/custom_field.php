<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// Heading
$_['heading_title']         = 'Champs personnalisés';

// Text
$_['text_success']			= 'Félicitations, vous avez modifié les <b>Champs personnalisés</b> avec succès !';
$_['text_choose']			= 'Choix';
$_['text_select']			= 'Sélecteur';
$_['text_radio']			= 'Bouton radio';
$_['text_checkbox']			= 'Case à cocher';
$_['text_input']			= 'Entrée de données';
$_['text_text']				= 'Texte';
$_['text_textarea']			= 'Zone de texte';
$_['text_file']				= 'Fichier';
$_['text_date']				= 'Date';
$_['text_datetime']			= 'Date &amp; Heure';
$_['text_time']				= 'Heure';
$_['text_customer']			= 'Client';
$_['text_address']			= 'Adresse';
$_['text_payment_address']	= 'Adresse de paiement';
$_['text_shipping_address']	= 'Adresse de livraison';

// Column
$_['column_name']           = 'Nom du champ spécial';
$_['column_type']           = 'Type';
$_['column_location']       = 'Emplacement';
$_['column_sort_order']     = 'Classement';
$_['column_action']         = 'Action';

// Entry
$_['entry_name']			= 'Nom du champ spécial :';
$_['entry_type']			= 'Type :';
$_['entry_value']			= 'Valeur :';
$_['entry_custom_value']	= 'Nom de la valeur du champ spécial :';
$_['entry_required']		= 'Requis :';
$_['entry_location']		= 'Localisation :';
$_['entry_position']		= 'Emplacement :';
$_['entry_sort_order']		= 'Classement :';

// Error
$_['error_permission']     = 'Attention, vous n’avez pas la permission de modifier les champs spéciaux !';
$_['error_name']           = 'Le nom de l’option doit être composé de 1 à 128 caractères !';
$_['error_type']           = 'Attention, la valeur de l’option est requise !';
$_['error_custom_value']   = 'Le nom de la valeur du champ spécial doit être composé de 1 à 128 caractères !';
$_['error_product']        = 'Attention, ce champ spécial ne peut pas être supprimé car %s produits lui sont encore associés !';
?>