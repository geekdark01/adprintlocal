<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//--------------------------------//

// Heading
$_['heading_title']		= 'Page introuvable !';

// Text
$_['text_not_found']	= 'La page que vous recherchez n’a pas pu être trouvée ! Veuillez contacter votre administrateur si le problème persiste.';
?>