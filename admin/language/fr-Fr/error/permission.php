<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//--------------------------------//

// Heading
$_['heading_title']		= 'Permission refusée !';

// Text
$_['text_permission']	= 'Vous n’avez pas la permission d’accèder à cette page, veuillez vous adresser à votre administrateur système.';
?>