<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//--------------------------------//

// Heading
$_['heading_title']			= 'Informations';

// Text
$_['text_success']			= 'Félicitations, vous avez modifié les <b>Informations</b> avec succès !';
$_['text_default']			= 'Par défaut';

// Column
$_['column_title']			= 'Titre des informations';
$_['column_sort_order']		= 'Classement';
$_['column_action']			= 'Action';

// Entry
$_['entry_title']			= 'Titre des informations :';
$_['entry_description']		= 'Description :';
$_['entry_store']			= 'Boutiques :';
$_['entry_keyword']			= 'Référencement "SEO Mots-clés" :';
$_['entry_bottom']			= 'Pied de page:<br/><span class="help">Afficher dans le pied de page.</span>';
$_['entry_status']			= 'État :';
$_['entry_sort_order']		= 'Classement :<br/><span class="help">D&eacutefinir à -1 pour ne pas afficher dans la liste</span>';
$_['entry_layout']			= 'Disposition :';

// Error
$_['error_warning']			= 'Attention, veuillez vérifier soigneusement le formulaire afin qu’il n’y ai pas d’erreurs !';
$_['error_permission']		= 'Attention, vous n’avez pas la permission de modifier les <b>Informations</b> !';
$_['error_title']			= 'Le <b>Titre des informations</b> doit être composé de 3 à 64 caractères !';
$_['error_description']		= 'La <b>Description</b> doit être composé de 3 caractères minimum !';
$_['error_account']			= 'Attention, cette page d’information ne peut pas être supprimée car elle est actuellement affectée par défaut au compte boutique !';
$_['error_checkout']		= 'Attention, cette page d’information ne peut pas être supprimée car elle est actuellement affectée par défaut à la validation de votre commande !';
$_['error_affiliate']		= 'Attention, cette page d’information ne peut pas être supprimée car elle est actuellement affectée par défaut aux conditions d’affiliation de la boutique !';
$_['error_store']			= 'Attention, cette page d’information ne peut pas être supprimée car elle est utilisée par les boutiques %s !';
?>