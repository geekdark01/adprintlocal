<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//--------------------------------//

// Heading
$_['heading_title']				= 'Catégories';

// Text
$_['text_success']				= 'Félicitations, vous avez modifié les <b>Catégories</b> avec succès !';
$_['text_default']				= 'Par défaut';
$_['text_image_manager']		= 'Gestionnaire d’images';
$_['text_browse']				= 'Parcourir les fichiers';
$_['text_clear']				= 'Effacer l’image';

// Column
$_['column_name']				= 'Nom de la catégorie';
$_['column_sort_order']			= 'Classement';
$_['column_action']				= 'Action';

// Entry
$_['entry_name']				= 'Nom de la catégorie :';
$_['entry_meta_keyword']		= 'Balise méta "Mots-clés" :';
$_['entry_meta_description']	= 'Balise méta "Description" :';
$_['entry_description']			= 'Description :';
$_['entry_parent']				= 'Catégorie parente :';
$_['entry_filter']				= 'Filtres :<br /><span class="help">(Complétion automatique)</span>';
$_['entry_store']				= 'Boutiques :';
$_['entry_keyword']				= 'SEO "Ré-écriture d’url" :<br /><span class="help">Mettre par ex. le nom de la catégorie. Pour activer, cocher <b>"Oui"</b> dans <b>"Utiliser des URL’s SEO"</b> situé dans l’onglet : <b>Système/Paramètres/Serveur</b>.</span>';
$_['entry_image']				= 'Image :';
$_['entry_top']              	= 'Top :<br/><span class="help">Affichage dans la barre de menu du haut. Ne fonctionne que pour les catégories parentes.</span>';
$_['entry_column']           	= 'Colonnes :<br/><span class="help">Nombre de colonnes à utiliser pour les 3 catégories inférieures. Ne fonctionne que pour les catégories parentes.</span>';
$_['entry_sort_order']			= 'Classement :';
$_['entry_status']				= 'État de la catégorie :';
$_['entry_layout']				= 'Disposition :';

// Error
$_['error_warning']				= 'Attention, veuillez vérifier soigneusement le formulaire afin qu’il n’y ai pas d’erreurs !';
$_['error_permission']			= 'Attention, vous n’avez pas la permission de modifier les <b>Catégories</b> !';
$_['error_name']				= 'Le <b>Nom de la catégorie</b> doit être composé de 2 à 32 caractères !';
?>