<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//--------------------------------//

// Heading
$_['heading_title']		= 'Téléchargements';

// Text
$_['text_success']		= 'Félicitations, vous avez modifié les <b>Téléchargements</b> avec succès !';
$_['text_upload']		= 'Votre fichier a été transféré avec succès !';

// Column
$_['column_name']		= 'Nom du Téléchargement';
$_['column_remaining']	= 'Total de Téléchargements Permis';
$_['column_action']		= 'Action';

// Entry
$_['entry_name']		= 'Nom du téléchargement :';
$_['entry_filename']	= 'Nom du fichier :<br /><span class="help">Vous pouvez télécharger via le bouton de téléchargement ou utiliser votre logiciel de transfert (FTP) pour transférer le répertoire de téléchargement et entrer les détails ci-dessous.<br /><br />Il est également recommandé que le nom du fichier et le masque soiet différents pour empêcher toute personne de se relier directement à vos téléchargements.</span>';
$_['entry_mask']		= 'Masque :';
$_['entry_remaining']	= 'Total de Téléchargements permis :';
$_['entry_update']		= 'Poussez aux clients précédents :<br /><span class="help">Vérifiez ceci pour mettre à jour des versions précédemment achetées.</span>';

// Error
$_['error_permission']	= 'Attention, vous n’avez pas la permission de modifier les <b>Téléchargements</b> !';
$_['error_name']		= 'Le <b>Nom</b> doit être composé de 3 à 64 caractères !';
$_['error_upload']		= 'Envoi nécessaire !';
$_['error_filename']	= 'Le <b>Nom du Fichier</b> doit être composé de 3 à 128 caractères !';
$_['error_exists']		= 'Le fichier n’existe pas !';
$_['error_mask']		= 'Le <b>Masque</b> doit être composé de 3 à 128 caractères !';
$_['error_filetype']	= 'Type de fichier invalide !';
$_['error_product']		= 'Attention, ce téléchargement ne peut pas être supprimé car %s produits lui sont encore assigné !';
?>