<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//--------------------------------//

// Heading
$_['heading_title']			= 'Produits'; 

// Text
$_['text_success']			= 'Félicitations, vous avez modifié les <b>Produits</b> avec succès !';
$_['text_plus']				= '+';
$_['text_minus']			= '-';
$_['text_default']			= 'Par défaut';
$_['text_image_manager']	= 'Gestionnaire d’images';
$_['text_browse']			= 'Parcourir les fichiers';
$_['text_clear']			= 'Effacer l’image';
$_['text_option']			= 'Option';
$_['text_option_value']		= 'Valeur de l’option';
$_['text_percent']			= 'Pourcentage';
$_['text_amount']			= 'Montant fixe';

// Column
$_['column_name']			= 'Nom du produit';
$_['column_model']			= 'Modèle';
$_['column_image']			= 'Image';
$_['column_price']			= 'Prix';
$_['column_quantity']		= 'Quantité';
$_['column_status']			= 'État';
$_['column_action']			= 'Action';

// Entry
$_['entry_name']			= 'Nom du produit ';
$_['entry_meta_keyword']	= 'Balise méta "Mots-clés" :';
$_['entry_meta_description']= 'Balise méta "Description" :';
$_['entry_description']		= 'Description :';
$_['entry_store']			= 'Boutiques :';
$_['entry_keyword']			= 'SEO "Ré-écriture d’url" :<br /><span class="help">Mettre par ex. le nom de l’article. Pour activer, cocher <b>"Oui"</b> dans <b>"Utiliser des URL’s SEO"</b> situé dans l’onglet : <b>Système/Paramètres/Serveur</b>.</span>';
$_['entry_model']			= 'Modèle ';
$_['entry_sku']				= 'Référence "Gestion des stocks" :';
$_['entry_upc']				= 'Code universel des produits :<br /><span class="help">(Code-barres)</span>';
$_['entry_ean']				= 'EAN (European Article Numbering) :<br/><span class="help">Code barre normalis&eacute pour l’Europe</span>';
$_['entry_jan']				= 'JAN (Japanese Article Number) :<br/><span class="help">Code barre normalis&eacute pour le Japon</span>';
$_['entry_isbn']			= 'ISBN (International Standard Book Number):<br/><span class="help">Numéro international normalisé du livre</span>';
$_['entry_mpn']				= 'MPN :<br/><span class="help">Numéro de pièce du fabricant</span>';
$_['entry_location']		= 'Localisation :';
$_['entry_shipping']		= 'Livraison requise :'; 
$_['entry_manufacturer']	= 'Fabricant :';
$_['entry_date_available']	= 'Date de disponibilité :';
$_['entry_quantity']		= 'Quantité';
$_['entry_minimum']			= 'Quantité minimum :<br /><span class="help">Forcer une quantité minimum de commande</span>';
$_['entry_stock_status']	= 'État si rupture :';
$_['entry_price']			= 'Prix H.T ';
$_['entry_tax_class']		= 'Type de taxe :';
$_['entry_points']			= 'Points :<br/><span class="help">Nombre de points nécessaires à l’achat de cet article. Si vous ne souhaitez pas que ce produit soit acheté avec les points, laisser à 0.</span>';
$_['entry_option_points']	= 'Points ';
$_['entry_subtract']		= 'Soustraire du stock';
$_['entry_weight_class']	= 'Unité de Poids :';
$_['entry_weight']			= 'Poids';
$_['entry_length']			= 'Unité de longueur :';
$_['entry_dimension']		= 'Dimensions (L x l x H) :';
$_['entry_image']			= 'Image :';
$_['entry_customer_group']	= 'Groupe client :';
$_['entry_date_start']		= 'Date de début :';
$_['entry_date_end']		= 'Date de fin :';
$_['entry_priority']		= 'Priorité :';
$_['entry_attribute']		= 'Attribut :';
$_['entry_attribute_group']	= 'Groupe d’attribut :';
$_['entry_text']			= 'Texte :';
$_['entry_option']			= 'Option :';
$_['entry_option_value']	= 'Valeur de l’option';
$_['entry_required']		= 'Requis :';
$_['entry_status']			= 'État :';
$_['entry_sort_order']		= 'Classement :';
$_['entry_category']		= 'Catégories :<br /><span class="help">(Complétion automatique)</span>';
$_['entry_filter']			= 'Filtres :<br /><span class="help">(Complétion automatique)</span>';
$_['entry_download']		= 'Téléchargements :<br /><span class="help">(Complétion automatique)</span>';
$_['entry_related']			= 'Produits apparentés :<br /><span class="help">(Complétion automatique)</span>';
$_['entry_tag']				= 'Balises du produit :<br /><span class="help">Elles doivent être séparées par une virgule <i><small>(Etiquettes visibles en bas de la page du produit)</small></i></span>';
$_['entry_reward']			= 'Points de fidélité :';
$_['entry_layout']			= 'Disposition :';
$_['entry_profile']			= 'Profil';
$_['text_recurring_help']	= 'Les montants récurrents sont calculés par fréquence et cycles.<br />Par exemple, si vous utilisez une fréquence de é semaine é et un cycle de "2", l’utilisateur sera facturé toutes les 2 semaines.<br />La longueur est le nombre de fois que l’utilisateur effectuera un paiement, réglez la longueur sur 0 si vous voulez payer jusqu’au solde des paiements.';
$_['text_recurring_title']	= 'Paiements récurrents ';
$_['text_recurring_trial']	= 'Période d’essai';
$_['entry_recurring']		= 'Facturation récurrente :';
$_['entry_recurring_price']	= 'Prix récurrent :';
$_['entry_recurring_freq']	= 'Fréquence récurrente :';
$_['entry_recurring_cycle']	= 'Cycles récurrents :<span class="help">Nombre de fois de facturation, doit étre de 1 ou plus</span>';
$_['entry_recurring_length']= 'Longueur récurrente :<span class="help">0 = jusqu’au solde des paiements</span>';
$_['entry_trial']			= 'Période d’essai :';
$_['entry_trial_price']		= 'Première instance de prix récurrent :';
$_['entry_trial_freq']		= 'Première instance de fréquence récurrente :';
$_['entry_trial_cycle']		= 'Première instance de cycles récurrents :<span class="help"Nombre de facturation, doit étre de 1 ou plus</span>';
$_['entry_trial_length']	= 'Première instance de longueur récurrente :';

// Text
$_['text_length_day']		= 'Jour';
$_['text_length_week']		= 'Semaine';
$_['text_length_month']		= 'Mois';
$_['text_length_month_semi']= 'Mois Jumelés';
$_['text_length_year']		= 'Année';

// Error
$_['error_warning']			= 'Attention, veuillez vérifier soigneusement le formulaire afin qu’il n’y ai pas d’erreurs !';
$_['error_permission']		= 'Attention, vous n’avez pas la permission de modifier les <b>Produits</b> !';
$_['error_name']			= 'Le <b>Nom du produit</b> doit être composé de 3 à 255 caractères !';
$_['error_model']			= 'Le <b>Modèle</b> doit être composé de 3 à 24 caractères !';
?>