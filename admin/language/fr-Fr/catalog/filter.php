<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// Heading
$_['heading_title']		= 'Filtres';

// Text
$_['text_success']		= 'Félicitations, vous avez modifié les <b>Filtres</b> avec succès !';

// Column
$_['column_group']		= 'Groupe de Filtres';
$_['column_sort_order']	= 'Classement';
$_['column_action']		= 'Action';

// Entry
$_['entry_group']		= 'Nom du groupe de filtres :';
$_['entry_name']		= 'Nom du filtre :';
$_['entry_sort_order']	= 'Classement :';

// Error
$_['error_permission']	= 'Attention, vous n’avez pas la permission de modifier les <b>Filtres</b> !';
$_['error_group']		= 'Le <b>Nom du groupe de filtres</b> doit être composé de 1 à 64 caractères !';
$_['error_name']		= 'Le <b>Nom du filtre</b> doit être composé de 1 à 64 caractères !';
?>