<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// Heading
$_['heading_title']			= 'Options';

// Text
$_['text_success']			= 'Félicitations, vous avez sauvegardé les <b>Options</b> avec succès !';
$_['text_choose']			= 'Choix';
$_['text_select']			= 'Sélection';
$_['text_radio']			= 'Bouton radio';
$_['text_checkbox']			= 'Case à cocher';
$_['text_image']			= 'Image';
$_['text_input']			= 'Entrée de données';
$_['text_text']				= 'Texte';
$_['text_textarea']			= 'Zone de texte';
$_['text_file']				= 'Fichier';
$_['text_date']				= 'Date';
$_['text_datetime']			= 'Date & heure';
$_['text_time']				= 'Temps';
$_['text_image_manager']	= 'Gestionnaire d’image';
$_['text_browse']			= 'Parcourir les fichiers';
$_['text_clear']			= 'Effacer l’image';

// Column
$_['column_name']			= 'Nom de l’option';
$_['column_sort_order']		= 'Classement';
$_['column_action']			= 'Action';

// Entry
$_['entry_name']			= 'Nom de l’option :';
$_['entry_type']			= 'Type :';
$_['entry_option_value']	= 'Nom de la valeur de l’option';
$_['entry_image']			= 'Image:';
$_['entry_sort_order']		= 'Classement';
$_['entry_price']			= 'Prix';

// Error
$_['error_permission']		= 'Attention, vous n’avez pas la permission de modifier les <b>Options</b> !';
$_['error_name']			= 'Le <b>Nom de l’option</b> doit être composé de 3 à 128 caractères !';
$_['error_type']			= 'Attention, la valeur de l’option est requise !';
$_['error_price']			= 'Prix invalide';
$_['error_option_value']	= 'Le <b>Nom de la valeur de l’option</b> doit être composé de 3 à 128 caractères !';
$_['error_product']     	= 'Attention, cette option ne peut pas être supprimée car elle est actuellement affectée à %s produits !';
?>