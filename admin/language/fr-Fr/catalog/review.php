<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//--------------------------------//

// Heading
$_['heading_title']		= 'Avis';

// Text
$_['text_success']		= 'Félicitations, vous avez modifié les <b>Avis</b> avec succès !';

// Column
$_['column_product']	= 'Produit';
$_['column_author']		= 'Auteur';
$_['column_rating']		= 'Évaluation';
$_['column_status']		= 'État';
$_['column_date_added']	= 'Date';
$_['column_action']		= 'Action';

// Entry
$_['entry_product']		= 'Produit :';
$_['entry_author']		= 'Auteur :';
$_['entry_rating']		= 'Évaluation :';
$_['entry_status']		= 'État :';
$_['entry_text']		= 'Texte :';
$_['entry_good']		= 'Bon';
$_['entry_bad']			= 'Mauvais';

// Error
$_['error_permission']	= 'Attention, vous n’avez pas la permission de modifier les <b>Avis</b> !';
$_['error_product']		= 'Attention, le produit  est requis !';
$_['error_author']		= 'L’<b>Auteur</b> doit être composé de 3 à 64 caractères !';
$_['error_text']		= 'Le <b>Commentaire</b> doit être composé de 25 à 1000 caractères !';
$_['error_rating']		= 'Attention, l’èvaluation est requise !';
?>