<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//--------------------------------//

// Heading
$_['heading_title']		= 'Fabricants';

// Text
$_['text_success']		= 'Félicitations, vous avez modifié les <b>Fabricants</b> avec succès !';
$_['text_default']		= 'Par défaut';
$_['text_image_manager']= 'Gestionnaire d’images';
$_['text_browse']		= 'Parcourir les fichiers';
$_['text_clear']		= 'Effacer l’image';
$_['text_percent']		= 'Pourcentage';
$_['text_amount']		= 'Montant fixe';

// Column
$_['column_name']		= 'Nom du fabricant';
$_['column_sort_order']	= 'Classement';
$_['column_action']		= 'Action';

// Entry
$_['entry_name']		= 'Nom du fabricant :';
$_['entry_store']		= 'Boutiques :';
$_['entry_keyword']		= 'Référencement "Mots-clés" :';
$_['entry_image']		= 'Image :';
$_['entry_sort_order']	= 'Classement :';
$_['entry_type']		= 'Type :';

// Error
$_['error_permission']	= 'Attention, vous n’avez pas la permission de modifier les <b>Fabricants</b> !';
$_['error_name']		= 'Le <b>Nom du fabricant</b> doit être composé de 3 à 64 caractères !';
$_['error_product']		= 'Attention, ce fabricant n’a pas pu être supprimé car %s produits lui sont encore assigné !';
?>