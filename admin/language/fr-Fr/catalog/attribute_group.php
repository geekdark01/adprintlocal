<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//--------------------------------//

// Heading
$_['heading_title']		= 'Groupes d’attribut';

// Text
$_['text_success']		= 'Félicitations, vous avez sauvegardé les <b>Groupes d’attribut</b> avec succès !';

// Column
$_['column_name']		= 'Nom du groupe d’attribut';
$_['column_sort_order']	= 'Classement';
$_['column_action']		= 'Action';

// Entry
$_['entry_name']		= 'Nom du groupe d’attribut :';
$_['entry_sort_order']	= 'Classement :';

// Error
$_['error_permission']	= 'Attention, vous n’avez pas la permission de modifier les <b>Groupes d’attribut</b> !';
$_['error_name']		= 'Le <b>Nom du groupe d’attribut</b> doit être composé de 3 à 64 caractères !';
$_['error_attribute']	= 'Attention, ce groupe d’attribut ne peut pas être supprimée car elle est actuellement affectée à %s attributs !';
$_['error_product']		= 'Attention, ce groupe d’attribut ne peut pas être supprimée car elle est actuellement affectée à %s produits !';
?>