<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//--------------------------------//

// Heading
$_['heading_title']				= 'Attributs';

// Text
$_['text_success']				= 'Félicitations, vous avez sauvegardé les <b>Attributs</b> avec succès !';

// Column
$_['column_name']				= 'Nom de l’attribut';
$_['column_attribute_group']	= 'Groupe d’attribut';
$_['column_sort_order']			= 'Classement';
$_['column_action']				= 'Action';

// Entry
$_['entry_name']				= 'Nom de l’attribut :';
$_['entry_attribute_group']		= 'Groupe d’attribut :';
$_['entry_sort_order']			= 'Classement :';

// Error
$_['error_permission']			= 'Attention, vous n’avez pas la permission de modifier les <b>Attributs</b> !';
$_['error_name']				= 'Le <b>Nom de l’attribut</b> doit être composé de 3 à 64 caractères !';
$_['error_product']     		= 'Attention, cet attribut ne peut pas être supprimée car elle est actuellement affectée à %s produits !';
?>