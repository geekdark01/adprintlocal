<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//--------------------------------//

// Heading
$_['heading_title']			= 'Profils';

// Button
$_['button_insert']			= 'Insérer';
$_['button_copy']			= 'Copier';
$_['button_delete']			= 'Supprimer';
$_['button_remove']			= 'déplacer';

// Text
$_['text_no_results']		= 'Aucun résultat !';
$_['text_remove']			= 'déplacer';
$_['text_edit']				= 'Éditer';
$_['text_enabled']			= 'Activé';
$_['text_disabled']			= 'Désactivé';
$_['text_success']			= 'Le profil a été ajouté avec succès !';
$_['text_day']				= 'Jour';
$_['text_week']				= 'Semaine';
$_['text_semi_month']		= 'Mois jumelés';
$_['text_month']			= 'Mois';
$_['text_year']				= 'Année';

// Entry
$_['entry_name']			= 'Nom :';
$_['entry_sort_order']		= 'Classement :';
$_['entry_price']			= 'Prix :';
$_['entry_duration']		= 'Durée :';
$_['entry_status']			= 'État :';
$_['entry_cycle']			= 'Cycle :';
$_['entry_frequency']		= 'Fréquence :';
$_['entry_trial_price']		= 'Première instance de prix :';
$_['entry_trial_duration']	= 'Première instance de durée :';
$_['entry_trial_status']	= 'Première instance d’état :';
$_['entry_trial_cycle']		= 'Première instance de cycle :';
$_['entry_trial_frequency']	= 'Première instance de fréquence :';

// Column
$_['column_name']			= 'Nom';
$_['column_sort_order']		= 'Classement';
$_['column_action']			= 'Action';

// Error
$_['error_warning']			= 'Attention, veuillez vérifier avec attention le journal d’erreurs !';
$_['error_permission']		= 'Attention, vous n’avez pas la permission de modifier les profils !';
$_['error_name']			= 'Le <b>Nom de profil</b> doit être composé entre 3 et 255 caractères !';

// Help
$_['text_recurring_help']	= 'Les montants récurrents sont calculés par fréquence et cycles.<br />Par exemple, si vous utilisez une fréquence de « semaine » et un cycle de "2", l’utilisateur sera facturé toutes les 2 semaines.<br />La longueur est le nombre de fois que l’utilisateur effectuera un paiement, réglez la longueur sur 0 si vous voulez payer jusqu’au solde des paiements.';
?>