<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// Heading
$_['heading_title']				= 'Parcelforce 48';

// Text
$_['text_shipping']				= 'Livraison';
$_['text_success']				= 'Félicitations, vous avez modifié la livraison <b>Parcelforce 48</b> avec succès !';

// Entry
$_['entry_rate']				= 'Tarifs de Parcelforce 48 :<br /><span class="help">Entrez des valeurs jusqu’à 5 chiffres après la virgule (12345,67) Exemple : .1:1, .25:1.27 - Poids inférieur ou égal à 0,1 kg coûte 1,00, poids inférieur ou égal à 0,25 g mais plus de 0,1 kg coûte 1.27. Ne pas entrer KG ou des symboles.</span>';
$_['entry_insurance']			= 'Parcelforce48 Compensation Rates:<br /><span class="help">Enter values upto 5,2 decimal places. (12345.67) Example: 34:0,100:1,250:2.25 - Insurance cover for cart values upto 34 would cost 0.00 extra, those values more than 100 and upto 250 will cost 2.25 extra. Do not enter currency symbols.</span>';
$_['entry_display_weight']		= 'Afficher le poids de livraison :<br /><span class="help">Voulez-vous afficher le poids d’expédition ? (par exemple :  Poids de livraison : 2,7674 kgs )</span>';
$_['entry_display_insurance']	= 'Afficher l’assurance :<br /><span class="help">Voulez-vous afficher l’assurance ? (par exemple : Assuré jusqu’à 500)</span>';
$_['entry_display_time']		= 'Afficher le délai de livraison :<br /><span class="help">Voulez-vous afficher le délai de livraison ? (par exemple : Livraison entre 3 et 5 jours)</span>';
$_['entry_tax_class']			= 'Classe de taxes :';
$_['entry_geo_zone']			= 'Zone géographique :';
$_['entry_status']				= 'État :';
$_['entry_sort_order']			= 'Classement :';

// Error
$_['error_permission']			= 'Attention, vous n’avez pas la permission de modifier la livraison <b>Parcelforce 48</b> !';
?>