<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// Heading
$_['heading_title']					= 'United Parcel Service';

// Text
$_['text_shipping']					= 'Livraison';
$_['text_success']					= 'Félicitations, vous avez modifié la livraison <b>UPS</b> avec succès !';
$_['text_regular_daily_pickup']		= 'Enlèvement quotidien régulier';
$_['text_daily_pickup']				= 'Enlèvement quotidien';
$_['text_customer_counter']			= 'Guichet client';
$_['text_one_time_pickup']			= 'Enlèvement unique';
$_['text_on_call_air_pickup']		= 'Enlèvement colis aérien';
$_['text_letter_center']			= 'Centre courrier';
$_['text_air_service_center']		= 'Centre ’Air Service’';
$_['text_suggested_retail_rates']	= 'Taux recommandées (UPS Boutique)';
$_['text_package']					= 'Colis';
$_['text_ups_letter']				= 'UPS Enveloppe';
$_['text_ups_tube']					= 'UPS Tube';
$_['text_ups_pak']					= 'UPS Pak';
$_['text_ups_express_box']			= 'UPS Express Box';
$_['text_ups_25kg_box']				= 'Boîte UPS 25kg';
$_['text_ups_10kg_box']				= 'Boîte UPS 10kg';
$_['text_us']						= 'Originaire des E.U.';
$_['text_ca']						= 'Originaire du Canada';
$_['text_eu']						= 'Originaire de l’Union Européenne';
$_['text_pr']						= 'Originaire de Puerto Rico';
$_['text_mx']						= 'Originaire du Mexique';
$_['text_other']					= 'Originaire d’autres pays';
$_['text_test']						= 'Test';
$_['text_production']				= 'Production';	
$_['text_residential']				= 'Résidentiel';
$_['text_commercial']				= 'Commercial';
$_['text_next_day_air']				= 'Jour suivant par Air';
$_['text_2nd_day_air']				= '2e jour suivant par Air';
$_['text_ground']					= 'Terrestre';
$_['text_3_day_select']				= '3 jours Sélectionner';
$_['text_next_day_air_saver']		= 'Jour suivant après-midi par Air';
$_['text_next_day_air_early_am']	= 'Matin suivant par Air';
$_['text_2nd_day_air_am']			= 'Matin 2e jour par Air';
$_['text_saver']					= 'Jour suivant après-midi';
$_['text_worldwide_express']		= 'Express international';
$_['text_worldwide_expedited']		= 'Rapide international';
$_['text_standard']					= 'Standard';
$_['text_worldwide_express_plus']	= 'Express international matin';
$_['text_express']					= 'Express';
$_['text_expedited']				= 'Rapide';
$_['text_express_early_am']			= 'Express Matin';
$_['text_express_plus']				= 'Express Plus';
$_['text_today_standard']			= 'Aujourd’hui standard';
$_['text_today_dedicated_courier']	= 'Aujourd’hui coursier dedié';
$_['text_today_intercity']			= 'Aujourd’hui inter-villes';
$_['text_today_express']			= 'Aujourd’hui Express';
$_['text_today_express_saver']		= 'Aujourd’hui Express Saver';

// Entry
$_['entry_key']						= 'Clé d’accès :<span class="help">Taux en format XML : Entrer votre clef d’accès fourni par UPS.</span>';
$_['entry_username']				= 'Nom d’utilisateur :<span class="help">Entrer le nom d’utilisateur de votre compte UPS Services.</span>';
$_['entry_password']				= 'Mot de passe :<span class="help">Entrer le mot de passe de votre compte UPS Services.</span>';
$_['entry_pickup']					= 'Méthode de ramassage :<span class="help">Quel méthode de ramassage UPS utilisez-vous (uniquement avec origine aux E.U.) ?</span>';
$_['entry_packaging']				= 'Type d’emballage :<span class="help">Quel type d’emballage utilisez-vous ?</span>';
$_['entry_classification']			= 'Code de classification client :<span class="help">01 - Facturation sur un compte UPS et avec enlèvement quotidien, 03 - Vous n’avez pas de compte UPS account ou facturation sur un compte UPS mais vous n’avez pas d’enlèvement quotidien, 04 - Vous expedier d’un magasin (uniquement avec origine aux E.U.)</span>';
$_['entry_origin']					= 'Code de livraison d’origine :<span class="help">Quel point d’origine devrait être utilisé (cette sélection modifie seulement les noms de produits que vera l’utilisateur) ?</span>';
$_['entry_city']					= 'Ville d’origine :<span class="help">Entrer la ville d’origine.</span>';
$_['entry_state']					= 'Département d’origine :<span class="help">Entrer les deux chiffres du département d’origine.</span>';
$_['entry_country']					= 'Pays d’origine :<span class="help">Entrer les deux chiffres du pays d’origine.</span>';
$_['entry_postcode']				= 'Code postal d’origine :<span class="help">Enter le code postal d’origine.</span>';
$_['entry_test']					= 'Mode essai ou production :<span class="help">Utiliser ce module en mode Essai ou Production ?</span>';
$_['entry_quote_type']				= 'Type de taux :<span class="help">Livraison residentiel ou commerciale.</span>';
$_['entry_service']					= 'Services :<span class="help">Selectionner les services UPS.</span>';
$_['entry_insurance']				= 'Activer l’assurance :<span class="help">Actif l’assurance pour la totalit&eacut; des produits ainsi que pour leur valeur</span>';
$_['entry_display_weight']			= 'Afficher le poids de livraison :<br /><span class="help">Voulez-vous afficher le poids d’expédition ? (par exemple : Poids de livraison : 2,7674 kgs)</span>';
$_['entry_weight_class']			= 'Classe de poids :<span class="help">Selectionner kgs ou livres (lbs).</span>';
$_['entry_length_class']			= 'Classe des longueurs :<span class="help">Définir en centimètres ou en pouces.</span>';
$_['entry_dimension']				= 'Dimensions (L x l x H) :';
$_['entry_tax_class']				= 'Classe de taxes :';
$_['entry_geo_zone']				= 'Zone Géographique : ';
$_['entry_status']					= 'État : ';
$_['entry_sort_order']				= 'Classement :';
$_['entry_debug']					= 'Mode de débogage :<br /><span class="help">Enregister, envoyer et recevoir des donn&eacutes dans le journal système</span>';

// Error
$_['error_permission']				= 'Attention, vous n’avez pas la permission de modifier la livraison <b>UPS</b> !';
$_['error_key']						= 'Attention, la clé d’accès est requise !';
$_['error_username']				= 'Attention, le nom d’utilisateur  est requis !';
$_['error_password']				= 'Attention, le mot de passe  est requis !';
$_['error_city']					= 'Attention, la ville d’origine est requise !';
$_['error_state']					= 'Attention, le département est requis !';
$_['error_country']					= 'Attention, le pays d’origine est requis !';
$_['error_dimension']				= 'Attention, les dimensions moyennes sont requises !';
?>