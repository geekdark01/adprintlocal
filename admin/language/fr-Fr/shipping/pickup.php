<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// Heading
$_['heading_title']		= 'Retrait au Magasin';

// Text 
$_['text_shipping']		= 'Livraison';
$_['text_success']		= 'Félicitations, vous avez modifié le <b>Retrait au Magasin</b> avec succès !';

// Entry
$_['entry_geo_zone']	= 'Zone géographique :';
$_['entry_status']		= 'État :';
$_['entry_sort_order']	= 'Classement :';

// Error
$_['error_permission']	= 'Attention, vous n’avez pas la permission de modifier le <b>Retrait au Magasin</b> !';
?>