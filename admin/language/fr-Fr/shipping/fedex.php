<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// Heading
$_['heading_title']								= 'Fedex';

// Text
$_['text_shipping']								= 'Livraison';
$_['text_success']								= 'Félicitations, vous avez modifié le module <b>Fedex</b> avec succès !';
$_['text_europe_first_international_priority'] 	= 'Europe First International Priority &reg;';
$_['text_fedex_1_day_freight']                	= 'Fedex 1 Day Freight';
$_['text_fedex_2_day']                         	= 'Fedex 2 Day';
$_['text_fedex_2_day_am']                      	= 'Fedex 2 Day AM';
$_['text_fedex_2_day_freight']                 	= 'Fedex 2 Day Freight';
$_['text_fedex_3_day_freight']                 	= 'Fedex 3 Day Freight';
$_['text_fedex_express_saver']                 	= 'Fedex Express Saver';
$_['text_fedex_first_freight']                 	= 'Fedex First Freight';
$_['text_fedex_freight_economy']              	= 'Fedex Freight Economy&reg;';
$_['text_fedex_freight_priority']              	= 'Fedex Freight Priority&reg;';
$_['text_fedex_ground']                        	= 'Fedex Ground';
$_['text_first_overnight']						= 'First Overnight&reg;';
$_['text_ground_home_delivery']                	= 'Ground Accueil Delivery';
$_['text_international_economy']               	= 'International Economy&reg;';
$_['text_international_economy_freight']    	= 'International Economy&reg; Freight';
$_['text_international_first']					= 'International First&reg;';
$_['text_international_priority']              	= 'International Priority&reg;';
$_['text_international_priority_freight']      	= 'International Priority&reg; Freight';
$_['text_priority_overnight']                  	= 'Priority Overnight&reg;';
$_['text_smart_post']                          	= 'Smart Post';
$_['text_standard_overnight']                  	= 'Standard Overnight';
$_['text_regular_pickup']                      	= 'Collection Normale';
$_['text_request_courier']                     	= 'Réserver une collection';
$_['text_drop_box']								= 'Drop Box';
$_['text_business_service_center']            	= 'Business Service Center';
$_['text_station']								= 'Station';
$_['text_fedex_envelope']                      	= 'FedEx Envellope';
$_['text_fedex_pak']							= 'FedEx Pak';
$_['text_fedex_box']							= 'FedEx Box';
$_['text_fedex_tube']                          	= 'FedEx Tube';
$_['text_fedex_10kg_box']                      	= 'FedEx 10 kg Box';
$_['text_fedex_25kg_box']                      	= 'FedEx 25 kg Box';
$_['text_your_packaging']                      	= 'Votre Emballage';
$_['text_list_rate']							= 'Liste des tarifs';
$_['text_account_rate']                        	= 'Tarifs avec compte';

// Entry
$_['entry_key']									= 'Clé :';
$_['entry_password']							= 'Mot de passe :';
$_['entry_account']								= 'N° de compte :';
$_['entry_meter']								= 'N° de compteur :';
$_['entry_postcode']							= 'Code postal :';
$_['entry_test']								= 'Mode de test :';
$_['entry_service']								= 'Services :';
$_['entry_dropoff_type']						= 'Type de dépôt :';
$_['entry_packaging_type']						= 'Type d’emballage :';
$_['entry_rate_type']							= 'Type de taux :';
$_['entry_display_time']						= 'Afficher le délai de livraison :<br /><span class="help">Voulez-vous afficher le délai de livraison ? (Ex. Livraison sous 3 à 5 jours)</span>';
$_['entry_display_weight']						= 'Afficher le poids de livraison :<br /><span class="help">Voulez-vous afficher le poids de livraison ? (Ex. Poids de livraison : 2,7674 Kgs)</span>';
$_['entry_weight_class']						= 'Classe de poids :<span class="help">Définir les kilos ou les grammes.</span>';
$_['entry_tax_class']							= 'Classe de taxes :';
$_['entry_geo_zone']							= 'Zone géographique :';
$_['entry_status']								= 'État :';
$_['entry_sort_order']							= 'Classement :';

// Error
$_['error_permission']							= 'Attention, vous n’avez pas la permission de modifier la livraison <b>Fedex</b> !';
$_['error_key']									= 'La <b>Cl&eacute</b> est requise !';
$_['error_password']							= 'Le <b>Mot de passe</b> est requis !';
$_['error_account']								= 'Le <b>Compte</b> est requis !.';
$_['error_meter']								= 'Le <b>Métre</b> est requis !';
$_['error_postcode']							= 'Le <b>Code postal</b> est requis !';
?>