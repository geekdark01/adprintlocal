<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// Heading
$_['heading_title']			= 'Australia Post';

// Text
$_['text_shipping']			= 'Livraison';
$_['text_success']			= 'Félicitations, vous avez modifié la livraison <b>Royal Mail</b> avec succès !';

// Entry
$_['entry_postcode']		= 'Code postal :';
$_['entry_express']			= 'Envoi express :';
$_['entry_standard']		= 'Envoi standard :';
$_['entry_display_time']	= 'Afficher le délai de livraison :<br /><span class="help">Voulez-vous afficher le délai de livraison ? (ex. Livraison en 3 à 5 jours)</span>';
$_['entry_weight_class']	= 'Classe de poids :<span class="help">Positionner sur grammes.</span>';
$_['entry_tax_class']		= 'Classe de taxes :';
$_['entry_geo_zone']		= 'Zone géographique :';
$_['entry_status']			= 'État :';
$_['entry_sort_order']		= 'Classement:';

// Error
$_['error_permission']		= 'Attention, vous n’avez pas la permission de modifier la livraison <b>Royal Mail</b> !';
$_['error_postcode']		= 'Le code postal doit être composé de 4 chiffres !';
?>