<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// Heading
$_['heading_title']		= 'Livraison Gratuite';

// Text
$_['text_shipping']		= 'Livraison';
$_['text_success']		= 'Félicitations, vous avez modifié la <b>Livraison Gratuite</b> avec succès !';

// Entry
$_['entry_total']		= 'Total : <br /><span class="help">Sous-Total nécessaire avant que le module de livraison gratuite ne soit disponible.</span>';
$_['entry_geo_zone']	= 'Zone géographique :';
$_['entry_status']		= 'État :';
$_['entry_sort_order']	= 'Classement :';

// Error
$_['error_permission']	= 'Attention, vous n’avez pas la permission de modifier la <b>Livraison Gratuite</b> !';
?>