<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// Heading
$_['heading_title']		= 'Taux Fixe';

// Text
$_['text_shipping']		= 'Livraison';
$_['text_success']		= 'Félicitations, vous avez modifié la livraison à <b>Taux fixe</b> avec succès !';

// Entry
$_['entry_cost']		= 'Coût :';
$_['entry_tax_class']	= 'Classe de taxes :';
$_['entry_geo_zone']	= 'Zone géographique :';
$_['entry_status']		= 'État :';
$_['entry_sort_order']	= 'Classement :';

// Error
$_['error_permission']	= 'Attention, vous n’avez pas la permission de modifier la livraison à <b>Taux fixe</b> !';
?>