<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// Heading
$_['heading_title']		= 'Citylink';

// Text
$_['text_shipping']		= 'Livraison';
$_['text_success']		= 'Félicitations, vous avez modifié la livraison <b>Citylink shipping</b> !';

// Entry
$_['entry_rate']		= 'Tarifs Citylink :<br /><span class="help">Entrez les valeurs jusqu’à 5,2 décimales. (12345,67) Exemple : .1:1, .25:1.27 - Poids inférieur ou égal à 0,1 kg coûte 1.00, poids inférieur ou égal à 0,25 g mais plus de 0,1 kg coûte 1.27. Ne pas entrer KG ou des symboles.</span>';
$_['entry_tax_class']	= 'Classe de taxes :';
$_['entry_geo_zone']	= 'Zone géographique :';
$_['entry_status']		= 'État :';
$_['entry_sort_order']	= 'Classement :';

// Error
$_['error_permission']	= 'Attention, vous n’avez pas la permission de modifier la livraison <b>Citylink shipping</b> !';
?>