<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// Heading
$_['heading_title']		= 'Livraison Basée sur le Poids';

// Text
$_['text_shipping']		= 'Livraison';
$_['text_success']		= 'Félicitations, vous avez modifié la <b>Livraison basée sur le poids</b> avec succès !';

// Entry
$_['entry_rate']		= 'Taux : <br /><span class="help">Exemple : 5:10.00,7:12.00 Poids:Coût,Poids:Coût, etc..</span>';
$_['entry_tax_class']	= 'Classe de Taxe :';
$_['entry_geo_zone']	= 'Zone géographique :';
$_['entry_status']		= 'État :';
$_['entry_sort_order']	= 'Classement :';

// Error
$_['error_permission']	= 'Attention, vous n’avez pas la permission de modifier la <b>Livraison basée sur le poids</b> !';
?>