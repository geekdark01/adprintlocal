<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// Heading
$_['heading_title']							= 'Royal Mail';

// Text
$_['text_shipping']							= 'Livraison';
$_['text_success']							= 'Félicitations, vous avez modifié la livraison <b>Royal Mail</b> avec succès !';

// Entry
$_['entry_rate']							= 'Tarifs :<br /><span class="help">Exemple: 5:10.00,7:12.00 Poids:Coût,Poids:Coût, etc..</span>';
$_['entry_insurance']						= 'Tarifs de compensation :<br /><span class="help">Entrez des valeurs jusqu’à 5,2 décimales (12345.67). Exemple : 34:0,100:1,250:2.25 - L’assurance couvre les valeurs du panier jusqu’&agrave 34 et coûte 0,00 supplémentaires, les valeurs comprises entre 100 et 250 coûte 2,25 supplémentaires. Ne pas entrer les symboles monétaires.</span>';
$_['entry_airmail_rate_1']					= 'Tarifs :<br /><span class="help">Exemple : 5:10.00,7:12.00 Poids:Coût,Poids:Coût, etc..<br /><br />Ces tarifs ne seront appliqué que pour les pays ci-dessous:<br />AL, AD, AM, AT, AZ, BY, BE, BA, BG, HR, CY, CZ, DK, EE, FO, FI, FR, GE, DE, GI, GR, GL, HU, IS, IE, IT, KZ, KG, LV, LI, LT, LU, MK, MT, MD, MC, NL, NO, PL, PT, RO, RU, SM, SK, SI, ES, SE, CH, TJ, TR, TM, UA, UZ, VA</span>';
$_['entry_airmail_rate_2']					= 'Tarifs :<br /><span class="help">Exemple : 5:10.00,7:12.00 Poids:Coût,Poids:Coût, etc..<br /><br />Ces tarifs seront appliqué à tous les autres pays ne se trouvant pas dans la liste ci-dessus.</span>';
$_['entry_international_signed_rate_1']		= 'Tarifs :<br /><span class="help">Exemple : 5:10.00,7:12.00 Poids:Coût,Poids:Coût, etc..<br /><br />Ces tarifs ne seront appliqué que pour les pays ci-dessous:<br />AL, AD, AM, AT, AZ, BY, BE, BA, BG, HR, CY, CZ, DK, EE, FO, FI, FR, GE, DE, GI, GR, GL, HU, IS, IE, IT, KZ, KG, LV, LI, LT, LU, MK, MT, MD, MC, NL, NO, PL, PT, RO, RU, SM, SK, SI, ES, SE, CH, TJ, TR, TM, UA, UZ, VA</span>';
$_['entry_international_signed_insurance_1']= 'Tarifs de compensation :<br /><span class="help">Entrez des valeurs jusqu’à 5,2 décimales (12345.67). Exemple : 34:0,100:1,250:2.25 - L’assurance couvre les valeurs du panier jusqu’&agrave 34 et coûte 0,00 supplémentaires, les valeurs comprises entre 100 et 250 coûte 2,25 supplémentaires. Ne pas entrer les symboles monétaires.<br /><br />Ces tarifs ne seront appliqué que pour les pays ci-dessous:<br />AL, AD, AM, AT, AZ, BY, BE, BA, BG, HR, CY, CZ, DK, EE, FO, FI, FR, GE, DE, GI, GR, GL, HU, IS, IE, IT, KZ, KG, LV, LI, LT, LU, MK, MT, MD, MC, NL, NO, PL, PT, RO, RU, SM, SK, SI, ES, SE, CH, TJ, TR, TM, UA, UZ, VA</span>';
$_['entry_international_signed_rate_2']		= 'Tarifs :<br /><span class="help">Exemple : 5:10.00,7:12.00 Poids:Coût,Poids:Coût, etc..<br /><br />Ces tarifs seront appliqué à tous les autres pays ne se trouvant pas dans la liste ci-dessus.</span>';
$_['entry_international_signed_insurance_2']= 'Tarifs de compensation :<br /><span class="help">Entrez des valeurs jusqu’à 5,2 décimales (12345.67). Exemple : 34:0,100:1,250:2.25 - L’assurance couvre les valeurs du panier jusqu’&agrave 34 et coûte 0,00 supplémentaires, les valeurs comprises entre 100 et 250 coûte 2,25 supplémentaires. Ne pas entrer les symboles monétaires.<br /><br />Ces tarifs seront appliqué à tous les autres pays ne se trouvant pas dans la liste ci-dessus.</span>';
$_['entry_airsure_rate_1']					= 'Tarifs :<br /><span class="help">Exemple : 5:10.00,7:12.00 Poids:Coût,Poids:Coût, etc..<br /><br />Ces tarifs ne seront appliqué que pour les pays ci-dessous:<br />AD, AT, BE, CH, DE, DK, ES, FO, FI, FR, IE, IS, LI, LU, MC, NL, PT, SE</span>';
$_['entry_airsure_insurance_1']				= 'Tarifs de compensation :<br /><span class="help">Entrez des valeurs jusqu’à 5,2 décimales (12345.67). Exemple : 34:0,100:1,250:2.25 - L’assurance couvre les valeurs du panier jusqu’&agrave 34 et coûte 0,00 supplémentaires, les valeurs comprises entre 100 et 250 coûte 2,25 supplémentaires. Ne pas entrer les symboles monétaires.<br /><br />Ces tarifs ne seront appliqué que pour les pays ci-dessous:<br />AD, AT, BE, CH, DE, DK, ES, FO, FI, FR, IE, IS, LI, LU, MC, NL, PT, SE</span>';
$_['entry_airsure_rate_2']					= 'Tarifs :<br /><span class="help">Exemple : 5:10.00,7:12.00 Poids:Coût,Poids:Coût, etc..<br /><br />Ces tarifs ne seront appliqué que pour les pays ci-dessous:<br />BR, CA, HK, MY, NZ, SG, US</span>';
$_['entry_airsure_insurance_2']				= 'Tarifs de compensation :<br /><span class="help">Entrez des valeurs jusqu’à 5,2 décimales (12345.67). Exemple : 34:0,100:1,250:2.25 - L’assurance couvre les valeurs du panier jusqu’&agrave 34 et coûte 0,00 supplémentaires, les valeurs comprises entre 100 et 250 coûte 2,25 supplémentaires. Ne pas entrer les symboles monétaires.<br /><br />Ces tarifs ne seront appliqué que pour les pays ci-dessous:<br />BR, CA, HK, MY, NZ, SG, US</span>';
$_['entry_display_weight']					= 'Afficher le poids de livraison :<br /><span class="help">Voulez-vous afficher le poids d’expédition ? (par exemple : Poids de livraison : 2,7674 kgs)</span>';
$_['entry_display_insurance']				= 'Afficher l’assurance :<br /><span class="help">Voulez-vous afficher l’assurance ? (par exemple : Assuré jusqu’à 500)</span>';
$_['entry_weight_class']					= 'Classe de poids :';
$_['entry_tax_class']						= 'Classe de taxes :';
$_['entry_geo_zone']						= 'Zone géographique :';
$_['entry_status']							= 'État :';
$_['entry_sort_order']						= 'Classement :';

// Tab
$_['tab_1st_class_standard']				= 'Courrier standard de première classe';
$_['tab_1st_class_recorded']				= 'Courrier enregistré de première classe';
$_['tab_2nd_class_standard']				= 'Courrier standard de seconde classe';
$_['tab_2nd_class_recorded']				= 'Courrier enregistré de seconde classe';
$_['tab_special_delivery_500']				= 'Livraison spéciale Jour suivant (&pound; 500)';
$_['tab_special_delivery_1000']				= 'Livraison spéciale Jour suivant (&pound; 1000)';
$_['tab_special_delivery_2500']				= 'Livraison spéciale Jour suivant (&pound; 2500)';
$_['tab_standard_parcels']					= 'Colis standard';
$_['tab_airmail']							= 'Poste aérienne';
$_['tab_international_signed']				= 'International avec signature';
$_['tab_airsure']							= 'Airsure';
$_['tab_surface']							= 'Surface';

// Error
$_['error_permission']						= 'Attention, vous n’avez pas la permission de modifier la livraison <b>Royal Mail</b> !';
?>