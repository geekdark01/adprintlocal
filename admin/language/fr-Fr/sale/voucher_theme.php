<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// Heading
$_['heading_title']		= 'Thèmes des chèques-cadeaux';

// Text
$_['text_success']		= 'Félicitations, vous avez sauvegardé les <b>Thèmes des chèques-cadeaux</b> avec succès !';
$_['text_image_manager']= 'Gestionnaire d’images';
$_['text_browse']		= 'Parcourir les fichiers';
$_['text_clear']		= 'Nettoyer l’image';

// Column
$_['column_name']		= 'Nom du thème';
$_['column_action']		= 'Action';

// Entry
$_['entry_name']		= 'Nom du thème :';
$_['entry_description']	= 'Description du thème :';
$_['entry_image']		= 'Image :';

// Error
$_['error_permission']	= 'Attention, vous n’avez pas la permission de modifier les <b>Thèmes des chèques-cadeaux</b> !';
$_['error_name']		= 'Le <b>Nom du thème</b> doit être composé de 3 à 32 caractères !';
$_['error_image']		= 'Attention, l’image est requise !';
$_['error_voucher']		= 'Attention, ce thème ne peut pas être supprimée car elle est actuellement affectée à %s chèques-cadeaux !';
?>