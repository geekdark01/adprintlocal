<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// Heading
$_['heading_title']		= 'Liste noire des IP clients';

// Text
$_['text_success']		= 'Félicitations, vous avez modifié la <b>Liste noire des IP clients</b> avec succès !';

// Column
$_['column_ip']			= 'IP';
$_['column_customer']	= 'Clients';
$_['column_action']		= 'Action';

// Entry
$_['entry_ip']			= 'IP :';

// Error
$_['error_permission']	= 'Attention, vous n’avez pas la permission de modifier la <b>Liste noire des IP clients</b> !';
$_['error_ip']			= 'L’<b>IP</b> doit être composé de 1 à 15 caractères !';
?>