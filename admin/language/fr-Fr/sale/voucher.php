<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// Heading
$_['heading_title']		= 'Chèques-cadeaux';

// Text
$_['text_send']			= 'Envoyer';
$_['text_success']		= 'Félicitations, vous avez modifié les <b>Chèques-cadeaux</b> avec succès !';
$_['text_sent']			= 'Félicitations, le courriel signifiant le <b>Chèque-cadeau</b> a été envoyé avec succès !';
$_['text_wait']			= 'Veuillez patienter !';

// Column
$_['column_name']		= 'Nom du chèque-cadeau';
$_['column_code']		= 'Code';
$_['column_from']		= 'De';
$_['column_to']			= 'À';
$_['column_theme']		= 'Thème';
$_['column_amount']		= 'Montant';
$_['column_status']		= 'État';
$_['column_order_id']	= 'N° de commande';
$_['column_customer']	= 'Client';
$_['column_date_added']	= 'Date d’ajout';
$_['column_action']		= 'Action';

// Entry
$_['entry_code']		= 'Code :<br /><span class="help">Le code du client doit être entré pour activer le chèque-cadeau.</span>';
$_['entry_from_name']	= 'Nom de l’expéditeur :';
$_['entry_from_email']	= 'Courriel de l’expéditeur :';
$_['entry_to_name']		= 'Nom du destinataire :';
$_['entry_to_email']	= 'Courriel du destinataire :';
$_['entry_theme']		= 'Thème :';
$_['entry_message']		= 'Message :';
$_['entry_amount']		= 'Montant :';
$_['entry_status']		= 'État :';

// Error
$_['error_permission']	= 'Attention, vous n’avez pas la permission de modifier les <b>Chèques-cadeaux</b> !';
$_['error_exists']		= 'Le code du chèque cadeau est déjà utilis&eacute !';
$_['error_code']		= 'Le <b>code</b> doit être composé de 3 à 10 caractères !';
$_['error_to_name']		= 'Le <b>Nom du destinataire</b> doit être composé de 1 à 64 caractères !';
$_['error_from_name']	= 'Le <b>Nom de l’expéditeur</b> doit être composé de 1 à 64 caractères !';
$_['error_email']		= 'L’<b>Adresse courriel</b> ne semble pas valide !';
$_['error_amount']		= 'Le montant doit être supérieur ou égal à 1 !';
$_['error_order']		= 'Attention, ce chèque cadeau ne peut pas être supprimé car il fait partie d’une <a href="%s">commande</a>!';
?>
