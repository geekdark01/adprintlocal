<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// Heading
$_['heading_title']				= 'Affiliation';

// Text
$_['text_success']				= 'Félicitations, vous avez modifié l’<b>Affiliation</b> avec succès !';
$_['text_approved']				= 'Vous avez approuvé le compte de %s !';
$_['text_wait']					= 'Veuillez patienter !';
$_['text_balance']				= 'Balance :';
$_['text_cheque']				= 'Chèque';
$_['text_paypal']				= 'PayPal';
$_['text_bank']					= 'Virement bancaire';

// Column
$_['column_name']				= 'Nom de l’affilié';
$_['column_email']				= 'Courriel';
$_['column_code']				= 'Code de suivi';
$_['column_balance']			= 'Balance';
$_['column_status']				= 'État';
$_['column_approved']			= 'Approuvé';
$_['column_date_added']			= 'Date d’ajout';
$_['column_description']		= 'Description';
$_['column_amount']				= 'Montant';
$_['column_action']				= 'Action';

// Entry
$_['entry_firstname']			= 'Prénom :';
$_['entry_lastname']			= 'Nom :';
$_['entry_email']				= 'Courriel :';
$_['entry_telephone']			= 'Téléphone :';
$_['entry_fax']					= 'Fax :';
$_['entry_status']				= 'État :';
$_['entry_password']			= 'Mot de passe :';
$_['entry_confirm']				= 'Confirmation :';
$_['entry_company']				= 'Société :';
$_['entry_address_1']			= 'Adresse :';
$_['entry_address_2']			= 'Complément d’adresse :';
$_['entry_city']				= 'Ville :';
$_['entry_postcode']			= 'Code postal :';
$_['entry_country']				= 'Pays :';
$_['entry_zone']				= 'Département :';
$_['entry_code']				= 'Code de suivi :<span class="help">Le code de suivi qui sera utilisé pour le suivi des parrainages.</span>';
$_['entry_commission']			= 'Commission (%):<span class="help">Pourcentage que l’affilié re&ccedil;oit sur chaque commande.</span>';
$_['entry_tax']					= 'Numéro d’identification fiscale :';
$_['entry_payment']				= 'Mode de paiement :';
$_['entry_cheque']				= 'Nom du bénéficiaire des chèques:';
$_['entry_paypal']				= 'Compte Courriel PayPal :';
$_['entry_bank_name']			= 'Nom de la banque :';
$_['entry_bank_branch_number']	= 'Numéro ABA/BSB (Direction générale) :';
$_['entry_bank_swift_code']		= 'Code SWIFT :';
$_['entry_bank_account_name']	= 'Nom du compte :';
$_['entry_bank_account_number']	= 'Numéro de compte :';
$_['entry_amount']				= 'Montant :';
$_['entry_description']     	= 'Description :';

// Error
$_['error_permission']			= 'Attention, vous n’avez pas la permission de modifier les <b>Clients</b> !';
$_['error_exists']				= 'Attention, l’adresse courriel est déjà enregistrée';
$_['error_firstname']			= 'Le <b>Prénom</b> doit être composé de 1 à 32 caractères !';
$_['error_lastname']			= 'Le <b>Nom</b> doit être composé de 1 à 32 caractères !';
$_['error_email']				= 'L’<b>Adresse courriel</b> ne semble pas valide !';
$_['error_telephone']			= 'Le <b>Téléphone</b> doit être composé de 3 à 32 caractères !';
$_['error_password']			= 'Le <b>Mot de passe</b> doit être composé de 4 à 20 caractères !';
$_['error_confirm']				= 'Le <b>mot de passe et la confirmation du mot de passe</b> ne correspondent pas !';
$_['error_address_1']			= 'L’<b>Adresse</b> doit être composé de 1 à 128 caractères !';
$_['error_city']				= 'La <b>Ville</b> doit être composé de 2 à 128 caractères !';
$_['error_postcode']			= 'Le <b>Code postal</b> doit être composé de 2 à 10 caractères pour cette ville !';
$_['error_country']				= 'Veuiller sélectionner un pays !';
$_['error_zone']				= 'Veuiller sélectionner un département !';
$_['error_code']				= 'Attention, le code de suivi  est requis !';
?>