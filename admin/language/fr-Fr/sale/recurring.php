<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// Heading
$_['heading_title']							= 'Profils de paiement';

// Text
$_['text_success']							= 'Félicitation, vous avez modifié les <b>Profils de paiement</b> avec succès !';
$_['text_payment_profiles']					= 'Profils de paiement';
$_['text_status_active']					= 'Actif';
$_['text_status_inactive']					= 'Inactif';
$_['text_status_cancelled']					= 'Annulé';
$_['text_status_suspended']					= 'Suspendu';
$_['text_status_expired']					= 'Expiré';
$_['text_status_pending']					= 'En attente';
$_['text_transactions']						= 'Transactions';
$_['text_return']							= 'Retour';
$_['text_cancel']							= 'Annuler';
$_['text_filter']							= 'Filtrer';
$_['text_cancel_confirm']					= 'L’annulation du profil ne peut pas être annulé ! êtes-vous sûr de vouloir le faire ?';
$_['text_transaction_created']				= 'Créé';
$_['text_transaction_payment']				= 'Paiement';
$_['text_transaction_outstanding_payment']	= 'Paiement impayé';
$_['text_transaction_skipped']				= 'Paiement ignoré';
$_['text_transaction_failed']				= 'Paiement échoué';
$_['text_transaction_cancelled']			= 'Annulé';
$_['text_transaction_suspended']			= 'Suspendu';
$_['text_transaction_suspended_failed']		= 'Suspendu pour paiement échoué';
$_['text_transaction_outstanding_failed']	= 'Impayé pour paiement échoué';
$_['text_transaction_expired']				= 'Expiré';

// Entry
$_['entry_cancel_payment']					= 'Paiement annulé :';
$_['entry_order_recurring']					= 'ID';
$_['entry_order_id']						= 'N° de commande';
$_['entry_payment_reference']				= 'Référence paiement';
$_['entry_customer']						= 'Client';
$_['entry_date_created']					= 'Date de création';
$_['entry_status']							= 'État';
$_['entry_type']							= 'Type';
$_['entry_action']							= 'Action';
$_['entry_email']							= 'Courriel';
$_['entry_profile_description']				= 'Description du profil';
$_['entry_product']							= 'Produit :';
$_['entry_quantity']						= 'Quantité :';
$_['entry_amount']							= 'Montant :';
$_['entry_profile']							= 'Profil :';
$_['entry_payment_type']					= 'Mode de paiement :';

// Error
$_['error_not_cancelled']					= 'Erreur : %s';
$_['error_not_found']						= 'Impossible d’annuler le profil';
$_['success_cancelled']						= 'Le paiement récurrent a été annulé';
?>