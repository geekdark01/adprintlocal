<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// Heading
$_['heading_title']				= 'Groupe clients';

// Text
$_['text_success']				= 'Félicitations, vous avez modifié le <b>Groupe clients</b> avec succès !';

// Column
$_['column_name']				= 'Nom du groupe clients';
$_['column_sort_order']			= 'Ordre de tri';
$_['column_action']				= 'Action';

// Entry
$_['entry_name']				= 'Nom du groupe clients :';
$_['entry_description']         = 'Description :';
$_['entry_approval']            = 'Approuver de nouveaux clients :<br /><span class="help">Les clients doivent être approuvés par l’administrateur avant de pouvoir se connecter.</span>';
$_['entry_company_id_display']  = 'Afficher l’identifiant société :<br /><span class="help">Afficher une société sans identifiant.</span>';
$_['entry_company_id_required'] = 'Identifiant société requis :<br /><span class="help">Sélectionnez les groupes de clients qui doivent entrer leur N° d’entreprise pour les adresses de facturation avant paiement.</span>';
$_['entry_tax_id_display']      = 'Afficher l’identifiant taxe :<br /><span class="help">Afficher une identification fiscale pour les adresses de facturation.</span>';
$_['entry_tax_id_required']     = 'Identifiant taxe requis :<br /><span class="help">Sélectionnez les groupes de clients qui doivent entrer leur numéro d’identification fiscale pour les adresses de facturation avant paiement.</span>';
$_['entry_sort_order']			= 'Classement :';

// Error
$_['error_permission']			= 'Attention, vous n’avez pas la permission de modifier le <b>Groupe clients</b> !';
$_['error_name']				= 'Le <b>Nom du groupe clients</b> doit être composé de 3 à 32 caractères!';
$_['error_default']				= 'Attention, ce groupe clients ne peut pas être supprimé, car il est actuellement assigné au groupe clients par défaut !';
$_['error_store']				= 'Attention, ce groupe clients ne peut pas être supprimé, car il est actuellement assigné à %s boutiques !';
$_['error_customer']			= 'Attention, ce groupe clients ne peut pas être supprimé, car il est actuellement assigné à %s clients !';
?>