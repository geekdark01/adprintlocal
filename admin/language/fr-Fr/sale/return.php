<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// Heading
$_['heading_title']			= 'Retours produits';

// Text
$_['text_opened']			= 'Ouvert';
$_['text_unopened']			= 'Non ouvert';
$_['text_success']			= 'Félicitations, vous avez modifié les <b>Retours produits</b> avec succès !!';
$_['text_wait']				= 'Veuillez patienter !';

// Text
$_['text_return_id']		= 'N° de retour :';
$_['text_order_id']			= 'N° de commande :';
$_['text_date_ordered']		= 'Date de commande :';
$_['text_customer']			= 'Client :';
$_['text_email']			= 'Courriel :';
$_['text_telephone']		= 'Téléphone :';
$_['text_return_status']	= 'État de retour :';
$_['text_date_added']		= 'Date d’ajout :';
$_['text_date_modified']	= 'Date de modification :';
$_['text_product']			= 'Produit :';
$_['text_model']			= 'Modèle :';
$_['text_quantity']			= 'Quantité :';
$_['text_return_reason']	= 'Raison du retour :';
$_['text_return_action']	= 'Action du retour :';
$_['text_comment']			= 'Commentaires :';

// Column
$_['column_return_id']		= 'N° de retour';
$_['column_order_id']		= 'N° de commande';
$_['column_customer']		= 'Client';
$_['column_product']		= 'Produit';
$_['column_model']			= 'Modèle';
$_['column_status']			= 'État';
$_['column_date_added']		= 'Date d’ajout';
$_['column_date_modified']	= 'Date de modification';
$_['column_comment']		= 'Commentaires';
$_['column_notify']			= 'Client notifié';
$_['column_action']			= 'Action';

// Entry
$_['entry_customer']		= 'Client :';
$_['entry_order_id']		= 'N° de commande :';
$_['entry_date_ordered']	= 'Date de commande :';
$_['entry_firstname']		= 'Prénom :';
$_['entry_lastname']		= 'Nom :';
$_['entry_email']			= 'Courriel :';
$_['entry_telephone']		= 'Téléphone :';
$_['entry_product']			= 'Produit :';
$_['entry_model']			= 'Modèle :';
$_['entry_quantity']		= 'Quantité :';
$_['entry_reason']			= 'Raison du retour :';
$_['entry_opened']			= 'Ouverture :';
$_['entry_comment']			= 'Commentaires :';
$_['entry_return_status']	= 'État du retour :';
$_['entry_notify']			= 'Client notifié :';
$_['entry_action']			= 'Action du retour :';

// Error
$_['error_warning']			= 'Attention, veuillez vérifier soigneusement le formulaire afin qu’il n’y ai pas d’erreurs !';
$_['error_permission']		= 'Attention, vous n’avez pas la permission de modifier les <b>Retours produits</b> !';
$_['error_order_id']		= 'Le N° de commande est requis !';
$_['error_firstname']		= 'Le <b>Prénom</b> doit être composé de 1 à 32 caractères !';
$_['error_lastname']		= 'Le <b>Nom</b> doit être composé de 1 à 32 caractères !';
$_['error_email']			= 'L’<b>Adresse courriel</b> ne semble pas valide !';
$_['error_telephone']		= 'Le <b>Téléphone</b> doit être composé de 3 à 32 caractères !';
$_['error_product']			= 'Le <b>Nom du produit</b> doit être composé de 3 à 255 caractères !';
$_['error_model']			= 'Le <b>Modèle</b> doit être composé de 3 à 64 caractères !';
?>