<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// Heading
$_['heading_title']			= 'Clients';

// Text
$_['text_success']			= 'Félicitations, vous avez modifié les <b>Clients</b> avec succès !';
$_['text_default']			= 'Défaut';
$_['text_approved']			= 'Vous avez approuvé le compte de %s !';
$_['text_wait']				= 'Veuillez patienter !';
$_['text_balance']			= 'Balance :';
$_['text_add_ban_ip']		= 'Ajout de l’IP bannie à la liste noire';
$_['text_remove_ban_ip']	= 'Retrait de l’IP bannie de la liste noire';

// Column
$_['column_name']			= 'Nom du client';
$_['column_email']			= 'Courriel';
$_['column_customer_group']	= 'Groupe client';
$_['column_status']			= 'État';
$_['column_login']			= 'Connexion au compte';
$_['column_approved']		= 'Approuvé';
$_['column_date_added']		= 'Date d’ajout';
$_['column_comment']        = 'Commentaires';
$_['column_description']	= 'Description';
$_['column_amount']			= 'Montant';
$_['column_points']			= 'Points';
$_['column_ip']				= 'IP';
$_['column_total']			= 'Total des comptes';
$_['column_action']			= 'Action';

// Entry
$_['entry_firstname']		= 'Prénom :';
$_['entry_lastname']		= 'Nom :';
$_['entry_email']			= 'Courriel :';
$_['entry_telephone']		= 'Téléphone :';
$_['entry_fax']				= 'Fax :';
$_['entry_newsletter']		= 'Lettre d’information :';
$_['entry_customer_group']	= 'Groupe clients :';
$_['entry_status']			= 'État :';
$_['entry_password']		= 'Mot de passe :';
$_['entry_confirm']			= 'Confirmation :';
$_['entry_company']			= 'Société :';
$_['entry_company_id']      = 'Identifiant société :';
$_['entry_tax_id']          = 'Identifiant taxe :';
$_['entry_address_1']		= 'Adresse :';
$_['entry_address_2']		= 'Complément d’adresse :';
$_['entry_city']			= 'Ville :';
$_['entry_postcode']		= 'Code postal :';
$_['entry_country']			= 'Pays :';
$_['entry_zone']			= 'Département :';
$_['entry_default']			= 'Adresse par défaut :';
$_['entry_comment']         = 'Commentaires:';
$_['entry_description']		= 'Description :';
$_['entry_amount']			= 'Montant :';
$_['entry_points']			= 'Points :';

// Error
$_['error_warning']			= 'Attention, veuillez vérifier soigneusement le formulaire afin qu’il n’y ai pas d’erreurs !';
$_['error_permission']		= 'Attention, vous n’avez pas la permission de modifier les <b>Clients</b> !';
$_['error_exists']			= 'Attention, l’adresse courriel est déjà enregistrée !';
$_['error_firstname']		= 'Le <b>Prénom</b> doit être composé de 1 à 32 caractères !';
$_['error_lastname']		= 'Le <b>Nom</b> doit être composé de 1 à 32 caractères !';
$_['error_email']			= 'L’<b>Adresse courriel</b> ne semble pas valide !';
$_['error_telephone']		= 'Le <b>Téléphone</b> doit être composé de 3 à 32 caractères !';
$_['error_password']		= 'Le <b>Mot de passe</b> doit être composé de 3 à 20 caractères !';
$_['error_confirm']			= 'Le <b>mot de passe et la confirmation du mot de passe</b> ne correspondent pas !';
$_['error_company_id']      = 'Identifiant société requis !';
$_['error_tax_id']          = 'Identifiant taxe requis !';
$_['error_vat']             = 'Numéro de TVA invalide !';
$_['error_address_1']		= 'L’<b>Adresse</b> doit être composé de 1 à 128 caractères !';
$_['error_city']			= 'La <b>Ville</b> doit être composé de 1 à 128 caractères !';
$_['error_postcode']		= 'Le <b>code postal</b> doit être composé de 2 à 10 caractères pour ce pays !';
$_['error_country']			= 'Veuiller sélectionner un pays !';
$_['error_zone']			= 'Veuiller sélectionner un département !';
?>