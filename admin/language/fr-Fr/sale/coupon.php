<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// Heading
$_['heading_title']			= 'Bon de réduction';

// Text
$_['text_success']			= 'Félicitations, vous avez modifié le <b>Bon de réduction</b> avec succès !';
$_['text_percent']			= 'Pourcentage';
$_['text_amount']			= 'Montant fixe';

// Column
$_['column_name']			= 'Nom du bon de réduction';
$_['column_code']			= 'Code';
$_['column_discount']		= 'Remise';
$_['column_date_start']		= 'Date de début';
$_['column_date_end']		= 'Date de fin';
$_['column_status']			= 'État';
$_['column_order_id']		= 'Numéro de commande';
$_['column_customer']		= 'Client';
$_['column_amount']			= 'Montant';
$_['column_date_added']		= 'Date d’ajoût';
$_['column_action']			= 'Action';

// Entry
$_['entry_name']			= 'Nom du bon de réduction :';
$_['entry_code']			= 'Code :<br /><span class="help">Code client pour l’obtention de la remise</span>';
$_['entry_type']			= 'Type :<br /><span class="help">Pourcentage ou montant fixe</span>';
$_['entry_discount']		= 'Remise :';
$_['entry_logged']			= 'Connexion client :<br /><span class="help">Le client doit être connecté pour utiliser le bon de réduction.</span>';
$_['entry_shipping']		= 'Livraison gratuite :';
$_['entry_total']			= 'Montant total :<br /><span class="help">Montant total devant être pris en compte pour l’obtention de la remise</span>';
$_['entry_category']		= 'Catégorie :<br /><span class="help">Choisir tous les produits de la catégorie sélectionnée.</span>';
$_['entry_product']			= 'Produits :<br /><span class="help">Sélectionner des produits spécifiques pour appliquer la réduction seulement à ceux-ci.<br />Ne pas sélectionner de produits pour appliquer la réduction à la totalité du panier.</span>';
$_['entry_date_start']		= 'Date de début :';
$_['entry_date_end']		= 'Date de fin :';
$_['entry_uses_total']		= 'Utilisation par bon de réduction :<br /><span class="help">Le nombre maximum de fois où le coupon peut être utilisé par n’importe quel client. Laissez vide pour un nombre illimité</span>';
$_['entry_uses_customer']	= 'Utilisation par le client :<br /><span class="help">Le nombre maximum de fois où le coupon peut être utilisé par un seul client. Laissez vide pour un nombre illimité</span>';
$_['entry_status']			= 'État :';

// Error
$_['error_permission']		= 'Attention, vous n’avez pas l’autorisation de modifier les <b>Bons de réduction</b> !';
$_['error_exists']			= 'Attention, le code du coupon est déjà en cours d’utilisation!!';
$_['error_name']			= 'Le <b>Bon de réduction</b> doit être composé de 3 à 64 caractères !';
$_['error_code']			= 'Le code</b> doit être composé de 3 à 10 caractères !';
?>