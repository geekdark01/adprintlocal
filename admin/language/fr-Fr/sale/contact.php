<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// Heading
$_['heading_title']			= 'Courriel';

// Text
$_['text_success']			= 'Votre message a été envoyé avec succès !';
$_['text_sent']				= 'Votre message a bien été envoyé à %s sur %s destinataires !';
$_['text_default']			= 'Par défaut';
$_['text_newsletter']		= 'Tous les souscripteurs à la lettre d’information';
$_['text_customer_all']		= 'Tous les clients';
$_['text_customer_group']	= 'Groupe client';
$_['text_customer']			= 'Clients';
$_['text_affiliate_all']	= 'Tous les affiliés';
$_['text_affiliate']		= 'Affiliés';
$_['text_product']			= 'Produits';

// Entry
$_['entry_store']			= 'De :';
$_['entry_to']				= 'Pour :';
$_['entry_customer_group']	= 'Groupe client :';
$_['entry_customer']		= 'Client :';
$_['entry_affiliate']		= 'Affilié :';
$_['entry_product']			= 'Aux clients qui ont commandé des produits :';
$_['entry_subject']			= 'Sujet :';
$_['entry_message']			= 'Message :';

// Error
$_['error_permission']		= 'Vous n’avez pas l’autorisation d’envoyer des courriels';
$_['error_subject']			= 'Le <b>Sujet est requis !';
$_['error_message']			= 'Le <b>Message est requis !';
?>