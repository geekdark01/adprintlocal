<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//--------------------------------//

// Heading
$_['heading_title']		= 'Google Base';

// Text
$_['text_feed']			= 'Flux des produits';
$_['text_success']		= 'Félicitations, vous avez modifié le flux <b>Google Base</b> avec succès !';

// Entry
$_['entry_status']		= 'État :';
$_['entry_data_feed']	= 'Adresse URL des données du flux :<br/><span class="help">Cette URL pointe vers votre flux. Collez-le dans votre serveur de flux.</span>';

// Error
$_['error_permission']	= 'Attention, vous n’avez pas la permission de modifier le paiement <b>Google Base feed<:b> !';
?>