<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// Heading
$_['heading_title']					= 'Gestionnaire de boutique';

// Text
$_['text_success']					= 'Félicitations, vous avez modifié <b>La boutique</b> avec succès !';
$_['text_items']					= 'Articles';
$_['text_tax']						= 'Taxes';
$_['text_account']					= 'Compte';
$_['text_checkout']					= 'Paiement';
$_['text_stock']					= 'Stock';
$_['text_image_manager']			= 'Gestionnaire d’images';
$_['text_browse']					= 'Parcourir les fichiers';
$_['text_clear']					= 'Nettoyer l’image';
$_['text_shipping']					= 'Adresse de livraison';
$_['text_payment']					= 'Adresse de paiement';

// Column
$_['column_name']					= 'Nom de la boutique';
$_['column_url']					= 'URL de la boutique';
$_['column_action']					= 'Action';

// Entry
$_['entry_url']						= 'URL de la boutique :<br /><span class="help">URL complète de votre magasin. Assurez-vous d’ajouter "/" à la fin, exemple : http://www.yourdomain.com/path/</span>';
$_['entry_ssl']						= 'Utiliser le SSL : <br /><span class="help">Pour utiliser le SSL vérifiez avec votre hébergeur si un certificat SSL est installé.</span>';
$_['entry_name']					= 'Nom de la boutique :';
$_['entry_owner']					= 'Propriétaire de la boutique :';
$_['entry_address']					= 'Adresse :';
$_['entry_email']					= 'Courriel :';
$_['entry_telephone']				= 'Téléphone:';
$_['entry_fax']						= 'Fax:';
$_['entry_title']					= 'Titre :';
$_['entry_meta_description']		= 'Balise méta "Description" :';
$_['entry_layout']					= 'Disposition par défaut :';
$_['entry_template']				= 'Modèle graphique :';
$_['entry_country']					= 'Pays :';
$_['entry_zone']					= 'Département :';
$_['entry_language']				= 'Langue :';
$_['entry_currency']				= 'Devise :';
$_['entry_catalog_limit']			= 'Nombre d’articles affichés par défaut et par page dans le catalogue :<br /><span class="help">Détermine le nombre d’articles qui seront affichés par page dans le catalogue (produits, catégories, etc...)</span>';
$_['entry_tax']						= 'Voir les prix taxe incluse :';
$_['entry_tax_default']				= 'Utiliser l’adresse fiscale de la boutique :<br /><span class="help">Utilisez l’adresse de la boutique pour calculer les taxes, si personne n’est connecté. Vous pouvez choisir d’utiliser l’adresse de la boutique pour l’expédition aux clients ou à l’adresse de paiement.</span>';
$_['entry_tax_customer']			= 'Utiliser l’adresse fiscale du client :<br /><span class="help">Utiliser l’adresse par défaut des clients lorsqu’ils se connectent pour calculer les taxes. Vous pouvez choisir d’utiliser l’adresse par défaut pour l’expédition aux clients ou à l’adresse de paiement.</span>';
$_['entry_customer_group']			= 'Groupe clients :<br /><span class="help">Groupe clients par défaut.</span>';
$_['entry_customer_group_display']	= 'Groupe de clients :<br /><span class="help">Afficher les groupes de clients que les nouveaux clients peuvent choisir d’utiliser (comme vente en gros et affaires) lors de l’inscription.</span>';
$_['entry_customer_price']			= 'Voir les prix si identification :<br /><span class="help">Afficher les prix uniquement quand le client est connecté.</span>';
$_['entry_account']					= 'Politique de confidentialité à la création de compte :<br /><span class="help">Obligation pour les utilisateurs d’accepter la Politique de confidentialité à la création du compte client.</span>';
$_['entry_cart_weight']				= 'Afficher le poids dans la page du panier :';
$_['entry_guest_checkout']			= 'Commander si invité :<br /><span class="help">Permission accordée aux clients de commander sans créer de compte. Ne sera pas disponible si un produit téléchargeable est dans le panier.</span>';
$_['entry_checkout']				= '<acronym title="Conditions Générales de Vente">C.G.V</acronym> à la commande :<br /><span class="help">Obligation pour les clients d’accepter les Conditions Générales de Vente à la validation de leur commande.</span>';
$_['entry_order_status']			= 'État de la commande :<br /><span class="help">Définir l’état de commande par défaut quand une commande est traitée.</span>';
$_['entry_stock_display']			= 'Voir le stock :<br /><span class="help">Afficher la quantité du stock sur la page du produit.</span>';
$_['entry_stock_checkout']			= 'Commander malgré la rupture de stock :<br /><span class="help">Permission accordée aux clients de passer commande même si le produit commandé n’est pas en stock.</span>';
$_['entry_logo']					= 'Logo de la boutique :';
$_['entry_icon']					= 'Icône :<br /><span class="help">L’icône doit être en extension PNG et doit avoir ces dimensions : 16px x 16px.</span>';
$_['entry_image_category']			= 'Taille de l’image de la liste Catégories :';
$_['entry_image_thumb']				= 'Taille de la vignette du produit :';
$_['entry_image_popup']				= 'Taille de l’image du produit dans la fenêtre :';
$_['entry_image_product']			= 'Taille de l’image de la liste des produits :';
$_['entry_image_additional']		= 'Taille des images supplémentaires des produits :';
$_['entry_image_related']			= 'Taille des images des produits apparentés :';
$_['entry_image_compare']			= 'Taille de l’image du produit dans la comparaison :';
$_['entry_image_wishlist']			= 'Taille de l’image du produit dans la liste de souhaits :';
$_['entry_image_cart']				= 'Taille de l’image du produit dans le panier :';
$_['entry_secure']					= 'Utiliser le SSL : <br /><span class="help">Pour utiliser le SSL, vérifier avec votre hébergeur si un certificat SSL est installé et si l’adresse du SSL est ajoutée à votre fichier de configuration.</span>';

// Error
$_['error_warning']					= 'Attention, veuillez vérifier soigneusement le formulaire afin qu’il n’y ai pas d’erreurs !';
$_['error_permission']				= 'Attention, vous n’avez pas la permission de modifier les Boutiques!';
$_['error_name']					= 'Le <b>Nom de la boutique</b> doit être composé de 3 à 32 caractères !';
$_['error_owner']					= 'Le <b>Propriétaire de la boutique</b> doit être composé de 3 à 64 caractères !';
$_['error_address']					= 'L’<b>Adresse de la boutique</b> doit être composé de 10 à 256 caractères !';
$_['error_email']					= 'L’<b>Adresse courriel</b> ne semble pas être valide !';
$_['error_telephone']				= 'Le <b>Téléphone</b> doit être composé de 3 à 32 caractères !';
$_['error_url']						= 'Attention, l’URL de la boutique  est requise !';
$_['error_title']					= 'Le <b>Titre</b> doit être composé de 3 à 32 caractères !';
$_['error_limit']					= 'Attention, la Limite est requise !';
$_['error_customer_group_display']	= 'Vous devez inclure le groupe de clients par défaut si vous voulez pouvoir utiliser cette fonctionnalité!';
$_['error_image_thumb']				= 'Attention, la taille de la vignette du produit est requise !';
$_['error_image_popup']				= 'Attention, la taille de l’image du produit dans la fenêtre est requise !';
$_['error_image_product']			= 'Attention, la taille de l’image de la liste des produits est requise !';
$_['error_image_category']			= 'Attention, la taille de l’image de la liste des catégories est requise !';
$_['error_image_additional']		= 'Attention, la taille des images supplémentaires pour les produits est requise !';
$_['error_image_related']			= 'Attention, la taille des images pour les produits apparentés est requise !';
$_['error_image_compare']			= 'Attention, la taille de l’image du produit dans la comparaison est requise !';
$_['error_image_wishlist']			= 'Attention, la taille de l’image du produit dans la liste de souhaits est requise !';
$_['error_image_cart']				= 'Attention, la taille des images pour les produits dans le panier est requise !';
$_['error_default']					= 'Attention, vous ne pouvez pas supprimer la boutique par défaut !';
$_['error_store']					= 'Attention, cette boutique ne peut être supprimé car elle est assignée à %s commandes !';
?>