<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// Heading
$_['heading_title']					= 'Paramètres';

// Text
$_['text_success']					= 'Félicitations, vous avez sauvegardé les <b>Paramètres</b> avec succès !';
$_['text_items']					= 'Articles';
$_['text_product']					= 'Produits';
$_['text_voucher']					= 'Chèques-cadeaux';
$_['text_tax']						= 'Taxes';
$_['text_account']					= 'Compte';
$_['text_checkout']					= 'Commande';
$_['text_stock']					= 'Stock';
$_['text_affiliate']				= 'Affiliés';
$_['text_return']					= 'Retours';
$_['text_image_manager']			= 'Gestionnaire d’images';
$_['text_browse']					= 'Parcourir les fichiers';
$_['text_clear']					= 'Nettoyer l’image';
$_['text_shipping']					= 'Adresse de livraison';
$_['text_payment']					= 'Adresse de paiement';
$_['text_mail']						= 'Mail';
$_['text_smtp']						= 'SMTP';

// Entry
$_['entry_name']					= 'Nom de la boutique :';
$_['entry_owner']					= 'Propriétaire de la boutique :';
$_['entry_address']					= 'Adresse :';
$_['entry_email']					= 'Courriel :';
$_['entry_telephone']				= 'Téléphone :';
$_['entry_fax']						= 'Fax :';
$_['entry_title']					= 'Titre :<br /><span class="help">Longueur maximum de 55 caractères.</span>';
$_['entry_meta_description']		= 'Balise méta "Description" :';
$_['entry_layout']					= 'Mise en page par défaut :';
$_['entry_template']				= 'Modèle graphique :';
$_['entry_country']					= 'Pays :';
$_['entry_zone']					= 'Département :';
$_['entry_language']				= 'Langue :';
$_['entry_admin_language']			= 'Langue de l’administration :';
$_['entry_currency']				= 'Devise :<br /><span class="help">Changer la devise  par défaut. Videz le cache de votre navigateur pour voir le changement et réinitialiser le cookie existant.</span>';
$_['entry_currency_auto']			= 'Actualisation automatique du taux des devises : <br /><span class="help">Paramétrer votre boutique pour actualiser automatiquement les taux des devises quotidiennement.</span>';
$_['entry_length_class']			= 'Unité de longueur :';
$_['entry_weight_class']			= 'Unité de poids :';
$_['entry_catalog_limit']			= 'Nombre d’articles affichés par défaut et par page dans le catalogue:<br /><span class="help">Détermine le nombre d’articles qui seront affichés par page dans le catalogue (produits, catégories, etc...)</span>';
$_['entry_admin_limit']				= 'Nombre d’articles affichés par défaut et par page dans l’administration :<br /><span class="help">Détermine le nombre d’articles qui seront affichés par page (commandes, clients, etc...)</span>';
$_['entry_product_count']			= 'Compteur de produits par catégorie:<br /><span class="help">Afficher le nombre de produits à l&acute;intérieur des sous-catégories dans le menu de la catégorie principale. ATTENTION, cela va provoquer une baisse de performance extrême pour les magasins ayant un grand nombre de sous-catégories!</span>';
$_['entry_review']					= 'Autoriser les avis : <br /><span class="help">Activer - Désactiver l’entrée de nouveaux avis et l’affichage des commentaires existants</span>';
$_['entry_download']				= 'Autoriser les téléchargements :';
$_['entry_voucher_min']				= 'Minimum de chèque-cadeaux :<br /><span class="help">Montant minimum qu’un client peut acheter pour un chéque-cadeaux.</span>';
$_['entry_voucher_max']				= 'Maximum de chèque-cadeaux<br /><span class="help">Montant maximum qu’un client peut acheter pour un chèque-cadeaux.</span>';
$_['entry_tax']						= 'Afficher le prix <acronym title="Toutes Taxes Comprises">T.T.C.</acronym> :';
$_['entry_vat']						= 'Numéro de TVA valide :<br /><span class="help">Valider le numéro de TVA avec la service http://ec.europa.eu</span>';
$_['entry_tax_default']				= 'Utiliser l’adresse fiscale de la boutique :<br /><span class="help">Utilisez l’adresse de la boutique pour calculer les taxes, si personne n’est connecté. Vous pouvez choisir d’utiliser l’adresse de la boutique pour l’expédition aux clients ou à l’adresse de paiement.</span>';
$_['entry_tax_customer']			= 'Utiliser l’adresse fiscale du client :<br /><span class="help">Utiliser l’adresse par défaut des clients lorsqu’ils se connectent pour calculer les taxes. Vous pouvez choisir d’utiliser l’adresse par défaut pour l’expédition aux clients ou à l’adresse de paiement.</span>';
$_['entry_customer_online']			= 'Clients en ligne :<br /><span class="help">Suivi en ligne des clients via la section rapports client.</span>';
$_['entry_customer_group']			= 'Groupe de clients : <br /><span class="help">Choisisser le Groupe Client par défaut.</span>';
$_['entry_customer_group_display']	= 'Groupe de clients :<br /><span class="help">Affichage des groupes de clients que les nouveaux clients peuvent choisir d’utiliser (comme Vente en gros et Affaires) lors de l’inscription.</span>';
$_['entry_customer_price']			= 'Afficher le prix : <br /><span class="help">Affiche les prix uniquement lorsque le visiteur est connecté.</span>';
$_['entry_account']					= 'Modalités du compte : <br /> <span class="help">Force les clients à accepter les conditions avant que le compte ne soit créé.</span>';
$_['entry_cart_weight']				= 'Afficher le poids sur la page panier :<br /><span class="help">Afficher le poids du panier sur la page du panier</span>';
$_['entry_guest_checkout']			= 'Client au comptant :<br /><span class="help">Permission aux clients de commander sans créer de compte. Indisponible quand un produit téléchargeable est dans le panier.</span>';
$_['entry_checkout']				= 'Conditions Générales de Vente : <br /><span class="help">Oblige les clients à accepter les <b>Conditions Générales de Vente</b> avant que leur commande ne soit validée.</span>';
$_['entry_order_edit']				= 'Modification de commande :<br /><span class="help">Nombre de jours autorisés à modifier une commande. Cela est nécessaire car les prix et les remises peuvent changer au fil du temps provoquant des erreurs en cas de modification.</span>';
$_['entry_invoice_prefix']			= 'Préfixe de facture :<br /><span class="help">Régler le préfixe de facture (ex. INV-2012-000). Le numéro de facture va commencer à 1 pour chaque préfixe unique</span>';
$_['entry_order_status']			= 'Suivi de commande :<br /><span class="help">Régler le statut de la commande par défaut quand une commande est traitée.</span>';
$_['entry_complete_status']			= 'Suivi de commande complète :<br /><span class="help">Régler le statut de la commande que le client doit atteindre avant qu’il ne soient autorisé à accéder aux téléchargements et chèques cadeaux.</span>';
$_['entry_stock_display']			= 'Afficher le stock : <br /><span class="help">Afficher les quantités en stock sur la page du produit.</span>';
$_['entry_stock_warning']			= 'Voir l’avertissement de rupture de stock :<br /><span class="help">Affichage du message concernant le stock sur la page du panier, quand un produit est en rupture de stock. Il faut pour cela que la case <b>Commande en rupture</b> soit cochée à <b>Oui</b>.<br />Si ce n’est pas le cas, le message sera affiché en permanence.</span>';
$_['entry_stock_checkout']			= 'Commande en rupture : <br /><span class="help">Permettre aux clients de commander même si le produit qu’ils commandent n’est pas en stock.</span>';
$_['entry_stock_status']			= 'État du stock :<br /><span class="help">Définir la valeur par défaut sur l’état des stocks par l’édition du produits sélectionnés.</span>';
$_['entry_affiliate']				= 'Conditions d’affiliation :<br /><span class="help">Force à accepter les conditions d’affiliation avant qu’un compte de partenariat ne soit créé.</span>';
$_['entry_commission']				= 'Commission d’affiliation (%):<span class="help">Pourcentage de commission d’affiliation par défaut.</span>';
$_['entry_return']					= 'Conditions des retour :<br /><span class="help">Obliger les utilisateurs à accepter les conditions avant de créer un compte de retour.</span>';
$_['entry_return_status']			= 'État des retours :<br /><span class="help">Définir l’état de retour par défaut quand une demande de retour est présentée.</span>';
$_['entry_logo']					= 'Logo de la boutique :';
$_['entry_icon']					= 'Icône :<br /><span class="help">L’icône doit être au format PNG et de dimension 16px x 16px.</span>';
$_['entry_image_category']			= 'Taille des images pour les catégories :';
$_['entry_image_thumb']				= 'Taille des vignettes pour les produits :';
$_['entry_image_popup']				= 'Taille des images pour les produits :';
$_['entry_image_product']			= 'Taille des listes pour les produits :';
$_['entry_image_additional']		= 'Taille des images supplémentaires pour les produits :';
$_['entry_image_related']			= 'Taille des images pour les produits apparentés :';
$_['entry_image_compare']			= 'Taille des images pour la comparaison :';
$_['entry_image_wishlist']			= 'Taille des images pour la liste de souhaits :';
$_['entry_image_cart']				= 'Taille des images pour les produits dans le panier :';
$_['entry_ftp_host']				= 'Hôte FTP :';
$_['entry_ftp_port']				= 'Port FTP :';
$_['entry_ftp_username']			= 'Nom d’utilisateur FTP :';
$_['entry_ftp_password']			= 'Mot de passe FTP :';
$_['entry_ftp_root']				= 'Racine FTP :<span class="help">Répertoire oû est stocké votre installation Opencart, normalement ’public_html/’.</span>';
$_['entry_ftp_status']				= 'Autoriser FTP :';
$_['entry_mail_protocol']			= 'Protocole de courriel :<span class="help">Choisir ’Mail’ à moins que l’hébergeur ai désactivé la fonction mail de php.';
$_['entry_mail_parameter']			= 'Paramètres des courriels :<span class="help">Lorsque vous utilisez <b>Mail</b> comme protocole de courriel, les adresses de courriel supplémentaires peuvent être ajoutées ici,<br>ex. : <b>email@storeaddress.com</b>';
$_['entry_smtp_host']				= 'Hôte du SMTP :';
$_['entry_smtp_username']			= 'Nom d’utilisateur du SMTP :';
$_['entry_smtp_password']			= 'Mot de passe du SMTP :';
$_['entry_smtp_port']				= 'Port du SMTP :';
$_['entry_smtp_timeout']			= 'Latence du SMTP :';
$_['entry_account_mail']			= 'Alerte courriel pour nouveau compte:<br /><span class="help">Envoyer un courriel au propriétaire de la boutique quand un nouveau compte est enregistré.</span>';
$_['entry_alert_mail']				= 'Alerte courriel :<br /><span class="help">Envoie un courriel au propriétaire de la boutique à chaque nouvelle commande créée.</span>';
$_['entry_alert_emails']			= 'Alertes courriel supplémentaires :<br /><span class="help">Ajouter les adresses courriel additionnelles auxquelles vous voulez recevoir les alertes, en plus du courriel principal de la boutique. (séparées par des virgules)</span>';
$_['entry_fraud_detection']			= 'Utiliser le système de détection des fraudes <b>MaxMind</b> :<br /><span class="help">MaxMind est un service de détections de fraude. Si vous n’avez pas de clé de licence que vous pouvez <a onclick="window.open(’http://www.maxmind.com/?rId=opencart’);"><u>vous inscrire ici</u></a>. Une fois la copie de la clé obtenue, veuillez la coller dans le champ ci-dessous.</span>';
$_['entry_fraud_key']				= 'Clé de licence <b>MaxMind</b> :</span>';
$_['entry_fraud_score']				= 'Score de risque MaxMind :<br /><span class="help">Plus le score est élevé, plus la commande est frauduleuse. Définir un score compris entre 0 à 100.</span>';
$_['entry_fraud_status']			= 'Suivi de commande antifraude MaxMind :<br /><span class="help">Les commandes au-dessus de votre score d’ensemble ne seront pas autorisées à atteindre le statut complet automatiquement.</span>';
$_['entry_secure']					= 'Utiliser le SSL :<br /><span class="help">Pour utiliser le SSL, vérifier avec votre hébergeur si un certificat SSL est installé et si l’adresse du SSL est ajoutée à votre fichier de configuration.</span>';
$_['entry_shared']					= 'Utiliser des sessions partagées :<br /><span class="help">Partager le cookie de session entre les boutiques pour pouvoir partager le panier entre les différents domaines.</span>';
$_['entry_robots']					= 'Robots :<br /><span class="help">Liste des agents utilisateurs de robot d’indexation qui ne seront pas utilisés avec les sessions partagées. Utiliser des lignes séparées pour chaque agent utilisateur.</span>';
$_['entry_seo_url']					= 'Utiliser des URL’s SEO :<br /><span class="help"> Pour utiliser des URL’s SEO le mod-rewrite (<i>réécriture des URL "à la volée"</i>) d’apache doit être installé et vous devez renommer le htaccess.txt en .htaccess.</span>';
$_['entry_file_extension_allowed']	= 'Extensions de fichiers admis :<br /><span class="help">Ajoutez ici les extensions des fichiers autorisés à être chargés. Indiquez chaque valeur sur une nouvelle ligne.</span>';
$_['entry_file_mime_allowed']		= 'Types de fichiers Mime admis :<br /><span class="help">Ajoutez ici les types de fichiers <b>mime</b> autorisés à être chargés. Indiquez chaque valeur sur une nouvelle ligne.</span>';
$_['entry_maintenance']				= 'Maintenance :<br /><span class="help">Empêche les clients de naviguer dans votre boutique. Ils verront à la place un message de maintenance. Par contre dans l’administration, vous ne verrez pas de changement.</span>';
$_['entry_password']				= 'Autoriser l’oubli du mot de passe :<br /><span class="help">Autorise l’utilisation des mots de passe oubliés pour l’administration. Désactivé automatiquement si le système détecte une tentative de piratage.</span>';
$_['entry_encryption']				= 'Clé de cryptage : <br /><span class="help">Veuillez fournir une clé secrète qui sera utilisée pour crypter les informations privées quand les commandes sont traitées.</span>';
$_['entry_compression']				= 'Niveau de compression en sortie : <br /><span class="help">GZIP pour un transfert plus efficace pour vos clients. Le niveau de compression doit être compris entre 0 et 9.</span>';
$_['entry_error_display']			= 'Afficher les erreurs :';
$_['entry_error_log']				= 'Écrire les erreurs dans le journal :';
$_['entry_error_filename']			= 'Nom du fichier du journal d’erreurs:';
$_['entry_google_analytics']		= 'Code de Google Analytics :<br /><span class="help">Après avoir créé votre profil sur <a onclick="window.open(’http://www.google.com/analytics/’);"><u>Google Analytics</u></a>, collez le code Google Analytics dans cette zone de texte.</span>';

// Error
$_['error_warning']					= 'Attention, veuillez vérifier soigneusement le formulaire afin qu’il n’y ai pas d’erreurs !';
$_['error_permission']				= 'Attention, vous n’avez pas la permission de modifier les <b>Paramètres</b> !';
$_['error_name']					= 'Le <b>Nom de la boutique</b> doit être composé de 3 à 32 caractères !';
$_['error_owner']					= 'Le <b>Propriétaire de la boutique</b> doit être composé de 3 à 64 caractères !';
$_['error_address']					= 'L’<b>Adresse de la boutique</b> doit être composé de 10 à 256 caractères !';
$_['error_email']					= 'L’<b>Adresse courriel</b> ne semble pas être valide !';
$_['error_telephone']				= 'Le <b>Téléphone</b> doit être composé de 3 à 32 caractères !';
$_['error_title']					= 'Le <b>Titre</b> doit être composé de 3 à 32 caractères !';
$_['error_limit']					= 'Attention, la limite est requise !';
$_['error_customer_group_display']	= 'Vous devez inclure le groupe par défaut du client pour pouvoir utiliser cette caractéristique !';
$_['error_voucher_min']				= 'Attention, le montant minimum du bon est requis !';
$_['error_voucher_max']				= 'Attention, le montant maximum du bon est requis !';
$_['error_image_thumb']				= 'Attention, la taille des vignettes pour les produits est requise !';
$_['error_image_popup']				= 'Attention, la taille des images pour les produits est requise !';
$_['error_image_product']			= 'Attention, la taille des listes pour les produits est requise !';
$_['error_image_category']			= 'Attention, la taille des listes pour les catégories est requise !';
$_['error_image_additional']		= 'Attention, la taille des images supplémentaires pour les produits est requise !';
$_['error_image_related']			= 'Attention, la taille des images pour les produits apparentés est requise !';
$_['error_image_compare']			= 'Attention, la taille des images pour les comparaisons est requise !';
$_['error_image_wishlist']			= 'Attention, la taille des images pour la liste de souhaits est requise !';
$_['error_image_cart']				= 'Attention, la taille des images pour les produits dans le panier est requise !';
$_['error_ftp_host']				= 'Attention, l’hôte FTP est requis !';
$_['error_ftp_port']				= 'Attention, le port FTP est requis !';
$_['error_ftp_username']			= 'Attention, le nom d’utilisateur FTP est requis !';
$_['error_ftp_password']			= 'Attention, le mot de passe FTP est requis !';
$_['error_error_filename']			= 'Attention, le nom du fichier du journal d’erreurs est requis !';
$_['error_encryption']				= 'Attention, la clé de cryptage doit être composé de 3 à 32 caractères !';
?>