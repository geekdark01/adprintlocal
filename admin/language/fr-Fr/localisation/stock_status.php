<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//--------------------------------//

// Heading
$_['heading_title']		= 'États du Stock';

// Text
$_['text_success']		= 'Félicitations : Vous avez modifié les <b>états du stock</b> avec succès !';

// Column
$_['column_name']		= 'Nom de l’état du stock';
$_['column_action']		= 'Action';

// Entry
$_['entry_name']		= 'Nom de l’état du stock :';

// Error
$_['error_permission']	= 'Attention, vous n’avez pas la permission de modifier les <b>états du stock</b> !';
$_['error_name']		= 'Le <b>Nom de l’état du Stock</b> doit être composé de 3 à 32 caractères !';
$_['error_product']		= 'Attention, cet état de stock ne peut être supprimé car il est assigné à %s produits !';
?>