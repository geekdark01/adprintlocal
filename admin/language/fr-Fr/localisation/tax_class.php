<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//--------------------------------//

// Heading
$_['heading_title']		= 'Type de Taxe';

// Text
$_['text_shipping']		= 'Adresse de livraison';
$_['text_payment']		= 'Adresse de paiement';
$_['text_store']		= 'Adresse de la boutique';
$_['text_success']		= 'Félicitations, vous avez modifié le <b>Type de taxe</b> avec succès !';

// Column
$_['column_title']		= 'Nom de la taxe';
$_['column_action']		= 'Action';

// Entry
$_['entry_title']		= 'Nom de la taxe :';
$_['entry_description']	= 'Description :';
$_['entry_rate']		= 'Taux de la taxe :';
$_['entry_based']		= 'Basé sur :';
$_['entry_geo_zone']	= 'Zone géographique :';
$_['entry_priority']	= 'Priorité :';

// Error
$_['error_permission']	= 'Attention, cVous n’avez pas la permission de modifier les <b>Types de taxes</b> !';
$_['error_title']		= 'Le <b>Titre du type de taxe</b> doit être composé de 3 à 32 caractères !';
$_['error_description']	= 'La <b>Description</b> doit être composé de 3 à 255 caractères !';
$_['error_product']		= 'Attention, ce type de taxe ne peut être supprimé car il est assigné à %s produits !';
?>