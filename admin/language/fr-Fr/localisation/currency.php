<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//--------------------------------//

// Heading
$_['heading_title']			= 'Devises';  

// Text
$_['text_success']			= 'Félicitations, vous avez modifié la <b>Devise</b> avec succès !';

// Column
$_['column_title']			= 'Titre de la devise';
$_['column_code']			= 'Code'; 
$_['column_value']			= 'Valeur';
$_['column_date_modified']	= 'Dernière actualisation';
$_['column_action']			= 'Action';

// Entry
$_['entry_title']			= 'Titre de la devise :';
$_['entry_code']			= 'Code :<br /><span class="help">Ne rien changer si c’est la devise par défaut.</span>';
$_['entry_value']			= 'Valeur :<br /><span class="help">Choisir 1,00000 s’il s’agit de votre devise par défaut.</span>';
$_['entry_symbol_left']		= 'Symbole de Gauche :';
$_['entry_symbol_right']	= 'Symbole de Droite :';
$_['entry_decimal_place']	= 'Nombre de chiffres décimaux :';
$_['entry_status']			= 'État :';

// Error
$_['error_permission']		= 'Attention, vous n’avez pas la permission de modifier les <b>Devises</b> !';
$_['error_title']			= 'Le <b>Titre de la devise</b> doit être composé de 3 à 32 caractères !';
$_['error_code']			= 'Le <b>Code</b> doit être composé de 3 caractères !';
$_['error_default']			= 'Attention, cette devise ne peut être supprimée car elle est assignée comme devise par défaut de la boutique !';
$_['error_store']			= 'Attention, cette devise ne peut être supprimée car elle est assignée à %s boutiques !';
$_['error_order']			= 'Attention, cette devise ne peut être supprimée car elle est assignée à %s commandes !';
?>