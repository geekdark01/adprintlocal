<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//--------------------------------//

// Heading
$_['heading_title']		= 'Classe des longueurs';

// Text
$_['text_success']		= 'Félicitations, vous avez modifié la <b>Classe des longueurs</b> avec succès !';

// Column
$_['column_title']		= 'Titre de la longueur';
$_['column_unit']		= 'Unité de longueur';
$_['column_value']		= 'Valeur';
$_['column_action']		= 'Action';

// Entry
$_['entry_title']		= 'Titre de la longueur :';
$_['entry_unit']		= 'Unité de longueur :';
$_['entry_value']		= 'Valeur :<br /><span class="help">Choisir 1.00000 si vous n’avez pas défini de longueur par défaut.</span>';

// Error
$_['error_permission']	= 'Attention, vous n’avez pas la permission de modifier la <b>Classe des longueurs</b> !';
$_['error_title']		= 'Le <b>Titre de la longueur</b> doit être composé de 3 à 32 caractères !';
$_['error_unit']		= 'L’<b>Unité de longueur</b> doit être composé de 1 à 4 caractères !';
$_['error_default']		= 'Attention, cette longueur ne peut pas être supprimée car elle est affectée à la classe de longueur par défaut !';
$_['error_product']		= 'Attention, cette longueur ne peut pas être supprimée car elle est affectée à %s produits !';
?>