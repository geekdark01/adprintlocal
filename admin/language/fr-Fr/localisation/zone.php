<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//--------------------------------//

// Heading
$_['heading_title']				= 'Zones';

// Text
$_['text_success']				= 'Félicitations, vous avez modifié la <b>Zone</b> avec succès !';

// Column
$_['column_name']				= 'Nom de la zone';
$_['column_code']				= 'Code de la zone';
$_['column_country']			= 'Pays';
$_['column_action']				= 'Action';

// Entry
$_['entry_status']				= 'État de la zone :';
$_['entry_name']				= 'Nom de la zone :';
$_['entry_code']				= 'Code de la zone :';
$_['entry_country']				= 'Pays :';

// Error
$_['error_permission']			= 'Attention, vous n’avez pas la permission de modifier les <b>Zones</b> !';
$_['error_name']				= 'Le <b>Nom de la zone</b> doit être composé de 3 à 128 caractères !';
$_['error_default']				= 'Attention, cette zone ne peut être supprimée car elle est assignée comme zone par défaut de la boutique !';
$_['error_store']				= 'Attention, cette zone ne peut être supprimée car elle est assignée à %s boutiques !';
$_['error_address']				= 'Attention, cette zone ne peut être supprimée car elle est assignée à %s entrées du carnet d’adresses !';
$_['error_affiliate']			= 'Attention, cette zone ne peut être supprimée car elle est assignée à %s affiliés !';
$_['error_zone_to_geo_zone']	= 'Attention, cette zone ne peut être supprimée car elle est assignée à %s zones des zones géographiques !';
?>