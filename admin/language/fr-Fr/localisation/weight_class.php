<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//--------------------------------//

// Heading
$_['heading_title']		= 'Unités de Poids';

// Text
$_['text_success']		= 'Félicitations, vous avez modifié l’<b>Unités de poids</b> avec succès !';

// Column
$_['column_title']		= 'Nom de l’unité';
$_['column_unit']		= 'Symbole';
$_['column_value']		= 'Valeur';
$_['column_action']		= 'Action';

// Entry
$_['entry_title']		= 'Nom de l’unité :';
$_['entry_unit']		= 'Symbole de l’unité :';
$_['entry_value']		= 'Valeur :<br /><span class="help">choisir 1,00000 pour le poids par défaut.</span>';

// Error
$_['error_permission']	= 'Attention, vous n’avez pas la permission de modifier les <b>Unités de poids</b> !';
$_['error_title']		= 'Le <b>Titre du poids</b> doit être composé de 3 à 32 caractères !';
$_['error_unit']		= 'L’<b>Unité de mesure</b> doit être composé de 1 à 4 caractères !';
$_['error_default']		= 'Attention, cette unité de poids ne peut être supprimé car elle est assignée comme unité de poids par défaut de la boutique !';
$_['error_product']		= 'Attention, cette unité de poids ne peut être supprimé car elle est assignée à %s produits !';
?>