<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//--------------------------------//

// Heading
$_['heading_title']			= 'Zones Géographiques';

// Text
$_['text_success']			= 'Félicitations, vous avez modifié la <b>Zone géographique</b> avec succès !';

// Column
$_['column_name']			= 'Nom de la zone géographique';
$_['column_description']	= 'Description';
$_['column_action']			= 'Action';

// Entry
$_['entry_name']			= 'Nom de la zone géographique :';
$_['entry_description']		= 'Description :';
$_['entry_country']			= 'Pays';
$_['entry_zone']			= 'Zone';

// Error
$_['error_permission']		= 'Attention, vous n’avez pas la permission de modifier les <b>Zones géographiques</b> !';
$_['error_name']			= 'Le <b>Nom de la zone géographique</b> doit être composé de 3 à 32 caractères !';
$_['error_description']		= 'La <b>Description</b> doit être composé de 3 à 255 caractères !';
$_['error_tax_rate']		= 'Attention, cette zone géographique ne peut être supprimé car elle est assignée à une ou plus types de taxes !';
?>