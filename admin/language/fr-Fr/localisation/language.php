<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//--------------------------------//

// Heading
$_['heading_title']		= 'Langues';  

// Text
$_['text_success']		= 'Félicitations, vous avez modifié la <b>Langue</b> avec succès !'; 

// Column
$_['column_name']		= 'Nom de la Langue';
$_['column_code']		= 'Code';
$_['column_sort_order']	= 'Classement';
$_['column_action']		= 'Action';

// Entry
$_['entry_name']		= 'Nom de la Langue :';
$_['entry_code']		= 'Code :<br /><span class="help">Ne pas modifiez s’il s’agit de votre langue par défaut.</span>';
$_['entry_locale']		= 'Localisation :<br /><span class="help">Exemple pour la France : fr_CA.UTF-8,fr_FR.UTF-</span>';
$_['entry_image']		= 'Image :<br /><span class="help">Exemple pour la France : fr.png</span>';
$_['entry_directory']	= 'Dossier :<br /><span class="help"Nom du répertoire de la langue (sensible à la casse)</span>';
$_['entry_filename']	= 'Nom du fichier :<br /><span class="help">Nom du fichier de langue principale (sans extension)</span>';
$_['entry_status']		= 'État :<br /><span class="help">Masquer ou Afficher la langue dans la liste déroulante de la boutique</span>';
$_['entry_sort_order']	= 'Classement :';

// Error
$_['error_permission']	= 'Attention, vous n’avez pas la permission de modifier les <b>Langues</b> !';
$_['error_name']		= 'Le <b>Nom</b> doit être composé de 3 à 32 caractères !';
$_['error_code']		= 'Le Code doit être composé de 2 caractères minimum !';
$_['error_locale']		= 'Attention, la localisationw est requise !';
$_['error_image']		= 'Le nom du fichier image doit être composé de 3 à 64 caractères !';
$_['error_directory']	= 'Attention, le dossier  est requis !';
$_['error_filename']	= 'Le nom du fichier doit être composé de 3 à 64 caractères !';
$_['error_default']		= 'Attention, cette langue ne peut être supprimée car elle est assignée comme langue par défaut de la boutique !';
$_['error_admin']		= 'Attention, cette langue ne peut être supprimée car elle est assignée comme langue de l’administration !';
$_['error_store']		= 'Attention, cette langue ne peut être supprimée car elle est assignée à %s boutiques!';
$_['error_order']		= 'Attention, cette langue ne peut être supprimée car elle est assignée à %s commandes !';
?>