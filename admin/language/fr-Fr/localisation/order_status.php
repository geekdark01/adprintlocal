<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//--------------------------------//

// Heading
$_['heading_title']		= 'États des commandes';

// Text
$_['text_success']		= 'Félicitations, vous avez modifié l’<b>état des commandes</b> avec succès !';

// Column
$_['column_name']		= 'Nom de l’état des commandes';
$_['column_action']		= 'Action';

// Entry
$_['entry_name']		= 'Nom de l’état des commandes :';

// Error
$_['error_permission']	= 'Attention, vous n’avez pas la permission de modifier les <b>états des commandes</b> !';
$_['error_name']		= 'Le <b>Nom de l’état des commandes</b> doit être composé de 3 à 32 caractères !';
$_['error_default']		= 'Attention, cet ètat de commande ne peut être supprimé car il est assigné par défaut comme ètat pour la boutique !';
$_['error_download']	= 'Attention, cet ètat de commande ne peut être supprimé car il est assigné par défaut comme ètat de téléchargement !';
$_['error_store']		= 'Attention, cet ètat de commande ne peut être supprimé car il est assigné à %s boutiques !';
$_['error_order']		= 'Attention, cet ètat de commande ne peut être supprimé car il est assigné à %s commandes !';
?>