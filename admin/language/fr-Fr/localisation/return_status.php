<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// Heading
$_['heading_title']		= 'États des retours';

// Text
$_['text_success']		= 'Félicitations, vous avez sauvegardé les <b>états des retours</b> avec succès !';

// Column
$_['column_name']		= 'Nom de l’état de retour';
$_['column_action']		= 'Action';

// Entry
$_['entry_name']		= 'Nom de l’état de retour :';

// Error
$_['error_permission']	= 'Attention, vous n’avez pas la permission de modifier les <b>états des retours</b> !';
$_['error_name']		= 'Le <b>Nom de l’état de retour</b> doit être composé de 3 à 32 caractères !';
$_['error_default']		= 'Attention, cet <b>état de retour</b> ne peut pas être supprimée car elle est actuellement affectée comme <b>état de retour</b> par dé !';
$_['error_return']		= 'Attention, cet <b>état de retour</b> ne peut pas être supprimée car elle est actuellement affectée à %s retour produits !';
?>