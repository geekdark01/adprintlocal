<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// Heading
$_['heading_title']		= 'Raisons des retours';

// Text
$_['text_success']		= 'Félicitations, vous avez sauvegardé les <b>Raisons des retours</b> avec succès !';

// Column
$_['column_name']		= 'Nom de la raison de retour';
$_['column_action']		= 'Action';

// Entry
$_['entry_name']		= 'Nom de la raison de retour :';

// Error
$_['error_permission']	= 'Attention, vous n’avez pas la permission de modifier les <b>Raisons des retours</b> !';
$_['error_name']		= 'Le <b>Nom de la raison de retour</b> doit être composé de 3 à 32 caractères !';
$_['error_return']		= 'Attention, cette <b>Raison de retour</b> ne peut pas être supprimée car elle est actuellement affectée à %s retour produits !';
?>