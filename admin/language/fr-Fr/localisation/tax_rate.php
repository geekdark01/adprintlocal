<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// Heading
$_['heading_title']			= 'Taux de taxe';

// Text
$_['text_percent']			= 'Pourcentage';
$_['text_amount']			= 'Montant fixe';
$_['text_success']			= 'Félicitations, vous avez modifié le <b>Taux de taxe</b> avec succès !';

// Column
$_['column_name']			= 'Nom de la taxe';
$_['column_rate']			= 'Taux de la taxe';
$_['column_type']			= 'Type';
$_['column_geo_zone']		= 'Zone géographique';
$_['column_date_added']		= 'Date d’ajout';
$_['column_date_modified']	= 'Date de modification';
$_['column_action']			= 'Action';

// Entry
$_['entry_name']			= 'Nom de la taxe :';
$_['entry_rate']			= 'Taux de la taxe :';
$_['entry_type']			= 'Type :';
$_['entry_customer_group']	= 'Groupe client :';
$_['entry_geo_zone']		= 'Zone géographique :';

// Error
$_['error_permission']		= 'Attention, vous n’avez pas la permission de modifier les <b>Classes de taxes</b> !';
$_['error_tax_rule']		= 'Attention, ce taux de taxe ne peut être supprimé car il est assigné à %s classes de taxes !';
$_['error_name']			= 'Le <b>Nom de la taxe</b> doit être composé de 3 à 32 caractères !';
$_['error_rate']			= 'Le taux de taxe est requis !';
?>
