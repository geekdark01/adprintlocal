<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// Heading
$_['heading_title']		= 'Journal d’erreurs';

// Text
$_['text_success']		= 'Félicitations, vous avez effacé les entrées du <b>Journal d’erreurs</b> avec succès !';
?>