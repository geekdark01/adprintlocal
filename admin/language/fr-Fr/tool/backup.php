<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// Heading
$_['heading_title']		= 'Restauration et sauvegarde';

// Text
$_['text_backup']		= 'Télécharger la Sauvegarde';
$_['text_success']		= 'Félicitations, vous avez importé votre <b>Base de données</b> avec succès !';

// Entry
$_['entry_restore']		= 'Restaurer la Sauvegarde :';
$_['entry_backup']		= 'Sauvegarde :';

// Error
$_['error_permission']	= 'Attention, vous n’avez pas la permission de modifier la <b>Restauration et sauvegarde</b> !';
$_['error_backup']		= 'Attention, vous devez sélectionner au moins une table pour la sauvegarde !';
$_['error_empty']		= 'Attention, le fichier que vous avez téléchargé est vide !';
?>