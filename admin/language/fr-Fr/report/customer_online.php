<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// Heading
$_['heading_title']		= 'Rapport sur les clients en ligne';

// Text 
$_['text_guest']        = 'Invité';
 
// Column
$_['column_ip']         = 'IP';
$_['column_customer']   = 'Client';
$_['column_url']        = 'Dernière page visitée';
$_['column_referer']    = 'Référant';
$_['column_date_added'] = 'Dernier clic';
$_['column_action']     = 'Action';
?>