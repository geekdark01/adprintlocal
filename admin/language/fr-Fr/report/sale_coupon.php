<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// Heading
$_['heading_title']		= 'Rapport sur les coupons';

// Column
$_['column_name']		= 'Nom du coupon';
$_['column_code']		= 'Code';
$_['column_orders']		= 'Commandes';
$_['column_total']		= 'Total';
$_['column_action']		= 'Action';

// Entry
$_['entry_date_start']	= 'Date de début :';
$_['entry_date_end']	= 'Date de fin :';
?>