<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// Heading
$_['heading_title']	= 'Rapport sur les produits les plus consultés';

// Text
$_['text_success']	= 'Félicitations, vous avez remis à zéro le <b>Rapport sur les produits les plus consultés</b> avec succès !';

// Column
$_['column_name']	= 'Nom du produit';
$_['column_model']	= 'Modèle';
$_['column_viewed']	= 'Nombre de consultations';
$_['column_percent']= 'Pourcentage';
?>