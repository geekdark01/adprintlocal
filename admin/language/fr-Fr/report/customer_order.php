<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// Heading
$_['heading_title']			= 'Rapport sur les commandes client';

// Text
$_['text_all_status']		= 'Tous les états';

// Column
$_['column_customer']		= 'Nom du client';
$_['column_email']			= 'Courriel';
$_['column_customer_group']	= 'Groupe clients';
$_['column_status']			= 'État';
$_['column_orders']			= 'Nombre de commande';
$_['column_products']		= 'Nombre de produit';
$_['column_total']			= 'Total';
$_['column_action']			= 'Action';

// Entry
$_['entry_date_start']		= 'Date de début :';
$_['entry_date_end']		= 'Date de fin :';
$_['entry_status']			= 'État de la commande :';
?>