<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// Heading
$_['heading_title']		= 'Rapport sur les retours';

// Text
$_['text_year']			= 'Années';
$_['text_month']		= 'Mois';
$_['text_week']			= 'Semaines';
$_['text_day']			= 'Jours';
$_['text_all_status']	= 'Tous les états';

// Column
$_['column_date_start']	= 'Date de début';
$_['column_date_end']	= 'Date de fin';
$_['column_returns']	= 'Nombre de retour';

// Entry
$_['entry_date_start']	= 'Date de début :';
$_['entry_date_end']	= 'Date de fin :';
$_['entry_group']		= 'Regrouper par :';
$_['entry_status']		= 'État de la commande :';
?>