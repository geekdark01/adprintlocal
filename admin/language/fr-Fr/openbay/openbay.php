<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// Heading
$_['heading_title']						= 'eBay';

// Install actions
$_['text_install']                      = 'Installer';
$_['text_edit']                         = 'Éditer';
$_['text_uninstall']                    = 'Désinstaller';

// Status
$_['text_enabled']                      = 'Activé';
$_['text_disabled']                     = 'Désactivé';

// Messages
$_['error_category_nosuggestions']      = 'Impossible de charger les catégories proposées';
$_['lang_text_success']                 = 'Vous avez enregistré vos modifications à l’extension eBay';
$_['lang_error_retry']          		= 'Connexion impossible au serveur OpenBay. ';
?>