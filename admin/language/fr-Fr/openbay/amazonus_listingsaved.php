<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

$_['lang_title']                    = 'OpenBay Pro pour Amazon US | Annonces sauvegardées';
$_['lang_saved_listings']           = 'Annonces sauvegardées';
$_['lang_openbay']                  = 'OpenBay Pro';
$_['lang_overview']                 = 'Amazon US vue d’ensemble';
$_['lang_btn_return']               = 'Annuler';
$_['lang_description']              = 'Il s’agit de la liste des annonces de produits enregistrées localement et qui sont prêts à être téléchargé sur Amazon. Cliquez sur Télécharger pour les poster.';
$_['lang_actions_edit']             = 'Éditer';
$_['lang_actions_remove']           = 'Supprimer';
$_['lang_btn_upload']               = 'Télécharger';
$_['lang_name_column']              = 'Nom';
$_['lang_model_column']             = 'Modèle';
$_['lang_sku_column']               = 'Référence SKU';
$_['lang_amazonus_sku_column']      = 'Référence SKU de l’article sur Amazon US';
$_['lang_actions_column']           = 'Action';
$_['lang_uploaded_alert']           = 'Annonces sauvegardées téléchargées !';
$_['lang_delete_confirm']           = 'étes-vous sûr ?';
?>