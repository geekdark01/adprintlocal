<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// Headings
$_['lang_openbay']                  = 'OpenBay Pro';
$_['lang_page_title']               = 'OpenBay Pro pour eBay';
$_['lang_ebay']                     = 'eBay';
$_['lang_heading']                  = 'Liens des articles';
$_['lang_linked_items']             = 'Articles liés';
$_['lang_unlinked_items']           = 'Articles déliés';

// Buttons
$_['lang_btn_resync']               = 'Re-synchroniser';
$_['lang_btn_return']               = 'Retour';
$_['lang_btn_save']                 = 'Sauvegarder';
$_['lang_btn_edit']                 = 'Éditer';
$_['lang_btn_check_unlinked']       = 'Cochez les articles non liés';
$_['lang_btn_remove_link']          = 'Supprimer le lien';

// Errors & alerts
$_['lang_error_validation']         = 'Vous devez vous inscrire pour votre clé API et activer le module.';
$_['lang_alert_stock_local']        = 'Votre annonce eBay sera mis à jour avec vos niveaux de stocks locaux !';
$_['lang_ajax_error_listings']      = 'Aucun article lié trouvé';
$_['lang_ajax_load_error']          = 'Désolé, aucune réponse. Essayez plus tard.';
$_['lang_ajax_error_link']          = 'Le lien de ce produit n’a pas de valeur';
$_['lang_ajax_error_link_no_sk']    = 'Un lien ne peut être créé pour un article en rupture de stock. Mettre fin à l’élément manuellement sur ​​eBay.';
$_['lang_ajax_loaded_ok']           = 'Les articles ont bien été chargés';
$_['lang_ajax_not_subtract']		= 'Ce produit est configuré pour ne être soustrait du stock d’OpenCart.';

// Text
$_['lang_link_desc1']               = 'Lier vos articles permettra un contrôle des stocks sur vos annonces eBay.';
$_['lang_link_desc2']               = 'Chaque élément mis à jour sur le stock local (stock disponible dans votre magasin OpenCart) engendrera la mise à jour de votre annonce sur eBay';
$_['lang_link_desc3']               = 'Votre stock local est le stock disponible pour la vente. Vos niveaux de stock sur eBay doivent correspondre à celui-ci.';
$_['lang_link_desc4']               = 'Votre magasin est affecté d’articles vendus, mais pas encore été réglés. Ces articles doivent être mis de côté et ne pas être calculés dans vos niveaux de stocks disponibles.';
$_['lang_text_linked_desc']         = 'Articles liés sont des articles OpenCart qui ont un lien vers une annonce eBay.';
$_['lang_text_unlinked_desc']       = 'Les articles déliés sont les annonces sur eBay qui ne sont pas liés pour vos produits OpenCart.';
$_['lang_text_unlinked_info']       = 'Cliquez sur le bouton de contrôle des articles non liés à la recherche de vos annonces actives sur eBay où des articles sont non liés. Cela peut prendre un certain temps si vous avez beaucoup d’annonces sur eBay.';
$_['lang_text_loading_items']       = 'Chargement des articles';
$_['lang_limit_reached']			= 'Le nombre maximal de contrôles par demande a été atteint, cliquez sur le bouton pour continuer la recherche';

// Tables
$_['lang_column_action']            = 'Action';
$_['lang_column_status']            = 'État';
$_['lang_column_variants']          = 'Variantes';
$_['lang_column_itemId']            = 'ID article sur eBay';
$_['lang_column_product']           = 'Produit';
$_['lang_column_product_auto']      = 'Produit<span class="help">(Autocomplétion depuis le nom)</span>';
$_['lang_column_stock_available']   = 'Stock local<br /><span class="help">Disponible à la vente</span>';
$_['lang_column_listing_title']     = 'Titre des annonces<span class="help">(Titre des annonces sur eBay)</span>';
$_['lang_column_allocated']         = 'Stock alloué<br /><span class="help">Vendu mais non réglé</span>';
$_['lang_column_ebay_stock']        = 'Stock sur eBay<span class="help">Sur annonce</span>';
$_['lang_column_stock_reserve']		= 'Stock de réserve';

// Filter
$_['lang_filter']					= 'Filtrer les résultats';
$_['lang_filter_title']				= 'Titre';
$_['lang_filter_range']				= 'Assortiment de stock';
$_['lang_filter_var']				= 'Inclure les variantes';
?>