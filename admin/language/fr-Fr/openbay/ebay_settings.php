<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

$_['lang_heading_title']					= 'OpenBay Pro pour eBay | Paramètres';
$_['lang_openbay']							= 'OpenBay Pro';
$_['lang_ebay']								= 'eBay';
$_['lang_settings']							= 'Paramètres';
$_['lang_save']								= 'Sauvegarder';
$_['lang_cancel']							= 'Annuler';
$_['lang_yes']								= 'Oui';
$_['lang_update']							= 'Mise à jour';
$_['lang_no']								= 'Non';
$_['lang_import']							= 'Import';
$_['lang_clear']							= 'Nettoyer';
$_['lang_add']								= 'Ajouter';
$_['lang_remove']							= 'Supprimer';
$_['lang_load']								= 'Charger';
$_['lang_text_success']						= 'Vos Paramètres ont bien été sauvegardé';

$_['lang_error_save_settings']				= 'Vous devez enregistrer vos paramètres en priorité.';

$_['lang_tab_token']						= 'Détails de l’API'; 
$_['lang_tab_general']						= 'Paramètres'; 
$_['lang_tab_setup']						= 'Paramètres'; 
$_['lang_tab_defaults']						= 'Annonces en nombre'; 
$_['lang_tab_shipping']						= 'Livraison par défaut';

$_['lang_legend_api']						= 'Détails de la connexion de l’API';
$_['lang_legend_app_settings']				= 'Paramètres de l’application';
$_['lang_legend_default_import']			= 'Paramètres de l’import par défaut';
$_['lang_legend_payments']					= 'Paiements';
$_['lang_legend_returns']					= 'Retours';
$_['lang_legend_linkup']					= 'Liens article';
$_['lang_legend_usage']						= 'Appeler le support';
$_['lang_legend_subscription']				= 'Souscription';

$_['lang_error_oc_version']					= 'Version d’OpenCart non supporté';

$_['lang_status']							= 'État';
$_['lang_enabled']							= 'Activé';
$_['lang_disabled']							= 'Désactivé';
$_['lang_obp_token']						= 'Jeton';
$_['lang_obp_token_register']				= 'Cliquer ici pour vous inscrire à votre jeton';
$_['lang_obp_token_renew']					= 'Cliquer ici pour renouveler votre jeton';
$_['lang_obp_secret']						= 'Secret';
$_['lang_obp_string1']						= 'Chaîne de cryptage 1';
$_['lang_obp_string2']						= 'Chaîne de cryptage 2';
$_['lang_app_setting_msg']					= 'Vos paramètres d’application vous permettent de configurer la manière dont OpenBay Pro fonctionne et s’intègre à votre système.';
$_['lang_app_end_ebay']						= 'Fin des articles ?<span class="help">Si les articles se vendent ou si l’inscription se termine sur eBay?</span>';
$_['lang_app_relist_ebay']					= 'Réinscrire si en stock ?<span class="help">Si un lien d’article existait avant la remise en vente de l’article précédent si en stock</span>';
$_['lang_app_nostock_ebay']					= 'Désactiver le produit en absence de stock ?<span class="help">Lorsque l’article est vendu cela désactive alors le produit dans OpenCart</span>';
$_['lang_app_logging']						= 'Activer la journalisation';
$_['lang_app_currency']						= 'Devise par défaut';
$_['lang_app_currency_msg']					= 'Basé sur les devises de votre boutique';
$_['lang_app_cust_grp']						= 'Groupe client<span class="help">Lorsque de nouveaux clients sont créés, doit on les ajouter à ce groupe ?';

$_['lang_app_stock_allocate']				= 'Stock alloué<span class="help">Lorsque le stock doit être réparti dans le magasin?</span>';
$_['lang_app_stock_1']						= 'Lorsque le client achète';
$_['lang_app_stock_2']						= 'Lorsque le client a payé';
$_['lang_created_hours']					= 'Nouvelle limite d’&acirc;ge de commande<span class="help">Les commandes sont nouvelles quand cette limite n’est pas dépassée (72 en heures, par défaut).</span>';

$_['lang_import_ebay_items']				= 'Import des articles eBay';

$_['lang_import_pending']					= 'Import des commandes impayées';
$_['lang_import_def_id']					= 'Import de l’état par défaut :';
$_['lang_import_paid_id']					= 'États des paiements :';
$_['lang_import_shipped_id']				= 'États des livraisons :';
$_['lang_import_cancelled_id']				= 'États des annulations:';
$_['lang_import_refund_id']					= 'États des remboursements :';
$_['lang_import_part_refund_id']			= 'États des remboursements partiels ';

$_['lang_developer']						= 'développeur';
$_['lang_developer_desc']					= 'Vous ne devriez pas utiliser cette zone, à moins d’y être invité.'; 
$_['lang_developer_empty']					= 'Vider tous les champs ?'; 
$_['lang_developer_locks']					= 'Retirer les commandes verrouillées';
$_['lang_setting_desc']						= 'Les paramètres par défaut sont utilisés pour pré-établir de nombreuses options d’eBay. Toutes les valeurs par défaut peuvent être modifiés sur la page d’inscription pour chaque élément.';
$_['lang_payment_instruction']				= 'Instructions de paiement';
$_['lang_payment_paypal']					= 'PayPal Accepté';
$_['lang_payment_paypal_add']				= 'Adresse courriel pour payPal';
$_['lang_payment_cheque']					= 'Chèque accepté';
$_['lang_payment_card']						= 'Cartes acceptées';
$_['lang_payment_desc']						= 'Voir description (Ex. : transfert bancaire)';
$_['lang_payment_desc']						= 'Voir description (Ex. : transfert bancaire)';
$_['lang_payment_imediate']					= 'Paiement immédiat requis';
$_['lang_tax_listing']						= 'Taxe du produit<span class="help">Si vous utilisez le taux appliqué à l’annonce, assurez-vous que vos articles sur eBay ont la bonne taxe</span>';
$_['lang_tax_use_listing']					= 'Utiliser un taux d’imposition pour votre annonce sur eBay';
$_['lang_tax_use_value']					= 'Utilisez une valeur définie pour tout';
$_['lang_tax']								= 'Taxe en % utilisée pour tout<span class="help">Utilisé lorsque vous importez des articles ou des commandes</span>';

$_['lang_ajax_dev_enter_pw']				= 'Veuillez entrer votre mot de passe administrateur';
$_['lang_ajax_dev_enter_warn']				= 'Cette action est dangereuse si le mot de passe est protégé.';

$_['lang_legend_notify_settings']			= 'Notification des Paramètres';
$_['lang_openbaypro_update_notify']         = 'Mise à jour des commandes<span class="help">Seulement pour les mises à jour automatiques</span>';
$_['lang_notify_setting_msg']               = 'Contrôle lorsque les clients reçoivent des notifications de l’application. L’activation des emails de mise à jour peut améliorer vos taux de DSR dès que l’utilisateur reçoit des mises à jour sur leur commande.';
$_['lang_openbaypro_confirm_notify']        = 'Nouvelle commande - Acheteur<span class="help">Notifier à l’acheteur de tous nouveaux messages de commande par défaut.</span>';
$_['lang_openbaypro_confirmadmin_notify']   = 'Nouvelle commande - Administrateur<span class="help">Notifier à l’administrateur de tous nouveaux messages de commande par défaut.</span>';
$_['lang_openbaypro_brand_disable']         = 'Désactiver le lien<span class="help">Supprime le lien OpenBay Pro dans les emails de commandes</span>';

$_['lang_listing_1day']						= '1 jour';
$_['lang_listing_3day']						= '3 jours';
$_['lang_listing_5day']						= '5 jours';
$_['lang_listing_7day']						= '7 jours';
$_['lang_listing_10day']					= '10 jours';
$_['lang_listing_30day']					= '30 jours';
$_['lang_listing_gtc']						= 'BJA - Bon jusqu’à l’annulation';
$_['lang_openbay_duration']					= 'Durée de l’annonce par défaut<span class="help">BJA est disponible uniquement si vous avez une boutique eBay.</span>';
$_['lang_address_format']					= 'Format de l’adresse par défaut<span class="help">Utilisé uniquement si le pays ne dispose pas de format d’adresse défini.</span>';

$_['lang_api_status']						= 'État de la connexion API';
$_['lang_api_checking']						= 'Vérification';
$_['lang_api_ok']							= 'Connexion OK, jeton expiré';
$_['lang_api_failed']						= 'Validation échouée';
$_['lang_api_connect_fail']					= 'échec à la connexion';
$_['lang_api_connect_error']				= 'Erreur de connexion';

$_['lang_create_date']						= 'Date de création des nouvelles commandes';
$_['lang_create_date_0']					= 'Lorsque ajouté à OpenCart';
$_['lang_create_date_1']					= 'Lorsque créé sur eBay';
$_['lang_obp_detail_update']				= 'Cliquez ici pour mettre à jour l’URL de votre magasin et être contacté par courriel';
$_['lang_developer_repairlinks']			= 'Réparer les liens de l’article';

$_['lang_timezone_offset']					= 'Décalage horaire<span class="help">Basé sur les heures. 0 est le décalage horaire GMT. Ne fonctionne que si le temps d’eBay est utilisé pour la création de la commande.</span>';
?>