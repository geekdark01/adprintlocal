<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

$_['lang_openbay']              = 'OpenBay Pro';
$_['lang_page_title']           = 'OpenBay Pro pour eBay';
$_['lang_ebay']                 = 'eBay';
$_['lang_heading']              = 'Synchronisation';
$_['lang_btn_return']           = 'Retour';
$_['lang_legend_ebay_sync']     = 'Synchroniser avec eBay';
$_['lang_sync_desc']            = 'Vous devez synchroniser votre magasin avec les dernières options et les catégories d’expédition d’eBay disponibles, ces données sont seulement pour les options lors de l’inscription d’un point à eBay - il ne sera pas importer de catégories de votre magasin, etc... <br /> <strong> Vous devriez mettre à jour fréquemment celles-cipour vous assurer d’avoir toujours les dernières données d’eBay.</strong>';
$_['lang_sync_cats_lbl']        = 'Obtenir les catégories principales<span class="help">Cela n’importera pas les catégories de votre magasin !</span>';
$_['lang_sync_shop_lbl']        = 'Obtenir les catégories de la boutique<span class="help">Cela n’importera pas les catégories de votre magasin !</span>';
$_['lang_sync_setting_lbl']     = 'Obtenir les Paramètres<span class="help">Importation de types de paiement disponibles, comme l’expédition, les lieux et plus encore.</span>';
$_['lang_sync_btn']             = 'Mise à jour';
$_['lang_ajax_ebay_categories'] = 'Cela peut prendre un certain temps, veuillez patienter 5 minutes avant de faire quoi que ce soit d’autre.';
$_['lang_ajax_cat_import']      = 'Les catégories de votre boutique ont été importés sur eBay.';
$_['lang_ajax_setting_import']  = 'Vos Paramètres ont bien été importés.';
$_['lang_ajax_setting_import_e']= 'Un erreur s’est produite lors du chargement des Paramètres';
$_['lang_ajax_load_error']      = 'Erreur de connexion au serveur';
?>