<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

$_['lang_page_title']               = 'eBay listing';
$_['lang_cancel']                   = 'Annuler';
$_['lang_none']                     = 'Aucun';
$_['lang_preview']                  = 'Aper&ccedil;u';
$_['lang_pixels']                   = 'Pixels';
$_['lang_yes']                      = 'Oui';
$_['lang_no']                       = 'Non';
$_['lang_add']                      = 'Ajouter';
$_['lang_remove']                   = 'Supprimer';
$_['lang_save']                     = 'Sauvegarder';
$_['lang_other']                    = 'Autres';
$_['lang_select']                   = 'Sélectionner';
$_['lang_loading']                  = 'Chargement';
$_['lang_confirm_action']           = 'êtes-vous sûr ?';

$_['lang_return']                   = 'Retour aux produits';
$_['lang_view']                     = 'Voir les annonces';
$_['lang_edit']                     = 'Éditer';

$_['lang_catalog_pretext']          = 'Cet onglet affiche tous les résultats pour les éléments trouvés dans le catalogue eBay - sélectionnez votre première catégorie.';
$_['lang_feature_pretext']          = 'Cet onglet affiche toutes les caractéristiques des articles disponibles - sélectionnez votre première catégorie.';

$_['lang_ajax_noload']              = 'Désolé, mais il est impossible de se connecter';
$_['lang_ajax_catproblem']          = 'Vous devez régler votre problème de catégorie avant de lister. Essayez de les re-synchroniser dans la zone du module de l’administration.';
$_['lang_ajax_item_condition']      = 'État de l’article';
$_['lang_ajax_error_cat']           = 'Veuillez choisir une catégorie eBay';
$_['lang_ajax_error_sku']           = 'Vous ne pouvez pas soumettre un produit sans SKU';
$_['lang_ajax_error_name']          = 'Vous ne pouvez pas soumettre un produit sans nom';
$_['lang_ajax_error_name_len']      = 'Le nom du produit doit être composé de moins de 80 caractères';
$_['lang_ajax_error_loc']           = 'Entrer un code postal pour la localisation de l’article';
$_['lang_ajax_error_time']          = 'Entrer un délai de livraison';
$_['lang_ajax_error_nat_svc']       = 'Ajouter au moins un service de livraison';
$_['lang_ajax_error_stock']         = 'Vous devez avoir en stock un article de la liste';
$_['lang_ajax_error_duration']      = 'Sélectionner une durée pour l’annonce et sélectionner la catégorie pour charger ces options';
$_['lang_ajax_image_size']          = 'Assurez-vous que vous avez une taille pour les images de la galerie ainsi que pour les vignettes';
$_['lang_ajax_duration']            = 'Sélectionner une durée pour l’annonce';
$_['lang_ajax_noimages']            = 'Etes-vous sûr de ne pas vouloir ajouter d’images sur eBay ?';
$_['lang_ajax_mainimage']           = 'Vous devez choisir une image principale à partir de votre sélection d’images sur eBay';

//Tabs
$_['lang_tab_general']              = 'Catégorie';
$_['lang_tab_feature']              = 'Fonctionnalités';
$_['lang_tab_description']          = 'Description';
$_['lang_tab_images']               = 'Thème &amp; galerie';
$_['lang_tab_price']                = 'Prix &amp; détails';
$_['lang_tab_payment']              = 'Paiement';
$_['lang_tab_shipping']             = 'Livraison';
$_['lang_tab_returns']              = 'Retours';

//Category
$_['lang_category']                 = 'Catégorie';
$_['lang_category_suggested']       = 'Catégorie suggéré pour eBay';
$_['lang_category_suggested_help']  = 'Basé sur votre titre';
$_['lang_category_suggested_check'] = 'Vérification des catégories proposées par eBay, veuillez patienter';
$_['lang_category_popular']         = 'Catégories recherchées';
$_['lang_category_popular_help']    = 'Basé sur votre historique';
$_['lang_category_checking']        = 'Vérification des exigencesde de la catégorie sur eBay, veuillez patienter';
$_['lang_category_features']        = 'Caractéristiques de l’article<span class="help">La saisie de détails au sujet de votre article aidera les acheteurs à affiner leur recherche sur le produit exact dont ils ont besoin. Cela peut également améliorer les performances du produit et eBay peut marquer une meilleure correspondance de valeur plus élevée.</span>';

//Description
$_['lang_title']                    = 'Titre';
$_['lang_title_error']				= 'Votre titre doit être de 80 caractères maximum';
$_['lang_subtitle']					= 'Sous-titre';
$_['lang_subtitle_help']			= 'eBay facturera des frais supplémentaires pour un sous-titre - <a href="http://pages.ebay.co.uk/help/sell/seller-fees.html" target="_BLANK" onclick="subtitleRefocus();"> cliquer ici pour les drais</a>';
$_['lang_template']					= 'Thème';
$_['lang_template_link']			= 'Besoin d’un thème personnalisé ?';
$_['lang_description']				= 'Description';

//Images
$_['lang_images_text_1']			= 'eBay images seront téléchargées sur eBay et engendrera des frais supplémentaires.<br />Supersize et galerie Plus engendreront également des frais supplémentaires.';
$_['lang_images_text_2']			= 'Les modèles d’images seront ajoutées à la description de votre annonce et hébergées sur votre site web, ils sont gratuits. (Votre modèle d’annonce doit avoir le tag {gallery})';
$_['lang_image_gallery']			= 'Taille de l’image Galerie';
$_['lang_image_thumb']				= 'Taille de la vignette';
$_['lang_template_image']			= 'Image du thème graphique';
$_['lang_main_image_ebay']			= 'Image principale eBay';
$_['lang_image_ebay']				= 'Image eBay';
$_['lang_images_none']				= 'Il n’y a aucune image pour ce produit';
$_['lang_images_supersize']			= 'Images Supersize<span class="help">Très grandes photos</span>';
$_['lang_images_gallery_plus']		= 'Galerie plus<span class="help">Grande photo dans les résultats de recherche</span>';

//Price and details
$_['lang_listing_condition']		= 'État de l’article';
$_['lang_listing_duration']			= 'Durée de parution de l’annonce';
$_['lang_listing_1day']				= '1 jour';
$_['lang_listing_3day']				= '3 jours';
$_['lang_listing_5day']				= '5 jours';
$_['lang_listing_7day']				= '7 jours';
$_['lang_listing_10day']			= '10 jours';
$_['lang_listing_30day']			= '30 jours';
$_['lang_listing_gtc']				= 'Jusqu’à l’annulation';
$_['lang_stock_matrix']				= 'Matrice de stock';
$_['lang_stock_col_qty_total']		= 'En stock';
$_['lang_stock_col_qty']			= 'Obtention de la liste';
$_['lang_stock_col_qty_reserve']	= 'Réservé';
$_['lang_stock_col_comb']			= 'Combinaison';
$_['lang_stock_col_price']			= 'Prix';
$_['lang_stock_col_enabled']		= 'Activé';
$_['lang_qty']						= 'Quantité par annonce<span class="help">Entrer un montant inférieur si vous voulez maintenir un niveau de stock inférieur sur eBay</span>';
$_['lang_price_ex_tax']				= 'Prix HT';
$_['lang_price_ex_tax_help']		= 'Prix standard de votre article hors taxes. Cette valeur n’est pas envoyée à eBay.';
$_['lang_price_inc_tax']			= 'Prix TTC';
$_['lang_price_inc_tax_help']		= 'C’est la valeur qui sera envoyée à eBay et que les utilisateurs devront régler.';
$_['lang_tax_inc']					= 'Taxe incluse';
$_['lang_offers']					= 'Permettre aux acheteurs de faire des offres';
$_['lang_private']					= 'Vente aux enchères privée (cacher les noms des acheteurs)';
$_['lang_imediate_payment']			= 'Paiement immédiat nécessaire ?';
$_['lang_payment']					= 'Paiements acceptés';
$_['lang_payment_pp_email']			= 'Courriel pour le paiement PayPal :';
$_['lang_payment_instruction']		= 'Instructions de paiement';

//Shipping tab
$_['lang_item_postcode']			= 'Code postal d’emplacement<span class="help">Un code postal aidera eBay à choisir un emplacement approprié pour votre annoce.</span>';
$_['lang_item_location']			= 'Ville ou région d’emplacement<span class="help">La saisie d’une ville est moins fiable qu’un code postal</span>';
$_['lang_despatch_time']			= 'Délai d’expédition<span class="help">Maximun de jours pour l’envoi</span>';
$_['lang_despatch_country']         = 'Pays d’expédition';
$_['lang_shipping_national']		= 'Services nationaux';
$_['lang_shipping_international']	= 'Services internationaux';
$_['lang_service']					= '<strong>Service</strong>';
$_['lang_shipping_cost_first']		= '<strong>Coût</strong>';
$_['lang_shipping_cost_add']		= '<strong>chaque article supplémentaire</strong>';
$_['lang_shipping_post_to']			= '<strong>Poster à</strong>';
$_['lang_shipping_post_ww']			= 'Monde entier';
$_['lang_shipping_post_dm']			= 'Domestique et suivante';
$_['lang_shipping_max_national']	= 'Pour la catégorie choisie, eBay permet à un responsable de l’expédition nationale maximale de ';
$_['lang_shipping_getitfast']		= 'Obtention rapide !';

//Returns
$_['lang_return_accepted']			= 'Retour accepté ?';
$_['lang_return_type']				= 'Type de retour';
$_['lang_return_policy']			= 'Politique de retour';
$_['lang_return_days']				= 'Jours de retour';
$_['lang_return_scosts']			= 'Coûts de livraison';
$_['lang_return_restock']           = 'Frais de restockage';


$_['lang_return_scosts_1']			= 'L’acheteur paie toute l’expédition de retour';
$_['lang_return_scosts_2']			= 'Le vendeur paie toute l’expédition de retour';

//Review page
$_['lang_review_costs']				= 'Co&uacir;ts de l’annonce';
$_['lang_review_costs_total']		= 'Totaux des frais eBay';
$_['lang_review_edit']				= 'Modifier l’annonce';
$_['lang_review_preview']			= 'Visualiser l’annonce';
$_['lang_review_preview_help']		= '(balises eBay non affichées)';

//Created
$_['lang_created_title']			= 'Annonce créée';
$_['lang_created_msg']				= 'Votre annonce eBay a été créé. Le numéro d’article eBay est ';

//Failed page
$_['lang_failed_title']				= 'L’annonce pour votre article a échoué';
$_['lang_failed_msg1']				= 'Il peut y avoir plusieurs raisons à cela.';
$_['lang_failed_li1']				= 'Si vous êtes un nouveau vendeur sur eBay (ou que vous n’avez pas vendu beaucoup par le passé), vous devrez contacter eBay pour retirer vos restrictions de vendeur';
$_['lang_failed_li2']				= 'Vous n’êtes pas abonné au Gestionnaire de ventes Pro sur eBay - c’est une exigence.';
$_['lang_failed_li3']				= 'Votre compte OpenBay Pro est suspendue, veuillez consulter via votre espace d’administration du module sous l’onglet "Mon compte"';
$_['lang_failed_contact']			= 'Si cette erreur persiste, veuillez contacter le support technique après avoir répondu à la question de ce qui précède.';
$_['lang_gallery_select_all']		= 'Tout sélectionner<span class="help">Cochez la case pour sélectionner toutes vos images à la fois.</span>';
$_['lang_template_images']			= 'Images du thème graphique';
$_['lang_ebay_images']				= 'Images eBay';
$_['lang_shipping_in_description']	= 'Information de transport dans la description<span class="help">US, UK, AU & CA seulement</span>';
$_['lang_profile_load']				= 'Chargement du profil';
$_['lang_shipping_first']			= 'Premier article : ';
$_['lang_shipping_add']				= 'Articles additionnels : ';
$_['lang_shipping_service']			= 'Service : ';
$_['lang_btn_remove']				= 'Enlever';
$_['lang_shop_category']			= 'Catégorie de la boutique';
$_['lang_tab_ebay_catalog']			= 'Catalogue eBay';

//Option images
$_['lang_option_images']			= 'Images de variation';
$_['lang_option_images_grp']		= 'Choisir un groupe d’option';
$_['lang_option_images_choice']		= 'Images';
$_['lang_option_description']		= 'Les images de variation peuvent être utilisées pour afficher une image précise lorsque l’utilisateur effectue une sélection d’une option. Vous pouvez utiliser un seul jeu de variation pour les images mais pouvez avoir jusqu’à 12 images par variation. Les images par défaut sont chargés de vos valeurs d’option (défini dans le catalogue > Options)';

//Product catalog
$_['lang_search_catalog']			= 'Recherche dans le catalogue eBay :';
$_['lang_image_catalog']			= 'Image par défaut :<span class="help">Cela va changer votre image principale et sera configuré pour utiliser l’image du catalogue eBay</span>';

//Errors
$_['lang_error_choose_category']	= 'Vous devez choisir une catégorie';
$_['lang_error_enter_text']			= 'Entrer le texte de recherche';
$_['lang_error_no_stock']			= 'Vous ne pouvez pas mettre un article ayant zéro comme quantité en stock';
$_['lang_error_no_catalog_data']	= 'Aucune donnée du catalogue eBay n’a été trouvé pour votre produit dans eBay';
$_['lang_error_missing_settings']   = 'Vous ne pouvez pas lister les articles que vous synchronisez avec les paramètres d’eBay';
?>