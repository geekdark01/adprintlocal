<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// Text
$_['text_filter']						= 'Filtrer les résultats';
$_['text_title']						= 'Titre';
$_['text_stock_range']					= 'Variation de stock';
$_['text_status']						= 'État';
$_['text_status_marketplace']			= 'État de la place de marché';
$_['text_price_range']					= 'Prix range';
$_['text_model']						= 'Modèle';
$_['text_category']						= 'Catégorie';
$_['text_category_missing']				= 'Catégorie manquante';
$_['text_manufacturer']					= 'Fabricant';
$_['text_populated']					= 'Peuplé';
$_['text_sku']							= 'Référence SKU';
$_['text_description']					= 'Description';
$_['text_variations']					= 'variations';
$_['text_variations_stock']				= 'stock';

// Text status options
$_['text_status_all']					= 'Tout';
$_['text_status_ebay_active']			= 'eBay actif';
$_['text_status_ebay_inactive']			= 'eBay inactif';
$_['text_status_amazoneu_saved']		= 'Amazon EU sauvegardé';
$_['text_status_amazoneu_processing']	= 'Amazon EU en traitement';
$_['text_status_amazoneu_active']		= 'Amazon EU actif';
$_['text_status_amazoneu_notlisted']	= 'Amazon EU non listé';
$_['text_status_amazoneu_failed']		= 'Amazon EU échoué';
$_['text_status_amazoneu_linked']		= 'Amazon EU lié';
$_['text_status_amazoneu_notlinked']	= 'Amazon EU délié';
$_['text_status_amazonus_saved']		= 'Amazon US sauvegardé';
$_['text_status_amazonus_processing']	= 'Amazon US en traitement';
$_['text_status_amazonus_active']		= 'Amazon US actif';
$_['text_status_amazonus_notlisted']	= 'Amazon US non listé';
$_['text_status_amazonus_failed']		= 'Amazon US échoué';
$_['text_status_amazonus_linked']		= 'Amazon US lié';
$_['text_status_amazonus_notlinked']	= 'Amazon US délié';