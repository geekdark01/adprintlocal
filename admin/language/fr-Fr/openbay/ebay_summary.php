<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

$_['lang_heading']              = 'eBay sommaire';
$_['lang_openbay']              = 'OpenBay Pro';
$_['lang_ebay']                 = 'eBay';
$_['lang_btn_return']           = 'Retour';
$_['lang_use_desc']             = 'Ceci est votre page de résumé de compte eBay. Il s’agit d’un aper&ccedil;u rapide de toutes les limites de votre compte ainsi que de vos performances de vente DSR.';
$_['lang_load']                 = 'Rafraîchir';
$_['lang_limits_heading']       = 'Limite des ventes';
$_['lang_error_validation']     = 'Vous devez vous inscrire à votre jeton d’API et activer le module.';
$_['lang_ajax_load_error']      = 'Désolé, la connexion au serveur a échoué';
$_['lang_ebay_limit_head']      = 'Limite des ventes de votre comte eBay';
$_['lang_ebay_limit_t1']        = 'Vous pouvez vendre';
$_['lang_ebay_limit_t2']        = 'plus d’articles (cela est le montant total des articles, et non pas les dossiers individuels) pour la valeur de';
$_['lang_ebay_limit_t3']        = 'Lorsque vous essayez de créer de nouvelles annonces, ce sera un échec si vous dépassez les montants ci-dessus.';
$_['lang_as_described']         = 'Article comme décrit';
$_['lang_communication']        = 'Communication';
$_['lang_shippingtime']         = 'Délai de livraison';
$_['lang_shipping_charge']      = 'Frais d’expédition';
$_['lang_score']                = 'Score';
$_['lang_count']                = 'Compteur';
$_['lang_report_30']            = '30 jours';
$_['lang_report_52']            = '52 semaines';
$_['lang_title_dsr']            = 'Rapports DSR';
?>