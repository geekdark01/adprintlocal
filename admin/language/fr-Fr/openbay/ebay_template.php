<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

$_['lang_heading']					= 'Modèle graphique de l’annonce';
$_['lang_ebay']                     = 'eBay';
$_['lang_openbay']                  = 'OpenBay Pro';
$_['lang_page_title']               = 'OpenBay Pro pour eBay';
$_['lang_btn_add']                  = 'Ajouter un modéle graphique';
$_['lang_btn_edit']                 = 'Éditer';
$_['lang_btn_delete']               = 'Supprimer';
$_['lang_btn_cancel']               = 'Annuler';
$_['lang_btn_return']               = 'Retourner';
$_['lang_btn_save']                 = 'Sauvegarder';
$_['lang_btn_remove']               = 'Déplacer';
$_['lang_yes']                      = 'Oui';
$_['lang_no']                       = 'Non';

$_['lang_title_list']               = 'Modèles graphique';
$_['lang_title_list_add']           = 'Ajouter un modéle graphique';
$_['lang_title_list_edit']          = 'Éditer le modéle graphique';
$_['lang_no_results']               = 'Aucun modéle graphique trouvé';
$_['lang_template_name']            = 'Nom';
$_['lang_template_html']            = 'HTML';
$_['lang_template_action']          = 'Action';
$_['lang_no_template']              = 'ID du modéle graphique inexistant';
$_['lang_added']                    = 'Le nouveau modéle graphique a bien été ajouté';
$_['lang_updated']                  = 'Le modéle graphique a bien été mis à jour';

$_['lang_error_name']               = 'Vous devez entrer un nom de modéle graphique';
$_['lang_confirm_delete']           = 'étes-vous sûr de vouloir supprimer le modéle graphique ?';
$_['invalid_permission']			= 'Attention, vous n’avez pas les droits nécessaires pour modifier le <b>Modèle graphique</b> !';
?>