<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// Heading
$_['lang_title']						= 'Annonce sur Amazon US';
$_['lang_openbay']						= 'OpenBay Pro';
$_['lang_amazonus']						= 'Amazon US';

// Button
$_['button_search']						= 'Recherche';
$_['button_new']						= 'Créer un nouveau produit';
$_['button_return']						= 'Retourner aux produits';
$_['button_amazonus_price']				= 'Obtenez le prix d’Amazon';
$_['button_list']						= 'Liste sur Amazon';

// Text
$_['text_view_on_amazonus']				= 'Voir sur Amazon US';
$_['text_list']							= 'Liste';
$_['text_new']							= 'Neuf';
$_['text_used_like_new']				= 'Occasion - Comme neuf';
$_['text_used_very_good']				= 'Occasion - Très bon état';
$_['text_used_good']					= 'Occasion - Bon état';
$_['text_used_acceptable']				= 'Occasion - Acceptable';
$_['text_collectible_like_new']			= 'Collection - Comme neuf';
$_['text_collectible_very_good']		= 'Collection - Très bon état';
$_['text_collectible_good']				= 'Collection - Bon état';
$_['text_collectible_acceptable']		= 'Collection - Acceptable';
$_['text_refurbished']					= 'Restauré';
$_['text_product_sent']					= 'Le produit a été envoyé avec succès à Amazon US.';
$_['text_product_not_sent']				= 'Le produit n’a pas été envoyé à Amazon US pour la raison suivante : %s';
$_['lang_no_results']					= 'Aucun résultat';
$_['lang_not_in_catalog']				= 'Ou, si ce n’est pas dans le catalogue&nbsp;&nbsp;&nbsp;';

// Columns
$_['column_image']						= 'Image';
$_['column_asin']						= 'ASIN';
$_['column_name']						= 'Nom du produit';
$_['column_price']						= 'Prix';
$_['column_action']						= 'Action';

// Entry
$_['entry_sku']							= 'Référence SKU :';
$_['entry_condition']					= 'État de l’article :';
$_['entry_condition_note']				= 'Note sur l’état :';
$_['entry_price']						= 'Prix :';
$_['entry_sale_price']					= 'Prix de vente :';
$_['entry_quantity']					= 'Quantité :';
$_['entry_start_selling']				= 'Disponible à partir de :';
$_['entry_restock_date']				= 'Date de ré-approvisionnement :';
$_['entry_country_of_origin']			= 'Pays d’origine :';
$_['entry_release_date']				= 'Date de sortie :';
$_['entry_from']						= 'Date à partir de';
$_['entry_to']							= 'Date jusqu’à';

// Form placeholders
$_['lang_placeholder_search']			= 'Entrer le nom du produit, UPC, EAN, ISBN ou ASIN';
$_['lang_placeholder_condition']		= 'Utilisez cet emplacement pour décrire l’état de vos produits.';

// Help
$_['help_sku']							= 'ID unique du produit assigné par le marchand';
$_['help_restock_date']					= 'Il s’agit de la date à laquelle vous serez en mesure d’expédier les articles au client. Cette date ne doit pas être plus de 30 jours à compter de la date indiquée ou les commandes reçues automatiquement peuvent être annulées.';
$_['help_sale_price']					= 'Le prix ??de vente doit avoir une date de début et une date de fin';

// Error
$_['error_text_missing']				= 'Vous devez entrer des informations de recherche.';
$_['error_data_missing']				= 'Les données requises sont manquantes';
$_['error_missing_asin']				= 'ASIN est manquant';
$_['error_marketplace_missing']			= 'Veuillez sélectionner une place de marché';
$_['error_condition_missing']			= 'Veuillez sélectionner un état pour le produit';
$_['error_fetch']						= 'Impossible d’obtenir les données';
$_['error_amazonus_price']				= 'Impossible d’obtenir le prix sur Amazon US';
$_['error_stock']						= 'Vous ne pouvez pas mettre un article ayant moins de 1 en quantité en stock';
$_['error_sku']							= 'Vous devez entrer un SKU pour cet article';
$_['error_price']						= 'Vous devez entrer un prix pour cet article';

// Tab
$_['tab_required_info']					= 'Information requise';
$_['tab_additional_info']				= 'Options additionnelles';

// Tab headers
$_['item_links_header_text']			= 'Liens des articles';
$_['quick_listing_header_text']			= 'Annonce rapide';
$_['advanced_listing_header_text']		= 'Annonce avancée';
$_['saved_header_text']					= 'Annonces sauvegardées';
$_['lang_tab_main']						= 'Page principale';

// Tabs
$_['item_links_tab_text']				= 'Liens des articles';
$_['quick_listing_tab_text']			= 'Annonce rapide';
$_['advanced_listing_tab_text']			= 'Annonce avancée';
$_['saved_tab_text']					= 'Annonces sauvegardées';

// Errors
$_['text_error_connecting']				= 'Attention : Il y a un problème de connexion aux serveurs de l’API Welford médias. Veuillez vérifier vos paramètres de l’ extension OpenBay Pro Amazon. Si le problème persiste, veuillez contacter le support Welford.';

// Quick/advanced listing tabs
$_['quick_listing_description']			= 'Utilisez cette méthode lorsque vous comparez le catalogue de produits existant déjà dans Amazon. La correspondance sera trouvée en utilisant l’ID standard du produit sur Amazon (ISBN, UPC, EAN)';
$_['advanced_listing_description']		= 'Utilisez cette méthode pour créer de nouvelles annonces sur Amazon.';
$_['listing_row_text']					= 'Annonce pour le produit :';
$_['already_saved_text']				= 'Ce produit se trouve déjà dans les annonces sauvegardées. Cliquez sur Modifier si vous souhaitez vérifier.';
$_['save_button_text']					= 'Sauvegarder';
$_['save_upload_button_text']			= 'Enregistrer et télécharger';
$_['saved_listings_button_text']		= 'Voir les annonces sauvegardées';
$_['cancel_button_text']				= 'Annuler';
$_['field_required_text']				= 'Le champ est requis !';
$_['not_saved_text']					= 'L’annonce n’a pas été enregistré. Vérifiez votre entrée.';
$_['chars_over_limit_text']				= 'Dépassement de la limite de caractères autorisés.';
$_['minimum_length_text']				= 'La longueur minimun est';
$_['characters_text']					= 'caractères';
$_['delete_confirm_text']				= '&Ecirctes-vous sûr';
$_['clear_image_text']					= 'Nettoyer';
$_['browse_image_text']					= 'Parcourir';
$_['category_selector_field_text']		= 'Catégorie sur Amazon :';

//Item links tab
$_['item_links_description']			= 'Ici vous pouvez ajouter et modifier les liens des articles existants sur Amazon sans les énumérer à partir OpenCart. Cela permettra aux actions de contrôle entre les marchés permis. Si vous avez installé openStock - Cela vous permettra de lier également les options d’un seul SKU d’article sur Amazon. (Téléchargement produits à partir OpenCart à Amazon va automatiquement ajouter des liens)';
$_['new_link_table_name']				= 'Nouveau lien';
$_['new_link_product_column']			= 'Produit';
$_['new_link_sku_column']				= 'Référence SKU';
$_['new_link_amazonus_sku_column']		= 'Référence SKU de l’article sur Amazon US';
$_['new_link_action_column']			= 'Action';
$_['item_links_table_name']				= 'Liens des articles';

// Marketplaces
$_['marketplaces_field_text']			= 'Place de marché';
$_['marketplaces_help']					= 'Vous pouvez choisir la place de marché par défaut dans les paramètres d’extension d’Amazon';
$_['text_germany']						= 'Allemagne';
$_['text_france']						= 'France';
$_['text_italy']						= 'Italie';
$_['text_spain']						= 'Espagne';
$_['text_united_kingdom']				= 'Angleterre';

// Saved listings tab
$_['saved_listings_description']		= 'Il s’agit de la liste des annonces de produits enregistrées localement et qui sont prêts à être téléchargé sur Amazon. Cliquez sur Télécharger pour les poster.';
$_['actions_edit_text']					= 'Éditer';
$_['actions_remove_text']				= 'Supprimer';
$_['upload_button_text']				= 'Télécharger';
$_['name_column_text']					= 'Nom';
$_['model_column_text']					= 'Modèle';
$_['sku_column_text']					= 'Référence SKU';
$_['amazonus_sku_column_text']			= 'Référence SKU de l’article sur Amazon US';
$_['actions_column_text']				= 'Action';
$_['saved_localy_text']					= 'Annonces sauvegardées localement.';
$_['uploaded_alert_text']				= 'Annonces sauvegardées téléchargées !';
$_['upload_failed']						= 'L’ajout du produit avec ce SKU a échoué : %s. Raison : %s Processus de Téléchargement Annulé.';

//Item links
$_['links_header_text']					= 'Liens des articles';
$_['links_desc1_text']					= 'Lier vos articles permet un contrôle du stock de vos annonces sur Amazon.<br /> Pour chaque article dont le stock est mis à jour en local (stock disponible dans votre magasin OpenCart) Amazon mettra à jour votre annonce';
$_['links_desc2_text']					= 'Vous pouvez lier manuellement des articles en entrant sur Amazon La référence SKU et le nom du produit ou charger tous les produits non liés sur Amazon puis entrer leur référence. (Le téléchargement des produits depuis OpenCart vers Amazon va automatiquement ajouter les liens correspondants)';
$_['links_load_btn_text']				= 'Cgargement';
$_['links_new_link_text']				= 'Nouveau lien';
$_['links_autocomplete_product_text']	= 'Produit<span class="help">(Autocomplétion depuis le nom)</span>';
$_['links_amazonus_sku_text']			= 'Référence SKU de l’article sur Amazon US';
$_['links_action_text']					= 'Action';
$_['links_add_text']					= 'Ajouter';
$_['links_add_sku_tooltip']				= 'Ajouter un nouveau SKU';
$_['links_remove_text']					= 'Supprimer';
$_['links_linked_items_text']			= 'Liens liés';
$_['links_unlinked_items_text']			= 'Liens déliés';
$_['links_name_text']					= 'Nom';
$_['links_model_text']					= 'Modèle';
$_['links_sku_text']					= 'Référence SKU';
$_['links_amazonus_sku_text']			= 'Référence SKU de l’article sur Amazon US';
$_['links_sku_empty_warning']			= 'La référence SKU de l’article sur Amazon ne peut pas être vide !';
$_['links_name_empty_warning']			= 'Le nom du produit ne peut pas être vide !';
$_['links_product_warning']				= 'Produit inexistant. Veuillez utiliser les valeurs de saisie semi-automatique.';
$_['option_default']					= '-- Sélectionner une option --';
$_['lang_error_load_nodes']				= 'Impossible de charger les n&oelig;uds de navigation';

// Listing edit page
$_['text_edit_heading']					= 'Vue d’ensemble des annonces';
$_['text_has_saved_listings']			= 'Ce produit a une ou plusieurs annonces enregistrées localement.';
$_['text_product_links']				= 'Liens des produits';
$_['button_create_new_listing']			= 'Créer une nouvelle annonce';
$_['button_remove_links']				= 'Supprimer les liens';
$_['button_saved_listings']				= 'Voir les annonces sauvegardées';
$_['column_name']						= 'Nom du produit';
$_['column_model']						= 'Modèle';
$_['column_combination']				= 'Combinaison';
$_['column_sku']						= 'Référence SKU';
$_['column_amazonus_sku']				= 'Référence SKU de l’article sur Amazon US';

// Messages
$_['text_links_removed']				= 'Liens des produits sur Amazon supprimés';
?>