<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// Heading
$_['heading_title']						= 'Liens en nombre';

// Button
$_['button_return']						= 'Retour';
$_['button_load']						= 'Charger';
$_['button_link']						= 'Lien';

// Text
$_['text_openbay']						= 'OpenBay Pro';
$_['text_overview']						= 'Vue d’ensemble';
$_['text_amazon']						= 'Amazon';
$_['text_local']						= 'Local';
$_['text_bulk_linking']					= 'Liens en nombre';
$_['text_load_listings']				= 'Chargez vos annonces à partir d’Amazon';
$_['text_load_listings_help']			= 'Le chargement de toutes vos annonces à partir d’Amazon peut prendre un certain temps (jusqu’à 2 heures dans certains cas). Si vous liez vos articles, les niveaux de stock sur Amazon seront mis à jour avec les niveaux des stocks de votre magasin.';
$_['text_loading_listings']				= 'Les annonces sont en cours de chargement sur ​​Amazon';
$_['text_report_requested']				= 'Le rapport sur les annonces demandé à Amazon s’ déroulé avec succès';
$_['text_report_request_failed']		= 'Demande de rapport sur les annonces impossible';
$_['text_uk']							= 'Angleterre';
$_['text_de']							= 'Allemagne';
$_['text_fr']							= 'France';
$_['text_it']							= 'Italie';
$_['text_es']							= 'Espagne';

// Column
$_['column_asin']						= 'ASIN';
$_['column_price']						= 'Prix';
$_['column_name']						= 'Nom';
$_['column_sku']						= 'Référence SKU';
$_['column_quantity']					= 'Quantité';
$_['column_combination']				= 'Combinaison';

// Error
$_['error_bulk_linking_not_allowed']	= 'Les liens en nombre ne sont pas autorisés. Votre compte doit être au moins à moyen sur le plan';
?>