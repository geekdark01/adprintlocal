<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

$_['lang_title']                    = 'OpenBay Pro pour Amazon EU | Paramètres';
$_['lang_openbay']                  = 'OpenBay Pro';
$_['lang_overview']                 = 'Amazon EU vue d’ensemble';
$_['lang_settings']                 = 'Paramètres';
$_['lang_token']                    = 'Jeton';
$_['lang_enc_string1']              = 'Chaîne de cryptage 1';
$_['lang_enc_string2']              = 'Chaîne de cryptage 2';
$_['lang_enabled']                  = 'Autorisé';
$_['lang_yes']                      = 'Oui';
$_['lang_no']                       = 'Non';
$_['lang_btn_save']                 = 'Sauvegarder';
$_['lang_btn_cancel']               = 'Annuler';
$_['lang_api_status']               = 'État de la connexion de l’API';
$_['lang_api_ok']                   = 'Connexion OK, Authentification OK';
$_['lang_api_auth_error']           = 'Connexion OK, Authentification échouée';
$_['lang_api_error']                = 'Erreur de connexion';
$_['lang_order_statuses']           = 'États des commandes';
$_['lang_unshipped']                = 'Non livré';
$_['lang_partially_shipped']        = 'Livré partiellement';
$_['lang_shipped']                  = 'Livré';
$_['lang_canceled']                 = 'Annulé';
$_['lang_import_tax']               = 'Taxe pour les articles importés';
$_['lang_import_tax_help']          = 'Utilisé si Amazon ne taxe pas les informations de retour';
$_['lang_other']                    = 'Autre';
$_['lang_customer_group']           = 'Groupe client';
$_['lang_customer_group_help']      = 'Sélectionner un groupe de clients à assigner aux commandes importées';
$_['lang_marketplaces']             = 'Places de marché';
$_['lang_markets']                  = 'Choisissez parmi les places de marchés auxquelles vous souhaitez importer vos commandes';
$_['lang_de']                       = 'Allemagne';
$_['lang_fr']                       = 'France';
$_['lang_it']                       = 'Italie';
$_['lang_es']                       = 'Espagne';
$_['lang_uk']                       = 'Angleterre';
$_['lang_setttings_updated']        = 'Les paramètres ont été correctement mis à jour.';
$_['lang_error_permission']         = 'Vous n’avez pas accès à ce module';
$_['lang_main_settings']            = 'Détails de l’API';
$_['lang_main_settings_heading']    = 'Détails de la connexion à l’API';
$_['lang_listings_heading']         = 'Annonces';
$_['lang_subscription']             = 'Souscription';
$_['lang_link_items']               = 'Liens des articles';
$_['lang_listing']                  = 'Annonces';
$_['lang_orders']                   = 'Commandes';
$_['lang_tax_percentage']           = 'Pourcentage ajouté par défaut au prix du produit';
$_['lang_default_condition']        = 'Type de l’état du produit par défaut';
$_['lang_default_mp']               = 'Place de marché par défaut';
$_['lang_default_mp_help']          = 'Place de marché par défaut pour les inscriptions et les recherches produits';
$_['lang_admin_notify']             = 'Notifier l’administrateur de chaque nouvelle commande';
$_['lang_default_shipping']			= 'Livraison par défaut';
$_['lang_default_shipping_help']	= 'Utilisé comme l’option pré-sélectionné dans la mise à jour de la commande en gros';
?>