<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

//Headings
$_['lang_title']                    = 'OpenBay Pro pour Amazon US | Mises à jour du stock';

//Text
$_['lang_stock_updates']            = 'Mises à jour du stock';
$_['lang_openbay']                  = 'OpenBay Pro';
$_['lang_overview']                 = 'Amazon US vue d’ensemble';
$_['lang_my_account']               = 'Mon compte';
$_['lang_btn_return']               = 'Annuler';

//Table columns
$_['lang_ref']                      = 'Référence';
$_['lang_date_requested']           = 'Date souhaitée';
$_['lang_date_updated']             = 'Date de mise à jour';
$_['lang_status']                   = 'État';
$_['lang_sku']                      = 'Référence SKU sur Amazon US';
$_['lang_stock']                    = 'Stock';

//Table headings
$_['lang_empty']                    = 'Aucun résultat !';
$_['lang_date_start']               = 'Date de début :';
$_['lang_date_end']                 = 'Date de fin :';
$_['lang_filter_btn']               = 'Filtrer';
?>