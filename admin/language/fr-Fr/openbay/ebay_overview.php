<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

$_['lang_title']                    = 'OpenBay Pro pour eBay';
$_['lang_heading']                  = 'eBay vue d’ensemble';
$_['lang_openbay']                  = 'OpenBay Pro';
$_['lang_heading_settings']         = 'Paramètres';
$_['lang_heading_sync']             = 'Synchroniser';
$_['lang_heading_account']          = 'Mon compte';
$_['lang_heading_links']            = 'Liens des articles';
$_['lang_heading_item_import']      = 'Import des articles';
$_['lang_heading_order_import']     = 'Import des commandes';
$_['lang_heading_adds']             = 'Add-ons installés';
$_['lang_heading_summary']          = 'eBay sommaire';
$_['lang_heading_profile']          = 'Profils';
$_['lang_heading_template']         = 'Thèmes graphiques';
$_['lang_heading_ebayacc']          = 'eBay compte';
$_['lang_heading_register']         = 'Enregistrement ici';
?>
