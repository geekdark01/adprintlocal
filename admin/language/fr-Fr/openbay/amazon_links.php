<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------


$_['lang_title']                    = 'OpenBay Pro pour Amazon | Liens des articles';
$_['lang_openbay']                  = 'OpenBay Pro';
$_['lang_overview']                 = 'Amazon vue d’ensemble';
$_['lang_btn_return']               = 'Retour';
$_['lang_link_items']               = 'Lier les articles';
$_['lang_item_links']               = 'Liens des articles';
$_['lang_desc1']                    = 'Lier vos articles permet un contrôle du stock de vos annonces sur Amazon.<br /> Pour chaque article dont le stock est mis à jour en local (stock disponible dans votre magasin OpenCart) Amazon mettra à jour votre annonce';
$_['lang_desc2']                    = 'Vous pouvez lier manuellement des articles en entrant sur Amazon La référence SKU et le nom du produit ou charger tous les produits non liés sur Amazon puis entrer leur référence. (Le téléchargement des produits depuis OpenCart vers Amazon va automatiquement ajouter les liens correspondants)';
$_['lang_load_btn']                 = 'Chargement';
$_['lang_new_link']                 = 'Nouveau lien';
$_['lang_autocomplete_product']     = 'Produit<span class="help">(Autocomplétion depuis le nom)</span>';
$_['lang_amazon_sku']               = 'Référence SKU de l’article sur Amazon';
$_['lang_action']                   = 'Action';
$_['lang_add']                      = 'Ajouter';
$_['lang_add_sku_tooltip']          = 'Ajouter une autre référence SKU';
$_['lang_remove']                   = 'Supprimer';
$_['lang_linked_items']             = 'Articles liés';
$_['lang_unlinked_items']           = 'Articles déliés';
$_['lang_name']                     = 'Nom';
$_['lang_model']                    = 'Modèle';
$_['lang_combination']              = 'Combinaison';
$_['lang_sku']                      = 'Référence SKU';
$_['lang_amazon_sku']               = 'Référence SKU de l’article sur Amazon';
$_['lang_sku_empty_warning']        = 'La référence SKU de l’article sur Amazon ne peut pas être vide !';
$_['lang_name_empty_warning']       = 'Le nom du produit ne peut pas être vide !';
$_['lang_product_warning']          = 'Produit inexistant. Veuillez utiliser les valeurs de saisie semi-automatique.';
?>