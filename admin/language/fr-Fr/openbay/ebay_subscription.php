<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

$_['lang_heading']				= 'Souscription';
$_['lang_openbay']              = 'OpenBay Pro';
$_['lang_ebay']                 = 'eBay';
$_['lang_page_title']           = 'OpenBay Pro pour eBay';
$_['lang_error_validation']     = 'Vous devez vous inscrire à votre jeton d’API et activer le module.';
$_['lang_btn_return']           = 'Retour';
$_['lang_load']                 = 'Rafraîchir';
$_['lang_usage_title']          = 'Utilisation';
$_['lang_subscription_current'] = 'Plan actuel';
$_['lang_subscription_avail']   = 'Plans disponibles';
$_['lang_subscription_avail1']  = 'La modification des plans seront immédiats et les appels non utilisés ne seront pas crédités.';
$_['lang_subscription_avail2']  = 'Pour rétrograder au régime de base veuillez annuler votre abonnement actif à PayPal.';
$_['lang_ajax_acc_load_plan']   = 'Identifiant de souscription PayPal : ';
$_['lang_ajax_acc_load_plan2']  = ', vous devriez annuler tous les autres abonnements vers nous';
$_['lang_ajax_acc_load_text1']  = 'Nom du plan';
$_['lang_ajax_acc_load_text2']  = 'Limite d’appels';
$_['lang_ajax_acc_load_text3']  = 'Prix (par mois)';
$_['lang_ajax_acc_load_text4']  = 'Description';
$_['lang_ajax_acc_load_text5']  = 'Plan actuel';
$_['lang_ajax_acc_load_text6']  = 'Changer de plan';
$_['lang_ajax_load_error']      = 'Désolé, pas de réponse. Essayez plus tard.';
?>