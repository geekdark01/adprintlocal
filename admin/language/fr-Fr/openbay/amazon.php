<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// Heading
$_['heading_title']				= 'Amazon (EU)';

// Lang
$_['lang_heading_title']		= 'OpenBay Pro pour Amazon EU';

//Settings
$_['token_text']				= 'Jeton :';
$_['enc_string1_text']			= 'Chaîne de cryptage 1';
$_['enc_string2_text']			= 'Chaîne de cryptage 2';
$_['enabled_text']				= 'Autorisé :';
$_['yes_text']					= 'Oui';
$_['no_text']					= 'Non';
$_['save_text']					= 'Sauvegarder';

// Orders tab
$_['order_statuses_text']		= 'États des commandes';
$_['unshipped_text']			= 'Non livré';
$_['partially_shipped_text']	= 'Livré partiellement';
$_['shipped_text']				= 'Livré';
$_['canceled_text']				= 'Annulé';
$_['import_tax_text']			= 'Taxe pour les articles importés';
$_['import_tax_help']			= 'Utilisé si Amazon ne renvoie pas d’informations fiscales';
$_['other_text']				= 'Autre';
$_['customer_group_text']		= 'Groupe client';
$_['customer_group_help_text']	= 'Sélectionnez un groupe de clients à assigner aux commandes importées';

// Marketplace tab
$_['marketplaces_text']			= 'Places de marché';
$_['markets_text']				= 'Sélectionnez une place de marchés à partir duquel vous souhaitez importer vos commandes';
$_['de_text']					= "Allemagne";
$_['fr_text']					= "France";
$_['it_text']					= "Italie";
$_['es_text']					= "Espagne";
$_['uk_text']					= 'Angleterre';

// Subscriptions tab
$_['register_invite_text']		= 'Vous ne disposez pas d’information d’identification ? Inscrivez-vous maintenant pour les obtenir';
$_['register_text']				= 'Enregistrement';
$_['loading_text']				= 'Chargement';
$_['change_plans_text']			= 'Changer les plans';
$_['your_plan_text']			= 'Plan actuel et balance de votre compte';
$_['change_plan_text']			= 'Changer le plan';
$_['change_plans_help_text']	= 'Non satisfait de votre plan actuel ?';
$_['name_text']					= 'Nom';
$_['description_text']			= 'Description';
$_['price_text']				= 'Prix';
$_['order_frequency_text']		= 'Fréquence d’importation des commandes';
$_['product_listings_text']		= 'Nouvelles listes de produits par mois';
$_['listings_remaining_text']	= 'Listes des produits restants';
$_['listings_reserved_text']	= 'Produits en cours de traitement';

// Listings tab
$_['tax_percentage_text']		= 'Pourcentage ajouté par défaut au prix du produit';
$_['default_mp_text']			= 'Place de marché par défaut pour les listes de produits et les recherches';

// Misc
$_['setttings_updated_text']	= 'Les Paramètres ont été mis à jour avec succès !';
$_['error_permission']			= 'Vous ne pouvez pas accéder à ce module';
$_['settings_text']				= 'Paramètres principaux';
$_['subscription_text']			= 'Souscription';
$_['link_items_text']			= 'Lien articles';
$_['listing_text']				= 'Annonces';
$_['orders_text']				= 'Commandes';
?>