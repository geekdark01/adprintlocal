<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// Heading
$_['heading_title']							= 'OpenBay Pro pour Amazon | Annonces en nombre';

// Text
$_['text_openbay']							= 'OpenBay Pro';
$_['text_overview']							= 'Amazon vue d’ensemble';
$_['text_bulk_listing']						= 'Annonces en nombre';
$_['text_searching']						= 'Recherche';
$_['text_finished']							= 'Terminé';
$_['text_marketplace']						= 'Place de march&eacute';
$_['text_de']								= 'Allemagne';
$_['text_fr']								= 'France';
$_['text_es']								= 'Espagne';
$_['text_it']								= 'Italie';
$_['text_uk']								= 'Angleterre';
$_['text_filter_results']					= 'Filtrer les résultats';
$_['text_dont_list']						= 'Ne pas énumérer';
$_['text_listing_values']					= 'Valeurs des annonces';
$_['text_condition']						= 'État de l’article';
$_['text_condition_note']					= 'Note sur l’état';
$_['text_start_selling']					= 'Démarrer la vente';
$_['text_new']								= 'Neuf';
$_['text_used_like_new']					= 'Occasion - Comme neuf';
$_['text_used_very_good']					= 'Occasion - Très bon état';
$_['text_used_good']						= 'Occasion - Bon état';
$_['text_used_acceptable']					= 'Occasion - Acceptable';
$_['text_collectible_like_new']				= 'Collection - Comme neuf';
$_['text_collectible_very_good']			= 'Collection - Très bon état';
$_['text_collectible_good']					= 'Collection - Bon état';
$_['text_collectible_acceptable']			= 'Collection - Acceptable';
$_['text_refurbished']						= 'Restauré';

// Column
$_['column_name']							= 'Nom';
$_['column_image']							= 'Image';
$_['column_model']							= 'Modèle';
$_['column_status']							= 'État';
$_['column_matches']						= 'Nombre de résultats';
$_['column_result']							= 'Résultat';

// Button
$_['button_return']							= 'Retour';
$_['button_list']							= 'Liste';
$_['button_search']							= 'Recherche';

// Error
$_['error_product_sku']						= 'Le produit doit avoir un SKU';
$_['error_product_no_searchable_fields']	= 'Le produit doit avoir un ISBN, EAN, JAN ou un champ UPC de rempli';
$_['error_bulk_listing_not_allowed']		= 'Les annonces en nombre ne sont pas autorisées. Votre compte doit être au moins à moyen sur le plan';
?>