<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

$_['lang_title']					= 'OpenBay Pro pour Amazon EU';
$_['lang_heading']					= 'Amazon EU vue d’ensemble';
$_['lang_overview']					= 'Amazon EU vue d’ensemble';
$_['lang_openbay']					= 'OpenBay Pro';
$_['lang_heading_settings']			= 'Paramètres';
$_['lang_heading_account']			= 'Mon compte';
$_['lang_heading_links']			= 'Liens des articles';
$_['lang_heading_bulk_listing']		= 'Annonces en nombre';
$_['lang_heading_register']			= 'Enregistrement';
$_['lang_heading_stock_updates']	= 'Mises à jour du stock';
$_['lang_heading_saved_listings']	= 'Annonces sauvegardées';
$_['lang_heading_bulk_linking']		= 'Lier en nombre';
?>