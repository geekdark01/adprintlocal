<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

//Headings
$_['lang_heading']						= 'Profils';
$_['lang_ebay']							= 'eBay';
$_['lang_openbay']						= 'OpenBay Pro';
$_['lang_page_title']					= 'OpenBay Pro pour eBay';

//Buttons
$_['lang_btn_add']						= 'Ajouter un profil';
$_['lang_btn_edit']						= 'Éditer';
$_['lang_btn_delete']					= 'Supprimer';
$_['lang_btn_cancel']					= 'Annuler';
$_['lang_btn_return']					= 'Retour';
$_['lang_btn_save']						= 'Sauvegarder';
$_['lang_btn_remove']					= 'Supprimer';

//General profile options
$_['lang_title_list']					= 'Profils';
$_['lang_title_list_add']				= 'Ajouter un profil';
$_['lang_title_list_edit']				= 'Éditer le profil';
$_['lang_no_results']					= 'Aucun profil trouvé';
$_['lang_profile_name']					= 'Nom';
$_['lang_profile_default']				= 'Défaut';
$_['lang_profile_type']					= 'Type';
$_['lang_profile_desc']					= 'Description';
$_['lang_profile_action']				= 'Action';
$_['lang_yes']							= 'Oui';
$_['lang_no']							= 'Non';

//Tabs
$_['lang_tab_shipping']					= 'Livraison';
$_['lang_tab_general']					= 'Général';
$_['lang_tab_returns']					= 'Retours';
$_['lang_tab_template']					= 'Thème graphque';
$_['lang_tab_gallery']					= 'Galerie';
$_['lang_tab_settings']					= 'Paramètres';

//Shipping Profile
$_['lang_shipping_dispatch_country']    = 'Livraison à partir du pays';
$_['lang_shipping_postcode']            = 'Localisation - code postal';
$_['lang_shipping_location']            = 'Localisation - ville ou état';
$_['lang_shipping_despatch']            = 'Délai d’expédition<span class="help">(Nombre maximum de jours)</span>';
$_['lang_shipping_nat']                 = 'Services de livraison nationaux';
$_['lang_shipping_intnat']              = 'Services de livraison internationaux';
$_['lang_shipping_first']               = 'Premier article : ';
$_['lang_shipping_add']                 = 'Articles additionnels : ';
$_['lang_shipping_service']             = 'Service : ';
$_['lang_shipping_in_desc']             = 'Infos du fret dans la description<span class="help">US, UK, AU & CA seulement</span>';
$_['lang_shipping_getitfast']           = 'Livraison express!';
$_['lang_shipping_zones']               = 'Expédition par zone';
$_['lang_shipping_worldwide']           = 'Partout dans le monde';

//Returns profile
$_['lang_returns_accept']				= 'Retours acceptés';
$_['lang_returns_inst']					= 'Politique de retour';
$_['lang_returns_days']					= 'Jours de retour';
$_['lang_returns_days10']				= '10 jours';
$_['lang_returns_days14']				= '14 jours';
$_['lang_returns_days30']				= '30 jours';
$_['lang_returns_days60']				= '60 jours';
$_['lang_returns_type']					= 'Type de retour';
$_['lang_returns_type_money']			= 'Satisfait ou remboursé';
$_['lang_returns_type_exch']			= 'Remboursé ou échangé';
$_['lang_returns_costs']				= 'Frais de retour';
$_['lang_returns_costs_b']				= 'Paiement acheteur';
$_['lang_returns_costs_s']				= 'Paiement vendeur';
$_['lang_returns_restock']				= 'Frais de restockage';

//Template profile
$_['lang_template_choose']				= 'Théme graphique par défaut<span class="help">Un théme graphique par défaut se charge automatiquement lors de l’inscription pour gagner du temps</span>';
$_['lang_image_gallery']				= 'Taille de l’image de la galerie<span class="help">Taille des pixels de la galerie d’images qui seront ajoutés à votre modéle graphique.</span>';
$_['lang_image_thumb']					= 'Taille de l’image de la vignette<span class="help">Taille des pixels des vignettes qui seront ajoutés à votre modéle graphique.</span>';
$_['lang_image_super']					= 'Taille maximale des images';
$_['lang_image_gallery_plus']			= 'Galerie plus';
$_['lang_image_all_ebay']				= 'Ajouter toutes les images à eBay';
$_['lang_image_all_template']			= 'Ajouter toutes les images au modéle graphique';
$_['lang_image_exclude_default']		= 'Exclure l’image par défaut<span class="help">Seulement pour fonction d’inscription en nombre ! Cela n’inclura pas l’image du produit par défaut dans la liste des thémes d’image</span>';
$_['lang_confirm_delete']				= 'êtas-vous sûr de vouloir supprimer le profil ?';

//General profile
$_['lang_general_private']				= 'Liste des articles des enchères privées';
$_['lang_general_price']				= 'Prix % modification <span class="help">0 est par défaut, -10 réduira de 10 %, 10 augmentera de 10 % (utilisée uniquement sur ​​les annonces en nombre)</span>';

//Success messages
$_['lang_added']						= 'Le nouveau profil a bien été ajouté';
$_['lang_updated']						= 'Le profil a bien été mis à jour';

//Errors
$_['invalid_permission']				= 'Vous n’avez pas la permission de modifier les profils';
$_['lang_error_name']					= 'Vous devez entrer un nom de profil';
$_['lang_no_template']					= 'ID du thème graphique inexistant';
$_['lang_error_missing_settings']		= 'Vous ne pouvez pas ajouter, modifier ou supprimer des profils jusqu’à ce que vous syncronisiez vos paramètres eBay';
?>