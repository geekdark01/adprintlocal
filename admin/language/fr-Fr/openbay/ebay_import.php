<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

$_['lang_heading']                      = 'Import d’article';
$_['lang_openbay']                      = 'OpenBay Pro';
$_['lang_page_title']                   = 'OpenBay Pro pour eBay';
$_['lang_ebay']                         = 'eBay';
$_['lang_btn_return']                   = 'Retour';
$_['lang_sync_import_line1']            = '<strong>Attention !</strong> Cela importera tous vos produits eBay et construira une structure de catégorie dans votre magasin. Il est conseillé de supprimer toutes les catégories et les produits avant d’exécuter cette option.<br />La structure de catégorie des catégories d’eBay normales, pas les catégories de votre boutique (si vous avez une boutique eBay). Vous pouvez renommer, supprimer et modifier les catégories importées sans affecter vos produits eBay.';
$_['lang_sync_import_line2']            = 'Cette option utilise un grand nombre d’appel à l’API, un pour chaque produit ainsi qu’un appel supplémentaire toutes les 20 articles.';
$_['lang_sync_import_line3']            = 'Vous devez vous assurer que votre serveur peut accepter et traiter les grandes tailles de données POST. 1000 objets eBay est d’:environ 40 Mo en taille, vous aurez besoin de calculer ce que vous désirez. Si votre appel échoue, alors il est probable que votre réglage est trop faible. Votre limite de mémoire de PHP doit être d’environ 128Mb.';
$_['lang_sync_server_size']             = 'Actuellement votre serveur peut accepter : ';
$_['lang_sync_memory_size']             = 'Votre limite de mémoire PHP : ';
$_['lang_sync_item_description']        = 'Descriptions importation d’articles<span class="help">Tout sera importé<br /> y compris HTML, compteurs etc...</span>';
$_['lang_sync_notes_location']          = 'Importer des notes eBay pour des données de localisation';
$_['lang_import_ebay_items']            = 'Importer les articles eBay';
$_['lang_import']                       = 'Import';
$_['lang_error_validation']             = 'Vous devez vous inscrire pour votre clé API et activer le module.';
$_['lang_ajax_import_confirm']          = 'Cela va importer tous vos objets eBay ainsi que de nouveaux produits, êtes-vous sûr ? Cela ne peut pas être annulé! Assurez-vous d’avoir d’abord une sauvegarde!';
$_['lang_ajax_import_notify']           = 'Votre demande d’importation a été envoyé pour traitement. Une importation dure environ 1 heure par 1000 articles.';
$_['lang_ajax_load_error']              = 'Impossible de se connecter au serveur';
$_['lang_maintenance_fail']             = 'Votre bourique est en maintenance. L’mportation échouera !';
$_['lang_import_images_msg1']           = 'Les images sont en cours d’importation et de copie sur eBay. Actualisez cette page, si le nombre ne diminue pas';
$_['lang_import_images_msg2']           = 'cliquer ici';
$_['lang_import_images_msg3']           = 'et patienter. Plus d’informations sur les raisons de ce qui s’est passé peut être trouvé sur<a href="http://shop.openbaypro.com/index.php?route=information/faq&topic=8_45" target="_blank">here</a>';
?>