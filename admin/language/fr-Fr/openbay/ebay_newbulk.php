<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

//Headings
$_['lang_page_title']               = 'Annonces en nombre';

//Buttons
$_['lang_cancel']                   = 'Annuler';
$_['lang_none']                     = 'Aucun';
$_['lang_preview']                  = 'Aper&ccedil;u';
$_['lang_add']                      = 'Ajouter';
$_['lang_remove']                   = 'Supprimer';
$_['lang_save']                     = 'Sauvegarder';
$_['lang_preview_all']              = 'Tout vérifier';
$_['lang_view']                     = 'Voir les annonces';
$_['lang_edit']                     = 'Éditer';
$_['lang_submit']                   = 'Soumettre';
$_['lang_features']                 = 'Éditer les fonctionnalités';
$_['lang_catalog']                  = 'Sélectionner le catalogue';
$_['lang_catalog_search']           = 'Rechercher le catalogue';

//Form options / text
$_['lang_pixels']                   = 'Pixels';
$_['lang_yes']                      = 'Oui';
$_['lang_no']                       = 'Non';
$_['lang_other']                    = 'Autres';
$_['lang_select']                   = 'Sélectionner';

//Profile names
$_['lang_profile_theme']            = 'Profil du thème :';
$_['lang_profile_shipping']         = 'Profil de la livraison :';
$_['lang_profile_returns']          = 'Profil des retours :';
$_['lang_profile_generic']          = 'Profil g&éacute;nérique :';

//Text
$_['lang_title']                    = 'Titre';
$_['lang_price']                    = 'Prix';
$_['lang_stock']                    = 'Stock';
$_['lang_search']                   = 'Recherche';
$_['lang_loading']                  = 'Chargement des détails';
$_['lang_preparing0']               = 'Préparation';
$_['lang_preparing1']               = 'de';
$_['lang_preparing2']               = 'éléments';
$_['lang_condition']                = 'État :';
$_['lang_duration']                 = 'Délai :';
$_['lang_category']                 = 'Catégorie :';
$_['lang_exists']                   = 'Certains éléments figurent déjà sur eBay ,ils ont donc été retirés';
$_['lang_error_count']              = 'Vous &avez sélectionné %s articles, cela peut prendre un certain temps pour traiter vos données';
$_['lang_verifying']                = 'Vérification des articles';
$_['lang_processing']               = 'Traitement <span id="activeItems"></span> des articles';
$_['lang_listed']                   = 'ID article listé : ';
$_['lang_ajax_confirm_listing']     = 'êtes-vous sûr de vouloir lister en nombre ces articles ?';
$_['lang_bulk_plan_error']          = 'Votre plan actuel ne permet pas d’ajout en nombre, mettre à niveau votre plan <a href="%s">ici</a>';
$_['lang_item_limit']               = 'Vous ne pouvez pas lister ces articles car vous dépassez la limite de votre plan, améliorer votre plan <a href="%s">ici</a>';
$_['lang_search_text']              = 'Entrer le texte de recherche';
$_['lang_catalog_no_products']      = 'Aucun élément du catalogue trouvé';
$_['lang_search_failed']            = 'Recherche échouée';
$_['lang_esc_key']                  = 'La page d’accueil a été caché, mais peut ne pas avoir terminé le chargement';

//Errors
$_['lang_error_ship_profile']       = 'Vous devez avoir un profil de livraison par défaut mis en place';
$_['lang_error_generic_profile']    = 'Vous devez avoir un profil générique par défaut mis en place';
$_['lang_error_return_profile']     = 'Vous devez avoir un profil des retours par défaut mis en place';
$_['lang_error_theme_profile']      = 'Vous devez avoir un profil de thème par défaut mis en place';
$_['lang_error_variants']           = 'Les articles avec des variations ne peuvent pas être répertoriés en nombre et n’ont pas été sélectionné';
$_['lang_error_stock']              = 'Certains articles ne sont pas en stock et ont été enlevés';
$_['lang_error_no_product']         = 'Il n’y a pas de produits sélectionnés susceptibles d’utiliser la fonction de transfert groupé';
$_['lang_error_reverify']           = 'Il y a une erreur, vous devez re-vérifier les articles et les modifier';
$_['lang_error_missing_settings']   = 'Vous ne pouvez pas lister vos articles en nombre tant que vous n’aurez pas synchronisé vos paramètres eBay';
?>