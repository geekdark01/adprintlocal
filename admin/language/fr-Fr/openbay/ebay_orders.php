<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

$_['lang_openbay']                      = 'OpenBay Pro';
$_['lang_page_title']                   = 'OpenBay Pro pour eBay';
$_['lang_ebay']                         = 'eBay';
$_['lang_heading']                      = 'Import commandes';
$_['lang_btn_return']                   = 'Retour';
$_['lang_sync_orders']                  = 'Commandes';
$_['lang_sync_pull_orders']             = 'Tirer de nouvelles commandes';
$_['lang_sync_pull_orders_text']        = 'Tirer les commandes';
$_['lang_ajax_load_error']              = 'Désolé, connexion impossible';
$_['lang_error_validation']             = 'Vous devez vous inscrire pour obtenir votre clé API et activer le module.';
$_['lang_sync_pull_notice']             = 'Cela tirera les nouvelles commandes depuis le dernier contrôle automatisé. Si vous venez de faire l’installation, puis ce sera par défaut sur ​​les dernières 24 heures.';
$_['lang_ajax_orders_import']           = 'Toutes les nouvelles commandes devraient apparaître dans quelques instants.';
?>