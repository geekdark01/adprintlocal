<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// Locale
$_['code']							= 'fr';
$_['direction']						= 'ltr';
$_['date_format_short']				= 'd/m/Y';
$_['date_format_long']				= 'l j F Y';
$_['time_format']					= 'H:i:s';
$_['decimal_point']					= ',';
$_['thousand_point']				= '.';

// Text
$_['text_yes']						= 'Oui';
$_['text_no']						= 'Non';
$_['text_enabled']					= 'Activé';
$_['text_disabled']					= 'Désactivé';
$_['text_none']						= ' --- Aucun --- ';
$_['text_select']					= ' -- Sélection -- ';
$_['text_select_all']				= 'Tout sélectionner';
$_['text_unselect_all']				= 'Tout désélectionner';
$_['text_all_zones']				= 'Toutes zones';
$_['text_default']					= ' <b>(par défaut)</b>';
$_['text_close']					= 'Fermer';
$_['text_pagination']				= 'Voir {start} à {end} de {total} ({pages} Pages)';
$_['text_no_results']				= 'Aucun résultat !';
$_['text_separator']				= ' » ';
$_['text_edit']						= 'Modifier';
$_['text_view']						= 'Visualiser';
$_['text_home']						= 'Accueil';

// Button
$_['button_insert']					= 'Insérer';
$_['button_delete']					= 'Supprimer';
$_['button_save']					= 'Sauvegarder';
$_['button_cancel']					= 'Annuler';
$_['button_clear']					= 'Effacer les entrées du journal';
$_['button_close']					= 'Fermer';
$_['button_filter']					= 'Filtrer';
$_['button_send']					= 'Envoyer';
$_['button_edit']					= 'Éditer';
$_['button_copy']					= 'Copier';
$_['button_back']					= 'Retour';
$_['button_remove']					= 'Retirer';
$_['button_backup']					= 'Sauvegarde';
$_['button_restore']				= 'Restauration';
$_['button_repair']					= 'Réparer';
$_['button_upload']					= 'Chargement';
$_['button_submit']					= 'Soumettre';
$_['button_invoice']				= 'Facture';
$_['button_add_address']			= 'Ajouter une adresse';
$_['button_add_attribute']			= 'Ajouter un attribut';
$_['button_add_banner']				= 'Ajouter une bannière';
$_['button_add_custom_field_value'] = 'Ajouter un champ spécial';
$_['button_add_product']			= 'Ajouter un produit';
$_['button_add_voucher']			= 'Ajouter un bon de réduction';
$_['button_add_filter']             = 'Ajouter un Filtre';
$_['button_add_option']				= 'Ajouter une option';
$_['button_add_option_value']		= 'Ajouter une valeur';
$_['button_add_discount']			= 'Ajouter une réduction';
$_['button_add_special']			= 'Ajouter une promotion';
$_['button_add_image']				= 'Ajouter une image';
$_['button_add_geo_zone']			= 'Ajouter une zone géographique';
$_['button_add_history']			= 'Ajouter un historique';
$_['button_add_transaction']		= 'Ajouter une transaction';
$_['button_add_total']				= 'Ajouter un total';
$_['button_add_reward']				= 'Ajouter des points de fidélité';
$_['button_add_route']				= 'Ajouter un chemin';
$_['button_add_rule' ]				= 'Ajouter une règle';
$_['button_add_module']				= 'Ajouter un module';
$_['button_add_link']				= 'Ajouter un lien';
$_['button_update_total']			= 'Mise à jour des totaux';
$_['button_approve']				= 'Approuver';
$_['button_reset']					= 'Réinitialiser';
$_['button_add_profile']			= 'Ajouter un profil';

// Tab
$_['tab_address']					= 'Adresse';
$_['tab_admin']						= 'Admin';
$_['tab_attribute']					= 'Attributs';
$_['tab_customer']					= 'Détails client';
$_['tab_data']						= 'Données';
$_['tab_design']					= 'Design';
$_['tab_discount']					= 'Réductions';
$_['tab_general']					= 'Général';
$_['tab_history']					= 'Historique';
$_['tab_fraud']						= 'Fraude';
$_['tab_ftp']						= 'FTP';
$_['tab_ip']						= 'Adresses IP';
$_['tab_links']						= 'Liens';
$_['tab_log']						= 'Journal';
$_['tab_image']						= 'Images';
$_['tab_option']					= 'Options';
$_['tab_server']					= 'Serveur';
$_['tab_store']						= 'Boutique';
$_['tab_special']					= 'Promotions';
$_['tab_local']						= 'Localisation';
$_['tab_mail']						= 'Courriel';
$_['tab_marketplace_links']         = '<abbr title="Liens des places de marché">Liens PM</abbr>';
$_['tab_module']					= 'Modules';
$_['tab_order']						= 'Détails des commandes';
$_['tab_payment']					= 'Détails des paiements';
$_['tab_product']					= 'Produits';
$_['tab_return']					= 'Détails des retours';
$_['tab_reward']					= 'Points de fidélité';
$_['tab_profile']					= 'Profils';
$_['tab_shipping']					= 'Adresse de livraison';
$_['tab_total']						= 'Totaux';
$_['tab_transaction']				= 'Transactions';
$_['tab_voucher']					= 'Bon de réduction';
$_['tab_voucher_history']			= 'Historique des récépissés';
$_['tab_price']						= 'Prix';

// Error
$_['error_upload_1']				= 'Attention, le fichier envoyé dépasse la taille maiximale de fichier définie dans php.ini (upload_max_filesize) !';
$_['error_upload_2']				= 'Attention, le fichier envoyé dépasse la taille maiximale de fichier définie dans dans le formulaire HTML (MAX_FILE_SIZE) !';
$_['error_upload_3']				= 'Attention, le fichier envoyé a seulement été partiellement envoyé !';
$_['error_upload_4']				= 'Attention, aucun fichier n’a été envoyé !';
$_['error_upload_6']				= 'Attention, il manque un dossier temporaire !';
$_['error_upload_7']				= 'Attention, échec d’écriture sur le disque !';
$_['error_upload_8']				= 'Attention, envoi du fichier arrêté par extension !';
$_['error_upload_999']				= 'Attention, aucun code d’erreur disponible !';
?>