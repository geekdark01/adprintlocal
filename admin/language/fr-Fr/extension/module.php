<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//--------------------------------//

// Heading
$_['heading_title']		= 'Modules';

// Text
$_['text_install']		= 'Installer';
$_['text_uninstall']	= 'Désinstaller';

// Column
$_['column_name']		= 'Nom du module';
$_['column_action']		= 'Action';

// Error
$_['error_permission']	= 'Attention, vous n’avez la permission de modifier les <b>Modules</b> !';
?>