<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//--------------------------------//

// Heading
$_['heading_title']		= 'Flux de produits';

// Text
$_['text_install']		= 'Installer';
$_['text_uninstall']	= 'Désinstaller';

// Column
$_['column_name']		= 'Nom du flux de produits';
$_['column_status']		= 'État';
$_['column_action']		= 'Action';

// Error
$_['error_permission']  = 'Attention, vous n’avez la permission de modifier le <b>Flux de produits</b> !';
?>