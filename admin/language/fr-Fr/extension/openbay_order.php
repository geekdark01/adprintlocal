<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

$_['lang_btn_status']           = 'Changement d’état';
$_['lang_order_channel']        = 'Canal de commande';
$_['lang_confirmed']            = 'commandes ont été marquées';
$_['lang_no_orders']            = 'Pas de commande sélectionnés à mettre à jour';
$_['lang_confirm_title']        = 'Mise à jour de l’état des avis en nombre';
$_['lang_confirm_change_text']  = 'Changer l’état de la commande';
$_['lang_column_addtional']     = 'Informations complémentaires';
$_['lang_column_comments']      = 'Commentaires';
$_['lang_column_notify']        = 'Notifier';
$_['lang_carrier']              = 'Transporteur';
$_['lang_tracking']             = 'Suivi';
$_['lang_other']                = 'Autre';
$_['lang_refund_reason']        = 'Raison du remboursement';
$_['lang_refund_message']       = 'Message du remboursement';
$_['lang_update']               = 'Mise à jour';
$_['lang_cancel']               = 'Annulation';
$_['lang_e_ajax_1']             = 'Un ordre de lecture est manquant au message de remboursement !';
$_['lang_e_ajax_2']             = 'Un ordre de lecture est absent aux informations de suivi !';
$_['lang_e_ajax_3']             = 'Il manque l’entrée d’ "Autre transporteur" dans la commande Amazon!';
$_['lang_title_order_update']   = 'Mise à jour en nombre des commandes';
?>