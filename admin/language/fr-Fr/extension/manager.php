<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// Heading
$_['heading_title']		= 'Gestionnaire d’extensions';

// Text
$_['text_success']		= 'Félicitations, vous avez installé le <b>Gestionnaire d’extensions</b> avec succès !';

// Error
$_['error_permission']	= 'Attention, vous n’avez pas la permission de modifier le <b>Gestionnaire d’extensions</b> !';
$_['error_upload']		= 'Le chargement est requis !';
$_['error_filetype']	= 'Type de fichier invalide !';
?>