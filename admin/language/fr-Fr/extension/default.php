<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//--------------------------------//

$_['lang_openbay_new']				= 'Créer un nouveau listing';
$_['lang_openbay_edit']             = 'Afficher / éditer le listing';
$_['lang_openbay_fix']              = 'Fixer les erreurs';
$_['lang_openbay_processing']       = 'Traitement';

$_['lang_amazonus_saved']           = 'Sauvegardé (pas téléchargé)';
$_['lang_amazon_saved']             = 'Sauvegardé (pas téléchargé)';

$_['lang_markets']                  = 'Marchés';
$_['lang_bulk_btn']                 = 'eBay transfert groupé';
$_['lang_bulk_amazon_btn']          = 'Transfert groupé EU';

$_['lang_marketplace']              = 'Place de marché';
$_['lang_status']                   = 'État';
$_['lang_option']                   = 'Option';
?>