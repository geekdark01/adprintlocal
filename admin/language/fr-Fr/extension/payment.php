<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//--------------------------------//

// Heading
$_['heading_title']		= 'Paiement';

// Text
$_['text_install']		= 'Installer';
$_['text_uninstall']	= 'Désinstaller';

// Column
$_['column_name']		= 'Mode de paiement';
$_['column_status']		= 'État';
$_['column_sort_order']	= 'Classement';
$_['column_action']		= 'Action';

// Error
$_['error_permission']	= 'Attention, vous n’avez la permission de modifier les <b>Paiements</b> !';
?>