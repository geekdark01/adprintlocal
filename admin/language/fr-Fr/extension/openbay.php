<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//--------------------------------//

// Heading
$_['lang_heading_title']				= 'OpenBay Pro'; 
$_['lang_text_manager']					= 'Gestion d’OpenBay Pro'; 

// Text
$_['text_install']						= 'Installer';
$_['text_uninstall']					= 'Désinstaller';
$_['lang_text_success']					= 'Félicitations, les paramètres ont bien été sauvegardés';
$_['lang_text_no_results']				= 'Aucun résultat';
$_['lang_checking_version']				= 'Vérification de la version';
$_['lang_btn_manage']					= 'Gestion';
$_['lang_btn_retry']					= 'réessayer';
$_['lang_btn_save']						= 'Sauvegarder';
$_['lang_btn_cancel']					= 'Annuler';
$_['lang_btn_update']					= 'Mise à jour';
$_['lang_btn_settings']					= 'Paramètres';
$_['lang_btn_patch']					= 'Chemin';
$_['lang_btn_test']						= 'Test de connexion';
$_['lang_latest']						= 'Vous utilisez la dernière version';
$_['lang_installed_version']			= 'Version installée :';
$_['lang_admin_dir']					= 'Répertoire d’administration :';
$_['lang_admin_dir_desc']				= 'Si vous avez mis à jour votre répertoire administration pour le nouvel emplacement';
$_['lang_version_old_1']				= 'Une nouvelle version est disponible. Votre version est la ';
$_['lang_version_old_2']				= 'la dernière est la ';
$_['lang_use_beta']						= 'Utilisez la version bêta :';
$_['lang_use_beta_2']					= 'Non suggéré !';
$_['lang_test_conn']					= 'Test de connexion FTP :';
$_['lang_text_run_1']					= 'Exécuter la mise à jour';
$_['lang_text_run_2']					= 'Exécuter';
$_['lang_no']							= 'Non';
$_['lang_yes']							= 'Oui';
$_['lang_language']						= 'Langue de la réponse pour l’API :';
$_['lang_getting_messages']				= 'Recevoir les messages d’OpenBay Pro';

// Column
$_['lang_column_name']					= 'Nom du plugin';
$_['lang_column_status']				= 'État';
$_['lang_column_action']				= 'Action';

// Error
$_['error_permission']					= 'Attention, vous n’avez pas la permission de modifier eBay extensions!';

// Updates
$_['lang_use_pasv']						= 'Utiliser le FTP passif :';
$_['field_ftp_user']					= 'Nom d’utilisateur pour le FTP :';
$_['field_ftp_pw']						= 'Mot de passe pour le FTP :';
$_['field_ftp_server_address']			= 'Adresse du serveur pour le FTP :';
$_['field_ftp_root_path']				= 'Chemin sur le serveur pour le FTP :';
$_['field_ftp_root_path_info']			= '(Ex. : httpdocs/www)';
$_['desc_ftp_updates']					= 'Activation des mises à jour d’ici signifie que vous n’avez pas à mettre à jour manuellement votre module en utilisant le standard glisser-déposer via votre FTP.<br />';

//Updates
$_['lang_run_patch_desc']               = 'Poster la mise à jour du chemin :<span class="help">Seulement nécessaire si vous désirez mettre à jour manuellement</span>';
$_['lang_run_patch']                    = 'Appliquer le correctif';
$_['update_error_username']             = 'Nom d’utilisateur attendu';
$_['update_error_password']             = 'Mot de passe attendu';
$_['update_error_server']               = 'Serveur attendu';
$_['update_error_admindir']             = 'Répertoire d’administration attendu';
$_['update_okcon_noadmin']              = 'Connexion OK mais votre répertoire d’administration OpenCart n’a pas été trouvé';
$_['update_okcon_nofiles']              = 'Connexion OK mais vos dossiers OpenCart n’ont pas été trouvé ! Le chemin de la racine est-il correct ?';
$_['update_okcon']                      = 'Connexion au serveur OK. Les dossiers OpenCart ont été trouvé';
$_['update_failed_user']                = 'Impossible de se connecter avec ce nom d’utilisateur';
$_['update_failed_connect']             = 'Impossible de se connecter au serveur';
$_['update_success']                    = 'Le module a été mis à jour (v.%s)';
$_['lang_patch_notes1']                 = 'Pour en savoir plus sur les mises à jour récentes et passées,';
$_['lang_patch_notes2']                 = 'cliquer ici';
$_['lang_patch_notes3']                 = 'L’outil de mise à jour apportera des modifications aux fichiers système de votre boutique. Assurez-vous que vous avez une sauvegarde avant d’utiliser cet outil.';

//Help tab
$_['lang_help_title']                   = 'Information sur l’aide et le support';
$_['lang_help_support_title']           = 'Support :';
$_['lang_help_support_description']     = 'Vous devriez consulter notre <a href="http://shop.openbaypro.com/index.php?route=information/faq" title="FAQ OpenBay Pro pour support opencart ">FAQ</a> afin de voir si votre question y figure. <br />Si vous ne trouvez pas de réponse, vous pouvez créer un ticket de support en cliquant <a href="http://support.welfordmedia.co.uk" title="Site support OpenBay Pro pour OpenCart">ici</a>';
$_['lang_help_template_title']          = 'Création de modèles graphique pour eBay :';
$_['lang_help_template_description']    = 'Informations pour les développeurs et les concepteurs sur la création des modèles personnalisés pour leurs annonces eBay, <a href="http://shop.openbaypro.com/index.php?route=information/faq&topic=30" title="OpenBay Pro HTML templates for eBay">cliquer ici</a>';

$_['lang_tab_help']                     = 'Aide';
$_['lang_help_guide']                   = 'Guides de l’utilisateur :';
$_['lang_help_guide_description']       = 'Pour télécharger et consulter les guides de l’utilisateur d’eBay et d’Amazon <a href="http://shop.openbaypro.com/index.php?route=information/faq&topic=37" title="Guides de l’utilisateur d’OpenBay Pro">cliquer ici</a>';

$_['lang_mcrypt_text_false']            = 'La fonction PHP "mcrypt_encrypt" n’:est pas activée. Contacter votre fournisseur d’:hébergement.';
$_['lang_mb_text_false']                = 'La librairie PHP "mb strings" n’est pas activé. Contacter votre fournisseur d’hébergement.';
$_['lang_ftp_text_false']               = 'Les fonctions PHP FTP ne sont pas activés. Contacter votre fournisseur d’hébergement.';
$_['lang_error_oc_version']             = 'Votre version d’OpenCart n’a pas été testé pour fonctionner avec ce module. Vous pourriez rencontrer des problèmes.';
$_['lang_patch_applied']                = 'Patch appliqué';
$_['faqbtn']                            = 'Voir la FAQ';
$_['lang_clearfaq']                     = 'Effacer les fenêtres contextuelles cachées de la FAQ :';
$_['lang_clearfaqbtn']                  = 'Nettoyer';

// Ajax elements
$_['lang_ajax_ebay_shipped']			= 'La commande sera marqué comme expédiée sur eBay automatiquement';
$_['lang_ajax_amazoneu_shipped']        = 'La commande sera marqué comme expédiée sur Amazon EU automatiquement';
$_['lang_ajax_amazonus_shipped']        = 'La commande sera marqué comme expédiée sur Amazon US automatiquement';
$_['lang_ajax_refund_reason']           = 'Raison du remboursement';
$_['lang_ajax_refund_message']          = 'Message du remboursement';
$_['lang_ajax_refund_entermsg']         = 'Vous devez saisir un message de remboursement';
$_['lang_ajax_refund_charmsg']          = 'Votre message de remboursement doit être inférieur à 1000 caractères';
$_['lang_ajax_refund_charmsg2']         = 'Votre message ne peut pas contenir les caractères > ou <';
$_['lang_ajax_courier']                 = 'Courrier';
$_['lang_ajax_courier_other']           = 'Autre courrier';
$_['lang_ajax_tracking']                = 'Suivi #';
$_['lang_ajax_tracking_msg']            = 'Vous devez entrer un ID de suivi, utiliser "none" si vous n’en n’avez pas.';
$_['lang_ajax_tracking_msg2']           = 'Votre ID de suivi ne peut pas contenir les caractères > ou <';
$_['lang_ajax_tracking_msg3']           = 'Vous devez sélectionner courrier si vous souhaitez télécharger un numéro de suivi.';
$_['lang_ajax_tracking_msg4']           = 'Veuillez laissez le champ de messagerie vide si vous souhaitez utiliser un courrier personnalisé.';

$_['lang_title_help']                   = 'Besoin d’aide avec OpenBay Pro ?';
$_['lang_pod_help']                     = 'Aide';
$_['lang_title_manage']                 = 'Gestion OpenBay Pro; mises à jour, paramètres et plus';
$_['lang_pod_manage']                   = 'Gestion';
$_['lang_title_shop']                   = 'OpenBay Pro boutique; addons, thèmes graphiques et plus';
$_['lang_pod_shop']                     = 'Boutique';

$_['lang_checking_messages']            = 'Vérification des messages';
$_['lang_title_messages']               = 'Messages & notifications';
$_['lang_error_retry']					= 'Impossible de se connecter au serveur OpenBay.';
?>