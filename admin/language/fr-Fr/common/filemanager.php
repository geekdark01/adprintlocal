<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//--------------------------------//

// Heading
$_['heading_title']		= 'Gestionnaire d’images';
 
// Text
$_['text_uploaded']		= 'Félicitations, votre fichier a été transféré avec succès !';
$_['text_file_delete']	= 'Le fichier a été supprimé !';
$_['text_create']		= 'Félicitations, votre répertoire a été créé avec succès !';
$_['text_delete']		= 'Félicitations, votre fichier ou répertoire a été supprimé avec succès !';
$_['text_move']			= 'Félicitations, votre fichier ou répertoire a été déplacé avec succès !';
$_['text_copy']			= 'Félicitations, votre fichier ou répertoire a été copié avec succès !';
$_['text_rename']		= 'Félicitations, votre fichier ou répertoire a été renommé avec succès !';

// Entry
$_['entry_folder']		= 'Créer répertoire :';
$_['entry_move']		= 'Déplacer :';
$_['entry_copy']		= 'Copier :';
$_['entry_rename']		= 'Renommer :';

// Error
$_['error_select']		= 'Attention, veuillez sélectionner un répertoire ou un fichier !';
$_['error_file']		= 'Attention, veuillez sélectionner un fichier !';
$_['error_directory']	= 'Attention, veuillez sélectionner un répertoire !';
$_['error_default']		= 'Attention, il est impossible de modifier le répertoire par défaut !';
$_['error_delete']		= 'Attention, vous ne pouvez pas supprimer ce répertoire !';
$_['error_filename']	= 'Le <b>Nom du fichier</b> doit être composé de 3 à 255 caractères !';
$_['error_missing']		= 'Attention, le fichier ou le répertoire est inexistant !';
$_['error_exists']		= 'Attention, un fichier ou un répertoire portant le même nom existe déjà !';
$_['error_name']		= 'Attention, veuillez entrer un nouveau nom !';
$_['error_move']		= 'Attention, déplacement vers le répertoire impossible';
$_['error_copy']		= 'Attention, impossible de copier ce fichier ou répertoire !';
$_['error_rename']		= 'Attention, impossible de renommer ce répertoire !';
$_['error_file_type']	= 'Attention, type de fichier incorrect !';
$_['error_file_size']	= 'Attention, fichier trop important, veuillez rester en dessous de 300kb et en dessous de 1000px pour la hauteur ou la largeur !';
$_['error_uploaded']	= 'Attention, le fichier n’a pas pu être téléchargé pour une raison inconnue !';
$_['error_permission']	= 'Attention, permission refusée !';

// Button
$_['button_folder']		= 'Créer répertoire';
$_['button_delete']		= 'Supprimer';
$_['button_move']		= 'Déplacer';
$_['button_copy']		= 'Copier';
$_['button_rename']		= 'Renommer';
$_['button_upload']		= 'Charger';
$_['button_refresh']	= 'Rafraîchir';
?>