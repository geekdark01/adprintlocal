<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//--------------------------------//

// Text
$_['text_footer']	= '<a href="http://www.opencart.com" target="_blank">OpenCart</a> &copy; 2009-' . date('Y') . ' Tous droits réservés.<br />Propulsé par <a href="http://www.opencart-france.fr" target="_blank">OpenCart France</a><br />Version %s';
?>