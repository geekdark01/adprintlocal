<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//--------------------------------//

// header
$_['heading_title']		= 'Connexion Utilisateur';

// Text
$_['text_heading']		= 'Administration';
$_['text_login']		= 'Veuillez saisir vos identifiants';
$_['text_forgotten']	= 'Mot de passe oublié';

// Entry
$_['entry_username']	= 'Nom d’utilisateur :';
$_['entry_password']	= 'Mot de passe :';

// Button
$_['button_login']		= 'Valider';

// Error
$_['error_login']		= 'Aucune correspondance trouvée entre le <br /><b>Nom d’utilisateur et/ou le Mot de passe</b>.';
$_['error_token']		= 'Session invalide, veuillez vous reconnecter.';
?>