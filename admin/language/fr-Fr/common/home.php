<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//--------------------------------//

// Heading
$_['heading_title']					= 'Tableau de bord';

// Text
$_['text_overview']					= 'Présentation';
$_['text_statistics']				= 'Statistiques';
$_['text_latest_10_orders']			= 'Les 10 dernières commandes';
$_['text_total_sale']				= 'Total des ventes :';
$_['text_total_sale_year']			= 'Total des ventes de cette année :';
$_['text_total_order']				= 'Total des commandes :';
$_['text_total_customer']			= 'Nombre de clients :';
$_['text_total_customer_approval']	= 'Clients en attente d’approbation :';
$_['text_total_review_approval']	= 'Avis en attente d’approbation :';
$_['text_total_affiliate']			= 'Nombre d’affiliés :';
$_['text_total_affiliate_approval']	= 'Affiliés en attente d’approbation :';
$_['text_day']						= 'Aujourd’hui';
$_['text_week']						= 'Cette Semaine';
$_['text_month']					= 'Ce Mois';
$_['text_year']						= 'Cette Année';
$_['text_order']					= 'Nombre de Commandes';
$_['text_customer']					= 'Nombre de Clients';

// Column
$_['column_order']					= 'Réf.';
$_['column_customer']				= 'Nom du Client';
$_['column_status']					= 'État';
$_['column_date_added']				= 'Date d’Ajout';
$_['column_total']					= 'Total';
$_['column_firstname']				= 'Prénom';
$_['column_lastname']				= 'Nom';
$_['column_action']					= 'Action';

// Entry
$_['entry_range']					= 'Choisir une période :';

// Error
$_['error_install']					= 'Attention, le répertoire <b>install</b> existe encore !';
$_['error_image']					= 'Attention, le répertoire <b>image</b> %s est non autorisé en écriture !';
$_['error_image_cache']				= 'Attention, le répertoire <b>image/cache</b> %s est non autorisé en écriture !';
$_['error_cache']					= 'Attention, le répertoire <b>cache</b> %s est non autorisé en écriture !';
$_['error_download']				= 'Attention, le répertoire <b>download</b> %s est non autorisé en écriture !';
$_['error_logs']					= 'Attention, le répertoire <b>log</b> %s est non autorisé en écriture !';
?>