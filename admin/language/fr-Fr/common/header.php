<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//--------------------------------//

// Heading
$_['heading_title']						= 'Administration';

// Text
$_['text_affiliate']					= 'Affiliations';
$_['text_attribute']					= 'Attributs';
$_['text_attribute_group']				= 'Groupes d’attributs';
$_['text_backup']						= 'Sauvegarder - Restaurer';
$_['text_banner']						= 'Bannières';
$_['text_catalog']						= 'Catalogue';
$_['text_category']						= 'Catégories';
$_['text_confirm']						= 'Confirmez-vous cette action ?';
$_['text_country']						= 'Pays'; 
$_['text_coupon']						= 'Bons de réduction';
$_['text_currency']						= 'Devises';
$_['text_customer']						= 'Clients';
$_['text_customer_group']				= 'Groupes clients';
$_['text_customer_ban_ip']				= 'IP Bannies';
$_['text_custom_field']					= 'Champs personnalisé';
$_['text_dashboard']					= 'Tableau de bord';
$_['text_design']						= 'Conception graphique';
$_['text_download']						= 'Téléchargements';
$_['text_error_log']					= 'Journal d’erreurs';
$_['text_extension']					= 'Extensions';
$_['text_feed']							= 'Flux des produits';
$_['text_filter']						= 'Filtres';
$_['text_front']						= 'Boutique';
$_['text_geo_zone']						= 'Zones géographiques';
$_['text_help']							= 'Aide';
$_['text_information']					= 'Informations';
$_['text_language']						= 'Langues';
$_['text_layout']						= 'Dispositions';
$_['text_localisation']					= 'Localisation';
$_['text_logged']						= 'Vous êtes connecté en tant que <span>%s</span>';
$_['text_logout']						= 'Déconnexion';
$_['text_contact']						= 'Courriel';
$_['text_manager']						= 'Gestionnaire d’extensions';
$_['text_manufacturer']					= 'Fabricants';
$_['text_module']						= 'Modules';
$_['text_option']						= 'Options';
$_['text_order']						= 'Commandes';
$_['text_order_status']					= 'États des commandes';
$_['text_opencart']						= 'Accueil';
$_['text_payment']						= 'Paiements';
$_['text_product']						= 'Produits';
$_['text_profile']						= 'Profils';
$_['text_reports']						= 'Rapports';
$_['text_report_sale_order']			= 'Commandes';
$_['text_report_sale_tax']				= 'Taxe';
$_['text_report_sale_shipping']			= 'Livraison';
$_['text_report_sale_return']			= 'Retours';
$_['text_report_sale_coupon']			= 'Coupons';
$_['text_report_product_viewed']		= 'Consultés';
$_['text_report_product_purchased']		= 'Achetés';
$_['text_report_customer_online']		= 'Clients en ligne';
$_['text_report_customer_order']		= 'Commandes';
$_['text_report_customer_reward']		= 'Points de fidélité';
$_['text_report_customer_credit']		= 'Crédit';
$_['text_report_affiliate_commission']	= 'Commission';
$_['text_review']						= 'Avis';
$_['text_return']						= 'Retours';
$_['text_return_action']				= 'Actions des retours';
$_['text_return_reason']				= 'Raisons des retours';
$_['text_return_status']				= 'États des retours';
$_['text_sale']							= 'Ventes';
$_['text_shipping']						= 'Livraisons';
$_['text_setting']						= 'Paramètres';
$_['text_stock_status']					= 'États des stocks';
$_['text_support']						= 'Support forum';
$_['text_system']						= 'Système';
$_['text_tax']							= 'Taxes';
$_['text_tax_class']					= 'Classe des taxes';
$_['text_tax_rate']						= 'Taux de taxe';
$_['text_total']						= 'Totaux commande';
$_['text_user']							= 'Utilisateur';
$_['text_documentation']				= 'Documentation';
$_['text_users']						= 'Utilisateurs';
$_['text_user_group']					= 'Groupes utilisateurs';
$_['text_voucher']						= 'Chèques-cadeaux';
$_['text_voucher_theme']				= 'Thèmes';
$_['text_weight_class']					= 'Classe des poids';
$_['text_length_class']					= 'Classe des longueurs';
$_['text_zone']							= 'Zones';
$_['text_openbay_extension']			= 'OpenBay Pro';
$_['text_openbay_dashboard']			= 'Tableau de bord';
$_['text_openbay_orders']				= 'Mise à jour en nombre des commandes';
$_['text_openbay_items']				= 'Gestion d’article';
$_['text_openbay_ebay']					= 'eBay';
$_['text_openbay_amazon']				= 'Amazon (EU)';
$_['text_openbay_amazonus']				= 'Amazon (US)';
$_['text_openbay_settings']				= 'Paramètres';
$_['text_openbay_links']				= 'Liens articles';
$_['text_openbay_report_price']			= 'Rapport de prix';
$_['text_openbay_order_import']			= 'Import commande';
$_['text_paypal_manage']				= 'PayPal';
$_['text_paypal_search']				= 'Recherche';
$_['text_recurring_profile']			= 'Profils récurrents';
?>