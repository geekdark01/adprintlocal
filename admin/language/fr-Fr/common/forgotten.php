<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// header
$_['heading_title']		= 'Oubli du mot de passe ?';

// Text
$_['text_forgotten']	= 'Mot de passe oublié';
$_['text_your_email']	= 'Votre adresse courriel';
$_['text_email']		= 'Entrez l’adresse courriel associée à votre compte. Cliquez sur soumettre pour obtenir un lien de réinitialisation par courriel.';
$_['text_success']		= 'Un courriel comprenant un lien de confirmation vient d’être envoyé à votre adresse courriel';

// Entry
$_['entry_email']		= 'Adresse courriel :';
$_['entry_password']	= 'Nouveau mot de passe :';
$_['entry_confirm']		= 'Confirmation :';

// Error
$_['error_email']		= 'Erreur : l’adresse courriel n’a pas été trouvée dans nos dossiers, veuillez essayer à nouveau !';
$_['error_password']	= 'Le <b>Mot de passe</b> doit être composé de 3 à 20 caractères !';
$_['error_confirm']		= 'Le mot de passe et la confirmation du mot de passe ne correspondent pas !';
?>