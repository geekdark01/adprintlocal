<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// header
$_['heading_title']		= 'Réinitialiser votre mot de passe !';

// Text
$_['text_reset']		= 'Réinitialiser votre mot de passe !';
$_['text_password']		= 'Entrez le nouveau mot de passe que vous souhaitez utiliser.';
$_['text_success']		= 'Félicitations, votre <b>Mot de passe</b> a été mis à jour avec succès !';

// Entry
$_['entry_password']	= 'Mot de passe :';
$_['entry_confirm']		= 'Confirmation du mot de passe :';

// Error
$_['error_password']	= 'Le <b>Mot de passe</b> doit être composé de 3 à 20 caractères !';
$_['error_confirm']		= 'Le mot de passe et la confirmation du mot de passe ne correspondent pas !';
?>