<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//--------------------------------//

// Text
$_['text_subject']	= '%s - Réinitialisation du mot de passe requis';
$_['text_greeting']	= 'Un nouveau mot de passe a été demandé pour l’administration de %s .';
$_['text_change']	= 'Pour réinitialiser votre mot de passe, cliquez sur le lien ci-dessous :';
$_['text_ip']		= 'L’adresse IP utilisée pour faire cette demande était :';
?>