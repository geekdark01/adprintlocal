<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// Text
$_['text_subject']			= '%s - Mise à jour du retour %s';
$_['text_return_id']		= 'Numéro de retour:';
$_['text_date_added']		= 'Date de retour :';
$_['text_return_status']	= 'Votre retour a été mis à jour avec l’état suivant:';
$_['text_comment']			= 'Les commentaires pour ce retour sont :';
$_['text_footer']			= 'Veuillez répondre à ce message si vous avez des questions.';
?>