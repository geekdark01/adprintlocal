<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//--------------------------------//

// Text
$_['text_subject']		= '%s - Mise à jour de la commande %s';
$_['text_order']		= 'N° de commande :';
$_['text_date_added']	= 'Date de commande :';
$_['text_order_status']	= 'Votre commande a été mise à jour avec l’état suivant :';
$_['text_comment']		= 'Les commentaires pour cette commande sont :';
$_['text_link']			= 'Pour consulter votre commande, cliquer sur le lien ci-dessous :';
$_['text_footer']		= 'Veuillez répondre à ce message si vous avez des questions.';
?>