<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//----------------------------------

// Text
$_['text_subject']	= 'Vous venez de recevoir un chèque-cadeau provenant de %s';
$_['text_greeting']	= 'Félicitations, Vous venez de recevoir un chèque-cadeau d’une valeur de %s';
$_['text_from']		= 'Ce chèque-cadeau a été envoyé de la part de %s';
$_['text_message']	= 'Avec le message suivant';
$_['text_redeem']	= 'Pour échanger ce chèque-cadeau, notez bien le code de rachat qui est <b>%s</b>.Cliquer ensuite sur le lien ci-dessous et acheter le produit pour lequel vous souhaitez utiliser ce chèque-cadeau. Vous pouvez entrer le code de chèque-cadeau sur la page du panier avant de valider la commande.';
$_['text_footer']	= 'Si vous avez des questions, veuillez répondre à ce message.';
?>