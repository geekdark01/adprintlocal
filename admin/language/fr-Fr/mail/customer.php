<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduction LeorLindel		  //
// Propriété d’opencart-france.fr //
//--------------------------------//

// Text
$_['text_approve_subject']		= '%s - Votre compte a été activé !';
$_['text_approve_welcome']		= 'Bienvenue et merci pour votre enregistrement sur %s !';
$_['text_approve_login']		= 'Votre compte a été créé et vous pouvez vous connecter en utilisant votre adresse courriel et mot de passe sur notre site Web ou à l’adresse suivante :';
$_['text_approve_services']		= 'Lors de la connexion, vous serez en mesure d’accéder à d’autres services, tels que la visualisation de vos commandes passées, l’impression de vos factures ou encore l’édition de vos informations de compte.';
$_['text_approve_thanks']		= 'Nous vous remercions,';
$_['text_transaction_subject']	= '%s - Compte créditeur';
$_['text_transaction_received']	= 'Vous venez de recevoir %s credit!';
$_['text_transaction_total']	= 'Votre montant total du crédit est désormais de %s.' . "\n\n" . 'Le crédit de votre compte sera automatiquement déduit de votre prochain achat.';
$_['text_reward_subject']		= '%s - Points de fidélité';
$_['text_reward_received']		= 'Vous venez de recevoir %s Points de fidélité !';
$_['text_reward_total']			= 'Votre nombre total de points de fidélité est désormais de %s.';
?>