<?php
class ControllerCommercialesCommerciales extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('commerciales/commerciales');

		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('sale/order');
		$this->load->model('customer/customer');
		$this->load->model('user/user');
		$data['user_group_id'] = $this->model_user_user->getUserGroupId($this->user->getId());
		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'id.title';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}
		
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('commerciales/commerciales', 'token=' . $this->session->data['token'] . $url, true)
		);
		$data['commerciales'] = array();

		$filter_data = array(
			'sort'                     => $sort,
			'order'                    => $order,
			'start'                    => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'                    => $this->config->get('config_admin_limit')
		);
		
		$results = $this->model_user_user->getUsers($filter_data);
		foreach ($results as $result) {
			$totals = 0;
			if($result['user_group_id'] == 3){
				$clients = $this->model_customer_customer->getCustomersCommerciale($filter_data,$result['user_id']);
				$i = 0;
				foreach ($clients as $client) {
					$i += 1 ;
				}
				$totalorder = $this->model_sale_order->getOrderCommerciale($result['user_id']);
				foreach ($totalorder as $total) {
					$totals += (int)$total['total'];
				}
				$data['commerciales'][] = array(
					'commerciale_id'    => $result['user_id'],
					'firstname'         => $result['firstname'],
					'lastname'          => $result['lastname'],
					'nombreclient'		=> $i,
					'email'          	=> $result['email'],
					'chiffreAffaire'    => $this->currency->format($totals,'MGA',''),
					'view'          	=> $this->url->link('commerciales/commerciales/details', 'token=' . $this->session->data['token'] . '&user_id=' . $result['user_id'] . $url, true),
				);
			}
		}
		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');

		$data['commerciale_name'] = $this->language->get('commerciale_name');
		$data['commerciale_email'] = $this->language->get('commerciale_email');
		$data['nombreclient'] = $this->language->get('nombreclient');
		$data['chiffre'] = $this->language->get('chiffre');
		$data['details'] = $this->language->get('details');
		$data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_title'] = $this->url->link('commerciales/commerciales', 'token=' . $this->session->data['token'] . '&sort=id.title' . $url, 'SSL');
		$data['sort_sort_order'] = $this->url->link('commerciales/commerciales', 'token=' . $this->session->data['token'] . '&sort=i.sort_order' . $url, 'SSL');
		
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		$this->load->model('design/layout');

		$data['layouts'] = $this->model_design_layout->getLayouts();

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('commerciales/commerciales', $data));
	}
	public function details() {
		$this->load->model('user/user');
		$this->load->model('sale/order');
		$this->load->model('customer/customer');
		if (isset($this->request->get['user_id'])) {
			$user_id = $this->request->get['user_id'];
		} else {
			$user_id = 0;
		}
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'id.title';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}
		
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$filter_data = array(
			'sort'                     => $sort,
			'order'                    => $order,
			'start'                    => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'                    => $this->config->get('config_admin_limit')
		);
		$detailsUser = $this->model_user_user->getUser($user_id);
		$data['firstname'] = $detailsUser['firstname'];
		$data['lastname' ] = $detailsUser['lastname'];
		$data['email']     = $detailsUser['email'];
		$this->load->language('commerciales/commerciales');
		$this->document->setTitle($this->language->get('heading_title'));
		$data['chiffre'] = $this->language->get('chiffre');
		$data['listeclient'] = $this->language->get('listeclient');
		$data['action']= $this->language->get('Action');
		$data['customer_name'] = $this->language->get('customer_name');
		$data['heading_title'] = $this->language->get('heading_title');
		$data['commerciale_details'] = $this->language->get('commerciale_details');
		$data['text_commerciale'] = $this->language->get('text_commerciale');
		$data['details'] = $this->language->get('details');
		$clientcommerces = $this->model_customer_customer->getCustomersCommerciale($filter_data,$user_id);
		$data['clients'] = array();
		$totals = 0;
		$totalorder = $this->model_sale_order->getOrderCommerciale($user_id);
				foreach ($totalorder as $total) {
					$totals += (int)$total['total'];
				}
		$data['chiffreAffaire'] = $this->currency->format($totals,'MGA','');
		foreach ($clientcommerces as $client) {
			$totalorder = 0;
			$clientorders = $this->model_sale_order->getOrderCommerceClient($client['customer_id'],$user_id);
			foreach ($clientorders as $clientorder) {
				if($clientorder['customer_id'] == $client['customer_id']){
					$totalorder += $clientorder['total'];
				}
			}

			$nom = $client['firstname'].' '.$client['lastname'];
			$data['clients'][] = array(
					'firstname'         => $client['firstname'],
					'lastname'         	=> $client['lastname'],
					'nom'				=> $nom,
					'chiffreAffaire'    => $this->currency->format($totalorder,'MGA',''),
					'view'          	=> $this->url->link('sale/order', 'token=' . $this->session->data['token'] . '&filter_customer='  . $nom . $url, true),
			);
		}
		$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			
			$data['breadcrumbs'] = array();

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('commerciales/commerciales', 'token=' . $this->session->data['token'] . $url, true)
			);


			// Custom field
			$data['header'] = $this->load->controller('common/header');
			$data['column_left'] = $this->load->controller('common/column_left');
			$data['footer'] = $this->load->controller('common/footer');		
            $this->response->setOutput($this->load->view('commerciales/commerciale_details', $data));
	
    }
}
