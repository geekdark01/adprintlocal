<?php foreach($toProductLivraison as $toLivraison): ?>
    <tr>
        <td colspan="2">
            Livraison J+<?php echo $toLivraison['delais_jour']  ; ?> dans la zone <?php echo $toLivraison['zone_livraison']  ; ?>
        </td>
        <td>
            <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? supprimerLivraison(<?php echo $toLivraison['livraison_id'] ; ?>) : false;"><i class="fa fa-trash-o"></i></button>
        </td>
    </tr>
<?php endforeach ; ?>