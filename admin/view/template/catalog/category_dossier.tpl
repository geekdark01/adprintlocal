<?php foreach($toCategorieDossier as $toDossier): ?>
    <tr>
        <th colspan="2">
            <?php echo $toDossier['dossier_nom']  ; ?>
            <?php
                $toItemCategorieId = array()  ;
                $toItemCategorieId = explode('_', $toDossier['dossier_serialize_item_id']) ;
            ?>
        </th>
        <td>
            <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? supprimerDossier(<?php echo $toDossier['dossier_id'] ; ?>) : false;"><i class="fa fa-trash-o"></i></button>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <?php foreach($toCategoriesPourDossier as $toCategoriePourDossier):?>
                <input onclick="updateCategorieDossier(<?php echo $toDossier['dossier_id'] ; ?>);" <?php if(in_array($toCategoriePourDossier['category_id'], $toItemCategorieId)): ?>checked="checked"<?php endif; ?> type="checkbox" class="checkDossier<?php echo $toDossier['dossier_id'] ; ?>" value="<?php echo $toCategoriePourDossier['category_id'] ; ?>"/>&nbsp;
                <?php echo $toCategoriePourDossier['category_name'] ;  ?><br/>
            <?php endforeach ;?>
            <input type="hidden" id="txtSerializeItem_<?php echo $toDossier['dossier_id'] ; ?>" value=""/>
        </td>
    </tr>
<?php endforeach ; ?>