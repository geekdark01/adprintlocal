<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
		  <div class="pull-right">
		  <!--<a href="<?php echo $insert; ?>" data-toggle="tooltip" title="<?php echo $button_insert; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a> -->
			 <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-header').submit() : false;"><i class="fa fa-trash-o"></i></button>
		  </div>
		  <h1><?php echo $heading_title; ?></h1>
		  <ul class="breadcrumb">
			<?php foreach ($breadcrumbs as $breadcrumb) { ?>
			<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
			<?php } ?>
		  </ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="panel panel-default">
			<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-header">
				<div class="table-responsive">     
					<table class="table table-bordered table-hover">
					  <thead>
							<tr>
								<th style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></th>
								<th class="left"><?php echo $client_name; ?></th> 
								<th class="left"><?php echo $client_mail; ?></th> 
								<th class="left"><?php echo $client_phone; ?></th> 
								<th class="left"><?php echo $client_company; ?></th> 
								<th class="left"><?php echo $client_subject; ?></th> 
								<th class="left"><?php echo $client_order; ?></th> 
								<th class="left"><?php echo $client_motif; ?></th> 
								<th class="left"><?php echo $client_souhait; ?></th> 
								<th class="left"><?php echo $client_comment; ?></th> 
								<th class="left"><?php echo $client_photos; ?></th> 
							</tr>
					  </thead>
					  <tbody>							
							<?php if ($selectclient) { ?>
							<?php foreach ($selectclient as $resultat) { ?>
							<tr>
								<td class="text-center"><?php if (in_array($resultat['idclientsat'], $selected)) { ?>
								<input type="checkbox" name="selected[]" value="<?php echo $resultat['idclientsat']; ?>" checked="checked" />
								<?php } else { ?>
								<input type="checkbox" name="selected[]" value="<?php echo $resultat['idclientsat']; ?>" />
								<?php } ?></td>
							  <td class="left"><?php echo $resultat['nomclientsat']; ?></td>
							  <td class="left"><?php echo $resultat['mailclientsat']; ?></td>
							  <td class="left"><?php echo $resultat['telephoneclientsat']; ?></td>
							  <td class="left"><?php echo $resultat['societeclientsat']; ?></td>
							  <td class="left"><?php echo $resultat['sujetdmdclientsat']; ?></td>
							  <td class="left"><?php echo $resultat['chxcmdclientsat']; ?></td>
							  <td class="left"><?php echo $resultat['motifclientsat']; ?></td>
							  <td class="left"><?php echo $resultat['souhaitclientsat']; ?></td>
							  <td class="left"><?php echo $resultat['commentclientsat']; ?></td>
							  
							  <td class="left">
								<?php 
									$images_service = explode("|", $resultat['liensimage']);
									if (!empty($images_service)){
										foreach($images_service as $pht){
											?>
												<img style="width:80px;height:auto;" src="<?php echo $pht ; ?>" />
											<?php
										}
									}
									else {
										echo "Pas de photo pour cette demande";
									}
								?>
							  </td>
							  
							</tr>
							<?php } ?>
							<?php } else { ?>
							<tr>
							  <td class="center" colspan="5"><?php echo $text_no_results; ?></td>
							</tr>
							<?php } ?>
					  </tbody>
					</table>
				</div>
			</form>
		</div>
	</div>
</div>
<?php echo $footer; ?>