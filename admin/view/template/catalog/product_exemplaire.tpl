<?php foreach($exemplaires as $exemplaire): ?>
    <tr>
        <td>
            <?php echo $exemplaire['nombre_ex']  ; ?> 
        </td>
        <td> <?php echo $exemplaire['prix_product_ex']  ; ?> </td>
        <td> 
            <?php 
                if($exemplaire['prix_remise'] == null) echo "Pas de remise"   ; 
                else echo $exemplaire['prix_remise'] ;
            ?> 
        </td>
        <td>
            <?php 
                if((int)$exemplaire['activation_remise'] == true) echo "Oui"   ; 
                if((int)$exemplaire['activation_remise'] == false) echo "Non"   ; 
            ?> 
        </td>
        <td>
            <?php 
                if((int)$exemplaire['recommander'] == true) echo "Oui"   ; 
                if((int)$exemplaire['recommander'] == false) echo "Non"   ; 
            ?> 
        </td>
		<td><?php echo $exemplaire['texterecommandation'] ; ?></td>
        <td>
            <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? deleteExemplaire(<?php echo $exemplaire['product_exemplaire_id'] ; ?>) : false;" ><i class="fa fa-trash-o"></i></button>
        </td>
    </tr>
<?php endforeach ; ?>