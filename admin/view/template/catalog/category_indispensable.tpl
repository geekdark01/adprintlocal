<?php foreach($toCategorieIndispensables as $toCategorieIndispensable): ?>
    <tr>
        <td>
            <?php echo $toCategorieIndispensable['type_text'] ; ?>
        </td>
        <td>
            <?php echo $toCategorieIndispensable['name'] ; ?>
        </td>
        <td>
            <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? suppressionIndispensable(<?php echo $toCategorieIndispensable['item_id'] ; ?>, '<?php echo $toCategorieIndispensable['type'] ; ?>') : false;"><i class="fa fa-trash-o"></i></button>
        </td>
    </tr>
<?php endforeach ; ?>