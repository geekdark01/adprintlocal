<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-category" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-category" class="form-horizontal">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
            <li><a href="#tab-data" data-toggle="tab"><?php echo $tab_data; ?></a></li>
            <li><a href="#tab-design" data-toggle="tab"><?php echo $tab_design; ?></a></li>
            <!--DEV103 Les indispensables-->
            <?php if($edit_category && isset($toCategoriesIndispensables)) : ?>
                <li><a href="#tab-indispensable" data-toggle="tab"><?php echo $tab_indispensables; ?></a></li>
            <?php endif ; ?>
            <!--DEV103 Les indispensables-->
            <!--DEV103 Dossiers-->
            <?php if($edit_category) : ?>
                <li><a href="#tab-dossier" data-toggle="tab"><?php echo $tab_dossier; ?></a></li>
            <?php endif ; ?>
            <!--DEV103 Dossiers-->
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="tab-general">
              <ul class="nav nav-tabs" id="language">
                <?php foreach ($languages as $language) { ?>
                <li><a href="#language<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                <?php } ?>
              </ul>
              <div class="tab-content">
                <?php foreach ($languages as $language) { ?>
                <div class="tab-pane" id="language<?php echo $language['language_id']; ?>">
                  <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-name<?php echo $language['language_id']; ?>"><?php echo $entry_name; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="category_description[<?php echo $language['language_id']; ?>][name]" value="<?php echo isset($category_description[$language['language_id']]) ? $category_description[$language['language_id']]['name'] : ''; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name<?php echo $language['language_id']; ?>" class="form-control" />
                      <?php if (isset($error_name[$language['language_id']])) { ?>
                      <div class="text-danger"><?php echo $error_name[$language['language_id']]; ?></div>
                      <?php } ?>
                    </div>
                  </div>
               
                  <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-meta-title<?php echo $language['language_id']; ?>"><?php echo $entry_meta_title; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="category_description[<?php echo $language['language_id']; ?>][meta_title]" value="<?php echo isset($category_description[$language['language_id']]) ? $category_description[$language['language_id']]['meta_title'] : ''; ?>" placeholder="<?php echo $entry_meta_title; ?>" id="input-meta-title<?php echo $language['language_id']; ?>" class="form-control" />
                      <?php if (isset($error_meta_title[$language['language_id']])) { ?>
                      <div class="text-danger"><?php echo $error_meta_title[$language['language_id']]; ?></div>
                      <?php } ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-meta-description<?php echo $language['language_id']; ?>"><?php echo $entry_meta_description; ?></label>
                    <div class="col-sm-10">
                      <textarea name="category_description[<?php echo $language['language_id']; ?>][meta_description]" rows="5" placeholder="<?php echo $entry_meta_description; ?>" id="input-meta-description<?php echo $language['language_id']; ?>" class="form-control"><?php echo isset($category_description[$language['language_id']]) ? $category_description[$language['language_id']]['meta_description'] : ''; ?></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-meta-keyword<?php echo $language['language_id']; ?>"><?php echo $entry_meta_keyword; ?></label>
                    <div class="col-sm-10">
                      <textarea name="category_description[<?php echo $language['language_id']; ?>][meta_keyword]" rows="5" placeholder="<?php echo $entry_meta_keyword; ?>" id="input-meta-keyword<?php echo $language['language_id']; ?>" class="form-control"><?php echo isset($category_description[$language['language_id']]) ? $category_description[$language['language_id']]['meta_keyword'] : ''; ?></textarea>
                    </div>
                  </div>
                </div>
                <?php } ?>
              </div>
            </div>
            <div class="tab-pane" id="tab-data">
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-parent"><?php echo $entry_parent; ?></label>
                <div class="col-sm-10">
                  <input type="text" name="path" value="<?php echo $path; ?>" placeholder="<?php echo $entry_parent; ?>" id="input-parent" class="form-control" />
                  <input type="hidden" name="parent_id" value="<?php echo $parent_id; ?>" />
                  <?php if ($error_parent) { ?>
                  <div class="text-danger"><?php echo $error_parent; ?></div>
                  <?php } ?>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-filter"><span data-toggle="tooltip" title="<?php echo $help_filter; ?>"><?php echo $entry_filter; ?></span></label>
                <div class="col-sm-10">
                  <input type="text" name="filter" value="" placeholder="<?php echo $entry_filter; ?>" id="input-filter" class="form-control" />
                  <div id="category-filter" class="well well-sm" style="height: 150px; overflow: auto;">
                    <?php foreach ($category_filters as $category_filter) { ?>
                    <div id="category-filter<?php echo $category_filter['filter_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $category_filter['name']; ?>
                      <input type="hidden" name="category_filter[]" value="<?php echo $category_filter['filter_id']; ?>" />
                    </div>
                    <?php } ?>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label"><?php echo $entry_store; ?></label>
                <div class="col-sm-10">
                  <div class="well well-sm" style="height: 150px; overflow: auto;">
                    <div class="checkbox">
                      <label>
                        <?php if (in_array(0, $category_store)) { ?>
                        <input type="checkbox" name="category_store[]" value="0" checked="checked" />
                        <?php echo $text_default; ?>
                        <?php } else { ?>
                        <input type="checkbox" name="category_store[]" value="0" />
                        <?php echo $text_default; ?>
                        <?php } ?>
                      </label>
                    </div>
                    <?php foreach ($stores as $store) { ?>
                    <div class="checkbox">
                      <label>
                        <?php if (in_array($store['store_id'], $category_store)) { ?>
                        <input type="checkbox" name="category_store[]" value="<?php echo $store['store_id']; ?>" checked="checked" />
                        <?php echo $store['name']; ?>
                        <?php } else { ?>
                        <input type="checkbox" name="category_store[]" value="<?php echo $store['store_id']; ?>" />
                        <?php echo $store['name']; ?>
                        <?php } ?>
                      </label>
                    </div>
                    <?php } ?>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-keyword"><span data-toggle="tooltip" title="<?php echo $help_keyword; ?>"><?php echo $entry_keyword; ?></span></label>
                <div class="col-sm-10">
                  <input type="text" name="keyword" value="<?php echo $keyword; ?>" placeholder="<?php echo $entry_keyword; ?>" id="input-keyword" class="form-control" />
                  <?php if ($error_keyword) { ?>
                  <div class="text-danger"><?php echo $error_keyword; ?></div>
                  <?php } ?>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label"><?php echo $entry_image; ?></label>
                <div class="col-sm-10"><a href="" id="thumb-image" data-toggle="image" class="img-thumbnail"><img src="<?php echo $thumb; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
                  <input type="hidden" name="image" value="<?php echo $image; ?>" id="input-image" />
                </div>
              </div>
			  
              <!--DEV103 Image promo-->
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php echo $entry_image_promo; ?></label>
					<div class="col-sm-10"><a href="" id="thumb-image_promo" data-toggle="image" class="img-thumbnail"><img src="<?php echo $thumb_promo; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
					  <input type="hidden" name="image_promo" value="<?php echo $image_promo; ?>" id="input-image_promo" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label" for="input-lien_image_menu">Lien image promo</label>
					<div class="col-sm-10">
						<input type="text" name="lien_image_promo" value="<?php echo $lien_image_promo; ?>" id="input-lien_image_promo" class="form-control" />
					</div>
				</div>
              <!--DEV103 Image promo-->
			  
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-top"><span data-toggle="tooltip" title="<?php echo $help_top; ?>"><?php echo $entry_top; ?></span></label>
                <div class="col-sm-10">
                  <div class="checkbox">
                    <label>
                      <?php if ($top) { ?>
                      <input type="checkbox" name="top" value="1" checked="checked" id="input-top" />
                      <?php } else { ?>
                      <input type="checkbox" name="top" value="1" id="input-top" />
                      <?php } ?>
                      &nbsp; </label>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-column"><span data-toggle="tooltip" title="<?php echo $help_column; ?>"><?php echo $entry_column; ?></span></label>
                <div class="col-sm-10">
                  <input type="text" name="column" value="<?php echo $column; ?>" placeholder="<?php echo $entry_column; ?>" id="input-column" class="form-control" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-sort-order"><?php echo $entry_sort_order; ?></label>
                <div class="col-sm-10">
                  <input type="text" name="sort_order" value="<?php echo $sort_order; ?>" placeholder="<?php echo $entry_sort_order; ?>" id="input-sort-order" class="form-control" />
                </div>
				</div>

				<div class="form-group">
                <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
                <div class="col-sm-10">
                  <select name="status" id="input-status" class="form-control">
                    <?php if ($status) { ?>
                    <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                    <option value="0"><?php echo $text_disabled; ?></option>
                    <?php } else { ?>
                    <option value="1"><?php echo $text_enabled; ?></option>
                    <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                    <?php } ?>
                  </select>
                </div>
				</div>
				
				<!-- -->
				<div class="form-group">
					<label class="col-sm-2 control-label" for="input-status">Product affinity</label>
					<div class="col-sm-10">
					  <select name="product_affinity" id="input-status" class="form-control">
						<option value=""></option>
						<?php foreach($affinities as $affinity){ 
						if($affinity['affinity_id'] == $product_affinity){
						?>
						<option selected value="<?php echo $affinity['affinity_id'];?>"><?php echo $affinity['libelle'];?></option>
						<?php }else{?>
							<option value="<?php echo $affinity['affinity_id'];?>"><?php echo $affinity['libelle'];?></option>
						<?php	
							} 
						}?>
					  </select>
					</div>
				</div>
              
				<!-- DEV101 : ajout parametre afficher dans sous-menu + affichage en tableau --> 
				<div class="form-group">
                <label class="col-sm-2 control-label" for="input-sous_menu"><?php echo $entry_sous_menu; ?></label>
                <div class="col-sm-10">
                  <select name="sous_menu" id="input-sous_menu" class="form-control">
                    <?php if ($sousMenu == 1) { ?>
                    <option value="1" selected="selected">Oui</option>
                    <option value="0">Non</option>
                    <?php } else { ?>
                    <option value="1">Oui</option>
                    <option value="0" selected="selected">Non</option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-affichage_tableau"><?php echo $entry_affichage_tableau; ?></label>
                <div class="col-sm-10">
                  <select name="affichage_tableau" id="input-affichage_tableau" class="form-control">
                    <?php if ($affichageTableau == 1) { ?>
                    <option value="1" selected="selected">Oui</option>
                    <option value="0">Non</option>
                    <?php } else { ?>
                    <option value="1">Oui</option>
                    <option value="0" selected="selected">Non</option>
                    <?php } ?>
                  </select>
                </div>
              </div>
               <!-- DEV101 : ajout parametre afficher dans sous-menu + affichage en tableau --> 
              
			  
			  
			  <!-- page principale : image, lien , texte  -->
			  <div class="form-group">
                <label class="col-sm-2 control-label">Image page principale</label>
                <div class="col-sm-10">
				<a href="" id="thumb-image-menu" data-toggle="image" class="img-thumbnail"><img src="<?php echo $thumb_menu; ?>" alt="" title=""  /></a>
                  <input type="hidden" name="image_menu" value="<?php echo $image_menu; ?>" id="input-image-menu" />
                </div>
              </div>
			  
			  <div class="form-group">
                <label class="col-sm-2 control-label" for="input-lien_image_menu">Lien image page principale</label>
                <div class="col-sm-10">
                  <input type="text" name="lien_image_menu" value="<?php echo $lien_image_menu; ?>" id="input-lien_image_menu" class="form-control" />
                </div>
              </div>
			  
			  
			     <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-description<?php echo $language['language_id']; ?>"><?php echo $entry_description; ?></label>
                    <div class="col-sm-10">
                      <textarea name="category_description[<?php echo $language['language_id']; ?>][description]" placeholder="<?php echo $entry_description; ?>" id="input-description<?php echo $language['language_id']; ?>" class="form-control summernote"><?php echo isset($category_description[$language['language_id']]) ? $category_description[$language['language_id']]['description'] : ''; ?></textarea>
                    </div>
                  </div>
			  
			  <!-- page principale : image, lien , texte  -->
			  
			  
            </div>
            <div class="tab-pane" id="tab-design">
              <div class="table-responsive">
                <table class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <td class="text-left"><?php echo $entry_store; ?></td>
                      <td class="text-left"><?php echo $entry_layout; ?></td>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td class="text-left"><?php echo $text_default; ?></td>
                      <td class="text-left"><select name="category_layout[0]" class="form-control">
                          <option value=""></option>
                          <?php foreach ($layouts as $layout) { ?>
                          <?php if (isset($category_layout[0]) && $category_layout[0] == $layout['layout_id']) { ?>
                          <option value="<?php echo $layout['layout_id']; ?>" selected="selected"><?php echo $layout['name']; ?></option>
                          <?php } else { ?>
                          <option value="<?php echo $layout['layout_id']; ?>"><?php echo $layout['name']; ?></option>
                          <?php } ?>
                          <?php } ?>
                        </select></td>
                    </tr>
                    <?php foreach ($stores as $store) { ?>
                    <tr>
                      <td class="text-left"><?php echo $store['name']; ?></td>
                      <td class="text-left"><select name="category_layout[<?php echo $store['store_id']; ?>]" class="form-control">
                          <option value=""></option>
                          <?php foreach ($layouts as $layout) { ?>
                          <?php if (isset($category_layout[$store['store_id']]) && $category_layout[$store['store_id']] == $layout['layout_id']) { ?>
                          <option value="<?php echo $layout['layout_id']; ?>" selected="selected"><?php echo $layout['name']; ?></option>
                          <?php } else { ?>
                          <option value="<?php echo $layout['layout_id']; ?>"><?php echo $layout['name']; ?></option>
                          <?php } ?>
                          <?php } ?>
                        </select></td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
            <!--DEV103 Les indispensables-->
            <?php if($edit_category && isset($toCategoriesIndispensables)) : ?>
                <div class="tab-pane" id="tab-indispensable">
                  <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <td>
                                        <input type="hidden" id="iCategorieId" value="<?php echo $iCategorieId ; ?>"/>
                                        <label>
                                            <?php echo $column_type ; ?> :
                                        </label>
                                        <select class="form-control" id="selChoixType" onchange="if(this.value=='p'){$('#selChoixProduit').show();$('#selChoixCategorie').hide();}else{$('#selChoixProduit').hide();$('#selChoixCategorie').show();}">
                                            <option value="c"><?php echo $text_type_cat ; ?></option>
                                            <option value="p"><?php echo $text_type_prd ; ?></option>
                                        </select>
                                    </td>
                                    <td>
                                        <select class="form-control" id="selChoixCategorie">
                                            <?php foreach($toCategoriesIndispensables as $toCategorie) : ?>
                                                <option value="<?php echo $toCategorie['category_id'] ; ?>"><?php echo $toCategorie['name'] ; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        <select class="form-control" id="selChoixProduit" style="display:none;">
                                            <?php if(!$toProduitsIndispensables): ?>
                                                <option value="0"><?php echo $text_aucun_resultat ; ?></option>
                                            <?php else: ?>
                                                <?php foreach($toProduitsIndispensables as $toProduit) : ?>
                                                    <option value="<?php echo $toProduit['product_id'] ; ?>"><?php echo $toProduit['name'] ; ?></option>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </select>

                                    </td>
                                    <td>
                                        <a href="javascript:void(0);" onclick="ajoutIndispensable();" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?php echo $column_type ; ?>
                                    </th>
                                    <th>
                                        <?php echo $text_name ; ?>
                                    </th>
                                    <th>
                                        <?php echo $tab_action ; ?>
                                    </th>
                                </tr>
                            </thead>
                            <tbody id="tbodyIndispensable">
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <script type="text/javascript">
                            $(document).ready(function(){
                                loadIndispensable() ;
                            }) ;
                        </script>
                  </div>
                </div>
            <?php endif ; ?>
            <!--DEV103 Les indispensables-->
            <!--DEV103 Dossiers-->
            <?php if($edit_category) : ?>
                <div class="tab-pane" id="tab-dossier">
                    <div class="table-responsive">
                        <input type="hidden" id="iCategorieId" value="<?php echo $iCategorieId ; ?>"/>
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <td colspan="2">
                                        <input type="text" id="txtDossier" value="NewDossier" class="form-control" placeholder="Nom du dossier"/>
                                    </td>
                                    <td>
                                        <a href="javascript:void(0);" onclick="ajoutDossier();" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
                                    </td>
                                </tr>
                            </thead>
                            <tbody id="tbodyDossier">
                                <tr>
                                    <td colspan="2">
                                        Aucun dossier
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <script type="text/javascript">
                            $(document).ready(function(){
                                loadDossier() ;
                            }) ;
                        </script>
                    </div>
                </div>
            <?php endif; ?>
            <!--DEV103 Dossiers-->
          </div>
        </form>
      </div>
    </div>
  </div>
  <script type="text/javascript" src="view/javascript/summernote/summernote.js"></script>
  <link href="view/javascript/summernote/summernote.css" rel="stylesheet" />
  <script type="text/javascript" src="view/javascript/summernote/opencart.js"></script> 
  <script type="text/javascript"><!--
$('input[name=\'path\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/category/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				json.unshift({
					category_id: 0,
					name: '<?php echo $text_none; ?>'
				});

				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['category_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'path\']').val(item['label']);
		$('input[name=\'parent_id\']').val(item['value']);
	}
});
//--></script> 
  <script type="text/javascript"><!--
$('input[name=\'filter\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/filter/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['filter_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'filter\']').val('');

		$('#category-filter' + item['value']).remove();

		$('#category-filter').append('<div id="category-filter' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="category_filter[]" value="' + item['value'] + '" /></div>');
	}
});

$('#category-filter').delegate('.fa-minus-circle', 'click', function() {
	$(this).parent().remove();
});
//DEV103 Les Indispensables
function ajoutIndispensable(){
    var zType   = $('#selChoixType').val() ;
    if(zType == 'c')
    {
        var iItemId = $('#selChoixCategorie').val() ;
    }
    else{
        var iItemId = $('#selChoixProduit').val() ;
    }
    var iCategorieId = $('#iCategorieId').val() ;
    console.log(zType + '==>' + iItemId) ;
    if(iItemId == 0)
    {
        return false ;
    }
    $.ajax({
		url: 'index.php?route=catalog/category/ajoutIndispensable&token=<?php echo $token; ?>',
		type: 'post',
		data: 'item_id=' + iItemId + '&type='+ zType + '&category_id='+ iCategorieId ,
		dataType: 'text',
		success: function(data) {
			//console.log(data) ;
            loadIndispensable() ;
		},
        async : false ,
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
	});
}
function loadIndispensable()
{
    var iCategorieId = $('#iCategorieId').val() ;
    $.ajax({
		url: 'index.php?route=catalog/category/loadIndispensable&token=<?php echo $token; ?>',
		type: 'post',
		data: 'category_id='+ iCategorieId ,
		dataType: 'text',
		success: function(data) {
			$('#tbodyIndispensable').html(data) ;
		},
        async : false ,
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
	});
}
function suppressionIndispensable(_iItemId, _zType){
    var iCategorieId = $('#iCategorieId').val() ;
    console.log(_zType + '==>' + _iItemId) ;
    
    $.ajax({
		url: 'index.php?route=catalog/category/suppressionIndispensable&token=<?php echo $token; ?>',
		type: 'post',
		data: 'item_id=' + _iItemId + '&type='+ _zType + '&category_id='+ iCategorieId ,
		dataType: 'text',
		success: function(data) {
            loadIndispensable() ;
		},
        async : false ,
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
	});
}
//DEV103 Les Indispensables
//DEV103 Dossiers
function ajoutDossier()
{
    var zDossierNom     = $('#txtDossier').val() ;
    var iCategorieId    = $('#iCategorieId').val() ;
    if(zDossierNom == '')
    {
        alert('Veuillez completer le nom de dossier') ;
        return false ;
    }
    $.ajax({
		url: 'index.php?route=catalog/category/ajoutDossier&token=<?php echo $token; ?>',
		type: 'post',
		data: 'category_id='+ iCategorieId + '&zDossierNom=' + zDossierNom,
		dataType: 'text',
		success: function(data) {
			loadDossier() ;
            $('#txtDossier').val('NewDossier') ;
		},
        async : false ,
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
	});
    
}
function updateCategorieDossier(_iDossierId)
{
    var zSerializeItem = '' ;
    $('.checkDossier' + _iDossierId).each(function(){
        if($(this).is(':checked'))
        {
            zSerializeItem += (zSerializeItem == '') ? $(this).val() : '_' + $(this).val() ;
        }
    }) ;
    $('#txtSerializeItem_' + _iDossierId).val(zSerializeItem) ;
    $.ajax({
		url: 'index.php?route=catalog/category/updateDossier&token=<?php echo $token; ?>',
		type: 'post',
		data: 'dossier_id='+ _iDossierId + '&zSerializeItem=' + zSerializeItem,
		dataType: 'text',
		success: function(data) {
			loadDossier() ;
		},
        async : false ,
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
	});
}
function supprimerDossier(_iDossierId)
{
    $.ajax({
		url: 'index.php?route=catalog/category/supprimerDossier&token=<?php echo $token; ?>',
		type: 'post',
		data: 'dossier_id='+ _iDossierId ,
		dataType: 'text',
		success: function(data) {
			loadDossier() ;
		},
        async : false ,
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
	});
}
function loadDossier()
{
    var iCategorieId = $('#iCategorieId').val() ;
    $.ajax({
		url: 'index.php?route=catalog/category/loadDossier&token=<?php echo $token; ?>',
		type: 'post',
		data: 'category_id='+ iCategorieId ,
		dataType: 'text',
		success: function(data) {
			$('#tbodyDossier').html(data) ;
		},
        async : false ,
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
	});
}
//DEV103 Dossiers
//--></script> 
  <script type="text/javascript"><!--
$('#language a:first').tab('show');
//--></script></div>
<?php echo $footer; ?>