<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
		  <h1><?php echo $heading_title; ?></h1>
		  <ul class="breadcrumb">
			<?php foreach ($breadcrumbs as $breadcrumb) { ?>
			<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
			<?php } ?>
		  </ul>
		</div>
	</div>
	<div class="container-fluid">
		 <div class="col-md-4">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-user"></i><?php echo $commerciale_details; ?></h3>
          </div>
          <table class="table">
            <tr>
              <td style="width: 1%;"><button data-toggle="tooltip" title="<?php echo $text_commerciale; ?>" class="btn btn-info btn-xs"><i class="fa fa-user fa-fw"></i></button></td>
              	<td>
                <a target="_blank"><?php echo $firstname; ?> <?php echo $lastname; ?></a>
               </td>
            </tr>
            <tr>
              <td><button data-toggle="tooltip" title="User-Groupe" class="btn btn-info btn-xs"><i class="fa fa-group fa-fw"></i></button></td>
              <td><a target="_blank"><?php echo $text_commerciale; ?></a></td>
            </tr>
            <tr>
              <td><button data-toggle="tooltip" title="Email" class="btn btn-info btn-xs"><i class="fa fa-envelope-o fa-fw"></i></button></td>
              <td><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></td>
            </tr>
             <tr>
              <td><button data-toggle="tooltip" title="Chiffre d'affaire" class="btn btn-info btn-xs"><i class="fa fa-shopping-cart fa-fw"></i></button></td>
              <td><a target="_blank"><?php echo $chiffreAffaire; ?></a></td>
            </tr>
          </table>
        </div>
      </div>
	</div>
	<div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-user"></i><?php echo $listeclient; ?></h3>
    </div>
	<div class="container-fluid">
		<div class="panel panel-default">
			<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-header">
				<div class="table-responsive">     
					<table class="table table-bordered table-hover">
					  <thead>
							<tr>
								<th class="left"><?php echo $customer_name; ?></th> 
								<th class="left"><?php echo $chiffre; ?></th> 
								<th class="left"><?php echo $action; ?></th> 
							</tr>
					  </thead>
					  <tbody>							
							<?php if ($clients) { ?>
								<?php foreach ($clients as $client) { ?>
									<tr>
							 			<td class="left"><?php echo $client['firstname']; echo ' '; echo $client['lastname']; ?></td>
							  			<td class="left"><?php echo $client['chiffreAffaire']; ?></td>
							  			<td class="text-center"><a href="<?php echo $client['view']; ?>" data-toggle="tooltip" title="<?php echo $details; ?>" class="btn btn-primary"><i class="fa fa-eye"></i></a></td>
									</tr>
								<?php } ?>
							<?php } else { ?>
							<tr>
							  <td class="center" colspan="5"><?php echo $text_no_results; ?></td>
							</tr>
							<?php } ?>
					  </tbody>
					</table>
				</div>
			</form>
		</div>
	</div>
</div>
<?php echo $footer; ?>