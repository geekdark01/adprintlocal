<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
		  <h1><?php echo $heading_title; ?></h1>
		  <ul class="breadcrumb">
			<?php foreach ($breadcrumbs as $breadcrumb) { ?>
			<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
			<?php } ?>
		  </ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="panel panel-default">
			<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-header">
				<div class="table-responsive">     
					<table class="table table-bordered table-hover">
					  <thead>
							<tr>
								<th class="left"><?php echo $commerciale_name; ?></th> 
								<th class="left"><?php echo $commerciale_email; ?></th> 
								<th class="left"><?php echo $chiffre; ?></th> 
								<th class="left"><?php echo $nombreclient; ?></th> 
								<th class="left"><?php echo $details; ?></th> 
							</tr>
					  </thead>
					  <tbody>							
							<?php if ($commerciales) { ?>
								<?php foreach ($commerciales as $commerciale) { ?>
									<tr>
							 			<td class="left"><?php echo $commerciale['firstname']; echo ' '; echo $commerciale['lastname']; ?></td>
							  			<td class="left"><?php echo $commerciale['email']; ?></td>
							  			<td class="left"><?php echo $commerciale['chiffreAffaire']; ?></td>
							  			<td class="left"><?php echo $commerciale['nombreclient']; ?></td> 
							  			<td class="left"> <a href="<?php echo $commerciale['view']; ?>" data-toggle="tooltip" title="<?php echo $details; ?>" class="btn btn-primary"><i class="fa fa-eye"></i></a></td>
									</tr>
								<?php } ?>
							<?php } else { ?>
							<tr>
							  <td class="center" colspan="5"><?php echo $text_no_results; ?></td>
							</tr>
							<?php } ?>
					  </tbody>
					</table>
				</div>
			</form>
		</div>
	</div>
</div>
<?php echo $footer; ?>