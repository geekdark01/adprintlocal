<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_install) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_install; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php foreach ($rows as $row) { ?>
    <div class="row">
      <?php foreach ($row as $dashboard_1) { ?>
      <?php $class = 'col-lg-' . $dashboard_1['width'] . ' col-md-3 col-sm-6'; ?>
      <?php foreach ($row as $dashboard_2) { ?>
      <?php if ($dashboard_2['width'] > 3) { ?>
      <?php $class = 'col-lg-' . $dashboard_1['width'] . ' col-md-12 col-sm-12'; ?>
      <?php } ?>
      <?php } ?>
      <div class="<?php echo $class; ?>"><?php echo $dashboard_1['output']; ?></div>
      <?php } ?>
    </div>
    <?php } ?>
	<div class="row">
	<!-- Parrainage -->
	 <hr/>
      <div class="col-lg-12">
        <h2>Validation parrainage :</h2>
          <table class="table table-bordered">
         
              <thead>
                    <tr>
                        <th>Client</th>
                        <th>Email client</th>
                        <th>Montant gagné</th>
                        <th>Date</th>
                    </tr>
                </thead>
              <tbody>
             <?php if(isset($parrainage)) { ?>
            <?php foreach($parrainage as $list){?>
                <tr>
                    <td><?php echo $list['nom'].' '.$list['prenom'];?></td>
                    <td><?php echo $list['email']; ?></td>
                    <td><?php echo $list['montants']; ?></td>
                    <td><?php echo $list['date_action']; ?></td>
                    <td><a class="btn btn-info" href="<?php echo $list['href']; ?>" data-toogle="tooltip" title="" data-original-title="Validate">
                            <i class="fa fa-check" style="display:inline-block; text-rendering:auto;"></i>
                            </a>
                    </td>
                </tr>
             <?php } ?>
         <?php }else{
                    ?>
					 <tr>
                    <td colspan='5' style='text-align:center'>Aucun parrainage &agrave; valider</td>
					</tr>
					<?php
                } ?>
				  </tbody>
            </table>
      </div>
	  <!-- Parrainage -->
	   </div>
  </div>
</div>
<?php echo $footer; ?>