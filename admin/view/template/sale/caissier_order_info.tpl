<?php echo $header; ?><?php echo $column_left; ?>

<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right"> <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-4">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-shopping-cart"></i> <?php echo $text_order_detail; ?></h3>
          </div>
          <table class="table">
            <tbody>
              <tr>
                <td style="width: 1%;"><button data-toggle="tooltip" title="<?php echo $text_store; ?>" class="btn btn-info btn-xs"><i class="fa fa-shopping-cart fa-fw"></i></button></td>
                <td><a href="<?php echo $store_url; ?>" target="_blank"><?php echo $store_name; ?></a></td>
              </tr>
              <tr>
                <td><button data-toggle="tooltip" title="<?php echo $text_date_added; ?>" class="btn btn-info btn-xs"><i class="fa fa-calendar fa-fw"></i></button></td>
                <td><?php echo $date_added; ?></td>
              </tr>
              <tr>
                <td><button data-toggle="tooltip" title="<?php echo $text_payment_method; ?>" class="btn btn-info btn-xs"><i class="fa fa-credit-card fa-fw"></i></button></td>
                <td><?php echo $payment_method; ?></td>
              </tr>
              <?php if ($shipping_method) { ?>
              <tr>
                <td><button data-toggle="tooltip" title="<?php echo $text_shipping_method; ?>" class="btn btn-info btn-xs"><i class="fa fa-truck fa-fw"></i></button></td>
                <td>
                  <?php echo $shipping_method; ?>
                    <?php if($shipping_method == "Livraison") { ?>
                      <?php if($weight <= 0.5) { ?>
                        - véhicule utilisé : moto
                      <?php } elseif(($weight <= 2.5) && ($weight > 0.5)) { ?>
                        - véhicule utilisé : Renault Express
                      <?php } else { ?>
                        - véhicule utilisé : camion
                      <?php } ?>
                    <?php } ?>
                  </td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
      <div class="col-md-4">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-user"></i><?php echo $text_customer_detail; ?></h3>
          </div>
          <table class="table">
            <tr>
              <td style="width: 1%;"><button data-toggle="tooltip" title="<?php echo $text_customer; ?>" class="btn btn-info btn-xs"><i class="fa fa-user fa-fw"></i></button></td>
              <td><?php if ($customer) { ?>
                <a href="<?php echo $customer; ?>" target="_blank"><?php echo $firstname; ?> <?php echo $lastname; ?></a>
                <?php } else { ?>
                <?php echo $firstname; ?> <?php echo $lastname; ?>
                <?php } ?></td>
            </tr>
            <tr>
              <td><button data-toggle="tooltip" title="<?php echo $text_customer_group; ?>" class="btn btn-info btn-xs"><i class="fa fa-group fa-fw"></i></button></td>
              <td><?php echo $customer_group; ?></td>
            </tr>
            <tr>
              <td><button data-toggle="tooltip" title="<?php echo $text_email; ?>" class="btn btn-info btn-xs"><i class="fa fa-envelope-o fa-fw"></i></button></td>
              <td><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></td>
            </tr>
            <tr>
              <td><button data-toggle="tooltip" title="<?php echo $text_telephone; ?>" class="btn btn-info btn-xs"><i class="fa fa-phone fa-fw"></i></button></td>
              <td><?php echo $telephone; ?></td>
            </tr>
          </table>
        </div>
      </div>
      <div class="col-md-4">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-cog"></i> <?php echo $text_option; ?></h3>
          </div>
          <table class="table">
            <tbody>	
              <?php if ($delai_valid) { ?>
        <tr>
                <td><?php echo $text_delai; ?></td>
                <td class="text-right">
          <div id="delai-value"><?php echo $delai; ?></div>
          <div id="delai-edit" class="checkbox-delai d-none" style="display:none">
            <label class="radio-inline"><input id="delai-0" name="delai" value="0" type="radio">Sans</label>
            <label class="radio-inline"><input id="delai-15" name="delai" value="15" type="radio">15 J</label>
            <label class="radio-inline"><input id="delai-30" name="delai" value="30" type="radio">30 J</label>
            <label class="radio-inline"><input id="delai-45" name="delai" value="45" type="radio">45 J</label>
          </div>
        </td>
                <td class="text-center">
          <div id="btn-value">
            <button id="button-delai-edit" data-loading-text="..." data-toggle="tooltip" title="Modifier" class="btn btn-waring btn-xs"><i class="fa fa-edit"></i></button>
            <button id="button-delai-valide" data-loading-text="..." data-toggle="tooltip" title="Confirmer" class="btn btn-success btn-xs"><i class="fa fa-check"></i></button>
          </div>
          <div id="btn-edit" style="display:none">
            <button id="button-delai-cancel" data-loading-text="..." data-toggle="tooltip" title="Cancel" class="btn btn-default btn-xs"><i class="fa fa-reply"></i></button>
            <button id="button-delai-submit" data-loading-text="..." data-toggle="tooltip" title="Submit" class="btn btn-danger btn-xs"><i class="fa fa-check"></i></button>
          </div>
        </td>
        </tr>
        <?php } else { ?>
        <tr>
                <td><?php echo $text_delai; ?></td>
                <td class="text-right">
          <div id="delai-value"><?php echo $delai; ?></div>
        </td>
                <td class="text-center">
          <div id="btn-value">
            <button id="button-delai-edit" disabled="disabled" data-loading-text="..." data-toggle="tooltip" title="Modifier" class="btn btn-waring btn-xs"><i class="fa fa-edit"></i></button>
            <button id="button-delai-valide" disabled="disabled" data-loading-text="..." data-toggle="tooltip" title="Confirmer" class="btn btn-success btn-xs"><i class="fa fa-check"></i></button>
          </div>
        </td>
        </tr>       
        <?php } ?>

            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-info-circle"></i> <?php echo $text_order; ?></h3>
      </div>
      <div class="panel-body">
        <table class="table table-bordered">
          <thead>
            <tr>
              <td style="width: 50%;" class="text-left"><?php echo $text_payment_address; ?></td>
              <?php if ($shipping_method) { ?>
              <td style="width: 50%;" class="text-left"><?php echo $text_shipping_address; ?></td>
              <?php } ?>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td class="text-left"><?php echo $payment_address; ?></td>
              <?php if ($shipping_method) { ?>
              <td class="text-left"><?php echo $shipping_address; ?></td>
              <?php } ?>
            </tr>
          </tbody>
        </table>
        <table class="table table-bordered">
          <thead>
            <tr>
              <td class="text-left"><?php echo $column_product; ?></td>
              <td class="text-left"><?php echo $column_model; ?></td>
              <td class="text-right"><?php echo $text_visuel; ?></td>
              <td class="text-right"><?php echo $text_validation; ?></td>
              <td class="text-right"><?php echo $column_quantity; ?></td>
              <td class="text-right"><?php echo $column_price; ?></td>
              <td class="text-right"><?php echo $column_total; ?></td>
              <!--DEV103 Envoi BAT-->
              <td class="text-right"><?php echo $text_bat; ?></td>
              <!--DEV103 Envoi BAT-->
            </tr>
          </thead>
          <tbody>
            <?php $bat = 0; ?>
            <?php foreach ($products as $product) { ?>
            <tr>
              <td class="text-left"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                <?php foreach ($product['option'] as $option) { ?>
                <br />
                <?php if ($option['type'] != 'file') { ?>
                &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
                <?php } else { ?>
                &nbsp;<small> - <?php echo $option['name']; ?>: <a href="<?php echo $option['href']; ?>"><?php echo $option['value']; ?></a></small>
                <?php } ?>
                <?php } ?></td>
              <td class="text-left"><?php echo $product['model']; ?></td>
              <td class="text-right">
                <!--San-01-12-2018-->
                    <?php if($product['fichier_bat'] && $product['code_fichier'] != null): ?>
                        <a href="<?php echo $product['code_fichier']; ?>"><?php echo $product['fichier_bat'];?></a>
                    <?php endif; ?> 
                    
                <!--San-01-12-2018-->  
              </td>
              <td class="text-right">
                    <?php if($product['code_fichier'] && $product['code_fichier'] != null){ ?>
                      <?php if($product['fichier_status'] == 0): ?>
                       <span class="val<?php echo $product['product_id'] . '_' .  $product['order_product_id'] ; ?>_1" style="display:none;"><?php echo $text_valide ; ?></span>
                       <span class="val<?php echo  $product['product_id'] . '_' .  $product['order_product_id'] ; ?>_2" style="display:none;"><?php echo $text_refuse ; ?></span>
                       <a class="val<?php echo  $product['product_id'] . '_' .  $product['order_product_id'] ; ?>_0 btn btn-link"  href="javascript:void(0);" onclick="actFichier(<?php echo $product['order_product_id'] ; ?>, <?php echo $product['product_id'] ; ?>, 1) ;"><?php echo $text_valider ; ?></a>
                                            &nbsp;
                       <a class="val<?php echo  $product['product_id'] . '_' .  $product['order_product_id'] ; ?>_0 btn btn-link"  href="javascript:void(0);" onclick="actFichier(<?php echo $product['order_product_id'] ; ?>, <?php echo $product['product_id'] ; ?>, 2) ;"><?php echo $text_refuser ; ?></a>
                       <?php elseif($product['fichier_status'] == 1): ?>
                       <span class="val<?php echo  $product['product_id'] . '_' .  $product['order_product_id'] ; ?>_1"><?php echo $text_valide ; ?></span>
                      <?php elseif($product['fichier_status'] == 2): ?>
                       <span class="val<?php echo  $product['product_id'] . '_' .  $product['order_product_id'] ; ?>_2"><?php echo $text_refuse ; ?></span>
                       <?php else:?>
                         &nbsp;-&nbsp;
                       <?php endif ; ?>
                    <?php } else{ ?>
                        <span ><?php echo 'Fichier manquante'; ?></span>
                    <?php } ?>
                  </td>
              </td>
              <td class="text-right"><?php echo $product['quantity']; ?></td>
              <td class="text-right"><?php echo $product['price']; ?></td>
              <td class="text-right"><?php echo $product['total']; ?></td>

              <!--DEV103 Envoi BAT--><!-- San-01-12-2018-->
              
              <?php foreach ($product['option'] as $option) { ?>
                   <?php if ($option['name'] == 'Envoi du BAT' && $option['value'] == 'Oui') { ?>
                   <?php $bat = $bat+1; ?>
                      <td class="text-right"><a href="javascript:void(0);" onclick="displayEnvoiBat(<?php echo $product['product_id'] ; ?>);"><?php echo $text_envoi_bat ; ?></a></td>
                   <?php } ?>
              <?php } ?>
              <!--DEV103 Envoi BAT--><!-- San-01-12-2018-->
            </tr>
            
            <!--DEV103 Envoi BAT-->
            <tr class="trEnvoiBat" style="display:none;" id="trEnvoiBat_<?php echo $product['product_id'] ; ?>">
                <td colspan="7">
                    <button type="button" id="button-upload_fichier_model" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default btn-block"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
                    <input type="hidden" name="code_fichier_model" value="" id="input-option_fichier_model_<?php echo $product['product_id'] ; ?>" />
                    <div id="bat_<?php echo $product['product_id'] ;?>">
                    </div>
                </td>
            </tr>
            <script type="text/javascript">
                $.ajax({
                    url: 'index.php?route=sale/order/loadBatPerProduct&token=<?php echo $token; ?>',
                    type: 'post',
                    data: 'product_id=<?php echo $product['product_id'] ;?>&order_id=<?php echo $order_id; ?>' ,
                    dataType: 'text',
                    success: function(data) {
                        $('#bat_<?php echo $product['product_id'] ;?>').html(data) ;
                    },
                    async : false ,
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            </script>
            <!--DEV103 Envoi BAT-->
            <?php } ?>
            <?php foreach ($vouchers as $voucher) { ?>
            <tr>
              <td class="text-left"><a href="<?php echo $voucher['href']; ?>"><?php echo $voucher['description']; ?></a></td>
              <td class="text-left"></td>
              <td class="text-right">1</td>
              <td class="text-right"><?php echo $voucher['amount']; ?></td>
              <td class="text-right"><?php echo $voucher['amount']; ?></td>
            </tr>
            <?php } ?>
            <?php foreach ($totals as $total) { ?>
            <tr>
              <td colspan="4" class="text-right"><?php echo $total['title']; ?></td>
              <td class="text-right"><?php echo $total['text']; ?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
        <?php if ($comment) { ?>
        <table class="table table-bordered">
          <thead>
            <tr>
              <td><?php echo $text_comment; ?></td>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><?php echo $comment; ?></td>
            </tr>
          </tbody>
        </table>
        <?php } ?>
      </div>
    </div> 
    <div>
            <fieldset>
              <legend> Validation paiement
              </legend>
              
                <div id="payment-edit">
                  <label class="radio-inline"><input id="payment-0" name="status_payment" value="0" type="radio">non payé</label>
                  <label class="radio-inline"><input id="payment-1" name="status_payment" value="1" type="radio">payé</label>
                  </div>
                
                <div>
                     <button id="button-payment-valide" data-loading-text="..." data-toggle="tooltip" title="Confirmer" class="btn btn-danger btn-xs"><i class="fa fa-check"></i></button>
                </div>
              
            </fieldset>
            
            </div>
  <script type="text/javascript"><!--
$(document).delegate('#button-ip-add', 'click', function() {
	$.ajax({
		url: 'index.php?route=user/api/addip&token=<?php echo $token; ?>&api_id=<?php echo $api_id; ?>',
		type: 'post',
		data: 'ip=<?php echo $api_ip; ?>',
		dataType: 'json',
		beforeSend: function() {
			$('#button-ip-add').button('loading');
		},
		complete: function() {
			$('#button-ip-add').button('reset');
		},
		success: function(json) {
			$('.alert').remove();

			if (json['error']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
			}

			if (json['success']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$(document).delegate('#button-invoice', 'click', function() {
	$.ajax({
		url: 'index.php?route=sale/order/createinvoiceno&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>',
		dataType: 'json',
		beforeSend: function() {
			$('#button-invoice').button('loading');
		},
		complete: function() {
			$('#button-invoice').button('reset');
		},
		success: function(json) {
			$('.alert').remove();

			if (json['error']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['invoice_no']) {
				$('#invoice').html(json['invoice_no']);

				$('#button-invoice').replaceWith('<button disabled="disabled" class="btn btn-success btn-xs"><i class="fa fa-cog"></i></button>');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$(document).delegate('#button-reward-add', 'click', function() {
	$.ajax({
		url: 'index.php?route=sale/order/addreward&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>',
		type: 'post',
		dataType: 'json',
		beforeSend: function() {
			$('#button-reward-add').button('loading');
		},
		complete: function() {
			$('#button-reward-add').button('reset');
		},
		success: function(json) {
			$('.alert').remove();

			if (json['error']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['success']) {
                $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

				$('#button-reward-add').replaceWith('<button id="button-reward-remove" data-toggle="tooltip" title="<?php echo $button_reward_remove; ?>" class="btn btn-danger btn-xs"><i class="fa fa-minus-circle"></i></button>');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$(document).delegate('#button-reward-remove', 'click', function() {
	$.ajax({
		url: 'index.php?route=sale/order/removereward&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>',
		type: 'post',
		dataType: 'json',
		beforeSend: function() {
			$('#button-reward-remove').button('loading');
		},
		complete: function() {
			$('#button-reward-remove').button('reset');
		},
		success: function(json) {
			$('.alert').remove();

			if (json['error']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['success']) {
                $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

				$('#button-reward-remove').replaceWith('<button id="button-reward-add" data-toggle="tooltip" title="<?php echo $button_reward_add; ?>" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i></button>');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

//Move1
$(document).delegate('#button-delai-edit', 'click', function() {
  
  $('#delai-value').hide();
  $('#btn-value').hide();
  $('#delai-edit').show();
  $('#btn-edit').show();
});
//move2
$(document).delegate('#button-delai-cancel', 'click', function() {
  
  $('#delai-value').show();
  $('#btn-value').show();
  $('#delai-edit').hide();
  $('#btn-edit').hide();
  $('input[name=\'delai\']').prop( "checked", false );
});
//Move3
$(document).delegate('#button-delai-valide', 'click', function() {
  $.ajax({
    url: 'index.php?route=sale/order/valideDelai&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>',
    type: 'post',
    dataType: 'json',
    beforeSend: function() {
      $('#button-delai-valide').button('loading');
    },
    complete: function() {
      $('#button-delai-valide').button('reset');
      
    },
    success: function(json) {
      $('.alert').remove();

      if (json['error']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
      }
      if (json['success']) {
                $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');
        $('#button-delai-valide').replaceWith('<button id="button-delai-valide" disabled="disabled" data-loading-text="..." data-toggle="tooltip" title="Confirmer" class="btn btn-success btn-xs"><i class="fa fa-check"></i></button>');
        $('#btn-value > button').attr('disabled','disabled');
        $('#delai-edit').remove();
        $('#btn-edit').remove();
      }
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
  });
});
//Move4
$(document).delegate('#button-delai-submit', 'click', function() {
  $.ajax({
    url: 'index.php?route=sale/order/addDelai&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>',
    type: 'post',
    data: $('#delai-edit input[name=\'delai\']:checked'),
    dataType: 'json',
    beforeSend: function() {
      $('#button-delai-submit').button('loading');
    },
    complete: function() {
      $('#button-delai-submit').button('reset');
    },
    success: function(json) {
      $('.alert').remove();

      if (json['error']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
      }
      if (json['warning']) {
                $('#content > .container-fluid').prepend('<div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> ' + json['warning'] + '</div>');
      }
      if (json['success']) {
                $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');
        $('#delai-value').show();
        $('#btn-value').show();
        $('#delai-edit').hide();
        $('#btn-edit').hide();
        $('#delai-value').html(json['delai']);
        $('input[name=\'delai\']').prop( "checked", false );

      }
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
  });
});

$(document).delegate('#button-payment-valide', 'click', function() {
  $.ajax({
    url: 'index.php?route=sale/order/sauverStatusPayment&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>',
    type: 'post',
    data: $('#payment-edit input[name=\'status_payment\']:checked'),
    dataType: 'json',
    beforeSend: function() {
      $('#button-payment-valide').button('loading');
    },
    complete: function() {
      $('#button-payment-valide').button('reset');
    },
    success: function(json) {
      $('.alert').remove();

      if (json['error']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
      }
      if (json['warning']) {
                $('#content > .container-fluid').prepend('<div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> ' + json['warning'] + '</div>');
      }
      if (json['success']) {
                $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');
        $('#payment-edit').hide();
        $('#button-payment-valide').replaceWith('<button id="button-payment-valide" disabled="disabled" data-loading-text="..." data-toggle="tooltip" title="Confirmer" class="btn btn-danger btn-xs"><i class="fa fa-check"></i></button>');
        $('input[name=\'status_payment\']').prop( "checked", false );

      }
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
  });
});

$(document).delegate('#button-commission-add', 'click', function() {
	$.ajax({
		url: 'index.php?route=sale/order/addcommission&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>',
		type: 'post',
		dataType: 'json',
		beforeSend: function() {
			$('#button-commission-add').button('loading');
		},
		complete: function() {
			$('#button-commission-add').button('reset');
		},
		success: function(json) {
			$('.alert').remove();

			if (json['error']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['success']) {
                $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

				$('#button-commission-add').replaceWith('<button id="button-commission-remove" data-toggle="tooltip" title="<?php echo $button_commission_remove; ?>" class="btn btn-danger btn-xs"><i class="fa fa-minus-circle"></i></button>');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$(document).delegate('#button-commission-remove', 'click', function() {
	$.ajax({
		url: 'index.php?route=sale/order/removecommission&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>',
		type: 'post',
		dataType: 'json',
		beforeSend: function() {
			$('#button-commission-remove').button('loading');
		},
		complete: function() {
			$('#button-commission-remove').button('reset');
		},
		success: function(json) {
			$('.alert').remove();

			if (json['error']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['success']) {
                $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

				$('#button-commission-remove').replaceWith('<button id="button-commission-add" data-toggle="tooltip" title="<?php echo $button_commission_add; ?>" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i></button>');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

var token = '';

// Login to the API
$.ajax({
	url: '<?php echo $catalog; ?>index.php?route=api/login',
	type: 'post',
	dataType: 'json',
	data: 'key=<?php echo $api_key; ?>',
	crossDomain: true,
	success: function(json) {
		$('.alert').remove();

        if (json['error']) {
    		if (json['error']['key']) {
    			$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['key'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
    		}

            if (json['error']['ip']) {
    			$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['ip'] + ' <button type="button" id="button-ip-add" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-danger btn-xs pull-right"><i class="fa fa-plus"></i> <?php echo $button_ip_add; ?></button></div>');
    		}
        }

        if (json['token']) {
			token = json['token'];
		}
	},
	error: function(xhr, ajaxOptions, thrownError) {
		alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	}
});

$('#history').delegate('.pagination a', 'click', function(e) {
	e.preventDefault();

	$('#history').load(this.href);
});

$('#history').load('index.php?route=sale/order/history&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>');

$('#button-history').on('click', function() {
	/*
	if (typeof verifyStatusChange == 'function'){
		if (verifyStatusChange() == false){
			return false;
		} else{
			addOrderInfo();
		}
	} else{
		addOrderInfo();
	}*/

	$.ajax({
		url: '<?php echo $catalog; ?>index.php?route=api/order/history&token=' + token + '&store_id=<?php echo $store_id; ?>&order_id=<?php echo $order_id; ?>',
		type: 'post',
		dataType: 'json',
		data: 'order_status_id=' + encodeURIComponent($('select[name=\'order_status_id\']').val()) + '&notify=' + ($('input[name=\'notify\']').prop('checked') ? 1 : 0) + '&override=' + ($('input[name=\'override\']').prop('checked') ? 1 : 0) + '&append=' + ($('input[name=\'append\']').prop('checked') ? 1 : 0) + '&comment=' + encodeURIComponent($('textarea[name=\'comment\']').val()),
		beforeSend: function() {
			$('#button-history').button('loading');
		},
		complete: function() {
			$('#button-history').button('reset');
		},
		success: function(json) {
			$('.alert').remove();

			if (json['error']) {
				$('#history').before('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
			}

			if (json['success']) {
				$('#history').load('index.php?route=sale/order/history&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>');

				$('#history').before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

				$('textarea[name=\'comment\']').val('');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

function changeStatus(){
	var status_id = $('select[name="order_status_id"]').val();

	$('#openbay-info').remove();

	$.ajax({
		url: 'index.php?route=extension/openbay/getorderinfo&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>&status_id=' + status_id,
		dataType: 'html',
		success: function(html) {
			$('#history').after(html);
		}
	});
}

function addOrderInfo(){
	var status_id = $('select[name="order_status_id"]').val();

	$.ajax({
		url: 'index.php?route=extension/openbay/addorderinfo&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>&status_id=' + status_id,
		type: 'post',
		dataType: 'html',
		data: $(".openbay-data").serialize()
	});
}

$(document).ready(function() {
	changeStatus();
});

$('select[name="order_status_id"]').change(function(){
	changeStatus();
});
//DEV103 Envoi BAT
function displayEnvoiBat(_iProduitId)
{
    $('.trEnvoiBat').slideUp() ;
    $('#trEnvoiBat_' + _iProduitId).slideDown() ;
}
$('button[id^=\'button-upload\']').on('click', function() {
    var node = this;

    $('#form-upload').remove();

    $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

    $('#form-upload input[name=\'file\']').trigger('click');

    if (typeof timer != 'undefined') {
        clearInterval(timer);
    }

    timer = setInterval(function() {
        if ($('#form-upload input[name=\'file\']').val() != '') {
            clearInterval(timer);

            $.ajax({
                url: 'index.php?route=tool/upload/upload&token=<?php echo $token; ?>',
                type: 'post',
                dataType: 'json',
                data: new FormData($('#form-upload')[0]),
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function() {
                    $(node).button('loading');
                },
                complete: function() {
                    $(node).button('reset');
                },
                success: function(json) {
                    $('.text-danger').remove();

                    if (json['error']) {
                        $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
                    }

                    if (json['success']) {
                        alert(json['success']);

                        $(node).parent().find('input').val(json['code']);
                        //input-option_fichier_model_
                        var zInputId    = $(node).parent().find('input').attr('id') ;
                        iProduitId = zInputId.replace('input-option_fichier_model_', '') ;
                        loadBatPerProduct(iProduitId, json['code']) ;
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        }
    }, 500);
});
function actFichier(_iOrder_product_Id, _iProductId, _iStatus)
{
    $.ajax({
    url: 'index.php?route=sale/order/actFichier&token=<?php echo $token; ?>',
    type: 'post',
    data: 'order_product_id='+_iOrder_product_Id+'&product_id='+_iProductId+'&fichier_status='+_iStatus,
    dataType: 'text',
    success: function(data) {
            $(".val" + _iProductId + "_" + _iOrder_product_Id + "_0").remove() ;
            $(".val" + _iProductId + "_" + _iOrder_product_Id + "_" + _iStatus).show() ;
    },
        async : false ,
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
  });
}
//DEV103 Envoi BAT
function loadBatPerProduct(_iProduitId, _zCodeFichier)
{
    if(_zCodeFichier != '')
    {
        $.ajax({
            url: 'index.php?route=sale/order/envoiBat&token=<?php echo $token; ?>',
            type: 'post',
            data: 'product_id=' + _iProduitId + '&order_id=<?php echo $order_id; ?>&code_fichier='+ _zCodeFichier ,
            dataType: 'text',
            success: function(data) {
                console.log('success ajout') ;
            },
            async : false ,
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
    $.ajax({
        url: 'index.php?route=sale/order/loadBatPerProduct&token=<?php echo $token; ?>',
        type: 'post',
        data: 'product_id=' + _iProduitId + '&order_id=<?php echo $order_id; ?>' ,
        dataType: 'text',
        success: function(data) {
            $('#bat_' + _iProduitId).html(data) ;
        },
        async : false ,
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
}
function supprimerBat(_iBatId, _iProduitId)
{
    $.ajax({
        url: 'index.php?route=sale/order/supprimerBat&token=<?php echo $token; ?>',
        type: 'post',
        data: 'bat_id=' + _iBatId ,
        dataType: 'text',
        success: function(data) {
            loadBatPerProduct(_iProduitId, '') ;
        },
        async : false ,
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
}
//DEV103 Envoi BAT


//DEV103 Envoi BAT
//--></script> 
</div>
</div>
<?php echo $footer; ?>