<?php 
    $_SESSION['batperproduct'] = $toBatPerProduct;
?>
<div class="col-md-12">
    <table class="table">
        <thead>
            <tr>
                <th>
                    <?php echo $text_fichier ; ?>
                </th>
                <th>
                    <?php echo $text_date_added ; ?>
                </th>
                <th>
                    <?php echo $text_status ; ?>
                </th>
                <th>
                    <?php echo $column_action ; ?>
                </th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($toBatPerProduct as $toBat): ?>
                <tr>
                    <td>
                        <a href="<?php echo $toBat['url_fichier'] ; ?>"><?php echo $toBat['fichier'] ; ?></a>
                    </td>
                    <td>
                        <?php echo $toBat['date_added'] ; ?>
                    </td>
                    <td>
                        <?php if($toBat['status'] == 0): ?>
                            <span><?php echo $text_pending ; ?></span>
                        <?php elseif($toBat['status'] == 1): ?>
                            <span><?php echo $text_valide ; ?>&nbsp;le&nbsp;<?php echo $toBat['date_validated'] ; ?></span>
                        <?php else: ?>
                            <span><?php echo $text_refuse ; ?>&nbsp;le&nbsp;<?php echo $toBat['date_validated'] ; ?></span>
                        <?php endif ; ?>
                    </td>
                    <td>
                        <a onclick="supprimerBat(<?php echo $toBat['bat_id'] ; ?>, <?php echo $toBat['product_id'] ; ?>);" href="javascript:void(0);" class="btn btn-danger"><?php echo $button_delete ; ?></a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>