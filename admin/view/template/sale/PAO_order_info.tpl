 <!--San-001-04-12-18-->
 <?php echo $header; ?><?php echo $column_left; ?>

<div id="content">
  <div class="container-fluid">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-info-circle"></i> <?php echo $text_order; ?></h3>
      </div>
      <div class="panel-body">
        <table class="table table-bordered">
          <thead>
            <tr>
              <td style="width: 50%;" class="text-left"><?php echo $text_payment_address; ?></td>
              <?php if ($shipping_method) { ?>
              <td style="width: 50%;" class="text-left"><?php echo $text_shipping_address; ?></td>
              <?php } ?>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td class="text-left"><?php echo $payment_address; ?></td>
              <?php if ($shipping_method) { ?>
              <td class="text-left"><?php echo $shipping_address; ?></td>
              <?php } ?>
            </tr>
          </tbody>
        </table>
        <table class="table table-bordered">
          <thead>
            <tr>
              <td class="text-left"><?php echo $column_product; ?></td>
              <td class="text-left"><?php echo $column_model; ?></td>
              <td class="text-right"><?php echo $text_visuel; ?></td>
              <td class="text-right"><?php echo $text_validation; ?></td>
              <td class="text-right"><?php echo $column_quantity; ?></td>
              <td class="text-right"><?php echo $column_price; ?></td>
              <td class="text-right"><?php echo $column_total; ?></td>
              <!--DEV103 Envoi BAT-->
              <td class="text-right"><?php echo $text_bat; ?></td>
              <!--DEV103 Envoi BAT-->
            </tr>
          </thead>
          <tbody>
            <?php $bat = 0 ?>
            <?php foreach ($products as $product) { ?>
            <tr>
              <td class="text-left"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                <?php foreach ($product['option'] as $option) { ?>
                  <?php if($option['name'] == 'Laize Max Chute' && $option['value'] != 0){ ?>
                     &nbsp;<small style= "color: red;"> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?> cm</small><br />
                  <?php } ?>
                <?php } ?>
                <?php foreach ($product['option'] as $option) { ?>
                  <?php if($option['name'] != 'Laize Max Chute'){ ?>
                    <?php if ($option['type'] != 'file') { ?>
                        <?php if($option['name'] == 'Largeur' || $option['name'] == 'Hauteur') { ?>
                            &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?> cm</a></small><br />
                        <?php }else{ ?>
                            &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></a></small><br />
                        <?php } ?>
                    <?php }else{ ?>
                      &nbsp;<small> - <?php echo $option['name']; ?>: <a href="<?php echo $option['href']; ?>"><?php echo $option['value']; ?></a></small><br />
                    <?php } ?>
                  <?php } ?>
                <?php } ?>
              </td>
              <td class="text-left"><?php echo $product['model']; ?></td>
              <td class="text-right">
                <!--San-01-12-2018-->
                    <?php if($product['fichier_bat'] && $product['code_fichier'] != null): ?>
                        <a href="<?php echo $product['code_fichier']; ?>"><?php echo $product['fichier_bat'];?></a>
                         <?php $product['code_fichier']= null  ?>
                    <?php endif; ?> 
                <!--San-01-12-2018-->  
              </td>
              <td class="text-right">
                    <?php if($product['code_fichier'] && $product['code_fichier'] != null){ ?>
                      <?php if($product['fichier_status'] == 0): ?>
                       <span class="val<?php echo $product['product_id'] . '_' .  $product['order_product_id'] ; ?>_1" style="display:none;"><?php echo $text_valide ; ?></span>
                       <span class="val<?php echo  $product['product_id'] . '_' .  $product['order_product_id'] ; ?>_2" style="display:none;"><?php echo $text_refuse ; ?></span>
                       <a class="val<?php echo  $product['product_id'] . '_' .  $product['order_product_id'] ; ?>_0 btn btn-link"  href="javascript:void(0);" onclick="actFichier(<?php echo $product['order_product_id'] ; ?>, <?php echo $product['product_id'] ; ?>, 1) ;"><?php echo $text_valider ; ?></a>
                                            &nbsp;
                       <a class="val<?php echo  $product['product_id'] . '_' .  $product['order_product_id'] ; ?>_0 btn btn-link"  href="javascript:void(0);" onclick="actFichier(<?php echo $product['order_product_id'] ; ?>, <?php echo $product['product_id'] ; ?>, 2) ;"><?php echo $text_refuser ; ?></a>
                       <?php elseif($product['fichier_status'] == 1): ?>
                       <span class="val<?php echo  $product['product_id'] . '_' .  $product['order_product_id'] ; ?>_1"><?php echo $text_valide ; ?></span>
                      <?php elseif($product['fichier_status'] == 2): ?>
                       <span class="val<?php echo  $product['product_id'] . '_' .  $product['order_product_id'] ; ?>_2"><?php echo $text_refuse ; ?></span>
                       <?php else:?>
                         &nbsp;-&nbsp;
                       <?php endif ; ?>
                    <?php } else{ ?>
                        <span ><?php echo 'Fichier manquante'; ?></span>
                    <?php } ?>
              </td>
              <td class="text-right"><?php echo $product['quantity']; ?></td>
              <td class="text-right"><?php echo $product['price']; ?></td>
              <td class="text-right"><?php echo $product['total']; ?></td>

              <!--DEV103 Envoi BAT--><!-- San-01-12-2018-->
              
                <?php foreach ($product['option'] as $option) { ?>
                   <?php if ($option['name'] == 'Envoi du BAT' && $option['value'] == 'Oui') { ?>
                     <?php $bat = $bat+1 ?>
              <td class="text-right"><a href="javascript:void(0);" onclick="displayEnvoiBat(<?php echo $product['product_id'] ; ?>);"><?php echo $text_envoi_bat ; ?></a></td>
                   <?php } ?>
                <?php } ?>
              <!--DEV103 Envoi BAT--><!-- San-01-12-2018-->
            </tr>
            
            <!--DEV103 Envoi BAT-->
            <tr class="trEnvoiBat" style="display:none;" id="trEnvoiBat_<?php echo $product['product_id'] ; ?>">
                <td colspan="7">
                    <button type="button" id="button-upload_fichier_model" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default btn-block"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
                    <input type="hidden" name="code_fichier_model" value="" id="input-option_fichier_model_<?php echo $product['product_id'] ; ?>" />
                    <div id="bat_<?php echo $product['product_id'] ;?>">
                    </div>
                </td>
            </tr>
            
            <script type="text/javascript">
                $.ajax({
                    url: 'index.php?route=sale/order/loadBatPerProduct&token=<?php echo $token; ?>',
                    type: 'post',
                    data: 'product_id=<?php echo $product['product_id'] ;?>&order_id=<?php echo $order_id; ?>' ,
                    dataType: 'text',
                    success: function(data) {
                        $('#bat_<?php echo $product['product_id'] ;?>').html(data) ;
                    },
                    async : false ,
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            </script>
            <!--DEV103 Envoi BAT-->
            <?php } ?>
            <?php foreach ($vouchers as $voucher) { ?>
            <tr>
              <td class="text-left"><a href="<?php echo $voucher['href']; ?>"><?php echo $voucher['description']; ?></a></td>
              <td class="text-left"></td>
              <td class="text-right">1</td>
              <td class="text-right"><?php echo $voucher['amount']; ?></td>
              <td class="text-right"><?php echo $voucher['amount']; ?></td>
            </tr>
            <?php } ?>
            <?php foreach ($totals as $total) { ?>
            <tr>
              <td colspan="4" class="text-right"><?php echo $total['title']; ?></td>
              <td class="text-right"><?php echo $total['text']; ?> Ar</td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
        <?php if ($comment) { ?>
        <table class="table table-bordered">
          <thead>
            <tr>
              <td><?php echo $text_comment; ?></td>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><?php echo $comment; ?></td>
            </tr>
          </tbody>
        </table>
        <?php } ?>
      </div>
    </div> 
  <script type="text/javascript"><!--
$(document).delegate('#button-ip-add', 'click', function() {
	$.ajax({
		url: 'index.php?route=user/api/addip&token=<?php echo $token; ?>&api_id=<?php echo $api_id; ?>',
		type: 'post',
		data: 'ip=<?php echo $api_ip; ?>',
		dataType: 'json',
		beforeSend: function() {
			$('#button-ip-add').button('loading');
		},
		complete: function() {
			$('#button-ip-add').button('reset');
		},
		success: function(json) {
			$('.alert').remove();

			if (json['error']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
			}

			if (json['success']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$(document).delegate('#button-invoice', 'click', function() {
	$.ajax({
		url: 'index.php?route=sale/order/createinvoiceno&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>',
		dataType: 'json',
		beforeSend: function() {
			$('#button-invoice').button('loading');
		},
		complete: function() {
			$('#button-invoice').button('reset');
		},
		success: function(json) {
			$('.alert').remove();

			if (json['error']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['invoice_no']) {
				$('#invoice').html(json['invoice_no']);

				$('#button-invoice').replaceWith('<button disabled="disabled" class="btn btn-success btn-xs"><i class="fa fa-cog"></i></button>');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$(document).delegate('#button-reward-add', 'click', function() {
	$.ajax({
		url: 'index.php?route=sale/order/addreward&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>',
		type: 'post',
		dataType: 'json',
		beforeSend: function() {
			$('#button-reward-add').button('loading');
		},
		complete: function() {
			$('#button-reward-add').button('reset');
		},
		success: function(json) {
			$('.alert').remove();

			if (json['error']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['success']) {
                $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

				$('#button-reward-add').replaceWith('<button id="button-reward-remove" data-toggle="tooltip" title="<?php echo $button_reward_remove; ?>" class="btn btn-danger btn-xs"><i class="fa fa-minus-circle"></i></button>');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$(document).delegate('#button-reward-remove', 'click', function() {
	$.ajax({
		url: 'index.php?route=sale/order/removereward&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>',
		type: 'post',
		dataType: 'json',
		beforeSend: function() {
			$('#button-reward-remove').button('loading');
		},
		complete: function() {
			$('#button-reward-remove').button('reset');
		},
		success: function(json) {
			$('.alert').remove();

			if (json['error']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['success']) {
                $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

				$('#button-reward-remove').replaceWith('<button id="button-reward-add" data-toggle="tooltip" title="<?php echo $button_reward_add; ?>" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i></button>');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$(document).delegate('#button-commission-add', 'click', function() {
	$.ajax({
		url: 'index.php?route=sale/order/addcommission&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>',
		type: 'post',
		dataType: 'json',
		beforeSend: function() {
			$('#button-commission-add').button('loading');
		},
		complete: function() {
			$('#button-commission-add').button('reset');
		},
		success: function(json) {
			$('.alert').remove();

			if (json['error']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['success']) {
                $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

				$('#button-commission-add').replaceWith('<button id="button-commission-remove" data-toggle="tooltip" title="<?php echo $button_commission_remove; ?>" class="btn btn-danger btn-xs"><i class="fa fa-minus-circle"></i></button>');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$(document).delegate('#button-commission-remove', 'click', function() {
	$.ajax({
		url: 'index.php?route=sale/order/removecommission&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>',
		type: 'post',
		dataType: 'json',
		beforeSend: function() {
			$('#button-commission-remove').button('loading');
		},
		complete: function() {
			$('#button-commission-remove').button('reset');
		},
		success: function(json) {
			$('.alert').remove();

			if (json['error']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['success']) {
                $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

				$('#button-commission-remove').replaceWith('<button id="button-commission-add" data-toggle="tooltip" title="<?php echo $button_commission_add; ?>" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i></button>');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

var token = '';

// Login to the API
$.ajax({
	url: '<?php echo $catalog; ?>index.php?route=api/login',
	type: 'post',
	dataType: 'json',
	data: 'key=<?php echo $api_key; ?>',
	crossDomain: true,
	success: function(json) {
		$('.alert').remove();

        if (json['error']) {
    		if (json['error']['key']) {
    			$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['key'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
    		}

            if (json['error']['ip']) {
    			$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['ip'] + ' <button type="button" id="button-ip-add" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-danger btn-xs pull-right"><i class="fa fa-plus"></i> <?php echo $button_ip_add; ?></button></div>');
    		}
        }

        if (json['token']) {
			token = json['token'];
		}
	},
	error: function(xhr, ajaxOptions, thrownError) {
		alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	}
});

$('#history').delegate('.pagination a', 'click', function(e) {
	e.preventDefault();

	$('#history').load(this.href);
});

$('#history').load('index.php?route=sale/order/history&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>');

$('#button-history').on('click', function() {
	$.ajax({
		url: '<?php echo $catalog; ?>index.php?route=api/order/history&token=' + token + '&store_id=<?php echo $store_id; ?>&order_id=<?php echo $order_id; ?>',
		type: 'post',
		dataType: 'json',
		data: 'order_status_id=' + encodeURIComponent($('select[name=\'order_status_id\']').val()) + '&notify=' + ($('input[name=\'notify\']').prop('checked') ? 1 : 0) + '&override=' + ($('input[name=\'override\']').prop('checked') ? 1 : 0) + '&append=' + ($('input[name=\'append\']').prop('checked') ? 1 : 0) + '&comment=' + encodeURIComponent($('textarea[name=\'comment\']').val()),
		beforeSend: function() {
			$('#button-history').button('loading');
		},
		complete: function() {
			$('#button-history').button('reset');
		},
		success: function(json) {
			$('.alert').remove();

			if (json['error']) {
				$('#history').before('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
			}

			if (json['success']) {
				$('#history').load('index.php?route=sale/order/history&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>');

				$('#history').before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

				$('textarea[name=\'comment\']').val('');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

function changeStatus(){
	var status_id = $('select[name="order_status_id"]').val();

	$('#openbay-info').remove();

	$.ajax({
		url: 'index.php?route=extension/openbay/getorderinfo&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>&status_id=' + status_id,
		dataType: 'html',
		success: function(html) {
			$('#history').after(html);
		}
	});
}

function addOrderInfo(){
	var status_id = $('select[name="order_status_id"]').val();

	$.ajax({
		url: 'index.php?route=extension/openbay/addorderinfo&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>&status_id=' + status_id,
		type: 'post',
		dataType: 'html',
		data: $(".openbay-data").serialize()
	});
}

$(document).ready(function() {
	changeStatus();
});

$('select[name="order_status_id"]').change(function(){
	changeStatus();
});
//DEV103 Envoi BAT
function displayEnvoiBat(_iProduitId)
{
    $('.trEnvoiBat').slideUp() ;
    $('#trEnvoiBat_' + _iProduitId).slideDown() ;
}
$('button[id^=\'button-upload\']').on('click', function() {
    var node = this;

    $('#form-upload').remove();

    $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

    $('#form-upload input[name=\'file\']').trigger('click');

    if (typeof timer != 'undefined') {
        clearInterval(timer);
    }

    timer = setInterval(function() {
        if ($('#form-upload input[name=\'file\']').val() != '') {
            clearInterval(timer);

            $.ajax({
                url: 'index.php?route=tool/upload/upload&token=<?php echo $token; ?>',
                type: 'post',
                dataType: 'json',
                data: new FormData($('#form-upload')[0]),
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function() {
                    $(node).button('loading');
                },
                complete: function() {
                    $(node).button('reset');
                },
                success: function(json) {
                    $('.text-danger').remove();

                    if (json['error']) {
                        $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
                    }

                    if (json['success']) {
                        alert(json['success']);

                        $(node).parent().find('input').val(json['code']);
                        //input-option_fichier_model_
                        var zInputId    = $(node).parent().find('input').attr('id') ;
                        iProduitId = zInputId.replace('input-option_fichier_model_', '') ;
                        loadBatPerProduct(iProduitId, json['code']) ;
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        }
    }, 500);
});
function actFichier(_iOrder_product_Id, _iProductId, _iStatus)
{
    $.ajax({
    url: 'index.php?route=sale/order/actFichier&token=<?php echo $token; ?>',
    type: 'post',
    data: 'order_product_id='+_iOrder_product_Id+'&product_id='+_iProductId+'&fichier_status='+_iStatus,
    dataType: 'text',
    success: function(data) {
            $(".val" + _iProductId + "_" + _iOrder_product_Id + "_0").remove() ;
            $(".val" + _iProductId + "_" + _iOrder_product_Id + "_" + _iStatus).show() ;
    },
        async : false ,
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
  });
}
//DEV103 Envoi BAT
function loadBatPerProduct(_iProduitId, _zCodeFichier)
{
    if(_zCodeFichier != '')
    {
        $.ajax({
            url: 'index.php?route=sale/order/envoiBat&token=<?php echo $token; ?>',
            type: 'post',
            data: 'product_id=' + _iProduitId + '&order_id=<?php echo $order_id; ?>&code_fichier='+ _zCodeFichier ,
            dataType: 'text',
            success: function(data) {
                console.log('success ajout') ;
            },
            async : false ,
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
    $.ajax({
        url: 'index.php?route=sale/order/loadBatPerProduct&token=<?php echo $token; ?>',
        type: 'post',
        data: 'product_id=' + _iProduitId + '&order_id=<?php echo $order_id; ?>' ,
        dataType: 'text',
        success: function(data) {
            $('#bat_' + _iProduitId).html(data) ;
        },
        async : false ,
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
}
function supprimerBat(_iBatId, _iProduitId)
{
    $.ajax({
        url: 'index.php?route=sale/order/supprimerBat&token=<?php echo $token; ?>',
        type: 'post',
        data: 'bat_id=' + _iBatId ,
        dataType: 'text',
        success: function(data) {
            loadBatPerProduct(_iProduitId, '') ;
        },
        async : false ,
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
}
//DEV103 Envoi BAT


//DEV103 Envoi BAT
//--></script> 
</div>
<?php echo $footer; ?> 
<!--San-001-04-12-18-->