ALTER TABLE `oc_order_status` DROP `user_id`;
ALTER TABLE `oc_order` DROP `user_id`; 
ALTER TABLE `oc_order` ADD `delai` INT(1) NOT NULL DEFAULT '0' AFTER `devis`, ADD `delai_valid` TINYINT(1) NOT NULL DEFAULT '1' AFTER `delai`;
ALTER TABLE `oc_order` DROP `status_paiement`;
